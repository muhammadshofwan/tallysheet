package synergy.project.tallysheet.adapter

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import splitties.activities.start
import splitties.alertdialog.*
import splitties.toast.toast
import synergy.project.tallysheet.R
import synergy.project.tallysheet.feature.login.LoginActivity
import synergy.project.tallysheet.feature.main.MainViewModel
import synergy.project.tallysheet.feature.survey.Entry.EntryHeaderActivity
import synergy.project.tallysheet.feature.survey.ListSurveyActivity
import synergy.project.tallysheet.model.MainMenu
import java.util.*

class MainMenuAdapter(
    private val activity: Activity,
    private val data: ArrayList<MainMenu>,
    private val viewModel: MainViewModel
) : RecyclerView.Adapter<MainMenuAdapter.ViewHolder>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {

        //inflate your layout and pass it to view holder
        val inflater = activity.layoutInflater
        val view = inflater.inflate(R.layout.item_menu, viewGroup, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
       // var item =     KurirTerdekat()
        val item = data[i]
        viewHolder.txt_title.text = item.title
        viewHolder.txt_subtitle.text = item.subtitle
        Glide.with(activity).load(item.image).into(viewHolder.img_gambar)
        if (i % 2 == 0) {
            viewHolder.rl_layout.setBackgroundResource(R.drawable.list_backgroundcolor_odd)
        } else {
            viewHolder.rl_layout.setBackgroundResource(R.drawable.list_backgroundcolor_even)
        }

        viewHolder.rl_layout.setOnClickListener {
            when(i){
                0 -> {
                    activity.alertDialog {
                        title = "Perhatian"
                        message =
                            "Data dari server akan menimpa data survey yang ada di HP, pastikan tidak ada data survey yang belum diupload, lanjut?"
                        okButton {
                            it.dismiss()
                            viewModel.downloadData()
                        }
                        cancelButton { it.dismiss() }
                    }.show()
                }
                //1 -> {activity.start<EntryHeaderActivity>()}
                1 -> {activity.start<ListSurveyActivity>()}
                2 -> {activity.start<ListSurveyActivity>()}
                3 -> {
                    val countUnsync = viewModel.survey.countUnsync()
                    val countData = viewModel.survey.countData()
                    val countUpload = viewModel.survey.countUpload()

                    activity.alertDialog {
                        title = "Perhatian"
                        message =
                            "Anda akan mengirim $countUnsync survey ke server. Pastikan $countUnsync survey itu sudah lengkap semua datanya.\n" +
                                    "Lanjutkan kirim?"
                        okButton {
                            it.dismiss()
                            viewModel.uploadData(0)
                            //toast("Upload Data")
                        }
                        cancelButton { it.dismiss() }
                    }.show()
                }
                4 -> {
                    val url = "http://bitly.com/appsarbi10"
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(url)
                 activity.startActivity(i)
                }
                5 ->{

                    activity.alertDialog {
                        title = "Perhatian"
                        message = "anda yakin ingin logout?"
                        okButton {
                            viewModel.prefManager.token = ""
                            viewModel.prefManager.userId = ""
                            activity.start<LoginActivity> { flags = Intent.FLAG_ACTIVITY_CLEAR_TOP }
                            activity.finish()
                            toast("Berhasil Logout")
                            it.dismiss()
                        }
                        cancelButton { it.dismiss() }
                    }.show()

                }
            }
        }

    }

    private fun onClickListener(position: Int): View.OnClickListener {
        return View.OnClickListener { }
    }

    override fun getItemCount(): Int {
        return data?.size ?: 0
    }

    /**
     * View holder to display each RecylerView item
     */
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val txt_title: TextView
        val txt_subtitle: TextView
        val img_gambar : ImageView
        val rl_layout: RelativeLayout

        init {
            txt_title = view.findViewById(R.id.txt_title)
            txt_subtitle = view.findViewById(R.id.txt_subtitle)
            img_gambar = view.findViewById(R.id.img_gambar)
            rl_layout = view.findViewById(R.id.rl_layout)
        }
    }




}