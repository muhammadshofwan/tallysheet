package synergy.project.tallysheet.adapter

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import splitties.activities.start
import synergy.project.tallysheet.R
import synergy.project.tallysheet.feature.survey.Entry.EntryHeaderActivity
import synergy.project.tallysheet.feature.survey.detail.DetailSurveyActivity
import java.util.*

class ListSurveyAdapter(
    private val activity: Activity,
    private val data: ArrayList<HashMap<String,String>>
) : RecyclerView.Adapter<ListSurveyAdapter.ViewHolder>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {

        //inflate your layout and pass it to view holder
        val inflater = activity.layoutInflater
        val view = inflater.inflate(R.layout.item_survey, viewGroup, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
       // var item =     KurirTerdekat()
        val item = data[i]
        viewHolder.txt_date.text = item["kecamatan"]
        viewHolder.txt_country.text = item["nama_khg"]
        viewHolder.txt_reef.text = item["nomorBoring"]

        val status = item["status"]
        if (status != null) {
            when(status){
                "0" -> viewHolder.img_status.visibility = View.GONE
                "1" ->{
                    viewHolder.img_status.visibility = View.VISIBLE
                    viewHolder.img_status.setImageResource(R.drawable.ic_sync)
                }
                "2" ->{
                    viewHolder.img_status.visibility = View.VISIBLE
                    viewHolder.img_status.setImageResource(R.drawable.ic_success)
                }
                else->{
                    viewHolder.img_status.visibility = View.VISIBLE
                    viewHolder.img_status.setImageResource(R.drawable.ic_failed)
                }
            }
        }else{
            viewHolder.img_status.visibility = View.GONE
        }


        if (i % 2 == 0) {
            viewHolder.rl_layout.setBackgroundResource(R.drawable.list_backgroundcolor_odd)
        } else {
            viewHolder.rl_layout.setBackgroundResource(R.drawable.list_backgroundcolor_even)
        }


        viewHolder.rl_layout.setOnClickListener {
            activity.start<EntryHeaderActivity> {
                putExtra("nama_khg",item["nama_khg"])
                putExtra("nomorBoring",item["nomorBoring"])
                putExtra("id",item["id"]!!.toInt())
                putExtra("prev","list")
            }
        }

    }

    private fun onClickListener(position: Int): View.OnClickListener {
        return View.OnClickListener { }
    }

    override fun getItemCount(): Int {
        return data?.size ?: 0
    }

    /**
     * View holder to display each RecylerView item
     */
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val txt_country: TextView
        val txt_reef: TextView
        val txt_date: TextView
        val img_status : ImageView
        val rl_layout: RelativeLayout

        init {
            txt_country = view.findViewById(R.id.txt_country)
            txt_reef = view.findViewById(R.id.txt_reef)
            txt_date = view.findViewById(R.id.txt_date)
            img_status = view.findViewById(R.id.img_status)
            rl_layout = view.findViewById(R.id.rl_layout)
        }
    }




}