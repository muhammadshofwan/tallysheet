package synergy.project.tallysheet.utils

import android.content.Context
import androidx.preference.PreferenceManager

class   PrefManager(private val context: Context) {
    fun Read(key: String?): String? {
        val pref = PreferenceManager.getDefaultSharedPreferences(context)
        return pref.getString(key, "")
    }

    fun Write(key: String?, value: String?) {
        val settings = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = settings.edit()
        editor.putString(key, value)
        editor.commit()
    }

    // Boolean
    fun ReadBoolean(key: String?, defaultValue: Boolean): Boolean {
        val settings = PreferenceManager.getDefaultSharedPreferences(context)
        return settings.getBoolean(key, defaultValue)
    }

    fun WriteBoolean(key: String?, value: Boolean) {
        val settings = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = settings.edit()
        editor.putBoolean(key, value)
        editor.commit()
    }

    var hostName: String?
        get() = Read("HostName")
        set(HostName) {
            Write("HostName", HostName)
        }

    var hostImage: String?
        get() = Read("HostImage")
        set(HostImage) {
            Write("HostImage", HostImage)
        }
    var statusLogin: String?
        get() = Read("statusLogin")
        set(statusLogin) {
            Write("statusLogin", statusLogin)
        }

    var token: String?
        get() = Read("token")
        set(token) {
            Write("token", token)
        }
    var userId: String?
        get() = Read("userId")
        set(userId) {
            Write("userId", userId)
        }
    var loginMethod: String?
        get() = Read("loginMethod")
        set(loginMethod) {
            Write("loginMethod", loginMethod)
        }

    var nama: String?
        get() = Read("nama")
        set(nama) {
            Write("nama", nama)
        }
    var email: String?
        get() = Read("email")
        set(email) {
            Write("email", email)
        }
    var noHp: String?
        get() = Read("noHp")
        set(noHp) {
            Write("noHp", noHp)
        }
    var jenisKelamin: String?
        get() = Read("jenisKelamin")
        set(jenisKelamin) {
            Write("jenisKelamin", jenisKelamin)
        }
    var alamat: String?
        get() = Read("alamat")
        set(alamat) {
            Write("alamat", alamat)
        }

    var provinsi: String?
        get() = Read("provinsi")
        set(provinsi) {
            Write("provinsi", provinsi)
        }
    var kota: String?
        get() = Read("kota")
        set(kota) {
            Write("kota", kota)
        }
    var kecamatan: String?
        get() = Read("kecamatan")
        set(kecamatan) {
            Write("kecamatan", kecamatan)
        }

    var kelurahan: String?
        get() = Read("kelurahan")
        set(kelurahan) {
            Write("kelurahan", kelurahan)
        }

    var loginAs: String?
    get() = Read("loginAs")
    set(loginAs) {Write("loginAs",loginAs)}
}