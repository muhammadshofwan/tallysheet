package synergy.project.tallysheet.utils

import android.app.Activity
import android.app.ProgressDialog

/**
 *
 * Shofwan
 * 2020
 *
 */
class Progress(private val activity: Activity) {
    private var progressDialog: ProgressDialog? = null

    fun progress(title: String?) {
        progressDialog = ProgressDialog(activity)
        progressDialog?.setTitle(title)
        progressDialog?.setMessage("Loading...")
        progressDialog?.setCancelable(false)
        progressDialog?.show()
    }

    fun changeMessage(message:String?){
        progressDialog?.setMessage(message?.toString())
    }

    fun dismiss() {
        progressDialog?.dismiss()
    }
}