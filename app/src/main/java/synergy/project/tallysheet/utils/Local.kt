package synergy.project.tallysheet.utils

import android.view.View
import android.widget.ImageButton
import java.text.DecimalFormat

class Local {
    companion object{
        fun celsiusToFrenheit(celcius: Double): String? {
            var farenheit = 0.0
            farenheit = celcius * 9 / 5.0 + 32
            val OutPut = DecimalFormat("###.#")
            return OutPut.format(farenheit)
        }

        fun ferenheitToCelcius(farenheit: Double): String? {
            var celcius = 0.0
            celcius = (farenheit - 32) * (5 / 9.0)
            val OutPut = DecimalFormat("###.#")
            return OutPut.format(celcius)
        }

        fun meterTofeet(meter: Double): String? {
            var feet = 0.0
            feet = meter / 0.3048
            val OutPut = DecimalFormat("###.#")
            return OutPut.format(feet)
        }

        fun feetToMeter(feet: Double): String? {
            var meter = 0.0
            meter = feet * 0.3048
            val OutPut = DecimalFormat("###.#")
            return OutPut.format(meter)
        }

        fun isVisible(image:ImageButton,delete:ImageButton,visible:Boolean){
            when(visible){
                true -> {
                    image.visibility = View.VISIBLE
                    delete.visibility = View.VISIBLE
                }
                false ->{
                    image.visibility = View.VISIBLE
                    delete.visibility = View.INVISIBLE
                }
            }
        }
    }

}