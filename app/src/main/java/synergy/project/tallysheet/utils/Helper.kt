package synergy.project.tallysheet.utils

class Helper {

    companion object {
        val host = "http://synergyinfinity.id/demo/tallysheet/backend/web/v1/"
        var mode = 0

        //array spiner
        val jenisTanah = arrayOf(" Pilih Jenis Tanah ", "Gambut", "Mineral")
        val jenisTanahSample = arrayOf(" Pilih Jenis Tanah ", "Gambut", "Mineral")
        val banjirSumberGenangan = arrayOf(" Pilih Sumber Genangan ", "Hujan", "Limpasan Sungai", "Kiriman Dari Hulu", "Lainnya")

        val banjirBulan = arrayOf(
                " Pilih Bulan ",
                "Januari",
                "Februari",
                "Maret",
                "April",
                "Mei",
                "Juni",
                "Juli",
                "Agustus",
                "September",
                "Oktober",
                "November",
                "Desember"
                )

        val tutupanLahanStatus = arrayOf(" Pilih Status ", "Masyarakat", "Perusahaan")
        var tutupanLahanPenggunaanLahan = arrayOf(
            " Pilih Tutupan Lahan ",
            "Hutan (Ht)",
            "Perkebunan (Pb)",
            "Kebun Campuran (Kc)",
            "Semak Belukar (Sb)",
            "Ladang/tegalan (Ld)",
            "Tambak/empang",
            "Sawah (Sw)",
            "Mangrove",
            "Tanah terbuka"
        )

        var flora = arrayOf(" Pilih Status ", "Ada", "Tidak ada")
        var fauna = arrayOf(" Pilih Status ", "Ada", "Tidak ada")

        var drainaseAlami = arrayOf(" Pilih Status ", "Ada", "Tidak ada")
        var drainaseBuatan = arrayOf(" Pilih Status ", "Ada", "Tidak ada")
        var drainaseBuatanJenis = arrayOf(" Pilih Jenis ", "Saluran Terbuka", "Saluran Terkontrol")

        var karakteristikSubstratumTanahNa = arrayOf(" Pilih Status ", "Ya", "Tidak")
        var karakteristikSubstratumTanahNaJenis = arrayOf(" Pilih Jenis ", "Mineral", "Badan Air")

        var tipeLuapanKemarau = arrayOf(" Pilih Status ", "A", "B", "C", "D")
        var tipeLuapanHujan= arrayOf(" Pilih Status ", "A", "B", "C", "D")


        var ketebalanGambutNa = arrayOf(" Pilih Status ", "Ya", "Tidak")
        var ketebalanGambutNaJenis = arrayOf(" Pilih Jenis ", "Mineral", "Badan Air")

        var ketebalanGambutJenis = arrayOf(" Pilih Jenis ", "Saprik", "Hemik" ,"Fibrik")

        var karakteristikSubstratumDibawahLapisanGambutNa = arrayOf(" Pilih Status ", "Ya", "Tidak")
        var karakteristikSubstratumDibawahLapisanGambutNaJenis = arrayOf(" Pilih Jenis ", "Mineral", "Badan Air")

        var karakteristikSubstratumDibawahLapisanGambutJenis = arrayOf(" Pilih Jenis ", "Pasir kwarsa", "Clay/sedimen sungai","Sedimen berpirit","Granit","Lainnya")


        var tingkatKerusakanLahanGambutDrainaseBuatan = arrayOf(" Pilih Status ", "Ada", "Tidak ada")
        var tingkatKerusakanLahanGambutSedimen = arrayOf(" Pilih Status ", "Ada", "Tidak ada")

        var tingkatKerusakanLahanKondisiTanaman = arrayOf(" Pilih Status ","Normal", "Tidak Normal", "Tidak Produktif", "Miring/tumbang", "Terjadi Subsiden")

        var tingkatKerusakanLahanKerapatanTajuk = arrayOf(" Pilih Status ", "Rapat", "Sedang", "Jarang")

        val kebakaranLahanBulan = arrayOf(
                " Pilih Bulan ",
                "Januari",
                "Februari",
                "Maret",
                "April",
                "Mei",
                "Juni",
                "Juli",
                "Agustus",
                "September",
                "Oktober",
                "November",
                "Desember"
        )
        var kebakaranLahanLamaKejadianHariMInggu = arrayOf(" Pilih Lama Kejadian ", "Hari", "Minggu")
        var kebakaranLahanUpayaPemadamanSwadayaMasyarakat = arrayOf(" Pilih Status ", "Ya", "Tidak")
        var kebakaranLahanUpayaPemadamanPemerintah = arrayOf(" Pilih Status ", "Ya", "Tidak")

        var hujanLamaKejadianJamHari = arrayOf(" Pilih Frekuensi Kejadian ", "Jam", "Hari")
        var hujanIntensitas = arrayOf(" Pilih Intensitas ", "Tinggi", "Sedang", "Rendah")


        var porositasKelengasanNa = arrayOf(" Pilih Status ", "Ya", "Tidak")
        var porositasKelengasanNaJenis = arrayOf(" Pilih Jenis ", "Mineral", "Badan Air")
    }
}