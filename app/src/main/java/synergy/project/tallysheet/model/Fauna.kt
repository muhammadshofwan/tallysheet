package synergy.project.tallysheet.model

import android.content.Context

class Fauna(val context:Context) {
    val TABLE_NAME = "fauna"
    var id = 0
    var idSurvey = 0
    var jenis = ""
    var status = 0

    fun getRecordList(idSurvey : String) : ArrayList<HashMap<String,String>>{
        var result = ArrayList<HashMap<String,String>>()
        val selecQuery = "SELECT id,idSurvey,jenis,status FROM $TABLE_NAME WHERE idSurvey=$idSurvey ORDER BY id DESC;"
        val dbHelper = DatabaseHelper(context)
        val db = dbHelper.writableDatabase

        val cursor =db.rawQuery(selecQuery,null)
        if (cursor.moveToFirst()){
            do {
                var map = HashMap<String,String>()
                map["id"] = cursor.getString(0)
                map["idSurvey"] =cursor.getString(1)
                map["jenis"] = cursor.getString(2)
                map["status"] =cursor.getString(3)
                result.add(map)
            } while (cursor.moveToNext())
        }
        db.close()
        return  result
    }
}