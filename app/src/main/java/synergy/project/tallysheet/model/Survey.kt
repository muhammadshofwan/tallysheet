package synergy.project.tallysheet.model

import android.content.ContentValues
import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import org.json.JSONArray
import java.io.Serializable

class Survey(val context: Context) : Serializable {
    val TABLE_NAME = "survey"
    var id: String? = null
    var nama_khg: String? = null
    var nomorBoring: String? = null
    var tanggal: String? = null
    var surveyorId: String? = null
    var reguId: String? = null
    var provinsi: String? = null
    var kota_kabupaten: String? = null
    var kecamatan: String? = null
    var kelurahan: String? = null
    var jenis_tanah: String? = null
    var titik_p10: String? = null
    var sample_p10: String? = null
    var jenis_sample: String? = null
    var longitude: String? = null
    var latitude: String? = null
    var longitude_target: String? = null
    var latitude_target: String? = null
    var longitude_hp: String? = null
    var latitude_hp: String? = null
    var kordinatImg1: String? = null
    var kordinatImg2: String? = null
    var kordinatImg3: String? = null
    var kordinatImg4: String? = null
    var kordinatImg5: String? = null
    var kordinatImg6: String? = null
    var elevasi: String? = null
    var elevasiImg1: String? = null
    var elevasiImg2: String? = null
    var elevasiImg3: String? = null
    var elevasiImg4: String? = null
    var kedalamanAirTanah: String? = null
    var genangan: String? = null
    var banjirBulan: String? = null
    var banjirLamanya: String? = null
    var banjirKetinggianAir: String? = null
    var banjirSumberGenangan: String? = null
    var elevasiImg5: String? = null
    var elevasiImg6: String? = null
    var banjirImg1: String? = null
    var banjirImg2: String? = null
    var banjirImg3: String? = null
    var banjirImg4: String? = null
    var tutupanLahanJenisTanaman: String? = null
    var tutupanLahanStatus: String? = null
    var tutupanLahanNamaPerusahaan: String? = null
    var tutupanLahanLuas: String? = null
    var tutupanLahanPenggunaanLahan: String? = null
    var banjirImg5: String? = null
    var banjirImg6: String? = null
    var tutupanLahanImg1: String? = null
    var tutupanLahanImg2: String? = null
    var tutupanLahanImg3: String? = null
    var tutupanLahanImg4: String? = null
    var flora: String? = null
    var floraJenis: String? = null
    var fauna: String? = null
    var faunaJenis: String? = null
    var tutupanLahanImg5: String? = null
    var tutupanLahanImg6: String? = null
    var floraFaunaImg1: String? = null
    var floraFaunaImg2: String? = null
    var floraFaunaImg3: String? = null
    var floraFaunaImg4: String? = null
    var drainaseAlami: String? = null
    var drainaseBuatan: String? = null
    var drainaseBuatanJenis: String? = null
    var drainaseBuatanTinggiMukaAir: String? = null
    var floraFaunaImg5: String? = null
    var floraFaunaImg6: String? = null
    var drainaseImg1: String? = null
    var drainaseImg2: String? = null
    var drainaseImg3: String? = null
    var drainaseImg4: String? = null
    var kualitasAirTanahPh: String? = null
    var kualitasAirTanahEc: String? = null
    var kualitasAirTanahTds: String? = null
    var drainaseImg5: String? = null
    var drainaseImg6: String? = null
    var kualitasiAirTanahImg1: String? = null
    var kualitasiAirTanahImg2: String? = null
    var kualitasiAirTanahImg3: String? = null
    var kualitasiAirTanahImg4: String? = null
    var karakteristikSubstratumTanahNa: String? = null
    var karakteristikSubstratumTanahNaJenis: String? = null
    var karakteristikSubstratumTanahLiatPh: String? = null
    var karakteristikSubstratumTanahLiatEc: String? = null
    var kualitasiAirTanahImg5: String? = null
    var kualitasiAirTanahImg6: String? = null
    var karakteristikSubstratumTanahLiatImg1: String? = null
    var karakteristikSubstratumTanahLiatImg2: String? = null
    var karakteristikSubstratumTanahLiatImg3: String? = null
    var karakteristikSubstratumTanahLiatImg4: String? = null
    var tipeLuapanKemarau: String? = null
    var tipeLuapanHujan: String? = null
    var karakteristikSubstratumTanahLiatImg5: String? = null
    var karakteristikSubstratumTanahLiatImg6: String? = null
    var tipeLuapanImg1: String? = null
    var tipeLuapanImg2: String? = null
    var tipeLuapanImg3: String? = null
    var tipeLuapanImg4: String? = null
    var ketebalanGambutNa: String? = null
    var ketebalanGambutNaJenis: String? = null
    var ketebalanGambutTinggi: String? = null
    var ketebalanGambutJenis: String? = null
    var tipeLuapanImg5: String? = null
    var tipeLuapanImg6: String? = null
    var ketebalanGambutImg1: String? = null
    var ketebalanGambutImg2: String? = null
    var ketebalanGambutImg3: String? = null
    var ketebalanGambutImg4: String? = null
    var karakteristikSubstratumDibawahLapisanGambutNa: String? = null
    var karakteristikSubstratumDibawahLapisanGambutNaJenis: String? = null
    var karakteristikSubstratumDibawahLapisanGambutJenis: String? = null
    var karakteristikSubstratumDibawahLapisanGambutLainnya: String? = null
    var ketebalanGambutImg5: String? = null
    var ketebalanGambutImg6: String? = null
    var karakteristikSubstratumDibawahLapisanGambutImg1: String? = null
    var karakteristikSubstratumDibawahLapisanGambutImg2: String? = null
    var karakteristikSubstratumDibawahLapisanGambutImg3: String? = null
    var karakteristikSubstratumDibawahLapisanGambutImg4: String? = null
    var tingkatKerusakanLahanGambutDrainaseBuatan: String? = null
    var tingkatKerusakanLahanGambutSedimen: String? = null
    var tingkatKerusakanLahanKondisiTanaman: String? = null
    var tingkatKerusakanLahanSubsiden: String? = null
    var tingkatKerusakanLahanKerapatanTajuk: String? = null
    var karakteristikSubstratumDibawahLapisanGambutImg5: String? = null
    var karakteristikSubstratumDibawahLapisanGambutImg6: String? = null
    var tingkatKerusakanLahanImg1: String? = null
    var tingkatKerusakanLahanImg2: String? = null
    var tingkatKerusakanLahanImg3: String? = null
    var tingkatKerusakanLahanImg4: String? = null
    var kebakaranLahanTahun: String? = null
    var kebakaranLahanBulan: String? = null
    var kebakaranLahanTanggal: String? = null
    var kebakaranLahanLamaKejadian: String? = null
    var kebakaranLahanLamaKejadianHariMInggu: String? = null
    var kebakaranLahanUpayaPemadamanSwadayaMasyarakat: String? = null
    var kebakaranLahanUpayaPemadamanPemerintah: String? = null
    var hujanTerakhir: String? = null
    var hujanLamaKejadian: String? = null
    var hujanLamaKejadianJamHari: String? = null
    var hujanIntensitas: String? = null
    var tingkatKerusakanLahanImg5: String? = null
    var tingkatKerusakanLahanImg6: String? = null
    var kebakaranHujanImg1: String? = null
    var kebakaranHujanImg2: String? = null
    var kebakaranHujanImg3: String? = null
    var kebakaranHujanImg4: String? = null
    var porositasKelengasanNa: String? = null
    var porositasKelengasanNaJenis: String? = null
    var porositasKelengasan1: String? = null
    var porositasKelengasan2: String? = null
    var porositasKelengasanKeterangan: String? = null
    var kebakaranHujanImg5: String? = null
    var kebakaranHujanImg6: String? = null
    var porositasKelengasanImg1: String? = null
    var porositasKelengasanImg2: String? = null
    var porositasKelengasanImg3: String? = null
    var porositasKelengasanImg4: String? = null
    var keterangan: String? = null
    var porositasKelengasanImg5: String? = null
    var porositasKelengasanImg6: String? = null
    var sketsaLokasi: String? = null
    var imgBatasBarat: String? = null
    var imgBatasTimur: String? = null
    var imgBatasSelatan: String? = null
    var imgBatasUtara: String? = null
    var status: Int? = null
    var ketebalanGambut: String? = null
    var karakteristikSubstratumDibawahLapisanGambutJenis1 : String?=null
    var karakteristikSubstratumDibawahLapisanGambutJenis2 : String?=null
    var karakteristikSubstratumDibawahLapisanGambutJenis3 : String?=null
    var karakteristikSubstratumDibawahLapisanGambutJenis4 : String?=null
    var karakteristikSubstratumDibawahLapisanGambutJenis5 : String?=null

    var banjirSumberGenangan1 : String?=null
    var banjirSumberGenangan2 : String?=null
    var banjirSumberGenangan3 : String?=null
    var banjirSumberGenangan4 : String?=null
    var banjirSumberGenanganLainnya : String?=null

    var tingkatKerusakanLahanKondisiTanaman1 : String?=null
    var tingkatKerusakanLahanKondisiTanaman2 : String?=null
    var tingkatKerusakanLahanKondisiTanaman3 : String?=null
    var tingkatKerusakanLahanKondisiTanaman4 : String?=null


    var kualitasAirTanahPhAt : String?=null
    var kualitasAirTanahPhAs : String?=null
    var kualitasAirTanahEcAt : String?=null
    var kualitasAirTanahEcAs : String?=null
    var kualitasAirTanahTdsAt : String?=null
    var kualitasAirTanahTdsAs : String?=null

    var dusun : String ?=null



    fun getLastId(): Int {
        var result = 0
        val dbHelper = DatabaseHelper(context)
        val db = dbHelper.writableDatabase
        val selectQuery = "SELECT * FROM $TABLE_NAME ;"
        return result
    }

    fun getSurveyList(sortColumn: Int, sortOrder: Int): ArrayList<HashMap<String, String>> {
        var result = ArrayList<HashMap<String, String>>()
        val selected = "SELECT nomorBoring, tanggal, kecamatan, status, id, nama_khg FROM $TABLE_NAME"
        var orderBy = ""
        when (sortColumn) {
            0 -> orderBy = "ORDER BY nomorBoring "
            1 -> orderBy = "ORDER BY status "
            else -> orderBy = "ORDER BY nomorBoring "
        }
        var orderAs = " ASC "
        if (sortOrder == 0) {
            orderAs = "ASC "
        } else {
            orderAs = "DESC "
        }
        val selectQuery = "$selected $orderBy $orderAs ;"

        val dbHelper = DatabaseHelper(context)
        val db = dbHelper.writableDatabase
        val cursor = db.rawQuery(selectQuery, null)
        if (cursor.moveToFirst()) {
            do {
                var map = HashMap<String, String>()
                map["nomorBoring"] = cursor.getString(0)
                map["tanggal"] = cursor.getString(1)
                map["kecamatan"] = cursor.getString(2)
                 map["status"] = cursor.getString(3)
                map["id"] = cursor.getString(4)
                map["nama_khg"] = cursor.getString(5)

                result.add(map)
            } while (cursor.moveToNext())
        }
        db.close()
        return result
    }

    fun search(input: String): ArrayList<HashMap<String, String>> {
        var result = ArrayList<HashMap<String, String>>()
        val selectQuery = "SELECT nomorBoring, tanggal, kecamatan, status, id, nama_khg FROM $TABLE_NAME " +
                "WHERE nomorBoring like '%${input.toUpperCase()}%' ORDER BY nomorBoring"

        val dbHelper = DatabaseHelper(context)
        val db = dbHelper.writableDatabase

        val cursor = db.rawQuery(selectQuery, null)
        if (cursor.moveToFirst()) {
            do {
                var map = HashMap<String, String>()
                map["nomorBoring"] = cursor.getString(0)
                map["tanggal"] = cursor.getString(1)
                map["kecamatan"] = cursor.getString(2)
                map["status"] = cursor.getString(3)
                map["id"] = cursor.getString(4)
                map["nama_khg"] = cursor.getString(5)
                result.add(map)
            } while (cursor.moveToNext())
        }
        db.close()
        return result
    }

    fun getAll(): ArrayList<HashMap<String, String>> {
        var result = ArrayList<HashMap<String, String>>()
        val selectQuery = "SELECT nomorBoring, tanggal,kecamatan, id, nama_khg status FROM $TABLE_NAME " +
                "ORDER BY nomorBoring DESC"

        val dbHelper = DatabaseHelper(context)
        val db = dbHelper.writableDatabase

        val cursor = db.rawQuery(selectQuery, null)
        if (cursor.moveToFirst()) {
            do {
                var map = HashMap<String, String>()
                map["nomorBoring"] = cursor.getString(0)
                map["tanggal"] = cursor.getString(1)
                map["kecamatan"] = cursor.getString(2)
                map["status"] = cursor.getString(3)
                map["id"] = cursor.getString(4)
                map["nama_khg"] = cursor.getString(5)
                result.add(map)
            } while (cursor.moveToNext())
        }
        db.close()
        return result
    }

    fun update() {
        val dbHelper = DatabaseHelper(context)
        val db = dbHelper.writableDatabase

        val values = ContentValues()
        values.put("id", id);
        values.put("id", id);
        values.put("nama_khg", nama_khg);
        values.put("nomorBoring", nomorBoring);
        values.put("tanggal", tanggal);
        values.put("surveyorId", surveyorId);
        values.put("reguId", reguId);
        values.put("provinsi", provinsi);
        values.put("kota_kabupaten", kota_kabupaten);
        values.put("kecamatan", kecamatan);
        values.put("kelurahan", kelurahan);
        values.put("jenis_tanah", jenis_tanah);
        values.put("titik_p10", titik_p10);
        values.put("sample_p10", sample_p10);
        values.put("jenis_sample", jenis_sample);
        values.put("longitude", longitude);
        values.put("latitude", latitude);
        values.put("longitude_target", longitude_target);
        values.put("latitude_target", latitude_target);
        values.put("longitude_hp", longitude_hp);
        values.put("latitude_hp", latitude_hp);
        values.put("kordinatImg1", kordinatImg1);
        values.put("kordinatImg2", kordinatImg2);
        values.put("kordinatImg3", kordinatImg3);
        values.put("kordinatImg4", kordinatImg4);
        values.put("kordinatImg5", kordinatImg5);
        values.put("kordinatImg6", kordinatImg6);
        values.put("elevasi", elevasi);
        values.put("elevasiImg1", elevasiImg1);
        values.put("elevasiImg2", elevasiImg2);
        values.put("elevasiImg3", elevasiImg3);
        values.put("elevasiImg4", elevasiImg4);
        values.put("kedalamanAirTanah", kedalamanAirTanah);
        values.put("genangan", genangan);
        values.put("banjirBulan", banjirBulan);
        values.put("banjirLamanya", banjirLamanya);
        values.put("banjirKetinggianAir", banjirKetinggianAir);
        values.put("banjirSumberGenangan", banjirSumberGenangan);
        values.put("elevasiImg5", elevasiImg5);
        values.put("elevasiImg6", elevasiImg6);
        values.put("banjirImg1", banjirImg1);
        values.put("banjirImg2", banjirImg2);
        values.put("banjirImg3", banjirImg3);
        values.put("banjirImg4", banjirImg4);
        values.put("tutupanLahanJenisTanaman", tutupanLahanJenisTanaman);
        values.put("tutupanLahanStatus", tutupanLahanStatus);
        values.put("tutupanLahanNamaPerusahaan", tutupanLahanNamaPerusahaan);
        values.put("tutupanLahanLuas", tutupanLahanLuas);
        values.put("tutupanLahanPenggunaanLahan", tutupanLahanPenggunaanLahan);
        values.put("banjirImg5", banjirImg5);
        values.put("banjirImg6", banjirImg6);
        values.put("tutupanLahanImg1", tutupanLahanImg1);
        values.put("tutupanLahanImg2", tutupanLahanImg2);
        values.put("tutupanLahanImg3", tutupanLahanImg3);
        values.put("tutupanLahanImg4", tutupanLahanImg4);
        values.put("flora", flora);
        values.put("floraJenis", floraJenis);
        values.put("fauna", fauna);
        values.put("faunaJenis", faunaJenis);
        values.put("tutupanLahanImg5", tutupanLahanImg5);
        values.put("tutupanLahanImg6", tutupanLahanImg6);
        values.put("floraFaunaImg1", floraFaunaImg1);
        values.put("floraFaunaImg2", floraFaunaImg2);
        values.put("floraFaunaImg3", floraFaunaImg3);
        values.put("floraFaunaImg4", floraFaunaImg4);
        values.put("drainaseAlami", drainaseAlami);
        values.put("drainaseBuatan", drainaseBuatan);
        values.put("drainaseBuatanJenis", drainaseBuatanJenis);
        values.put("drainaseBuatanTinggiMukaAir", drainaseBuatanTinggiMukaAir);
        values.put("floraFaunaImg5", floraFaunaImg5);
        values.put("floraFaunaImg6", floraFaunaImg6);
        values.put("drainaseImg1", drainaseImg1);
        values.put("drainaseImg2", drainaseImg2);
        values.put("drainaseImg3", drainaseImg3);
        values.put("drainaseImg4", drainaseImg4);
        values.put("kualitasAirTanahPh", kualitasAirTanahPh);
        values.put("kualitasAirTanahEc", kualitasAirTanahEc);
        values.put("kualitasAirTanahTds", kualitasAirTanahTds);
        values.put("drainaseImg5", drainaseImg5);
        values.put("drainaseImg6", drainaseImg6);
        values.put("kualitasiAirTanahImg1", kualitasiAirTanahImg1);
        values.put("kualitasiAirTanahImg2", kualitasiAirTanahImg2);
        values.put("kualitasiAirTanahImg3", kualitasiAirTanahImg3);
        values.put("kualitasiAirTanahImg4", kualitasiAirTanahImg4);
        values.put("karakteristikSubstratumTanahNa", karakteristikSubstratumTanahNa);
        values.put("karakteristikSubstratumTanahNaJenis", karakteristikSubstratumTanahNaJenis);
        values.put("karakteristikSubstratumTanahLiatPh", karakteristikSubstratumTanahLiatPh);
        values.put("karakteristikSubstratumTanahLiatEc", karakteristikSubstratumTanahLiatEc);
        values.put("kualitasiAirTanahImg5", kualitasiAirTanahImg5);
        values.put("kualitasiAirTanahImg6", kualitasiAirTanahImg6);
        values.put("karakteristikSubstratumTanahLiatImg1", karakteristikSubstratumTanahLiatImg1);
        values.put("karakteristikSubstratumTanahLiatImg2", karakteristikSubstratumTanahLiatImg2);
        values.put("karakteristikSubstratumTanahLiatImg3", karakteristikSubstratumTanahLiatImg3);
        values.put("karakteristikSubstratumTanahLiatImg4", karakteristikSubstratumTanahLiatImg4);
        values.put("tipeLuapanKemarau", tipeLuapanKemarau);
        values.put("tipeLuapanHujan", tipeLuapanHujan);
        values.put("karakteristikSubstratumTanahLiatImg5", karakteristikSubstratumTanahLiatImg5);
        values.put("karakteristikSubstratumTanahLiatImg6", karakteristikSubstratumTanahLiatImg6);
        values.put("tipeLuapanImg1", tipeLuapanImg1);
        values.put("tipeLuapanImg2", tipeLuapanImg2);
        values.put("tipeLuapanImg3", tipeLuapanImg3);
        values.put("tipeLuapanImg4", tipeLuapanImg4);
        values.put("ketebalanGambutNa", ketebalanGambutNa);
        values.put("ketebalanGambutNaJenis", ketebalanGambutNaJenis);
        values.put("ketebalanGambutTinggi", ketebalanGambutTinggi);
        values.put("ketebalanGambutJenis", ketebalanGambutJenis);
        values.put("tipeLuapanImg5", tipeLuapanImg5);
        values.put("tipeLuapanImg6", tipeLuapanImg6);
        values.put("ketebalanGambutImg1", ketebalanGambutImg1);
        values.put("ketebalanGambutImg2", ketebalanGambutImg2);
        values.put("ketebalanGambutImg3", ketebalanGambutImg3);
        values.put("ketebalanGambutImg4", ketebalanGambutImg4);
        values.put("karakteristikSubstratumDibawahLapisanGambutNa", karakteristikSubstratumDibawahLapisanGambutNa);
        values.put("karakteristikSubstratumDibawahLapisanGambutNaJenis", karakteristikSubstratumDibawahLapisanGambutNaJenis);
        values.put("karakteristikSubstratumDibawahLapisanGambutJenis", karakteristikSubstratumDibawahLapisanGambutJenis);
        values.put("karakteristikSubstratumDibawahLapisanGambutLainnya", karakteristikSubstratumDibawahLapisanGambutLainnya);
        values.put("ketebalanGambutImg5", ketebalanGambutImg5);
        values.put("ketebalanGambutImg6", ketebalanGambutImg6);
        values.put("karakteristikSubstratumDibawahLapisanGambutImg1", karakteristikSubstratumDibawahLapisanGambutImg1);
        values.put("karakteristikSubstratumDibawahLapisanGambutImg2", karakteristikSubstratumDibawahLapisanGambutImg2);
        values.put("karakteristikSubstratumDibawahLapisanGambutImg3", karakteristikSubstratumDibawahLapisanGambutImg3);
        values.put("karakteristikSubstratumDibawahLapisanGambutImg4", karakteristikSubstratumDibawahLapisanGambutImg4);
        values.put("tingkatKerusakanLahanGambutDrainaseBuatan", tingkatKerusakanLahanGambutDrainaseBuatan);
        values.put("tingkatKerusakanLahanGambutSedimen", tingkatKerusakanLahanGambutSedimen);
        values.put("tingkatKerusakanLahanKondisiTanaman", tingkatKerusakanLahanKondisiTanaman);
        values.put("tingkatKerusakanLahanSubsiden", tingkatKerusakanLahanSubsiden);
        values.put("tingkatKerusakanLahanKerapatanTajuk", tingkatKerusakanLahanKerapatanTajuk);
        values.put("karakteristikSubstratumDibawahLapisanGambutImg5", karakteristikSubstratumDibawahLapisanGambutImg5);
        values.put("karakteristikSubstratumDibawahLapisanGambutImg6", karakteristikSubstratumDibawahLapisanGambutImg6);
        values.put("tingkatKerusakanLahanImg1", tingkatKerusakanLahanImg1);
        values.put("tingkatKerusakanLahanImg2", tingkatKerusakanLahanImg2);
        values.put("tingkatKerusakanLahanImg3", tingkatKerusakanLahanImg3);
        values.put("tingkatKerusakanLahanImg4", tingkatKerusakanLahanImg4);
        values.put("kebakaranLahanTahun", kebakaranLahanTahun);
        values.put("kebakaranLahanBulan", kebakaranLahanBulan);
        values.put("kebakaranLahanTanggal", kebakaranLahanTanggal);
        values.put("kebakaranLahanLamaKejadian", kebakaranLahanLamaKejadian);
        values.put("kebakaranLahanLamaKejadianHariMInggu", kebakaranLahanLamaKejadianHariMInggu);
        values.put("kebakaranLahanUpayaPemadamanSwadayaMasyarakat", kebakaranLahanUpayaPemadamanSwadayaMasyarakat);
        values.put("kebakaranLahanUpayaPemadamanPemerintah", kebakaranLahanUpayaPemadamanPemerintah);
        values.put("hujanTerakhir", hujanTerakhir);
        values.put("hujanLamaKejadian", hujanLamaKejadian);
        values.put("hujanLamaKejadianJamHari", hujanLamaKejadianJamHari);
        values.put("hujanIntensitas", hujanIntensitas);
        values.put("tingkatKerusakanLahanImg5", tingkatKerusakanLahanImg5);
        values.put("tingkatKerusakanLahanImg6", tingkatKerusakanLahanImg6);
        values.put("kebakaranHujanImg1", kebakaranHujanImg1);
        values.put("kebakaranHujanImg2", kebakaranHujanImg2);
        values.put("kebakaranHujanImg3", kebakaranHujanImg3);
        values.put("kebakaranHujanImg4", kebakaranHujanImg4);
        values.put("porositasKelengasanNa", porositasKelengasanNa);
        values.put("porositasKelengasanNaJenis", porositasKelengasanNaJenis);
        values.put("porositasKelengasan1", porositasKelengasan1);
        values.put("porositasKelengasan2", porositasKelengasan2);
        values.put("porositasKelengasanKeterangan", porositasKelengasanKeterangan);
        values.put("kebakaranHujanImg5", kebakaranHujanImg5);
        values.put("kebakaranHujanImg6", kebakaranHujanImg6);
        values.put("porositasKelengasanImg1", porositasKelengasanImg1);
        values.put("porositasKelengasanImg2", porositasKelengasanImg2);
        values.put("porositasKelengasanImg3", porositasKelengasanImg3);
        values.put("porositasKelengasanImg4", porositasKelengasanImg4);
        values.put("keterangan", keterangan);
        values.put("porositasKelengasanImg5", porositasKelengasanImg5);
        values.put("porositasKelengasanImg6", porositasKelengasanImg6);
        values.put("sketsaLokasi", sketsaLokasi);
        values.put("imgBatasBarat", imgBatasBarat);
        values.put("imgBatasTimur", imgBatasTimur);
        values.put("imgBatasSelatan", imgBatasSelatan);
        values.put("imgBatasUtara", imgBatasUtara);
        values.put("status", status);
        values.put("ketebalanGambut", ketebalanGambut)
        values.put("karakteristikSubstratumDibawahLapisanGambutJenis1", karakteristikSubstratumDibawahLapisanGambutJenis1)
        values.put("karakteristikSubstratumDibawahLapisanGambutJenis2", karakteristikSubstratumDibawahLapisanGambutJenis2)
        values.put("karakteristikSubstratumDibawahLapisanGambutJenis3", karakteristikSubstratumDibawahLapisanGambutJenis3)
        values.put("karakteristikSubstratumDibawahLapisanGambutJenis4", karakteristikSubstratumDibawahLapisanGambutJenis4)
        values.put("karakteristikSubstratumDibawahLapisanGambutJenis5", karakteristikSubstratumDibawahLapisanGambutJenis5)

        values.put("banjirSumberGenangan1", banjirSumberGenangan1)
        values.put("banjirSumberGenangan2", banjirSumberGenangan2)
        values.put("banjirSumberGenangan3", banjirSumberGenangan3)
        values.put("banjirSumberGenangan4", banjirSumberGenangan4)
        values.put("banjirSumberGenanganLainnya", banjirSumberGenanganLainnya)

        values.put("tingkatKerusakanLahanKondisiTanaman1", tingkatKerusakanLahanKondisiTanaman1)
        values.put("tingkatKerusakanLahanKondisiTanaman2", tingkatKerusakanLahanKondisiTanaman2)
        values.put("tingkatKerusakanLahanKondisiTanaman3", tingkatKerusakanLahanKondisiTanaman3)
        values.put("tingkatKerusakanLahanKondisiTanaman4", tingkatKerusakanLahanKondisiTanaman4)



        values.put("kualitasAirTanahPhAt", kualitasAirTanahPhAt)
        values.put("kualitasAirTanahPhAs", kualitasAirTanahPhAs)
        values.put("kualitasAirTanahEcAt", kualitasAirTanahEcAt)
        values.put("kualitasAirTanahEcAs", kualitasAirTanahEcAs)
        values.put("kualitasAirTanahTdsAt", kualitasAirTanahTdsAt)
        values.put("kualitasAirTanahTdsAs", kualitasAirTanahTdsAs)

        values.put("dusun", dusun)

        db.update(TABLE_NAME, values, "id='${id}'", null)
        db.close()
    }

    fun getListNamaKhg(): ArrayList<HashMap<String, String>> {
        var result = ArrayList<HashMap<String, String>>()
        val selectQuery =
            "SELECT distinct(nama_khg)  FROM $TABLE_NAME  ORDER BY nama_khg ASC"
        val dbHelper = DatabaseHelper(context)
        val db = dbHelper.writableDatabase

        val cursor = db.rawQuery(selectQuery, null)
        if (cursor.moveToFirst()) {
            do {
                val map = HashMap<String, String>()
                map["nama_khg"] = cursor.getString(0)
                result.add(map)
            } while (cursor.moveToNext())
        }
        db.close()
        return result
    }

    fun getListNomorBoring(namaKhg:String): ArrayList<HashMap<String, String>> {
        var result = ArrayList<HashMap<String, String>>()
        val selectQuery =
            "SELECT nomorBoring  FROM $TABLE_NAME WHERE nama_khg='$namaKhg' ORDER BY nomorBoring ASC"
        val dbHelper = DatabaseHelper(context)
        val db = dbHelper.writableDatabase

        val cursor = db.rawQuery(selectQuery, null)
        if (cursor.moveToFirst()) {
            do {
                val map = HashMap<String, String>()
                map["nomor_boring"] = cursor.getString(0)
                result.add(map)
            } while (cursor.moveToNext())
        }
        db.close()
        return result
    }

    fun getIdSurvey(): Int{
        var result = 0
        val dbHelper = DatabaseHelper(context)
        val db = dbHelper.writableDatabase

        val cursor = db.query(
            TABLE_NAME, arrayOf(
                "id"
            ), "nomorBoring = ? AND nama_khg = ?", arrayOf(nomorBoring,nama_khg), null, null, null, null
        )
        if (cursor != null && cursor.moveToFirst()) {
            result = cursor.getInt(0)
        } else {
            result = 0
        }

        db.close()
        return result
    }


    fun getData(id: Int): Boolean {
        var result = false
        val dbHelper = DatabaseHelper(context)
        val db = dbHelper.writableDatabase

        val cursor = db.query(
            TABLE_NAME, arrayOf(
                "id",
                "nama_khg",
                "nomorBoring",
                "tanggal",
                "surveyorId",
                "reguId",
                "provinsi",
                "kota_kabupaten",
                "kecamatan",
                "kelurahan",
                "jenis_tanah",
                "titik_p10",
                "sample_p10",
                "jenis_sample",
                "longitude",
                "latitude",
                "longitude_target",
                "latitude_target",
                "longitude_hp",
                "latitude_hp",
                "kordinatImg1",
                "kordinatImg2",
                "kordinatImg3",
                "kordinatImg4",
                "kordinatImg5",
                "kordinatImg6",
                "elevasi",
                "elevasiImg1",
                "elevasiImg2",
                "elevasiImg3",
                "elevasiImg4",
                "kedalamanAirTanah",
                "genangan",
                "banjirBulan",
                "banjirLamanya",
                "banjirKetinggianAir",
                "banjirSumberGenangan",
                "elevasiImg5",
                "elevasiImg6",
                "banjirImg1",
                "banjirImg2",
                "banjirImg3",
                "banjirImg4",
                "tutupanLahanJenisTanaman",
                "tutupanLahanStatus",
                "tutupanLahanNamaPerusahaan",
                "tutupanLahanLuas",
                "tutupanLahanPenggunaanLahan",
                "banjirImg5",
                "banjirImg6",
                "tutupanLahanImg1",
                "tutupanLahanImg2",
                "tutupanLahanImg3",
                "tutupanLahanImg4",
                "flora",
                "floraJenis",
                "fauna",
                "faunaJenis",
                "tutupanLahanImg5",
                "tutupanLahanImg6",
                "floraFaunaImg1",
                "floraFaunaImg2",
                "floraFaunaImg3",
                "floraFaunaImg4",
                "drainaseAlami",
                "drainaseBuatan",
                "drainaseBuatanJenis",
                "drainaseBuatanTinggiMukaAir",
                "floraFaunaImg5",
                "floraFaunaImg6",
                "drainaseImg1",
                "drainaseImg2",
                "drainaseImg3",
                "drainaseImg4",
                "kualitasAirTanahPh",
                "kualitasAirTanahEc",
                "kualitasAirTanahTds",
                "drainaseImg5",
                "drainaseImg6",
                "kualitasiAirTanahImg1",
                "kualitasiAirTanahImg2",
                "kualitasiAirTanahImg3",
                "kualitasiAirTanahImg4",
                "karakteristikSubstratumTanahNa",
                "karakteristikSubstratumTanahNaJenis",
                "karakteristikSubstratumTanahLiatPh",
                "karakteristikSubstratumTanahLiatEc",
                "kualitasiAirTanahImg5",
                "kualitasiAirTanahImg6",
                "karakteristikSubstratumTanahLiatImg1",
                "karakteristikSubstratumTanahLiatImg2",
                "karakteristikSubstratumTanahLiatImg3",
                "karakteristikSubstratumTanahLiatImg4",
                "tipeLuapanKemarau",
                "tipeLuapanHujan",
                "karakteristikSubstratumTanahLiatImg5",
                "karakteristikSubstratumTanahLiatImg6",
                "tipeLuapanImg1",
                "tipeLuapanImg2",
                "tipeLuapanImg3",
                "tipeLuapanImg4",
                "ketebalanGambutNa",
                "ketebalanGambutNaJenis",
                "ketebalanGambutTinggi",
                "ketebalanGambutJenis",
                "tipeLuapanImg5",
                "tipeLuapanImg6",
                "ketebalanGambutImg1",
                "ketebalanGambutImg2",
                "ketebalanGambutImg3",
                "ketebalanGambutImg4",
                "karakteristikSubstratumDibawahLapisanGambutNa",
                "karakteristikSubstratumDibawahLapisanGambutNaJenis",
                "karakteristikSubstratumDibawahLapisanGambutJenis",
                "karakteristikSubstratumDibawahLapisanGambutLainnya",
                "ketebalanGambutImg5",
                "ketebalanGambutImg6",
                "karakteristikSubstratumDibawahLapisanGambutImg1",
                "karakteristikSubstratumDibawahLapisanGambutImg2",
                "karakteristikSubstratumDibawahLapisanGambutImg3",
                "karakteristikSubstratumDibawahLapisanGambutImg4",
                "tingkatKerusakanLahanGambutDrainaseBuatan",
                "tingkatKerusakanLahanGambutSedimen",
                "tingkatKerusakanLahanKondisiTanaman",
                "tingkatKerusakanLahanSubsiden",
                "tingkatKerusakanLahanKerapatanTajuk",
                "karakteristikSubstratumDibawahLapisanGambutImg5",
                "karakteristikSubstratumDibawahLapisanGambutImg6",
                "tingkatKerusakanLahanImg1",
                "tingkatKerusakanLahanImg2",
                "tingkatKerusakanLahanImg3",
                "tingkatKerusakanLahanImg4",
                "kebakaranLahanTahun",
                "kebakaranLahanBulan",
                "kebakaranLahanTanggal",
                "kebakaranLahanLamaKejadian",
                "kebakaranLahanLamaKejadianHariMInggu",
                "kebakaranLahanUpayaPemadamanSwadayaMasyarakat",
                "kebakaranLahanUpayaPemadamanPemerintah",
                "hujanTerakhir",
                "hujanLamaKejadian",
                "hujanLamaKejadianJamHari",
                "hujanIntensitas",
                "tingkatKerusakanLahanImg5",
                "tingkatKerusakanLahanImg6",
                "kebakaranHujanImg1",
                "kebakaranHujanImg2",
                "kebakaranHujanImg3",
                "kebakaranHujanImg4",
                "porositasKelengasanNa",
                "porositasKelengasanNaJenis",
                "porositasKelengasan1",
                "porositasKelengasan2",
                "porositasKelengasanKeterangan",
                "kebakaranHujanImg5",
                "kebakaranHujanImg6",
                "porositasKelengasanImg1",
                "porositasKelengasanImg2",
                "porositasKelengasanImg3",
                "porositasKelengasanImg4",
                "keterangan",
                "porositasKelengasanImg5",
                "porositasKelengasanImg6",
                "sketsaLokasi",
                "imgBatasBarat",
                "imgBatasTimur",
                "imgBatasSelatan",
                "imgBatasUtara",
                "status",
                "ketebalanGambut",
                "karakteristikSubstratumDibawahLapisanGambutJenis1",
                "karakteristikSubstratumDibawahLapisanGambutJenis2",
                "karakteristikSubstratumDibawahLapisanGambutJenis3",
                "karakteristikSubstratumDibawahLapisanGambutJenis4",
                "karakteristikSubstratumDibawahLapisanGambutJenis5",
                "banjirSumberGenangan1",
                "banjirSumberGenangan2",
                "banjirSumberGenangan3",
                "banjirSumberGenangan4",
                "banjirSumberGenanganLainnya",
                "tingkatKerusakanLahanKondisiTanaman1",
                "tingkatKerusakanLahanKondisiTanaman2",
                "tingkatKerusakanLahanKondisiTanaman3",
                "tingkatKerusakanLahanKondisiTanaman4",
                "kualitasAirTanahPhAt",
                "kualitasAirTanahPhAs",
                "kualitasAirTanahEcAt",
                "kualitasAirTanahEcAs",
                "kualitasAirTanahTdsAt",
                "kualitasAirTanahTdsAs",
                "dusun"

        ), "id = ? ", arrayOf( id.toString() ), null, null, null, null
        )

        if (cursor != null && cursor.moveToFirst()) {
            this.id = cursor.getString(0)
            nama_khg = cursor.getString(1)
            nomorBoring = cursor.getString(2)
            tanggal = cursor.getString(3)
            surveyorId = cursor.getString(4)
            reguId = cursor.getString(5)
            provinsi = cursor.getString(6)
            kota_kabupaten = cursor.getString(7)
            kecamatan = cursor.getString(8)
            kelurahan = cursor.getString(9)
            jenis_tanah = cursor.getString(10)
            titik_p10 = cursor.getString(11)
            sample_p10 = cursor.getString(12)
            jenis_sample = cursor.getString(13)
            longitude = cursor.getString(14)
            latitude = cursor.getString(15)
            longitude_target = cursor.getString(16)
            latitude_target = cursor.getString(17)
            longitude_hp = cursor.getString(18)
            latitude_hp = cursor.getString(19)
            kordinatImg1 = cursor.getString(20)
            kordinatImg2 = cursor.getString(21)
            kordinatImg3 = cursor.getString(22)
            kordinatImg4 = cursor.getString(23)
            kordinatImg5 = cursor.getString(24)
            kordinatImg6 = cursor.getString(25)
            elevasi = cursor.getString(26)
            elevasiImg1 = cursor.getString(27)
            elevasiImg2 = cursor.getString(28)
            elevasiImg3 = cursor.getString(29)
            elevasiImg4 = cursor.getString(30)
            kedalamanAirTanah = cursor.getString(31)
            genangan = cursor.getString(32)
            banjirBulan = cursor.getString(33)
            banjirLamanya = cursor.getString(34)
            banjirKetinggianAir = cursor.getString(35)
            banjirSumberGenangan = cursor.getString(36)
            elevasiImg5 = cursor.getString(37)
            elevasiImg6 = cursor.getString(38)
            banjirImg1 = cursor.getString(39)
            banjirImg2 = cursor.getString(40)
            banjirImg3 = cursor.getString(41)
            banjirImg4 = cursor.getString(42)
            tutupanLahanJenisTanaman = cursor.getString(43)
            tutupanLahanStatus = cursor.getString(44)
            tutupanLahanNamaPerusahaan = cursor.getString(45)
            tutupanLahanLuas = cursor.getString(46)
            tutupanLahanPenggunaanLahan = cursor.getString(47)
            banjirImg5 = cursor.getString(48)
            banjirImg6 = cursor.getString(49)
            tutupanLahanImg1 = cursor.getString(50)
            tutupanLahanImg2 = cursor.getString(51)
            tutupanLahanImg3 = cursor.getString(52)
            tutupanLahanImg4 = cursor.getString(53)
            flora = cursor.getString(54)
            floraJenis = cursor.getString(55)
            fauna = cursor.getString(56)
            faunaJenis = cursor.getString(57)
            tutupanLahanImg5 = cursor.getString(58)
            tutupanLahanImg6 = cursor.getString(59)
            floraFaunaImg1 = cursor.getString(60)
            floraFaunaImg2 = cursor.getString(61)
            floraFaunaImg3 = cursor.getString(62)
            floraFaunaImg4 = cursor.getString(63)
            drainaseAlami = cursor.getString(64)
            drainaseBuatan = cursor.getString(65)
            drainaseBuatanJenis = cursor.getString(66)
            drainaseBuatanTinggiMukaAir = cursor.getString(67)
            floraFaunaImg5 = cursor.getString(68)
            floraFaunaImg6 = cursor.getString(69)
            drainaseImg1 = cursor.getString(70)
            drainaseImg2 = cursor.getString(71)
            drainaseImg3 = cursor.getString(72)
            drainaseImg4 = cursor.getString(73)
            kualitasAirTanahPh = cursor.getString(74)
            kualitasAirTanahEc = cursor.getString(75)
            kualitasAirTanahTds = cursor.getString(76)
            drainaseImg5 = cursor.getString(77)
            drainaseImg6 = cursor.getString(78)
            kualitasiAirTanahImg1 = cursor.getString(79)
            kualitasiAirTanahImg2 = cursor.getString(80)
            kualitasiAirTanahImg3 = cursor.getString(81)
            kualitasiAirTanahImg4 = cursor.getString(82)
            karakteristikSubstratumTanahNa = cursor.getString(83)
            karakteristikSubstratumTanahNaJenis = cursor.getString(84)
            karakteristikSubstratumTanahLiatPh = cursor.getString(85)
            karakteristikSubstratumTanahLiatEc = cursor.getString(86)
            kualitasiAirTanahImg5 = cursor.getString(87)
            kualitasiAirTanahImg6 = cursor.getString(88)
            karakteristikSubstratumTanahLiatImg1 = cursor.getString(89)
            karakteristikSubstratumTanahLiatImg2 = cursor.getString(90)
            karakteristikSubstratumTanahLiatImg3 = cursor.getString(91)
            karakteristikSubstratumTanahLiatImg4 = cursor.getString(92)
            tipeLuapanKemarau = cursor.getString(93)
            tipeLuapanHujan = cursor.getString(94)
            karakteristikSubstratumTanahLiatImg5 = cursor.getString(95)
            karakteristikSubstratumTanahLiatImg6 = cursor.getString(96)
            tipeLuapanImg1 = cursor.getString(97)
            tipeLuapanImg2 = cursor.getString(98)
            tipeLuapanImg3 = cursor.getString(99)
            tipeLuapanImg4 = cursor.getString(100)
            ketebalanGambutNa = cursor.getString(101)
            ketebalanGambutNaJenis = cursor.getString(102)
            ketebalanGambutTinggi = cursor.getString(103)
            ketebalanGambutJenis = cursor.getString(104)
            tipeLuapanImg5 = cursor.getString(105)
            tipeLuapanImg6 = cursor.getString(106)
            ketebalanGambutImg1 = cursor.getString(107)
            ketebalanGambutImg2 = cursor.getString(108)
            ketebalanGambutImg3 = cursor.getString(109)
            ketebalanGambutImg4 = cursor.getString(110)
            karakteristikSubstratumDibawahLapisanGambutNa = cursor.getString(111)
            karakteristikSubstratumDibawahLapisanGambutNaJenis = cursor.getString(112)
            karakteristikSubstratumDibawahLapisanGambutJenis = cursor.getString(113)
            karakteristikSubstratumDibawahLapisanGambutLainnya = cursor.getString(114)
            ketebalanGambutImg5 = cursor.getString(115)
            ketebalanGambutImg6 = cursor.getString(116)
            karakteristikSubstratumDibawahLapisanGambutImg1 = cursor.getString(117)
            karakteristikSubstratumDibawahLapisanGambutImg2 = cursor.getString(118)
            karakteristikSubstratumDibawahLapisanGambutImg3 = cursor.getString(119)
            karakteristikSubstratumDibawahLapisanGambutImg4 = cursor.getString(120)
            tingkatKerusakanLahanGambutDrainaseBuatan = cursor.getString(121)
            tingkatKerusakanLahanGambutSedimen = cursor.getString(122)
            tingkatKerusakanLahanKondisiTanaman = cursor.getString(123)
            tingkatKerusakanLahanSubsiden = cursor.getString(124)
            tingkatKerusakanLahanKerapatanTajuk = cursor.getString(125)
            karakteristikSubstratumDibawahLapisanGambutImg5 = cursor.getString(126)
            karakteristikSubstratumDibawahLapisanGambutImg6 = cursor.getString(127)
            tingkatKerusakanLahanImg1 = cursor.getString(128)
            tingkatKerusakanLahanImg2 = cursor.getString(129)
            tingkatKerusakanLahanImg3 = cursor.getString(130)
            tingkatKerusakanLahanImg4 = cursor.getString(131)
            kebakaranLahanTahun = cursor.getString(132)
            kebakaranLahanBulan = cursor.getString(133)
            kebakaranLahanTanggal = cursor.getString(134)
            kebakaranLahanLamaKejadian = cursor.getString(135)
            kebakaranLahanLamaKejadianHariMInggu = cursor.getString(136)
            kebakaranLahanUpayaPemadamanSwadayaMasyarakat = cursor.getString(137)
            kebakaranLahanUpayaPemadamanPemerintah = cursor.getString(138)
            hujanTerakhir = cursor.getString(139)
            hujanLamaKejadian = cursor.getString(140)
            hujanLamaKejadianJamHari = cursor.getString(141)
            hujanIntensitas = cursor.getString(142)
            tingkatKerusakanLahanImg5 = cursor.getString(143)
            tingkatKerusakanLahanImg6 = cursor.getString(144)
            kebakaranHujanImg1 = cursor.getString(145)
            kebakaranHujanImg2 = cursor.getString(146)
            kebakaranHujanImg3 = cursor.getString(147)
            kebakaranHujanImg4 = cursor.getString(148)
            porositasKelengasanNa = cursor.getString(149)
            porositasKelengasanNaJenis = cursor.getString(150)
            porositasKelengasan1 = cursor.getString(151)
            porositasKelengasan2 = cursor.getString(152)
            porositasKelengasanKeterangan = cursor.getString(153)
            kebakaranHujanImg5 = cursor.getString(154)
            kebakaranHujanImg6 = cursor.getString(155)
            porositasKelengasanImg1 = cursor.getString(156)
            porositasKelengasanImg2 = cursor.getString(157)
            porositasKelengasanImg3 = cursor.getString(158)
            porositasKelengasanImg4 = cursor.getString(159)
            keterangan = cursor.getString(160)
            porositasKelengasanImg5 = cursor.getString(161)
            porositasKelengasanImg6 = cursor.getString(162)
            sketsaLokasi = cursor.getString(163)
            imgBatasBarat = cursor.getString(164)
            imgBatasTimur = cursor.getString(165)
            imgBatasSelatan = cursor.getString(166)
            imgBatasUtara = cursor.getString(167)
            status = cursor.getInt(168)
            ketebalanGambut = cursor.getString(169)
            karakteristikSubstratumDibawahLapisanGambutJenis1 = cursor.getString(170)
            karakteristikSubstratumDibawahLapisanGambutJenis2 = cursor.getString(171)
            karakteristikSubstratumDibawahLapisanGambutJenis3 = cursor.getString(172)
            karakteristikSubstratumDibawahLapisanGambutJenis4 = cursor.getString(173)
            karakteristikSubstratumDibawahLapisanGambutJenis5 = cursor.getString(174)

            banjirSumberGenangan1 = cursor.getString(175)
            banjirSumberGenangan2 = cursor.getString(176)
            banjirSumberGenangan3 = cursor.getString(177)
            banjirSumberGenangan4 = cursor.getString(178)
            banjirSumberGenanganLainnya = cursor.getString(179)

            tingkatKerusakanLahanKondisiTanaman1 = cursor.getString(180)
            tingkatKerusakanLahanKondisiTanaman2 = cursor.getString(181)
            tingkatKerusakanLahanKondisiTanaman3 = cursor.getString(182)
            tingkatKerusakanLahanKondisiTanaman4 = cursor.getString(183)

            kualitasAirTanahPhAt    =   cursor.getString(184)
            kualitasAirTanahPhAs    =   cursor.getString(185)
            kualitasAirTanahEcAt    =   cursor.getString(186)
            kualitasAirTanahEcAs    =   cursor.getString(187)
            kualitasAirTanahTdsAt    =   cursor.getString(188)
            kualitasAirTanahTdsAs    =   cursor.getString(189)
            dusun = cursor.getString(190)

            result = true
        } else {
            result = false
        }

        db.close()
        return result
    }

    fun delete(nomorBoring: String) {
        val dbHelper = DatabaseHelper(context)
        val db = dbHelper.writableDatabase

        db.delete(TABLE_NAME, "id = '${id.toString()}'", null)
        //db.delete("lahan", "nomorBoring = '${nomorBoring.toUpperCase()}'", null)
        db.close()
    }

    fun updateStatus(nomorBoring: String, status: String) {
        val dbHelper = DatabaseHelper(context)
        val db = dbHelper.writableDatabase

        val values = ContentValues()
        values.put("status", status)
        db.update(TABLE_NAME, values, "id = '${id.toString()}'", null)

        db.close()
    }

    fun deleteAll() {
        val dbHelper = DatabaseHelper(context)
        val db = dbHelper.writableDatabase

        db.delete(TABLE_NAME, null, null)
        db.close()
    }

    fun importData(data: JSONArray) {
        deleteAll()

        val dbHelper = DatabaseHelper(context)
        val db = dbHelper.writableDatabase

        val sql =
            "INSERT INTO survey(id, nama_khg, nomorBoring, tanggal, surveyorId, reguId, provinsi, kota_kabupaten, kecamatan, kelurahan, jenis_tanah, titik_p10, sample_p10, jenis_sample, longitude, latitude, longitude_target, latitude_target, longitude_hp, latitude_hp, kordinatImg1, kordinatImg2, kordinatImg3, kordinatImg4, kordinatImg5, kordinatImg6, elevasi, elevasiImg1, elevasiImg2, elevasiImg3, elevasiImg4, kedalamanAirTanah, genangan, banjirBulan, banjirLamanya, banjirKetinggianAir, banjirSumberGenangan, elevasiImg5, elevasiImg6, banjirImg1, banjirImg2, banjirImg3, banjirImg4, tutupanLahanJenisTanaman, tutupanLahanStatus, tutupanLahanNamaPerusahaan, tutupanLahanLuas, tutupanLahanPenggunaanLahan, banjirImg5, banjirImg6, tutupanLahanImg1, tutupanLahanImg2, tutupanLahanImg3, tutupanLahanImg4, flora, floraJenis, fauna, faunaJenis, tutupanLahanImg5, tutupanLahanImg6, floraFaunaImg1, floraFaunaImg2, floraFaunaImg3, floraFaunaImg4, drainaseAlami, drainaseBuatan, drainaseBuatanJenis, drainaseBuatanTinggiMukaAir, floraFaunaImg5, floraFaunaImg6, drainaseImg1, drainaseImg2, drainaseImg3, drainaseImg4, kualitasAirTanahPh, kualitasAirTanahEc, kualitasAirTanahTds, drainaseImg5, drainaseImg6, kualitasiAirTanahImg1, kualitasiAirTanahImg2, kualitasiAirTanahImg3, kualitasiAirTanahImg4, karakteristikSubstratumTanahNa, karakteristikSubstratumTanahNaJenis, karakteristikSubstratumTanahLiatPh, karakteristikSubstratumTanahLiatEc, kualitasiAirTanahImg5, kualitasiAirTanahImg6, karakteristikSubstratumTanahLiatImg1, karakteristikSubstratumTanahLiatImg2, karakteristikSubstratumTanahLiatImg3, karakteristikSubstratumTanahLiatImg4, tipeLuapanKemarau, tipeLuapanHujan, karakteristikSubstratumTanahLiatImg5, karakteristikSubstratumTanahLiatImg6, tipeLuapanImg1, tipeLuapanImg2, tipeLuapanImg3, tipeLuapanImg4, ketebalanGambutNa, ketebalanGambutNaJenis, ketebalanGambutTinggi, ketebalanGambutJenis, tipeLuapanImg5, tipeLuapanImg6, ketebalanGambutImg1, ketebalanGambutImg2, ketebalanGambutImg3, ketebalanGambutImg4, karakteristikSubstratumDibawahLapisanGambutNa, karakteristikSubstratumDibawahLapisanGambutNaJenis, karakteristikSubstratumDibawahLapisanGambutJenis, karakteristikSubstratumDibawahLapisanGambutLainnya, ketebalanGambutImg5, ketebalanGambutImg6, karakteristikSubstratumDibawahLapisanGambutImg1, karakteristikSubstratumDibawahLapisanGambutImg2, karakteristikSubstratumDibawahLapisanGambutImg3, karakteristikSubstratumDibawahLapisanGambutImg4, tingkatKerusakanLahanGambutDrainaseBuatan, tingkatKerusakanLahanGambutSedimen, tingkatKerusakanLahanKondisiTanaman, tingkatKerusakanLahanSubsiden, tingkatKerusakanLahanKerapatanTajuk, karakteristikSubstratumDibawahLapisanGambutImg5, karakteristikSubstratumDibawahLapisanGambutImg6, tingkatKerusakanLahanImg1, tingkatKerusakanLahanImg2, tingkatKerusakanLahanImg3, tingkatKerusakanLahanImg4, kebakaranLahanTahun, kebakaranLahanBulan, kebakaranLahanTanggal, kebakaranLahanLamaKejadian, kebakaranLahanLamaKejadianHariMInggu, kebakaranLahanUpayaPemadamanSwadayaMasyarakat, kebakaranLahanUpayaPemadamanPemerintah, hujanTerakhir, hujanLamaKejadian, hujanLamaKejadianJamHari, hujanIntensitas, tingkatKerusakanLahanImg5, tingkatKerusakanLahanImg6, kebakaranHujanImg1, kebakaranHujanImg2, kebakaranHujanImg3, kebakaranHujanImg4, porositasKelengasanNa, porositasKelengasanNaJenis, porositasKelengasan1, porositasKelengasan2, porositasKelengasanKeterangan, kebakaranHujanImg5, kebakaranHujanImg6, porositasKelengasanImg1, porositasKelengasanImg2, porositasKelengasanImg3, porositasKelengasanImg4, keterangan, porositasKelengasanImg5, porositasKelengasanImg6, sketsaLokasi, imgBatasBarat, imgBatasTimur, imgBatasSelatan, imgBatasUtara, status, ketebalanGambut, " +
                    "karakteristikSubstratumDibawahLapisanGambutJenis1,karakteristikSubstratumDibawahLapisanGambutJenis2,karakteristikSubstratumDibawahLapisanGambutJenis3,karakteristikSubstratumDibawahLapisanGambutJenis4,karakteristikSubstratumDibawahLapisanGambutJenis5,banjirSumberGenangan1,banjirSumberGenangan2,banjirSumberGenangan3,banjirSumberGenangan4,banjirSumberGenanganLainnya,tingkatKerusakanLahanKondisiTanaman1,tingkatKerusakanLahanKondisiTanaman2,tingkatKerusakanLahanKondisiTanaman3,tingkatKerusakanLahanKondisiTanaman4, " +
                    "kualitasAirTanahPhAt," +
                    "kualitasAirTanahPhAs," +
                    "kualitasAirTanahEcAt," +
                    "kualitasAirTanahEcAs," +
                    "kualitasAirTanahTdsAt," +
                    "kualitasAirTanahTdsAs, dusun) VALUES " +
                    "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);"
        var statement = db.compileStatement(sql)
        db.beginTransaction()

        for (i in 0 until data.length()) {
            val c = data.getJSONObject(i)
            val id = c.getString("id")
            val nama_khg = c.getString("nama_khg")
            val nomorBoring = c.getString("nomorBoring")
            val tanggal = c.getString("tanggal")
            val surveyorId = c.getString("surveyorId")
            val reguId = c.getString("reguId")
            val provinsi = c.getString("provinsi")
            val kota_kabupaten = c.getString("kota_kabupaten")
            val kecamatan = c.getString("kecamatan")
            val kelurahan = c.getString("kelurahan")
            val jenis_tanah = c.getString("jenis_tanah")
            val titik_p10 = c.getString("titik_p10")
            val sample_p10 = c.getString("sample_p10")
            val jenis_sample = c.getString("jenis_sample")
            val longitude = c.getString("longitude")
            val latitude = c.getString("latitude")
            val longitude_target = c.getString("longitude_target")
            val latitude_target = c.getString("latitude_target")
            val longitude_hp = c.getString("longitude_hp")
            val latitude_hp = c.getString("latitude_hp")
            val kordinatImg1 = ""
            val kordinatImg2 = ""
            val kordinatImg3 = ""
            val kordinatImg4 = ""
            val kordinatImg5 = ""
            val kordinatImg6 = ""
            val elevasi = c.getString("elevasi")
            val elevasiImg1 = ""
            val elevasiImg2 = ""
            val elevasiImg3 = ""
            val elevasiImg4 = ""
            val kedalamanAirTanah = c.getString("kedalamanAirTanah")
            val genangan = c.getString("genangan")
            val banjirBulan = c.getString("banjirBulan")
            val banjirLamanya = c.getString("banjirLamanya")
            val banjirKetinggianAir = c.getString("banjirKetinggianAir")
            val banjirSumberGenangan = c.getString("banjirSumberGenangan")
            val elevasiImg5 = ""
            val elevasiImg6 = ""
            val banjirImg1 = ""
            val banjirImg2 = ""
            val banjirImg3 = ""
            val banjirImg4 = ""
            val tutupanLahanJenisTanaman = c.getString("tutupanLahanJenisTanaman")
            val tutupanLahanStatus = c.getString("tutupanLahanStatus")
            val tutupanLahanNamaPerusahaan = c.getString("tutupanLahanNamaPerusahaan")
            val tutupanLahanLuas = c.getString("tutupanLahanLuas")
            val tutupanLahanPenggunaanLahan = c.getString("tutupanLahanPenggunaanLahan")
            val banjirImg5 = ""
            val banjirImg6 = ""
            val tutupanLahanImg1 = ""
            val tutupanLahanImg2 = ""
            val tutupanLahanImg3 = ""
            val tutupanLahanImg4 = ""
            val flora = c.getString("flora")
            val floraJenis = c.getString("floraJenis")
            val fauna = c.getString("fauna")
            val faunaJenis = c.getString("faunaJenis")
            val tutupanLahanImg5 = ""
            val tutupanLahanImg6 = ""
            val floraFaunaImg1 = ""
            val floraFaunaImg2 = ""
            val floraFaunaImg3 = ""
            val floraFaunaImg4 = ""
            val drainaseAlami = c.getString("drainaseAlami")
            val drainaseBuatan = c.getString("drainaseBuatan")
            val drainaseBuatanJenis = c.getString("drainaseBuatanJenis")
            val drainaseBuatanTinggiMukaAir = c.getString("drainaseBuatanTinggiMukaAir")
            val floraFaunaImg5 = ""
            val floraFaunaImg6 = ""
            val drainaseImg1 = ""
            val drainaseImg2 = ""
            val drainaseImg3 = ""
            val drainaseImg4 = ""
            val kualitasAirTanahPh = c.getString("kualitasAirTanahPh")
            val kualitasAirTanahEc = c.getString("kualitasAirTanahEc")
            val kualitasAirTanahTds = c.getString("kualitasAirTanahTds")
            val drainaseImg5 = ""
            val drainaseImg6 = ""
            val kualitasiAirTanahImg1 = ""
            val kualitasiAirTanahImg2 = ""
            val kualitasiAirTanahImg3 = ""
            val kualitasiAirTanahImg4 = ""
            val karakteristikSubstratumTanahNa = c.getString("karakteristikSubstratumTanahNa")
            val karakteristikSubstratumTanahNaJenis = c.getString("karakteristikSubstratumTanahNaJenis")
            val karakteristikSubstratumTanahLiatPh = c.getString("karakteristikSubstratumTanahLiatPh")
            val karakteristikSubstratumTanahLiatEc = c.getString("karakteristikSubstratumTanahLiatEc")
            val kualitasiAirTanahImg5 = ""
            val kualitasiAirTanahImg6 = ""
            val karakteristikSubstratumTanahLiatImg1 = ""
            val karakteristikSubstratumTanahLiatImg2 = ""
            val karakteristikSubstratumTanahLiatImg3 = ""
            val karakteristikSubstratumTanahLiatImg4 = ""
            val tipeLuapanKemarau = c.getString("tipeLuapanKemarau")
            val tipeLuapanHujan = c.getString("tipeLuapanHujan")
            val karakteristikSubstratumTanahLiatImg5 = ""
            val karakteristikSubstratumTanahLiatImg6 = ""
            val tipeLuapanImg1 = ""
            val tipeLuapanImg2 = ""
            val tipeLuapanImg3 = ""
            val tipeLuapanImg4 = ""
            val ketebalanGambutNa = c.getString("ketebalanGambutNa")
            val ketebalanGambutNaJenis = c.getString("ketebalanGambutNaJenis")
            val ketebalanGambutTinggi = c.getString("ketebalanGambutTinggi")
            val ketebalanGambutJenis = c.getString("ketebalanGambutJenis")
            val tipeLuapanImg5 = ""
            val tipeLuapanImg6 = ""
            val ketebalanGambutImg1 = ""
            val ketebalanGambutImg2 = ""
            val ketebalanGambutImg3 = ""
            val ketebalanGambutImg4 = ""
            val karakteristikSubstratumDibawahLapisanGambutNa = c.getString("karakteristikSubstratumDibawahLapisanGambutNa")
            val karakteristikSubstratumDibawahLapisanGambutNaJenis = c.getString("karakteristikSubstratumDibawahLapisanGambutNaJenis")
            val karakteristikSubstratumDibawahLapisanGambutJenis = c.getString("karakteristikSubstratumDibawahLapisanGambutJenis")
            val karakteristikSubstratumDibawahLapisanGambutLainnya = c.getString("karakteristikSubstratumDibawahLapisanGambutLainnya")
            val ketebalanGambutImg5 = ""
            val ketebalanGambutImg6 = ""
            val karakteristikSubstratumDibawahLapisanGambutImg1 = ""
            val karakteristikSubstratumDibawahLapisanGambutImg2 = ""
            val karakteristikSubstratumDibawahLapisanGambutImg3 = ""
            val karakteristikSubstratumDibawahLapisanGambutImg4 = ""
            val tingkatKerusakanLahanGambutDrainaseBuatan = c.getString("tingkatKerusakanLahanGambutDrainaseBuatan")
            val tingkatKerusakanLahanGambutSedimen = c.getString("tingkatKerusakanLahanGambutSedimen")
            val tingkatKerusakanLahanKondisiTanaman = c.getString("tingkatKerusakanLahanKondisiTanaman")
            val tingkatKerusakanLahanSubsiden = c.getString("tingkatKerusakanLahanSubsiden")
            val tingkatKerusakanLahanKerapatanTajuk = c.getString("tingkatKerusakanLahanKerapatanTajuk")
            val karakteristikSubstratumDibawahLapisanGambutImg5 = ""
            val karakteristikSubstratumDibawahLapisanGambutImg6 = ""
            val tingkatKerusakanLahanImg1 = ""
            val tingkatKerusakanLahanImg2 = ""
            val tingkatKerusakanLahanImg3 = ""
            val tingkatKerusakanLahanImg4 = ""
            val kebakaranLahanTahun = c.getString("kebakaranLahanTahun")
            val kebakaranLahanBulan = c.getString("kebakaranLahanBulan")
            val kebakaranLahanTanggal = c.getString("kebakaranLahanTanggal")
            val kebakaranLahanLamaKejadian = c.getString("kebakaranLahanLamaKejadian")
            val kebakaranLahanLamaKejadianHariMInggu = c.getString("kebakaranLahanLamaKejadianHariMInggu")
            val kebakaranLahanUpayaPemadamanSwadayaMasyarakat = c.getString("kebakaranLahanUpayaPemadamanSwadayaMasyarakat")
            val kebakaranLahanUpayaPemadamanPemerintah = c.getString("kebakaranLahanUpayaPemadamanPemerintah")
            val hujanTerakhir = c.getString("hujanTerakhir")
            val hujanLamaKejadian = c.getString("hujanLamaKejadian")
            val hujanLamaKejadianJamHari = c.getString("hujanLamaKejadianJamHari")
            val hujanIntensitas = c.getString("hujanIntensitas")
            val tingkatKerusakanLahanImg5 = ""
            val tingkatKerusakanLahanImg6 = ""
            val kebakaranHujanImg1 = ""
            val kebakaranHujanImg2 = ""
            val kebakaranHujanImg3 = ""
            val kebakaranHujanImg4 = ""
            val porositasKelengasanNa = c.getString("porositasKelengasanNa")
            val porositasKelengasanNaJenis = c.getString("porositasKelengasanNaJenis")
            val porositasKelengasan1 = c.getString("porositasKelengasan1")
            val porositasKelengasan2 = c.getString("porositasKelengasan2")
            val porositasKelengasanKeterangan = c.getString("porositasKelengasanKeterangan")
            val kebakaranHujanImg5 = ""
            val kebakaranHujanImg6 = ""
            val porositasKelengasanImg1 = ""
            val porositasKelengasanImg2 = ""
            val porositasKelengasanImg3 = ""
            val porositasKelengasanImg4 = ""
            val keterangan = c.getString("keterangan")
            val porositasKelengasanImg5 = ""
            val porositasKelengasanImg6 = ""
            val sketsaLokasi = ""
            val imgBatasBarat = ""
            val imgBatasTimur = ""
            val imgBatasSelatan = ""
            val imgBatasUtara = ""
            val status  = "0"
            val ketebalanGambut  =c.getString("ketebalanGambut")
            val karakteristikSubstratumDibawahLapisanGambutJenis1  = c.getString("karakteristikSubstratumDibawahLapisanGambutJenis1")
            val karakteristikSubstratumDibawahLapisanGambutJenis2  = c.getString("karakteristikSubstratumDibawahLapisanGambutJenis2")
            val karakteristikSubstratumDibawahLapisanGambutJenis3  = c.getString("karakteristikSubstratumDibawahLapisanGambutJenis3")
            val karakteristikSubstratumDibawahLapisanGambutJenis4  = c.getString("karakteristikSubstratumDibawahLapisanGambutJenis4")
            val karakteristikSubstratumDibawahLapisanGambutJenis5  = c.getString("karakteristikSubstratumDibawahLapisanGambutJenis5")

            val banjirSumberGenangan1  = c.getString("banjirSumberGenangan1")
            val banjirSumberGenangan2  = c.getString("banjirSumberGenangan2")
            val banjirSumberGenangan3  = c.getString("banjirSumberGenangan3")
            val banjirSumberGenangan4  = c.getString("banjirSumberGenangan4")
            val banjirSumberGenanganLainnya  = c.getString("banjirSumberGenanganLainnya")

            val tingkatKerusakanLahanKondisiTanaman1  = c.getString("tingkatKerusakanLahanKondisiTanaman1")
            val tingkatKerusakanLahanKondisiTanaman2  = c.getString("tingkatKerusakanLahanKondisiTanaman2")
            val tingkatKerusakanLahanKondisiTanaman3  = c.getString("tingkatKerusakanLahanKondisiTanaman3")
            val tingkatKerusakanLahanKondisiTanaman4  = c.getString("tingkatKerusakanLahanKondisiTanaman4")

            val kualitasAirTanahPhAt    = c.getString("kualitasAirTanahPhAt")
            val kualitasAirTanahPhAs    = c.getString("kualitasAirTanahPhAs")
            val kualitasAirTanahEcAt    = c.getString("kualitasAirTanahEcAt")
            val kualitasAirTanahEcAs    = c.getString("kualitasAirTanahEcAs")
            val kualitasAirTanahTdsAt   = c.getString("kualitasAirTanahTdsAt")
            val kualitasAirTanahTdsAs   = c.getString("kualitasAirTanahTdsAs")
            val dusun = c.getString("dusun")


            statement.clearBindings()
            statement.bindString(1, id)
            statement.bindString(2, nama_khg)
            statement.bindString(3, nomorBoring)
            statement.bindString(4, tanggal)
            statement.bindString(5, surveyorId)
            statement.bindString(6, reguId)
            statement.bindString(7, provinsi)
            statement.bindString(8, kota_kabupaten)
            statement.bindString(9, kecamatan)
            statement.bindString(10, kelurahan)
            statement.bindString(11, jenis_tanah)
            statement.bindString(12, titik_p10)
            statement.bindString(13, sample_p10)
            statement.bindString(14, jenis_sample)
            statement.bindString(15, longitude)
            statement.bindString(16, latitude)
            statement.bindString(17, longitude_target)
            statement.bindString(18, latitude_target)
            statement.bindString(19, longitude_hp)
            statement.bindString(20, latitude_hp)
            statement.bindString(21, kordinatImg1)
            statement.bindString(22, kordinatImg2)
            statement.bindString(23, kordinatImg3)
            statement.bindString(24, kordinatImg4)
            statement.bindString(25, kordinatImg5)
            statement.bindString(26, kordinatImg6)
            statement.bindString(27, elevasi)
            statement.bindString(28, elevasiImg1)
            statement.bindString(29, elevasiImg2)
            statement.bindString(30, elevasiImg3)
            statement.bindString(31, elevasiImg4)
            statement.bindString(32, kedalamanAirTanah)
            statement.bindString(33, genangan)
            statement.bindString(34, banjirBulan)
            statement.bindString(35, banjirLamanya)
            statement.bindString(36, banjirKetinggianAir)
            statement.bindString(37, banjirSumberGenangan)
            statement.bindString(38, elevasiImg5)
            statement.bindString(39, elevasiImg6)
            statement.bindString(40, banjirImg1)
            statement.bindString(41, banjirImg2)
            statement.bindString(42, banjirImg3)
            statement.bindString(43, banjirImg4)
            statement.bindString(44, tutupanLahanJenisTanaman)
            statement.bindString(45, tutupanLahanStatus)
            statement.bindString(46, tutupanLahanNamaPerusahaan)
            statement.bindString(47, tutupanLahanLuas)
            statement.bindString(48, tutupanLahanPenggunaanLahan)
            statement.bindString(49, banjirImg5)
            statement.bindString(50, banjirImg6)
            statement.bindString(51, tutupanLahanImg1)
            statement.bindString(52, tutupanLahanImg2)
            statement.bindString(53, tutupanLahanImg3)
            statement.bindString(54, tutupanLahanImg4)
            statement.bindString(55, flora)
            statement.bindString(56, floraJenis)
            statement.bindString(57, fauna)
            statement.bindString(58, faunaJenis)
            statement.bindString(59, tutupanLahanImg5)
            statement.bindString(60, tutupanLahanImg6)
            statement.bindString(61, floraFaunaImg1)
            statement.bindString(62, floraFaunaImg2)
            statement.bindString(63, floraFaunaImg3)
            statement.bindString(64, floraFaunaImg4)
            statement.bindString(65, drainaseAlami)
            statement.bindString(66, drainaseBuatan)
            statement.bindString(67, drainaseBuatanJenis)
            statement.bindString(68, drainaseBuatanTinggiMukaAir)
            statement.bindString(69, floraFaunaImg5)
            statement.bindString(70, floraFaunaImg6)
            statement.bindString(71, drainaseImg1)
            statement.bindString(72, drainaseImg2)
            statement.bindString(73, drainaseImg3)
            statement.bindString(74, drainaseImg4)
            statement.bindString(75, kualitasAirTanahPh)
            statement.bindString(76, kualitasAirTanahEc)
            statement.bindString(77, kualitasAirTanahTds)
            statement.bindString(78, drainaseImg5)
            statement.bindString(79, drainaseImg6)
            statement.bindString(80, kualitasiAirTanahImg1)
            statement.bindString(81, kualitasiAirTanahImg2)
            statement.bindString(82, kualitasiAirTanahImg3)
            statement.bindString(83, kualitasiAirTanahImg4)
            statement.bindString(84, karakteristikSubstratumTanahNa)
            statement.bindString(85, karakteristikSubstratumTanahNaJenis)
            statement.bindString(86, karakteristikSubstratumTanahLiatPh)
            statement.bindString(87, karakteristikSubstratumTanahLiatEc)
            statement.bindString(88, kualitasiAirTanahImg5)
            statement.bindString(89, kualitasiAirTanahImg6)
            statement.bindString(90, karakteristikSubstratumTanahLiatImg1)
            statement.bindString(91, karakteristikSubstratumTanahLiatImg2)
            statement.bindString(92, karakteristikSubstratumTanahLiatImg3)
            statement.bindString(93, karakteristikSubstratumTanahLiatImg4)
            statement.bindString(94, tipeLuapanKemarau)
            statement.bindString(95, tipeLuapanHujan)
            statement.bindString(96, karakteristikSubstratumTanahLiatImg5)
            statement.bindString(97, karakteristikSubstratumTanahLiatImg6)
            statement.bindString(98, tipeLuapanImg1)
            statement.bindString(99, tipeLuapanImg2)
            statement.bindString(100, tipeLuapanImg3)
            statement.bindString(101, tipeLuapanImg4)
            statement.bindString(102, ketebalanGambutNa)
            statement.bindString(103, ketebalanGambutNaJenis)
            statement.bindString(104, ketebalanGambutTinggi)
            statement.bindString(105, ketebalanGambutJenis)
            statement.bindString(106, tipeLuapanImg5)
            statement.bindString(107, tipeLuapanImg6)
            statement.bindString(108, ketebalanGambutImg1)
            statement.bindString(109, ketebalanGambutImg2)
            statement.bindString(110, ketebalanGambutImg3)
            statement.bindString(111, ketebalanGambutImg4)
            statement.bindString(112, karakteristikSubstratumDibawahLapisanGambutNa)
            statement.bindString(113, karakteristikSubstratumDibawahLapisanGambutNaJenis)
            statement.bindString(114, karakteristikSubstratumDibawahLapisanGambutJenis)
            statement.bindString(115, karakteristikSubstratumDibawahLapisanGambutLainnya)
            statement.bindString(116, ketebalanGambutImg5)
            statement.bindString(117, ketebalanGambutImg6)
            statement.bindString(118, karakteristikSubstratumDibawahLapisanGambutImg1)
            statement.bindString(119, karakteristikSubstratumDibawahLapisanGambutImg2)
            statement.bindString(120, karakteristikSubstratumDibawahLapisanGambutImg3)
            statement.bindString(121, karakteristikSubstratumDibawahLapisanGambutImg4)
            statement.bindString(122, tingkatKerusakanLahanGambutDrainaseBuatan)
            statement.bindString(123, tingkatKerusakanLahanGambutSedimen)
            statement.bindString(124, tingkatKerusakanLahanKondisiTanaman)
            statement.bindString(125, tingkatKerusakanLahanSubsiden)
            statement.bindString(126, tingkatKerusakanLahanKerapatanTajuk)
            statement.bindString(127, karakteristikSubstratumDibawahLapisanGambutImg5)
            statement.bindString(128, karakteristikSubstratumDibawahLapisanGambutImg6)
            statement.bindString(129, tingkatKerusakanLahanImg1)
            statement.bindString(130, tingkatKerusakanLahanImg2)
            statement.bindString(131, tingkatKerusakanLahanImg3)
            statement.bindString(132, tingkatKerusakanLahanImg4)
            statement.bindString(133, kebakaranLahanTahun)
            statement.bindString(134, kebakaranLahanBulan)
            statement.bindString(135, kebakaranLahanTanggal)
            statement.bindString(136, kebakaranLahanLamaKejadian)
            statement.bindString(137, kebakaranLahanLamaKejadianHariMInggu)
            statement.bindString(138, kebakaranLahanUpayaPemadamanSwadayaMasyarakat)
            statement.bindString(139, kebakaranLahanUpayaPemadamanPemerintah)
            statement.bindString(140, hujanTerakhir)
            statement.bindString(141, hujanLamaKejadian)
            statement.bindString(142, hujanLamaKejadianJamHari)
            statement.bindString(143, hujanIntensitas)
            statement.bindString(144, tingkatKerusakanLahanImg5)
            statement.bindString(145, tingkatKerusakanLahanImg6)
            statement.bindString(146, kebakaranHujanImg1)
            statement.bindString(147, kebakaranHujanImg2)
            statement.bindString(148, kebakaranHujanImg3)
            statement.bindString(149, kebakaranHujanImg4)
            statement.bindString(150, porositasKelengasanNa)
            statement.bindString(151, porositasKelengasanNaJenis)
            statement.bindString(152, porositasKelengasan1)
            statement.bindString(153, porositasKelengasan2)
            statement.bindString(154, porositasKelengasanKeterangan)
            statement.bindString(155, kebakaranHujanImg5)
            statement.bindString(156, kebakaranHujanImg6)
            statement.bindString(157, porositasKelengasanImg1)
            statement.bindString(158, porositasKelengasanImg2)
            statement.bindString(159, porositasKelengasanImg3)
            statement.bindString(160, porositasKelengasanImg4)
            statement.bindString(161, keterangan)
            statement.bindString(162, porositasKelengasanImg5)
            statement.bindString(163, porositasKelengasanImg6)
            statement.bindString(164, sketsaLokasi)
            statement.bindString(165, imgBatasBarat)
            statement.bindString(166, imgBatasTimur)
            statement.bindString(167, imgBatasSelatan)
            statement.bindString(168, imgBatasUtara)
            statement.bindString(169, status)
            statement.bindString(170, ketebalanGambut)
            statement.bindString(171, karakteristikSubstratumDibawahLapisanGambutJenis1)
            statement.bindString(172, karakteristikSubstratumDibawahLapisanGambutJenis2)
            statement.bindString(173, karakteristikSubstratumDibawahLapisanGambutJenis3)
            statement.bindString(174, karakteristikSubstratumDibawahLapisanGambutJenis4)
            statement.bindString(175, karakteristikSubstratumDibawahLapisanGambutJenis5)

            statement.bindString(176, banjirSumberGenangan1)
            statement.bindString(177, banjirSumberGenangan2)
            statement.bindString(178, banjirSumberGenangan3)
            statement.bindString(179, banjirSumberGenangan4)
            statement.bindString(180, banjirSumberGenanganLainnya)

            statement.bindString(181, tingkatKerusakanLahanKondisiTanaman1)
            statement.bindString(182, tingkatKerusakanLahanKondisiTanaman2)
            statement.bindString(183, tingkatKerusakanLahanKondisiTanaman3)
            statement.bindString(184, tingkatKerusakanLahanKondisiTanaman4)

            statement.bindString(185,kualitasAirTanahPhAt)
            statement.bindString(186,kualitasAirTanahPhAs)
            statement.bindString(187,kualitasAirTanahEcAt)
            statement.bindString(188,kualitasAirTanahEcAs)
            statement.bindString(189,kualitasAirTanahTdsAt)
            statement.bindString(190,kualitasAirTanahTdsAs)
            statement.bindString(191,dusun)


            statement.execute()
        }

        db.setTransactionSuccessful()
        db.endTransaction()

    }

    fun getForUpload(): ArrayList<HashMap<String, String>> {
        var result = ArrayList<HashMap<String, String>>()
        val selectQuery =
            "SELECT id FROM $TABLE_NAME WHERE status='1' OR status='3' ORDER BY id DESC"
        val dbHelper = DatabaseHelper(context)
        val db = dbHelper.writableDatabase

        val cursor = db.rawQuery(selectQuery, null)
        if (cursor.moveToFirst()) {
            do {
                val map = HashMap<String, String>()
                map["id"] = cursor.getString(0)
                result.add(map)
            } while (cursor.moveToNext())
        }
        db.close()
        return result
    }

    fun getUnsync(): Int {
        var result = 0
        val selectQuery =
            "SELECT nomorBoring FROM $TABLE_NAME WHERE status='1' ORDER BY nomorBoring DESC"

        val dbHelper = DatabaseHelper(context)
        val db = dbHelper.writableDatabase
        val cursor = db.rawQuery(selectQuery, null)
        result = cursor.count
        db.close()
        return result
    }

    fun countData(): Int {
        var result = 0
        val selectQuery = "SELECT id FROM $TABLE_NAME ORDER BY id DESC"

        val dbHelper = DatabaseHelper(context)
        val db = dbHelper.writableDatabase
        val cursor = db.rawQuery(selectQuery, null)
        result = cursor.count
        db.close()
        return result
    }

    fun countUnsync(): Int {
        var result = 0
        val selectQuery =
            "SELECT id FROM $TABLE_NAME WHERE status='1' ORDER BY id DESC"

        val dbHelper = DatabaseHelper(context)
        val db = dbHelper.writableDatabase
        val cursor = db.rawQuery(selectQuery, null)
        result = cursor.count
        db.close()
        return result
    }

    fun countUpload(): Int {
        var result = 0
        val selectQuery =
            "SELECT id FROM $TABLE_NAME WHERE status='2' ORDER BY id DESC"

        val dbHelper = DatabaseHelper(context)
        val db = dbHelper.writableDatabase
        val cursor = db.rawQuery(selectQuery, null)
        result = cursor.count
        db.close()
        return result
    }



}