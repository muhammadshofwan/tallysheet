package synergy.project.tallysheet.model

import android.content.ContentValues
import android.content.Context
import java.text.SimpleDateFormat
import java.util.*

class Info(val context:Context) {
    val TABLE_NAME = "info"
    var kode_batch = ""
    var nama = ""
    var last_sync = ""
    var error_sync = ""
    var success_sync = ""
    var failed_sync = ""

    fun get_kode_batch() :String{
        var result = ""
        val dbHelper = DatabaseHelper(context)
        val db = dbHelper.writableDatabase
        val query = ""

        val cursor = db.query(TABLE_NAME, arrayOf("kode_batch"),null,null, null, null, null, null)
        if (cursor != null && cursor.moveToFirst()) {
            result = cursor.getString(0)
        }else{
            result = ""
        }
        db.close()
        return  result
    }

    fun get_nama() :String{
        var result = ""
        val dbHelper = DatabaseHelper(context)
        val db = dbHelper.writableDatabase
        val query = ""

        val cursor = db.query(TABLE_NAME, arrayOf("nama"),null,null, null, null, null, null)
        if (cursor != null && cursor.moveToFirst()) {
            result = cursor.getString(0)
        }else{
            result = ""
        }
        db.close()
        return  result
    }
    fun get_error_sync() :String{
        var result = ""
        val dbHelper = DatabaseHelper(context)
        val db = dbHelper.writableDatabase
        val query = ""

        val cursor = db.query(TABLE_NAME, arrayOf("error_sync"),null,null, null, null, null, null)
        if (cursor != null && cursor.moveToFirst()) {
            result = cursor.getString(0)
        }else{
            result = ""
        }
        db.close()
        return  result
    }

    fun get_success_sync() :String{
        var result = ""
        val dbHelper = DatabaseHelper(context)
        val db = dbHelper.writableDatabase
        val query = ""

        val cursor = db.query(TABLE_NAME, arrayOf("success_sync"),null,null, null, null, null, null)
        if (cursor != null && cursor.moveToFirst()) {
            result = cursor.getString(0)
        }else{
            result = ""
        }
        db.close()
        return  result
    }

    fun get_failed_sync() :String{
        var result = ""
        val dbHelper = DatabaseHelper(context)
        val db = dbHelper.writableDatabase
        val query = ""

        val cursor = db.query(TABLE_NAME, arrayOf("failed_sync"),null,null, null, null, null, null)
        if (cursor != null && cursor.moveToFirst()) {
            result = cursor.getString(0)
        }else{
            result = ""
        }
        db.close()
        return  result
    }

    fun get_last_sync() :String{
        var result = ""
        val dbHelper = DatabaseHelper(context)
        val db = dbHelper.writableDatabase
        val query = ""

        val cursor = db.query(TABLE_NAME, arrayOf("last_sync"),null,null, null, null, null, null)
        if (cursor != null && cursor.moveToFirst()) {
            val dateFormat =
                SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val date = Date()
            val dateStart = cursor.getString(0) // 2014-12-29 12:10:57

            val dateStop = dateFormat.format(date)

            //Custom date format
            val format =
                SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

            var d1: Date? = null
            var d2: Date? = null
                d1 = format.parse(dateStart)
                d2 = format.parse(dateStop)

            // Get msec from each, and subtract.

            // Get msec from each, and subtract.
            val diff = d2.time - d1.time
            val diffSeconds = diff / 1000
            val diffMinutes = diff / (60 * 1000)
            val diffHours = diff / (60 * 60 * 1000)
            val diffDays = diff / (60 * 60 * 1000 * 24)
            val diffMonths = diff / (60 * 60 * 1000 * 24 * 30)

            val hasil: String

            if (diffSeconds < 60) {
                hasil = "$diffSeconds detik lalu"
            } else if (diffMinutes < 60) {
                hasil = "$diffMinutes menit lalu"
            } else if (diffHours < 24) {
                hasil = "$diffHours jam lalu"
            } else if (diffDays < 30) {
                hasil = "$diffDays hari lalu"
            } else {
                val dtf =
                    SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
                if (d2.year == d1.year) {
                    val formatter = SimpleDateFormat("MMM dd")
                    val tanggal: Date
                    tanggal = dtf.parse(dateStart)
                    hasil = formatter.format(tanggal)
                } else {
                    val formatter =
                        SimpleDateFormat("yyyy-MM-dd")
                    val tanggal: Date
                    tanggal = dtf.parse(dateStart)
                    hasil = formatter.format(tanggal)
                }
            }

            result = hasil


        }else{
            result = ""
        }
        db.close()
        return  result
    }

    fun setStatus(){
        val dbHelper = DatabaseHelper(context)
        val db = dbHelper.writableDatabase
        val query = ""

        val cursor = db.query(TABLE_NAME,
            arrayOf("kode_batch,nama,last_sync"),null,null, null, null, null, null)
        val values = ContentValues()
        if (this.kode_batch != null) {
            if (this.kode_batch != "") {
                values.put("kode_batch", this.kode_batch)
            }
        }

        if (this.nama != null) {
            if (this.nama != "") {
                values.put("nama", this.nama)
            }
        }

        if (this.last_sync != null) {
            if (this.last_sync != "") {
                values.put("last_sync", this.last_sync)
            }
        }

        if (this.error_sync != null) {
            //if(!this.getError_sync().equals("")){
            values.put("error_sync", this.error_sync)
            //}
        }

        if (this.success_sync != null) {
            if (this.success_sync != "") {
                values.put("success_sync", this.success_sync)
            }
        }

        if (this.failed_sync != null) {
            if (this.failed_sync != "") {
                values.put("failed_sync", this.failed_sync)
            }
        }


        if (cursor != null && cursor.count > 0) {
            // update
            db.update(TABLE_NAME, values, null, null)
        } else {
            // add
            db.insert(TABLE_NAME, null, values)
        }

        db.close()


    }
}