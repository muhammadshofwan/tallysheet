package synergy.project.tallysheet.model

data class MainMenu(val title:String,val subtitle:String,val image:Int)