package synergy.project.tallysheet.model

import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DatabaseHelper(
    context: Context?
) : SQLiteOpenHelper(context, DATABASE_NAME, null,7) {


    companion object{
        val DATABASE_NAME = "tallySheet.db"
    }
    private val CREATE_TABLE_SURVEY = "CREATE TABLE survey (" +
            " id TEXT  DEFAULT '' , " +
            " nama_khg TEXT  DEFAULT '' , " +
            " nomorBoring TEXT  DEFAULT '' , " +
            " tanggal TEXT  DEFAULT '' , " +
            " surveyorId TEXT  DEFAULT '' , " +
            " reguId TEXT  DEFAULT '' , " +
            " provinsi TEXT  DEFAULT '' , " +
            " kota_kabupaten TEXT  DEFAULT '' , " +
            " kecamatan TEXT  DEFAULT '' , " +
            " kelurahan TEXT  DEFAULT '' , " +
            " jenis_tanah TEXT  DEFAULT '' , " +
            " titik_p10 TEXT  DEFAULT '' , " +
            " sample_p10 TEXT  DEFAULT '' , " +
            " jenis_sample TEXT  DEFAULT '' , " +
            " longitude TEXT  DEFAULT '' , " +
            " latitude TEXT  DEFAULT '' , " +
            " longitude_target TEXT  DEFAULT '' , " +
            " latitude_target TEXT  DEFAULT '' , " +
            " longitude_hp TEXT  DEFAULT '' , " +
            " latitude_hp TEXT  DEFAULT '' , " +
            " kordinatImg1 TEXT  DEFAULT '' , " +
            " kordinatImg2 TEXT  DEFAULT '' , " +
            " kordinatImg3 TEXT  DEFAULT '' , " +
            " kordinatImg4 TEXT  DEFAULT '' , " +
            " kordinatImg5 TEXT  DEFAULT '' , " +
            " kordinatImg6 TEXT  DEFAULT '' , " +
            " elevasi TEXT  DEFAULT '' , " +
            " elevasiImg1 TEXT  DEFAULT '' , " +
            " elevasiImg2 TEXT  DEFAULT '' , " +
            " elevasiImg3 TEXT  DEFAULT '' , " +
            " elevasiImg4 TEXT  DEFAULT '' , " +
            " kedalamanAirTanah TEXT  DEFAULT '' , " +
            " genangan TEXT  DEFAULT '' , " +
            " banjirBulan TEXT  DEFAULT '' , " +
            " banjirLamanya TEXT  DEFAULT '' , " +
            " banjirKetinggianAir TEXT  DEFAULT '' , " +
            " banjirSumberGenangan TEXT  DEFAULT '' , " +
            " elevasiImg5 TEXT  DEFAULT '' , " +
            " elevasiImg6 TEXT  DEFAULT '' , " +
            " banjirImg1 TEXT  DEFAULT '' , " +
            " banjirImg2 TEXT  DEFAULT '' , " +
            " banjirImg3 TEXT  DEFAULT '' , " +
            " banjirImg4 TEXT  DEFAULT '' , " +
            " tutupanLahanJenisTanaman TEXT  DEFAULT '' , " +
            " tutupanLahanStatus TEXT  DEFAULT '' , " +
            " tutupanLahanNamaPerusahaan TEXT  DEFAULT '' , " +
            " tutupanLahanLuas TEXT  DEFAULT '' , " +
            " tutupanLahanPenggunaanLahan TEXT  DEFAULT '' , " +
            " banjirImg5 TEXT  DEFAULT '' , " +
            " banjirImg6 TEXT  DEFAULT '' , " +
            " tutupanLahanImg1 TEXT  DEFAULT '' , " +
            " tutupanLahanImg2 TEXT  DEFAULT '' , " +
            " tutupanLahanImg3 TEXT  DEFAULT '' , " +
            " tutupanLahanImg4 TEXT  DEFAULT '' , " +
            " flora TEXT  DEFAULT '' , " +
            " floraJenis TEXT  DEFAULT '' , " +
            " fauna TEXT  DEFAULT '' , " +
            " faunaJenis TEXT  DEFAULT '' , " +
            " tutupanLahanImg5 TEXT  DEFAULT '' , " +
            " tutupanLahanImg6 TEXT  DEFAULT '' , " +
            " floraFaunaImg1 TEXT  DEFAULT '' , " +
            " floraFaunaImg2 TEXT  DEFAULT '' , " +
            " floraFaunaImg3 TEXT  DEFAULT '' , " +
            " floraFaunaImg4 TEXT  DEFAULT '' , " +
            " drainaseAlami TEXT  DEFAULT '' , " +
            " drainaseBuatan TEXT  DEFAULT '' , " +
            " drainaseBuatanJenis TEXT  DEFAULT '' , " +
            " drainaseBuatanTinggiMukaAir TEXT  DEFAULT '' , " +
            " floraFaunaImg5 TEXT  DEFAULT '' , " +
            " floraFaunaImg6 TEXT  DEFAULT '' , " +
            " drainaseImg1 TEXT  DEFAULT '' , " +
            " drainaseImg2 TEXT  DEFAULT '' , " +
            " drainaseImg3 TEXT  DEFAULT '' , " +
            " drainaseImg4 TEXT  DEFAULT '' , " +
            " kualitasAirTanahPh TEXT  DEFAULT '' , " +
            " kualitasAirTanahEc TEXT  DEFAULT '' , " +
            " kualitasAirTanahTds TEXT  DEFAULT '' , " +
            " drainaseImg5 TEXT  DEFAULT '' , " +
            " drainaseImg6 TEXT  DEFAULT '' , " +
            " kualitasiAirTanahImg1 TEXT  DEFAULT '' , " +
            " kualitasiAirTanahImg2 TEXT  DEFAULT '' , " +
            " kualitasiAirTanahImg3 TEXT  DEFAULT '' , " +
            " kualitasiAirTanahImg4 TEXT  DEFAULT '' , " +
            " karakteristikSubstratumTanahNa TEXT  DEFAULT '' , " +
            " karakteristikSubstratumTanahNaJenis TEXT  DEFAULT '' , " +
            " karakteristikSubstratumTanahLiatPh TEXT  DEFAULT '' , " +
            " karakteristikSubstratumTanahLiatEc TEXT  DEFAULT '' , " +
            " kualitasiAirTanahImg5 TEXT  DEFAULT '' , " +
            " kualitasiAirTanahImg6 TEXT  DEFAULT '' , " +
            " karakteristikSubstratumTanahLiatImg1 TEXT  DEFAULT '' , " +
            " karakteristikSubstratumTanahLiatImg2 TEXT  DEFAULT '' , " +
            " karakteristikSubstratumTanahLiatImg3 TEXT  DEFAULT '' , " +
            " karakteristikSubstratumTanahLiatImg4 TEXT  DEFAULT '' , " +
            " tipeLuapanKemarau TEXT  DEFAULT '' , " +
            " tipeLuapanHujan TEXT  DEFAULT '' , " +
            " karakteristikSubstratumTanahLiatImg5 TEXT  DEFAULT '' , " +
            " karakteristikSubstratumTanahLiatImg6 TEXT  DEFAULT '' , " +
            " tipeLuapanImg1 TEXT  DEFAULT '' , " +
            " tipeLuapanImg2 TEXT  DEFAULT '' , " +
            " tipeLuapanImg3 TEXT  DEFAULT '' , " +
            " tipeLuapanImg4 TEXT  DEFAULT '' , " +
            " ketebalanGambutNa TEXT  DEFAULT '' , " +
            " ketebalanGambutNaJenis TEXT  DEFAULT '' , " +
            " ketebalanGambutTinggi TEXT  DEFAULT '' , " +
            " ketebalanGambutJenis TEXT  DEFAULT '' , " +
            " tipeLuapanImg5 TEXT  DEFAULT '' , " +
            " tipeLuapanImg6 TEXT  DEFAULT '' , " +
            " ketebalanGambutImg1 TEXT  DEFAULT '' , " +
            " ketebalanGambutImg2 TEXT  DEFAULT '' , " +
            " ketebalanGambutImg3 TEXT  DEFAULT '' , " +
            " ketebalanGambutImg4 TEXT  DEFAULT '' , " +
            " karakteristikSubstratumDibawahLapisanGambutNa TEXT  DEFAULT '' , " +
            " karakteristikSubstratumDibawahLapisanGambutNaJenis TEXT  DEFAULT '' , " +
            " karakteristikSubstratumDibawahLapisanGambutJenis TEXT  DEFAULT '' , " +
            " karakteristikSubstratumDibawahLapisanGambutLainnya TEXT  DEFAULT '' , " +
            " ketebalanGambutImg5 TEXT  DEFAULT '' , " +
            " ketebalanGambutImg6 TEXT  DEFAULT '' , " +
            " karakteristikSubstratumDibawahLapisanGambutImg1 TEXT  DEFAULT '' , " +
            " karakteristikSubstratumDibawahLapisanGambutImg2 TEXT  DEFAULT '' , " +
            " karakteristikSubstratumDibawahLapisanGambutImg3 TEXT  DEFAULT '' , " +
            " karakteristikSubstratumDibawahLapisanGambutImg4 TEXT  DEFAULT '' , " +
            " tingkatKerusakanLahanGambutDrainaseBuatan TEXT  DEFAULT '' , " +
            " tingkatKerusakanLahanGambutSedimen TEXT  DEFAULT '' , " +
            " tingkatKerusakanLahanKondisiTanaman TEXT  DEFAULT '' , " +
            " tingkatKerusakanLahanSubsiden TEXT  DEFAULT '' , " +
            " tingkatKerusakanLahanKerapatanTajuk TEXT  DEFAULT '' , " +
            " karakteristikSubstratumDibawahLapisanGambutImg5 TEXT  DEFAULT '' , " +
            " karakteristikSubstratumDibawahLapisanGambutImg6 TEXT  DEFAULT '' , " +
            " tingkatKerusakanLahanImg1 TEXT  DEFAULT '' , " +
            " tingkatKerusakanLahanImg2 TEXT  DEFAULT '' , " +
            " tingkatKerusakanLahanImg3 TEXT  DEFAULT '' , " +
            " tingkatKerusakanLahanImg4 TEXT  DEFAULT '' , " +
            " kebakaranLahanTahun TEXT  DEFAULT '' , " +
            " kebakaranLahanBulan TEXT  DEFAULT '' , " +
            " kebakaranLahanTanggal TEXT  DEFAULT '' , " +
            " kebakaranLahanLamaKejadian TEXT  DEFAULT '' , " +
            " kebakaranLahanLamaKejadianHariMInggu TEXT  DEFAULT '' , " +
            " kebakaranLahanUpayaPemadamanSwadayaMasyarakat TEXT  DEFAULT '' , " +
            " kebakaranLahanUpayaPemadamanPemerintah TEXT  DEFAULT '' , " +
            " hujanTerakhir TEXT  DEFAULT '' , " +
            " hujanLamaKejadian TEXT  DEFAULT '' , " +
            " hujanLamaKejadianJamHari TEXT  DEFAULT '' , " +
            " hujanIntensitas TEXT  DEFAULT '' , " +
            " tingkatKerusakanLahanImg5 TEXT  DEFAULT '' , " +
            " tingkatKerusakanLahanImg6 TEXT  DEFAULT '' , " +
            " kebakaranHujanImg1 TEXT  DEFAULT '' , " +
            " kebakaranHujanImg2 TEXT  DEFAULT '' , " +
            " kebakaranHujanImg3 TEXT  DEFAULT '' , " +
            " kebakaranHujanImg4 TEXT  DEFAULT '' , " +
            " porositasKelengasanNa TEXT  DEFAULT '' , " +
            " porositasKelengasanNaJenis TEXT  DEFAULT '' , " +
            " porositasKelengasan1 TEXT  DEFAULT '' , " +
            " porositasKelengasan2 TEXT  DEFAULT '' , " +
            " porositasKelengasanKeterangan TEXT  DEFAULT '' , " +
            " kebakaranHujanImg5 TEXT  DEFAULT '' , " +
            " kebakaranHujanImg6 TEXT  DEFAULT '' , " +
            " porositasKelengasanImg1 TEXT  DEFAULT '' , " +
            " porositasKelengasanImg2 TEXT  DEFAULT '' , " +
            " porositasKelengasanImg3 TEXT  DEFAULT '' , " +
            " porositasKelengasanImg4 TEXT  DEFAULT '' , " +
            " keterangan TEXT  DEFAULT '' , " +
            " porositasKelengasanImg5 TEXT  DEFAULT '' , " +
            " porositasKelengasanImg6 TEXT  DEFAULT '' , " +
            " sketsaLokasi TEXT  DEFAULT '' , " +
            " imgBatasBarat TEXT  DEFAULT '' , " +
            " imgBatasTimur TEXT  DEFAULT '' , " +
            " imgBatasSelatan TEXT  DEFAULT '' , " +
            " imgBatasUtara TEXT  DEFAULT '' , " +
            " status INT   ," +
            " ketebalanGambut TEXT,   " +
            " karakteristikSubstratumDibawahLapisanGambutJenis1 TEXT,   " +
            " karakteristikSubstratumDibawahLapisanGambutJenis2 TEXT,   " +
            " karakteristikSubstratumDibawahLapisanGambutJenis3 TEXT,   " +
            " karakteristikSubstratumDibawahLapisanGambutJenis4 TEXT,   " +
            " karakteristikSubstratumDibawahLapisanGambutJenis5 TEXT,   " +
            " banjirSumberGenangan1 TEXT  DEFAULT '' , " +
            " banjirSumberGenangan2 TEXT  DEFAULT '' , " +
            " banjirSumberGenangan3 TEXT  DEFAULT '' , " +
            " banjirSumberGenangan4 TEXT  DEFAULT '' , " +
            " banjirSumberGenanganLainnya TEXT  DEFAULT '' , " +
            " tingkatKerusakanLahanKondisiTanaman1 TEXT  DEFAULT '' , " +
            " tingkatKerusakanLahanKondisiTanaman2 TEXT  DEFAULT '' , " +
            " tingkatKerusakanLahanKondisiTanaman3 TEXT  DEFAULT '' , " +
            " tingkatKerusakanLahanKondisiTanaman4 TEXT  ,  " +
            " kualitasAirTanahPhAt TEXT  DEFAULT '' , " +
            " kualitasAirTanahPhAs TEXT  DEFAULT '' , " +
            " kualitasAirTanahEcAt TEXT  DEFAULT '' , " +
            " kualitasAirTanahEcAs TEXT  DEFAULT '' , " +
            " kualitasAirTanahTdsAt TEXT  DEFAULT '' , " +
            " kualitasAirTanahTdsAs TEXT DEFAULT '' , " +
            " dusun TEXT DEFAULT '' " +
            ");"

    private val CREATE_TABLE_LAHAN = "CREATE TABLE lahan (" +
            " id INTEGER PRIMARY KEY AUTOINCREMENT," +
            " nomorBoring TEXT, " +
            " kedalaman INTEGER, " +
            " specialFeature TEXT " +
            ");"
    private val CREATE_TABLE_INFO = "CREATE TABLE info (" +
            " kode_batch TEXT," +
            " nama TEXT, " +
            " last_sync TEXT, " +
            " error_sync TEXT, " +
            " success_sync TEXT, " +
            " failed_sync TEXT " +
            ");"


    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(CREATE_TABLE_SURVEY)
     //   db?.execSQL(CREATE_TABLE_LAHAN)
        db?.execSQL(CREATE_TABLE_INFO)

    }



    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        if(oldVersion == 1 && newVersion == 2){
            db?.execSQL("ALTER TABLE survey ADD COLUMN kordinatImg5 TEXT DEFAULT '';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN kordinatImg6 TEXT DEFAULT '';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN elevasiImg5 TEXT DEFAULT '';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN elevasiImg6 TEXT DEFAULT '';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN banjirImg5 TEXT DEFAULT '';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN banjirImg6 TEXT DEFAULT '';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN tutupanLahanImg5 TEXT DEFAULT '';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN tutupanLahanImg6 TEXT DEFAULT '';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN floraFaunaImg5 TEXT DEFAULT '';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN floraFaunaImg6 TEXT DEFAULT '';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN drainaseImg5 TEXT DEFAULT '';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN drainaseImg6 TEXT DEFAULT '';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN kualitasiAirTanahImg5 TEXT DEFAULT '';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN kualitasiAirTanahImg6 TEXT DEFAULT '';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN karakteristikSubstratumTanahLiatImg5 TEXT DEFAULT '';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN karakteristikSubstratumTanahLiatImg6 TEXT DEFAULT '';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN tipeLuapanImg5 TEXT DEFAULT '';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN tipeLuapanImg6 TEXT DEFAULT '';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN ketebalanGambutImg5 TEXT DEFAULT '';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN ketebalanGambutImg6 TEXT DEFAULT '';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN karakteristikSubstratumDibawahLapisanGambutImg5 TEXT DEFAULT '';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN karakteristikSubstratumDibawahLapisanGambutImg6 TEXT DEFAULT '';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN tingkatKerusakanLahanImg5 TEXT DEFAULT '';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN tingkatKerusakanLahanImg6 TEXT DEFAULT '';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN kebakaranHujanImg5 TEXT DEFAULT '';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN kebakaranHujanImg6 TEXT DEFAULT '';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN porositasKelengasanImg5 TEXT DEFAULT '';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN porositasKelengasanImg6 TEXT DEFAULT '';")
        }else if(oldVersion==2 && newVersion==3){
            db?.execSQL("ALTER TABLE survey ADD COLUMN reguId TEXT DEFAULT '';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN ketebalanGambut TEXT DEFAULT '';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN karakteristikSubstratumDibawahLapisanGambutJenis1 TEXT default '0';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN karakteristikSubstratumDibawahLapisanGambutJenis2 TEXT default '0';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN karakteristikSubstratumDibawahLapisanGambutJenis3 TEXT default '0';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN karakteristikSubstratumDibawahLapisanGambutJenis4 TEXT default '0';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN karakteristikSubstratumDibawahLapisanGambutJenis5 TEXT default '0';")
        }else if(newVersion==5){

            db?.execSQL("ALTER TABLE survey ADD COLUMN banjirSumberGenangan1 TEXT default '0';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN banjirSumberGenangan2 TEXT default '0';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN banjirSumberGenangan3 TEXT default '0';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN banjirSumberGenangan4 TEXT default '0';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN banjirSumberGenanganLainnya TEXT default '0';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN tingkatKerusakanLahanKondisiTanaman1 TEXT default '0';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN tingkatKerusakanLahanKondisiTanaman2 TEXT default '0';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN tingkatKerusakanLahanKondisiTanaman3 TEXT default '0';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN tingkatKerusakanLahanKondisiTanaman4 TEXT default '0';")


            db?.execSQL("ALTER TABLE survey ADD COLUMN kualitasAirTanahPhAt TEXT default '0';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN kualitasAirTanahPhAs TEXT default '0';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN kualitasAirTanahEcAt TEXT default '0';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN kualitasAirTanahEcAs TEXT default '0';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN kualitasAirTanahTdsAt TEXT default '0';")
            db?.execSQL("ALTER TABLE survey ADD COLUMN kualitasAirTanahTdsAs TEXT default '0';")


        }else if (newVersion==6){
            if (!isFieldExist(db!!,"survey","kordinatImg5")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN kordinatImg5 TEXT DEFAULT '';")
            }

            if (!isFieldExist(db!!,"survey","kordinatImg6")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN kordinatImg6 TEXT DEFAULT '';")
            }

            if (!isFieldExist(db!!,"survey","elevasiImg5")){
                 db?.execSQL("ALTER TABLE survey ADD COLUMN elevasiImg5 TEXT DEFAULT '';")
            }

            if (!isFieldExist(db!!,"survey","elevasiImg6")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN elevasiImg6 TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","banjirImg5")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN banjirImg5 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","banjirImg6")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN banjirImg6 TEXT DEFAULT '';")

            }

            if (!isFieldExist(db!!,"survey","tutupanLahanImg5")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN tutupanLahanImg5 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","tutupanLahanImg6")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN tutupanLahanImg6 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","floraFaunaImg5")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN floraFaunaImg5 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","floraFaunaImg6")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN floraFaunaImg6 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","porositasKelengasanImg6")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN porositasKelengasanImg6 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","drainaseImg6")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN drainaseImg6 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","kualitasiAirTanahImg5")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN kualitasiAirTanahImg5 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","drainaseImg5")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN drainaseImg5 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","kualitasiAirTanahImg6")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN kualitasiAirTanahImg6 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","karakteristikSubstratumTanahLiatImg5")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN karakteristikSubstratumTanahLiatImg5 TEXT DEFAULT '';")

            }

            if (!isFieldExist(db!!,"survey","karakteristikSubstratumTanahLiatImg6")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN karakteristikSubstratumTanahLiatImg6 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","tipeLuapanImg5")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN tipeLuapanImg5 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","tipeLuapanImg6")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN tipeLuapanImg6 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","ketebalanGambutImg5")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN ketebalanGambutImg5 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","ketebalanGambutImg6")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN ketebalanGambutImg6 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","karakteristikSubstratumDibawahLapisanGambutImg5")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN karakteristikSubstratumDibawahLapisanGambutImg5 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","karakteristikSubstratumDibawahLapisanGambutImg6")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN karakteristikSubstratumDibawahLapisanGambutImg6 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","tingkatKerusakanLahanImg5")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN tingkatKerusakanLahanImg5 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","tingkatKerusakanLahanImg6")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN tingkatKerusakanLahanImg6 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","kebakaranHujanImg5")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN kebakaranHujanImg5 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","kebakaranHujanImg6")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN kebakaranHujanImg6 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","porositasKelengasanImg5")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN porositasKelengasanImg5 TEXT DEFAULT '';")
            }

            if (!isFieldExist(db!!,"survey","reguId")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN reguId TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","ketebalanGambut")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN ketebalanGambut TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","karakteristikSubstratumDibawahLapisanGambutJenis1")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN karakteristikSubstratumDibawahLapisanGambutJenis1 TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","karakteristikSubstratumDibawahLapisanGambutJenis2")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN karakteristikSubstratumDibawahLapisanGambutJenis2 TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","karakteristikSubstratumDibawahLapisanGambutJenis3")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN karakteristikSubstratumDibawahLapisanGambutJenis3 TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","karakteristikSubstratumDibawahLapisanGambutJenis4")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN karakteristikSubstratumDibawahLapisanGambutJenis4 TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","karakteristikSubstratumDibawahLapisanGambutJenis5")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN karakteristikSubstratumDibawahLapisanGambutJenis5 TEXT DEFAULT '';")
            }


            if (!isFieldExist(db!!,"survey","banjirSumberGenangan1")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN banjirSumberGenangan1 TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","banjirSumberGenangan2")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN banjirSumberGenangan2 TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","banjirSumberGenangan3")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN banjirSumberGenangan3 TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","banjirSumberGenangan4")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN banjirSumberGenangan4 TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","banjirSumberGenanganLainnya")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN banjirSumberGenanganLainnya TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","tingkatKerusakanLahanKondisiTanaman1")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN tingkatKerusakanLahanKondisiTanaman1 TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","tingkatKerusakanLahanKondisiTanaman2")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN tingkatKerusakanLahanKondisiTanaman2 TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","tingkatKerusakanLahanKondisiTanaman3")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN tingkatKerusakanLahanKondisiTanaman3 TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","tingkatKerusakanLahanKondisiTanaman4")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN tingkatKerusakanLahanKondisiTanaman4 TEXT DEFAULT '';")
            }

            if (!isFieldExist(db!!,"survey","kualitasAirTanahPhAt")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN kualitasAirTanahPhAt TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","kualitasAirTanahPhAs")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN kualitasAirTanahPhAs TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","kualitasAirTanahEcAt")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN kualitasAirTanahEcAt TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","kualitasAirTanahEcAs")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN kualitasAirTanahEcAs TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","kualitasAirTanahTdsAt")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN kualitasAirTanahTdsAt TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","kualitasAirTanahTdsAs")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN kualitasAirTanahTdsAs TEXT DEFAULT '';")
            }


        }else if(newVersion==7){
            if (!isFieldExist(db!!,"survey","kordinatImg5")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN kordinatImg5 TEXT DEFAULT '';")
            }

            if (!isFieldExist(db!!,"survey","kordinatImg6")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN kordinatImg6 TEXT DEFAULT '';")
            }

            if (!isFieldExist(db!!,"survey","elevasiImg5")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN elevasiImg5 TEXT DEFAULT '';")
            }

            if (!isFieldExist(db!!,"survey","elevasiImg6")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN elevasiImg6 TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","banjirImg5")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN banjirImg5 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","banjirImg6")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN banjirImg6 TEXT DEFAULT '';")

            }

            if (!isFieldExist(db!!,"survey","tutupanLahanImg5")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN tutupanLahanImg5 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","tutupanLahanImg6")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN tutupanLahanImg6 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","floraFaunaImg5")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN floraFaunaImg5 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","floraFaunaImg6")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN floraFaunaImg6 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","porositasKelengasanImg6")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN porositasKelengasanImg6 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","drainaseImg6")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN drainaseImg6 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","kualitasiAirTanahImg5")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN kualitasiAirTanahImg5 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","drainaseImg5")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN drainaseImg5 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","kualitasiAirTanahImg6")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN kualitasiAirTanahImg6 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","karakteristikSubstratumTanahLiatImg5")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN karakteristikSubstratumTanahLiatImg5 TEXT DEFAULT '';")

            }

            if (!isFieldExist(db!!,"survey","karakteristikSubstratumTanahLiatImg6")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN karakteristikSubstratumTanahLiatImg6 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","tipeLuapanImg5")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN tipeLuapanImg5 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","tipeLuapanImg6")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN tipeLuapanImg6 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","ketebalanGambutImg5")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN ketebalanGambutImg5 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","ketebalanGambutImg6")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN ketebalanGambutImg6 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","karakteristikSubstratumDibawahLapisanGambutImg5")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN karakteristikSubstratumDibawahLapisanGambutImg5 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","karakteristikSubstratumDibawahLapisanGambutImg6")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN karakteristikSubstratumDibawahLapisanGambutImg6 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","tingkatKerusakanLahanImg5")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN tingkatKerusakanLahanImg5 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","tingkatKerusakanLahanImg6")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN tingkatKerusakanLahanImg6 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","kebakaranHujanImg5")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN kebakaranHujanImg5 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","kebakaranHujanImg6")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN kebakaranHujanImg6 TEXT DEFAULT '';")

            }
            if (!isFieldExist(db!!,"survey","porositasKelengasanImg5")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN porositasKelengasanImg5 TEXT DEFAULT '';")
            }

            if (!isFieldExist(db!!,"survey","reguId")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN reguId TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","ketebalanGambut")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN ketebalanGambut TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","karakteristikSubstratumDibawahLapisanGambutJenis1")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN karakteristikSubstratumDibawahLapisanGambutJenis1 TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","karakteristikSubstratumDibawahLapisanGambutJenis2")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN karakteristikSubstratumDibawahLapisanGambutJenis2 TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","karakteristikSubstratumDibawahLapisanGambutJenis3")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN karakteristikSubstratumDibawahLapisanGambutJenis3 TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","karakteristikSubstratumDibawahLapisanGambutJenis4")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN karakteristikSubstratumDibawahLapisanGambutJenis4 TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","karakteristikSubstratumDibawahLapisanGambutJenis5")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN karakteristikSubstratumDibawahLapisanGambutJenis5 TEXT DEFAULT '';")
            }


            if (!isFieldExist(db!!,"survey","banjirSumberGenangan1")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN banjirSumberGenangan1 TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","banjirSumberGenangan2")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN banjirSumberGenangan2 TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","banjirSumberGenangan3")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN banjirSumberGenangan3 TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","banjirSumberGenangan4")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN banjirSumberGenangan4 TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","banjirSumberGenanganLainnya")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN banjirSumberGenanganLainnya TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","tingkatKerusakanLahanKondisiTanaman1")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN tingkatKerusakanLahanKondisiTanaman1 TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","tingkatKerusakanLahanKondisiTanaman2")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN tingkatKerusakanLahanKondisiTanaman2 TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","tingkatKerusakanLahanKondisiTanaman3")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN tingkatKerusakanLahanKondisiTanaman3 TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","tingkatKerusakanLahanKondisiTanaman4")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN tingkatKerusakanLahanKondisiTanaman4 TEXT DEFAULT '';")
            }

            if (!isFieldExist(db!!,"survey","kualitasAirTanahPhAt")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN kualitasAirTanahPhAt TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","kualitasAirTanahPhAs")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN kualitasAirTanahPhAs TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","kualitasAirTanahEcAt")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN kualitasAirTanahEcAt TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","kualitasAirTanahEcAs")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN kualitasAirTanahEcAs TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","kualitasAirTanahTdsAt")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN kualitasAirTanahTdsAt TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","kualitasAirTanahTdsAs")){
                db?.execSQL("ALTER TABLE survey ADD COLUMN kualitasAirTanahTdsAs TEXT DEFAULT '';")
            }
            if (!isFieldExist(db!!,"survey","dusun")){
                db .execSQL("ALTER TABLE survey ADD COLUMN dusun TEXT DEFAULT '';")
            }

        }
    }

    fun isFieldExist(db: SQLiteDatabase, tableName: String, fieldName: String?): Boolean {
        var isExist = false
        var res: Cursor? = null
        try {
            res = db.rawQuery("Select * from $tableName limit 1", null)
            val colIndex: Int = res.getColumnIndex(fieldName)
            if (colIndex != -1) {
                isExist = true
            }
        } catch (e: Exception) {

        } finally {
            try {
                if (res != null) {
                    res.close()
                }
            } catch (e1: Exception) {
            }
        }
        return isExist
    }
}