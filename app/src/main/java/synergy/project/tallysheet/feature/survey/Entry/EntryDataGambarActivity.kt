package synergy.project.tallysheet.feature.survey.Entry

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import kotlinx.android.synthetic.main.layout_data_gambar.*
import kotlinx.android.synthetic.main.layout_elevasi_lahan.*
import kotlinx.android.synthetic.main.layout_header.*
import kotlinx.android.synthetic.main.layout_header.btn_simpan
import kotlinx.android.synthetic.main.layout_koordinat_titik_survey.*
import splitties.activities.start
import synergy.project.tallysheet.R
import synergy.project.tallysheet.feature.main.MainActivity
import synergy.project.tallysheet.model.Survey
import synergy.project.tallysheet.utils.DatePickerFragment
import synergy.project.tallysheet.utils.PrefManager

class  EntryDataGambarActivity : AppCompatActivity() {
    lateinit var survey: Survey
    lateinit var prefManager: PrefManager
    var id : Int= 0

    var imgBatasBarat = ""
    var imgBatasTimur = ""
    var imgBatasSelatan = ""
    var imgBatasUtara = ""
    var sketsaLokasi = ""

    val activity = EntryDataGambarActivity@this


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_data_gambar)
        val bundle = intent.extras
        if (bundle != null) {
            id = bundle["id"] as Int
        }
        survey = Survey(applicationContext)
        prefManager = PrefManager(applicationContext)
        // ini untuk menampilkan data
        if (survey.getData(id)) {
            Glide.with(activity).load(survey.imgBatasBarat).into(img_batas_barat)
            Glide.with(activity).load(survey.imgBatasSelatan).into(img_batas_selatan)
            Glide.with(activity).load(survey.imgBatasTimur).into(img_batas_timur)
            Glide.with(activity).load(survey.imgBatasUtara).into(img_batas_utara)
            Glide.with(activity).load(survey.sketsaLokasi).into(img_sketsa)
        }

        supportActionBar?.title = "Isi Survey"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayUseLogoEnabled(false)

        initView()
        klik()
    }

    private fun initView() {
    }

    fun klik(){
        btn_simpan.setOnClickListener {
            survey.update()
            finishAffinity();
            start<MainActivity> {}
        }

        btn_get_sketsa.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(157) }
        btn_get_barat.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(158) }
        btn_get_timur.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(159) }
        btn_get_selatan.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(160) }
        btn_get_utara.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(161) }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode==157){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")) {
                img_sketsa.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_sketsa)
                survey.sketsaLokasi = filePath
            }


        }
        if (requestCode==158){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")) {
                img_batas_barat.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_batas_barat)
                survey.imgBatasBarat = filePath
            }

        }
        if (requestCode==159){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")) {
                img_batas_timur.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_batas_timur)
                survey.imgBatasTimur = filePath
            }

        }
        if (requestCode==160){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")) {
                img_batas_selatan.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_batas_selatan)
                survey.imgBatasSelatan = filePath
            }

        }
        if (requestCode==161){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")) {
                img_batas_utara.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_batas_utara)
                survey.imgBatasUtara = filePath
            }

        }
    }

    fun returnData(id:Int){
        if (survey.getData(id)) {

        } else {

        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


}