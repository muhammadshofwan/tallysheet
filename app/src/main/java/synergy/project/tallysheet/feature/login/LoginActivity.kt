package synergy.project.tallysheet.feature.login

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_login.*
import synergy.project.tallysheet.R
import synergy.project.tallysheet.utils.Helper

class LoginActivity : AppCompatActivity() {
    lateinit var viewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        viewModel = ViewModelProvider(this)[LoginViewModel::class.java]
        viewModel.setData(LoginActivity@ this)

        val PERMISSION_ALL = 1
        val PERMISSIONS = arrayOf(
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
        )

        if (!hasPermissions(this, *PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL)
        }

        error_box.visibility = View.GONE
        txt_error_message.visibility = View.GONE
        txt_error_message_label.visibility = View.GONE

        val connMgr = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo =connMgr.activeNetworkInfo

        if (networkInfo!=null && networkInfo.isConnected){
            Helper.mode = 1
            btn_login.setBackgroundResource(R.drawable.button_blue)
            btn_login.setEnabled(true)
            txt_mode.setText(getString(R.string.login_online_mode))
            footer.setBackgroundResource(R.color.online_mode_text_background)
            img_icon.setImageResource(R.drawable.ic_action_accept)
        }else{

            Helper.mode = 0
            btn_login.setBackgroundResource(R.drawable.button_grey)
            btn_login.setEnabled(false)
            txt_mode.setText(getString(R.string.login_offline_mode))
            footer.setBackgroundResource(R.color.offline_mode_text_background)
            img_icon.setImageResource(R.drawable.ic_action_warning)
        }

        btn_login.setOnClickListener {
            var noError = true
            var errorMessage = ""
            if (txt_email.getText().toString() == "") {
                errorMessage = errorMessage + getString(R.string.login_error_email_blank) + '\n'
                noError = false
            }

            if (txt_password.text.toString() == "") {
                errorMessage = errorMessage + getString(R.string.login_error_password_blank) + '\n'
                noError = false
            }

            if (!noError) {
                error_box.visibility = View.VISIBLE
                txt_error_message.visibility = View.VISIBLE
                txt_error_message.setText(errorMessage)
                return@setOnClickListener
            }

            error_box.visibility = View.GONE
            txt_error_message.visibility = View.GONE

            val params = HashMap<String,String>()
            params["username"] = txt_email.text.toString()
            params["password"] = txt_password.text.toString()
            viewModel.login(params)
        }
    }

    fun hasPermissions(context: Context?, vararg permissions: String?): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (permission in permissions) {
                if (ActivityCompat.checkSelfPermission(
                        context,
                        permission!!
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    return false
                }
            }
        }
        return true
    }
}