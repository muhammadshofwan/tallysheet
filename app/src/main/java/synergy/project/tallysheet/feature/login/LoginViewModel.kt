package synergy.project.tallysheet.feature.login

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import org.json.JSONObject
import splitties.activities.start
import splitties.toast.toast
import synergy.project.tallysheet.feature.main.MainActivity
import synergy.project.tallysheet.model.Survey
import synergy.project.tallysheet.utils.Helper
import synergy.project.tallysheet.utils.PrefManager
import synergy.project.tallysheet.utils.Progress

class LoginViewModel : ViewModel() {
    lateinit var activity: Activity
    lateinit var prefManager: PrefManager
    lateinit var progress: Progress
    fun setData(activity: Activity){
        this.activity = activity
        prefManager = PrefManager(activity.applicationContext)
        progress = Progress(activity)
        AndroidNetworking.initialize(activity.applicationContext)

    }

    fun login(params : HashMap<String,String>){
        progress.progress("Login")

        val host = "${Helper.host}auth/login"
        AndroidNetworking.post(host).addBodyParameter(params).setPriority(Priority.HIGH).setTag(activity)
            .build().getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    progress.dismiss()
                    prefManager.token = response?.getString("token")
                    prefManager.nama = params["username"]

                    activity.start<MainActivity> { flags = Intent.FLAG_ACTIVITY_CLEAR_TOP }
                    activity.finish()
                    toast("Berhasil Login")
                }

                override fun onError(anError: ANError?) {
                    progress.dismiss()
                    toast("Gagal Login")

                }

            })
    }
}