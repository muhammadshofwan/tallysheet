package synergy.project.tallysheet.feature.survey.Entry

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import kotlinx.android.synthetic.main.layout_genangan.*
import kotlinx.android.synthetic.main.layout_header.btn_simpan
import kotlinx.android.synthetic.main.layout_kebakaran_hujan.*
import kotlinx.android.synthetic.main.layout_kebakaran_hujan.btn_kembali
import kotlinx.android.synthetic.main.layout_kebakaran_hujan.btn_navigasi_1
import kotlinx.android.synthetic.main.layout_kebakaran_hujan.btn_navigasi_10
import kotlinx.android.synthetic.main.layout_kebakaran_hujan.btn_navigasi_11
import kotlinx.android.synthetic.main.layout_kebakaran_hujan.btn_navigasi_12
import kotlinx.android.synthetic.main.layout_kebakaran_hujan.btn_navigasi_13
import kotlinx.android.synthetic.main.layout_kebakaran_hujan.btn_navigasi_14
import kotlinx.android.synthetic.main.layout_kebakaran_hujan.btn_navigasi_15
import kotlinx.android.synthetic.main.layout_kebakaran_hujan.btn_navigasi_2
import kotlinx.android.synthetic.main.layout_kebakaran_hujan.btn_navigasi_3
import kotlinx.android.synthetic.main.layout_kebakaran_hujan.btn_navigasi_4
import kotlinx.android.synthetic.main.layout_kebakaran_hujan.btn_navigasi_5
import kotlinx.android.synthetic.main.layout_kebakaran_hujan.btn_navigasi_6
import kotlinx.android.synthetic.main.layout_kebakaran_hujan.btn_navigasi_7
import kotlinx.android.synthetic.main.layout_kebakaran_hujan.btn_navigasi_8
import kotlinx.android.synthetic.main.layout_kebakaran_hujan.btn_navigasi_9
import kotlinx.android.synthetic.main.layout_kebakaran_hujan.spn_banjirBulan
import kotlinx.android.synthetic.main.layout_koordinat_titik_survey.btn_kordinatImg1
import kotlinx.android.synthetic.main.layout_koordinat_titik_survey.btn_kordinatImg2
import kotlinx.android.synthetic.main.layout_koordinat_titik_survey.btn_kordinatImg3
import kotlinx.android.synthetic.main.layout_koordinat_titik_survey.btn_kordinatImg4
import splitties.activities.start
import splitties.toast.toast
import synergy.project.tallysheet.R
import synergy.project.tallysheet.model.Survey
import synergy.project.tallysheet.utils.DatePickerFragment
import synergy.project.tallysheet.utils.Helper
import synergy.project.tallysheet.utils.Local
import synergy.project.tallysheet.utils.PrefManager

class  EntryKebakaranLahanActivity : AppCompatActivity() {
    lateinit var survey: Survey
    lateinit var prefManager: PrefManager
    var id : Int= 0

    var pathkebakaranHujanImg1 = ""
    var pathkebakaranHujanImg2 = ""
    var pathkebakaranHujanImg3 = ""
    var pathkebakaranHujanImg4 = ""
    var pathkebakaranHujanImg5 = ""
    var pathkebakaranHujanImg6 = ""

    val activity = EntryKebakaranLahanActivity@this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_kebakaran_hujan)
        val bundle = intent.extras
        if (bundle != null) {
            id = bundle["id"] as Int
        }
        survey = Survey(applicationContext)
        survey.getData(id)
        prefManager = PrefManager(applicationContext)

        // ini untuk ini value spinner
        val dataAdapter_kebakaranLahanBulan: ArrayAdapter<String> = ArrayAdapter<String>(
            this@EntryKebakaranLahanActivity,// ganti jadi this@Nama Activity
            android.R.layout.simple_dropdown_item_1line,
            Helper.kebakaranLahanBulan
       )

        val dataAdapter_kebakaranLahanLamaKejadianHariMInggu: ArrayAdapter<String> = ArrayAdapter<String>(
            this@EntryKebakaranLahanActivity,// ganti jadi this@Nama Activity
            android.R.layout.simple_dropdown_item_1line,
            Helper.kebakaranLahanLamaKejadianHariMInggu
        )

        val dataAdapter_kebakaranLahanUpayaPemadamanSwadayaMasyarakat: ArrayAdapter<String> = ArrayAdapter<String>(
            this@EntryKebakaranLahanActivity,// ganti jadi this@Nama Activity
            android.R.layout.simple_dropdown_item_1line,
            Helper.kebakaranLahanUpayaPemadamanSwadayaMasyarakat
        )

        val dataAdapter_kebakaranLahanUpayaPemadamanPemerintah: ArrayAdapter<String> = ArrayAdapter<String>(
            this@EntryKebakaranLahanActivity,// ganti jadi this@Nama Activity
            android.R.layout.simple_dropdown_item_1line,
            Helper.kebakaranLahanUpayaPemadamanPemerintah
        )

        val dataAdapter_hujanLamaKejadianJamHari: ArrayAdapter<String> = ArrayAdapter<String>(
            this@EntryKebakaranLahanActivity,// ganti jadi this@Nama Activity
            android.R.layout.simple_dropdown_item_1line,
            Helper.hujanLamaKejadianJamHari
        )

        val dataAdapter_hujanIntensitas: ArrayAdapter<String> = ArrayAdapter<String>(
            this@EntryKebakaranLahanActivity,// ganti jadi this@Nama Activity
            android.R.layout.simple_dropdown_item_1line,
            Helper.hujanIntensitas
        )

        spn_banjirBulan.adapter = dataAdapter_kebakaranLahanBulan
        spn_kebakaranLahanLamaKejadianHariMInggu.adapter = dataAdapter_kebakaranLahanLamaKejadianHariMInggu
        spn_kebakaranLahanUpayaPemadamanSwadayaMasyarakat.adapter = dataAdapter_kebakaranLahanUpayaPemadamanSwadayaMasyarakat
        spn_kebakaranLahanUpayaPemadamanPemerintah.adapter = dataAdapter_kebakaranLahanUpayaPemadamanPemerintah
        spn_hujanLamaKejadianJamHari.adapter = dataAdapter_hujanLamaKejadianJamHari
        spn_hujanIntensitas.adapter = dataAdapter_hujanIntensitas

        if (survey.getData(id)) {
            txt_kebakaranLahanTahun.setText(survey.kebakaranLahanTahun)


            val banjirBulanAdp = spn_banjirBulan.adapter as ArrayAdapter<String>
            val banjirBulanPosition = banjirBulanAdp.getPosition(survey.kebakaranLahanBulan)
            spn_banjirBulan.setSelection(banjirBulanPosition)

            txt_kebakaranLahanTanggal.setText(survey.kebakaranLahanTanggal)
            txt_kebakaran_lama_kejadian.setText(survey.kebakaranLahanLamaKejadian)
            val kebakaranLahanLamaKejadianHariMIngguAdp = spn_kebakaranLahanLamaKejadianHariMInggu.adapter as ArrayAdapter<String>
            val kebakaranLahanLamaKejadianHariMIngguPosition = kebakaranLahanLamaKejadianHariMIngguAdp.getPosition(survey.kebakaranLahanLamaKejadianHariMInggu)
            spn_kebakaranLahanLamaKejadianHariMInggu.setSelection(kebakaranLahanLamaKejadianHariMIngguPosition)
            val kebakaranLahanUpayaPemadamanSwadayaMasyarakatAdp = spn_kebakaranLahanUpayaPemadamanSwadayaMasyarakat.adapter as ArrayAdapter<String>
            val kebakaranLahanUpayaPemadamanSwadayaMasyarakatPosition = kebakaranLahanUpayaPemadamanSwadayaMasyarakatAdp.getPosition(survey.kebakaranLahanUpayaPemadamanSwadayaMasyarakat)
            spn_kebakaranLahanUpayaPemadamanSwadayaMasyarakat.setSelection(kebakaranLahanUpayaPemadamanSwadayaMasyarakatPosition)
            val kebakaranLahanUpayaPemadamanPemerintahAdp = spn_kebakaranLahanUpayaPemadamanPemerintah.adapter as ArrayAdapter<String>
            val kebakaranLahanUpayaPemadamanPemerintahPosition = kebakaranLahanUpayaPemadamanPemerintahAdp.getPosition(survey.kebakaranLahanUpayaPemadamanPemerintah)
            spn_kebakaranLahanUpayaPemadamanPemerintah.setSelection(kebakaranLahanUpayaPemadamanPemerintahPosition)
            txt_hujanTerakhir.setText(survey.hujanTerakhir)
            txt_hujanLamaKejadian.setText(survey.hujanLamaKejadian)
            val hujanLamaKejadianJamHariAdp = spn_hujanLamaKejadianJamHari.adapter as ArrayAdapter<String>
            val hujanLamaKejadianJamHariPosition = hujanLamaKejadianJamHariAdp.getPosition(survey.hujanLamaKejadianJamHari)
            spn_hujanLamaKejadianJamHari.setSelection(hujanLamaKejadianJamHariPosition)
            val hujanIntensitasAdp = spn_hujanIntensitas.adapter as ArrayAdapter<String>
            val hujanIntensitasPosition = hujanIntensitasAdp.getPosition(survey.hujanIntensitas)
            spn_hujanIntensitas.setSelection(hujanIntensitasPosition)

            Glide.with(activity).load(survey.kebakaranHujanImg1).into(img_kebakaranHujanImg1)
            Glide.with(activity).load(survey.kebakaranHujanImg2).into(img_kebakaranHujanImg2)
            Glide.with(activity).load(survey.kebakaranHujanImg3).into(img_kebakaranHujanImg3)
            Glide.with(activity).load(survey.kebakaranHujanImg4).into(img_kebakaranHujanImg4)
            Glide.with(activity).load(survey.kebakaranHujanImg5).into(img_kebakaranHujanImg5)
            Glide.with(activity).load(survey.kebakaranHujanImg6).into(img_kebakaranHujanImg6)

            pathkebakaranHujanImg1 = survey.kebakaranHujanImg1.toString()
            pathkebakaranHujanImg2 = survey.kebakaranHujanImg2.toString()
            pathkebakaranHujanImg3 = survey.kebakaranHujanImg3.toString()
            pathkebakaranHujanImg4 = survey.kebakaranHujanImg4.toString()
            pathkebakaranHujanImg5 = survey.kebakaranHujanImg5.toString()
            pathkebakaranHujanImg6 = survey.kebakaranHujanImg6.toString()
        }




        supportActionBar?.title = "Isi Survey"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayUseLogoEnabled(false)

        initView()
        klik()
    }

    private fun initView() {
        if(pathkebakaranHujanImg1!="") Local.isVisible(btn_kebakaranHujan1,btn_delete_btn_ketebalanGambut1,true)
        else Local.isVisible(btn_kebakaranHujan1,btn_delete_btn_ketebalanGambut1,false)

        if(pathkebakaranHujanImg2!="") Local.isVisible(btn_kebakaranHujan2,btn_delete_btn_ketebalanGambut2,true)
        else Local.isVisible(btn_kebakaranHujan2,btn_delete_btn_ketebalanGambut2,false)

        if(pathkebakaranHujanImg3!="") Local.isVisible(btn_kebakaranHujan3,btn_delete_btn_ketebalanGambut3,true)
        else Local.isVisible(btn_kebakaranHujan3,btn_delete_btn_ketebalanGambut3,false)

        if(pathkebakaranHujanImg4!="") Local.isVisible(btn_kebakaranHujan4,btn_delete_kebakaranHujan4,true)
        else Local.isVisible(btn_kebakaranHujan4,btn_delete_kebakaranHujan4,false)

        if(pathkebakaranHujanImg5!="") Local.isVisible(btn_kebakaranHujan5,btn_delete_kebakaranHujan5,true)
        else Local.isVisible(btn_kebakaranHujan5,btn_delete_kebakaranHujan5,false)

        if(pathkebakaranHujanImg6!="") Local.isVisible(btn_kebakaranHujan6,btn_delete_kebakaranHujan6,true)
        else Local.isVisible(btn_kebakaranHujan6,btn_delete_kebakaranHujan6,false)
    }

    fun klik(){
        btn_simpan.setOnClickListener {

            survey.kebakaranLahanTahun = txt_kebakaranLahanTahun.text.toString()
            survey.kebakaranLahanBulan = spn_banjirBulan.selectedItem.toString()
            survey.kebakaranLahanTanggal = txt_kebakaranLahanTanggal.text.toString()
            survey.kebakaranLahanLamaKejadian = txt_kebakaran_lama_kejadian.text.toString()
            survey.kebakaranLahanLamaKejadianHariMInggu = spn_kebakaranLahanLamaKejadianHariMInggu.selectedItem.toString()
            survey.kebakaranLahanUpayaPemadamanSwadayaMasyarakat = spn_kebakaranLahanUpayaPemadamanSwadayaMasyarakat.selectedItem.toString()
            survey.kebakaranLahanUpayaPemadamanPemerintah = spn_kebakaranLahanUpayaPemadamanPemerintah.selectedItem.toString()
            survey.hujanTerakhir = txt_hujanTerakhir.text.toString()
            survey.hujanLamaKejadian = txt_hujanLamaKejadian.text.toString()
            survey.hujanLamaKejadianJamHari = spn_hujanLamaKejadianJamHari.selectedItem.toString()
            survey.hujanIntensitas = spn_hujanIntensitas.selectedItem.toString()

            survey.kebakaranHujanImg1 = pathkebakaranHujanImg1
            survey.kebakaranHujanImg2 = pathkebakaranHujanImg2
            survey.kebakaranHujanImg3 = pathkebakaranHujanImg3
            survey.kebakaranHujanImg4 = pathkebakaranHujanImg4
            survey.kebakaranHujanImg5 = pathkebakaranHujanImg5
            survey.kebakaranHujanImg6 = pathkebakaranHujanImg6

            survey.update()
            start<EntryPorositasKelengasanActivity> {
                putExtra("id", survey.id?.toInt())
            }

        }

        btn_kembali.setOnClickListener {
            survey.kebakaranLahanTahun = txt_kebakaranLahanTahun.text.toString()
            survey.kebakaranLahanBulan = spn_banjirBulan.selectedItem.toString()
            survey.kebakaranLahanTanggal = txt_kebakaranLahanTanggal.text.toString()
            survey.kebakaranLahanLamaKejadian = txt_kebakaran_lama_kejadian.text.toString()
            survey.kebakaranLahanLamaKejadianHariMInggu = spn_kebakaranLahanLamaKejadianHariMInggu.selectedItem.toString()
            survey.kebakaranLahanUpayaPemadamanSwadayaMasyarakat = spn_kebakaranLahanUpayaPemadamanSwadayaMasyarakat.selectedItem.toString()
            survey.kebakaranLahanUpayaPemadamanPemerintah = spn_kebakaranLahanUpayaPemadamanPemerintah.selectedItem.toString()
            survey.hujanTerakhir = txt_hujanTerakhir.text.toString()
            survey.hujanLamaKejadian = txt_hujanLamaKejadian.text.toString()
            survey.hujanLamaKejadianJamHari = spn_hujanLamaKejadianJamHari.selectedItem.toString()
            survey.hujanIntensitas = spn_hujanIntensitas.selectedItem.toString()

            survey.kebakaranHujanImg1 = pathkebakaranHujanImg1
            survey.kebakaranHujanImg2 = pathkebakaranHujanImg2
            survey.kebakaranHujanImg3 = pathkebakaranHujanImg3
            survey.kebakaranHujanImg4 = pathkebakaranHujanImg4
            survey.kebakaranHujanImg5 = pathkebakaranHujanImg5
            survey.kebakaranHujanImg6 = pathkebakaranHujanImg6

            survey.update()
            onBackPressed()
        }

        btn_kebakaranHujan1.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(173) }
        btn_kebakaranHujan2.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(174) }
        btn_kebakaranHujan3.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(175) }
        btn_kebakaranHujan4.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(176) }
        btn_kebakaranHujan5.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(177) }
        btn_kebakaranHujan6.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(178) }

        //delete image
        /*deleteImage(btndeletekeba,img_banjirImg1,1)
        deleteImage(btn_delete_kebakaranHujanImg2,img_banjirImg2,2)
        deleteImage(btn_delete_kebakaranHujanImg3,img_banjirImg3,3)
        deleteImage(btn_delete_kebakaranHujanImg4,img_banjirImg4,4)
        deleteImage(btn_delete_kebakaranHujanImg5,img_banjirImg5,5)
        deleteImage(btn_delete_kebakaranHujanImg6,img_banjirImg6,6)*/

        //navigasi
        selectNavigasi(btn_navigasi_1,1)
        selectNavigasi(btn_navigasi_2,2)
        selectNavigasi(btn_navigasi_3,3)
        selectNavigasi(btn_navigasi_4,4)
        selectNavigasi(btn_navigasi_5,5)
        selectNavigasi(btn_navigasi_6,6)
        selectNavigasi(btn_navigasi_7,7)
        selectNavigasi(btn_navigasi_8,8)
        selectNavigasi(btn_navigasi_9,9)
        selectNavigasi(btn_navigasi_10,10)
        selectNavigasi(btn_navigasi_11,11)
        selectNavigasi(btn_navigasi_12,12)
        selectNavigasi(btn_navigasi_13,13)
        selectNavigasi(btn_navigasi_14,14)
        selectNavigasi(btn_navigasi_15,15)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode==173){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_kebakaranHujanImg1.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_kebakaranHujanImg1)
                survey.kebakaranHujanImg1 = filePath
                pathkebakaranHujanImg1 = filePath
            }
        }

        if (requestCode==174){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_kebakaranHujanImg2.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_kebakaranHujanImg2)
                survey.kebakaranHujanImg2 = filePath
                pathkebakaranHujanImg2 = filePath
            }
        }

        if (requestCode==175){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_kebakaranHujanImg3.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_kebakaranHujanImg3)
                survey.kebakaranHujanImg3 = filePath
                pathkebakaranHujanImg3 = filePath
            }
        }

        if (requestCode==176){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_kebakaranHujanImg4.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_kebakaranHujanImg4)
                survey.kebakaranHujanImg4 = filePath
                pathkebakaranHujanImg4 = filePath
            }
        }

        if (requestCode==177){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_kebakaranHujanImg5.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_kebakaranHujanImg5)
                survey.kebakaranHujanImg5 = filePath
                pathkebakaranHujanImg5 = filePath
            }
        }

        if (requestCode==178){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_kebakaranHujanImg6.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_kebakaranHujanImg6)
                survey.kebakaranHujanImg6 = filePath
                pathkebakaranHujanImg6 = filePath
            }
        }
    }

    fun returnData(id:Int){
        if (survey.getData(id)) {

        } else {

        }
    }

    fun selectNavigasi(button: Button, code:Int){
        button.setOnClickListener {
            when(code){
                1 -> navigasiIntent(EntryKordinatActivity::class.java)
                2 -> navigasiIntent(EntryElevasiActivity::class.java)
                3 -> navigasiIntent(EntryGenanganActivity::class.java)
                4 -> navigasiIntent(EntryTutupanLahanActivity::class.java)
                5 -> navigasiIntent(EntryFloraFaunaActivity::class.java)
                6 ->  navigasiIntent(EntryDrainaseActivity::class.java)
                7 -> navigasiIntent(EntryKualitasAirTanahActivity::class.java)
                8 -> navigasiIntent(EntryKarakteristikSubstratumTanahActivity::class.java)
                9 -> navigasiIntent(EntryTipeLuapanActivity::class.java)
                10 -> navigasiIntent(EntryKetebalanGambutActivity::class.java)
                11 -> navigasiIntent(EntryKarakteristikSubstratumDibawahLapisanGambutActivity::class.java)
                12 -> navigasiIntent(EntryTingkatKerusakanLahanActivity::class.java)
                //13 -> navigasiIntent(EntryKebakaranLahanActivity::class.java)
                14 -> navigasiIntent(EntryPorositasKelengasanActivity::class.java)
                15 -> navigasiIntent(EntryDataGambarActivity::class.java)
            }
        }
    }

    private fun navigasiIntent(classtujuan: Class<*>) {
        survey.kebakaranLahanTahun = txt_kebakaranLahanTahun.text.toString()
        survey.kebakaranLahanBulan = spn_banjirBulan.selectedItem.toString()
        survey.kebakaranLahanTanggal = txt_kebakaranLahanTanggal.text.toString()
        survey.kebakaranLahanLamaKejadian = txt_kebakaran_lama_kejadian.text.toString()
        survey.kebakaranLahanLamaKejadianHariMInggu = spn_kebakaranLahanLamaKejadianHariMInggu.selectedItem.toString()
        survey.kebakaranLahanUpayaPemadamanSwadayaMasyarakat = spn_kebakaranLahanUpayaPemadamanSwadayaMasyarakat.selectedItem.toString()
        survey.kebakaranLahanUpayaPemadamanPemerintah = spn_kebakaranLahanUpayaPemadamanPemerintah.selectedItem.toString()
        survey.hujanTerakhir = txt_hujanTerakhir.text.toString()
        survey.hujanLamaKejadian = txt_hujanLamaKejadian.text.toString()
        survey.hujanLamaKejadianJamHari = spn_hujanLamaKejadianJamHari.selectedItem.toString()
        survey.hujanIntensitas = spn_hujanIntensitas.selectedItem.toString()

        survey.kebakaranHujanImg1 = pathkebakaranHujanImg1
        survey.kebakaranHujanImg2 = pathkebakaranHujanImg2
        survey.kebakaranHujanImg3 = pathkebakaranHujanImg3
        survey.kebakaranHujanImg4 = pathkebakaranHujanImg4
        survey.kebakaranHujanImg5 = pathkebakaranHujanImg5
        survey.kebakaranHujanImg6 = pathkebakaranHujanImg6

        survey.update()
        val intent = Intent(activity,classtujuan)
        intent.putExtra("id", survey.id?.toInt())
        startActivity(intent)

    }

    fun deleteImage(button: ImageButton, imageView: ImageView, code: Int){
        button.setOnClickListener {
            when(code){
                1-> pathkebakaranHujanImg1 = ""
                2-> pathkebakaranHujanImg2 = ""
                3-> pathkebakaranHujanImg3 = ""
                4-> pathkebakaranHujanImg4 = ""
                5-> pathkebakaranHujanImg5 = ""
                6-> pathkebakaranHujanImg6 = ""
            }

            Glide.with(activity).load("").into(imageView)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
    override fun onResume() {
        if(id != null){
            survey.getData(id)
        }
        super.onResume()
    }

}