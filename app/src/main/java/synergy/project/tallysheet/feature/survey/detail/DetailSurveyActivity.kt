package synergy.project.tallysheet.feature.survey.detail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_detail_survey.*
import splitties.toast.toast
import synergy.project.tallysheet.R
import synergy.project.tallysheet.feature.survey.detail.tab.SurveyFotoTabFragment
import synergy.project.tallysheet.feature.survey.detail.tab.SurveyTabFragment

class DetailSurveyActivity : AppCompatActivity() {
    companion object{
        var nomorBoring = ""
        var id = ""
    }

    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_survey)
        val bundle = intent.extras
        if (bundle != null) {
            nomorBoring = bundle["nomorBoring"] as String
            id = bundle["id"] as String
        }

        supportActionBar?.title = nomorBoring
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayUseLogoEnabled(false)

        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)
        view_pager.setAdapter(mSectionsPagerAdapter)
        view_pager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
        tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(view_pager))
    }

    class PlaceholderFragment : Fragment() {
        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                                  savedInstanceState: Bundle?): View? {
            return inflater.inflate(R.layout.fragment_survey_tab, container, false)
        }
        companion object {
            private const val ARG_SECTION_NUMBER = "section_number"
            fun newInstance(sectionNumber: Int): PlaceholderFragment {
                val fragment = PlaceholderFragment()
                val args = Bundle()
                args.putInt(ARG_SECTION_NUMBER, sectionNumber)
                fragment.arguments = args
                return fragment
            }
        }
    }


    class SectionsPagerAdapter(fm: FragmentManager?) : FragmentPagerAdapter(fm!!) {
        override fun getItem(position: Int): Fragment {
            return if (position == 0) {
                val fragment = SurveyTabFragment()
                fragment.id_survey = id.toInt()
                return fragment
            } else if (position == 1) {
                val fragment = SurveyFotoTabFragment()
                fragment.id_survey = id.toInt()
                return fragment
            } else {
                val fragment = SurveyTabFragment()
                fragment.id_survey = id.toInt()
                return fragment

            }
        }

        override fun getCount(): Int {
            // Show 4 total pages.
            return 2
        }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}