package synergy.project.tallysheet.feature.survey.Entry

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import kotlinx.android.synthetic.main.layout_genangan.*
import kotlinx.android.synthetic.main.layout_header.btn_simpan
import kotlinx.android.synthetic.main.layout_karakteristik_substratum.*
import kotlinx.android.synthetic.main.layout_karakteristik_substratum.btn_kembali
import kotlinx.android.synthetic.main.layout_karakteristik_substratum.btn_navigasi_1
import kotlinx.android.synthetic.main.layout_karakteristik_substratum.btn_navigasi_10
import kotlinx.android.synthetic.main.layout_karakteristik_substratum.btn_navigasi_11
import kotlinx.android.synthetic.main.layout_karakteristik_substratum.btn_navigasi_12
import kotlinx.android.synthetic.main.layout_karakteristik_substratum.btn_navigasi_13
import kotlinx.android.synthetic.main.layout_karakteristik_substratum.btn_navigasi_14
import kotlinx.android.synthetic.main.layout_karakteristik_substratum.btn_navigasi_15
import kotlinx.android.synthetic.main.layout_karakteristik_substratum.btn_navigasi_2
import kotlinx.android.synthetic.main.layout_karakteristik_substratum.btn_navigasi_3
import kotlinx.android.synthetic.main.layout_karakteristik_substratum.btn_navigasi_4
import kotlinx.android.synthetic.main.layout_karakteristik_substratum.btn_navigasi_5
import kotlinx.android.synthetic.main.layout_karakteristik_substratum.btn_navigasi_6
import kotlinx.android.synthetic.main.layout_karakteristik_substratum.btn_navigasi_7
import kotlinx.android.synthetic.main.layout_karakteristik_substratum.btn_navigasi_8
import kotlinx.android.synthetic.main.layout_karakteristik_substratum.btn_navigasi_9
import splitties.activities.start
import splitties.toast.toast
import synergy.project.tallysheet.R
import synergy.project.tallysheet.model.Survey
import synergy.project.tallysheet.utils.DatePickerFragment
import synergy.project.tallysheet.utils.Helper
import synergy.project.tallysheet.utils.Local
import synergy.project.tallysheet.utils.PrefManager

class  EntryKarakteristikSubstratumTanahActivity : AppCompatActivity() {
    lateinit var survey: Survey
    lateinit var prefManager: PrefManager
    var id : Int= 0

    var pathkarakteristikSubstratumTanahLiatImg1 = ""
    var pathkarakteristikSubstratumTanahLiatImg2 = ""
    var pathkarakteristikSubstratumTanahLiatImg3 = ""
    var pathkarakteristikSubstratumTanahLiatImg4 = ""
    var pathkarakteristikSubstratumTanahLiatImg5 = ""
    var pathkarakteristikSubstratumTanahLiatImg6 = ""

    val activity = EntryKarakteristikSubstratumTanahActivity@this


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_karakteristik_substratum)
        val bundle = intent.extras
        if (bundle != null) {
            id = bundle["id"] as Int
        }
        survey = Survey(applicationContext)
         prefManager = PrefManager(applicationContext)

        val dataAdapter_karakteristikSubstratumTanahNa: ArrayAdapter<String> = ArrayAdapter<String>(
                this@EntryKarakteristikSubstratumTanahActivity,// ganti jadi this@Nama Activity
                android.R.layout.simple_dropdown_item_1line,
                Helper.karakteristikSubstratumTanahNa
        )
        spn_karakteristikSubstratumTanahNa.adapter = dataAdapter_karakteristikSubstratumTanahNa

        val dataAdapter_karakteristikSubstratumTanahNaJenis: ArrayAdapter<String> = ArrayAdapter<String>(
                this@EntryKarakteristikSubstratumTanahActivity,// ganti jadi this@Nama Activity
                android.R.layout.simple_dropdown_item_1line,
                Helper.karakteristikSubstratumTanahNaJenis
        )
        spn_karakteristikSubstratumTanahNaJenis.adapter = dataAdapter_karakteristikSubstratumTanahNaJenis


        if (survey.getData(id)) {

            val karakteristikSubstratumTanahNaAdp = spn_karakteristikSubstratumTanahNa.adapter as ArrayAdapter<String>
            val karakteristikSubstratumTanahNaPosition = karakteristikSubstratumTanahNaAdp.getPosition(survey.karakteristikSubstratumTanahNa)
            spn_karakteristikSubstratumTanahNa.setSelection(karakteristikSubstratumTanahNaPosition)
            val karakteristikSubstratumTanahNaJenisAdp = spn_karakteristikSubstratumTanahNaJenis.adapter as ArrayAdapter<String>
            val karakteristikSubstratumTanahNaJenisPosition = karakteristikSubstratumTanahNaJenisAdp.getPosition(survey.karakteristikSubstratumTanahNaJenis)
            spn_karakteristikSubstratumTanahNaJenis.setSelection(karakteristikSubstratumTanahNaJenisPosition)
            txt_karakteristikSubstratumTanahPh.setText(survey.karakteristikSubstratumTanahLiatPh)
            txt_karakteristikSubstratumTanahEc.setText(survey.karakteristikSubstratumTanahLiatEc)

            Glide.with(activity).load(survey.karakteristikSubstratumTanahLiatImg1).into(img_karakteristikSubstratumTanahLiatImg1)
            Glide.with(activity).load(survey.karakteristikSubstratumTanahLiatImg2).into(img_karakteristikSubstratumTanahLiatImg2)
            Glide.with(activity).load(survey.karakteristikSubstratumTanahLiatImg3).into(img_karakteristikSubstratumTanahLiatImg3)
            Glide.with(activity).load(survey.karakteristikSubstratumTanahLiatImg4).into(img_karakteristikSubstratumTanahLiatImg4)
            Glide.with(activity).load(survey.karakteristikSubstratumTanahLiatImg5).into(img_karakteristikSubstratumTanahLiatImg5)
            Glide.with(activity).load(survey.karakteristikSubstratumTanahLiatImg6).into(img_karakteristikSubstratumTanahLiatImg6)

            pathkarakteristikSubstratumTanahLiatImg1 = survey.karakteristikSubstratumTanahLiatImg1.toString()
            pathkarakteristikSubstratumTanahLiatImg2 = survey.karakteristikSubstratumTanahLiatImg2.toString()
            pathkarakteristikSubstratumTanahLiatImg3 = survey.karakteristikSubstratumTanahLiatImg3.toString()
            pathkarakteristikSubstratumTanahLiatImg4 = survey.karakteristikSubstratumTanahLiatImg4.toString()
            pathkarakteristikSubstratumTanahLiatImg5 = survey.karakteristikSubstratumTanahLiatImg5.toString()
            pathkarakteristikSubstratumTanahLiatImg6 = survey.karakteristikSubstratumTanahLiatImg6.toString()
        }

        supportActionBar?.title = "Isi Survey"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayUseLogoEnabled(false)

        initView()
        klik()
    }

    private fun initView() {
        if(pathkarakteristikSubstratumTanahLiatImg1!="") Local.isVisible(btn_karakteristikSubstratumTanahLiatImg1,btn_delete_karakteristikSubstratumTanahLiatImg1,true)
        else Local.isVisible(btn_karakteristikSubstratumTanahLiatImg1,btn_delete_karakteristikSubstratumTanahLiatImg1,false)

        if(pathkarakteristikSubstratumTanahLiatImg2!="") Local.isVisible(btn_karakteristikSubstratumTanahLiatImg2,btn_delete_karakteristikSubstratumTanahLiatImg2,true)
        else Local.isVisible(btn_karakteristikSubstratumTanahLiatImg2,btn_delete_karakteristikSubstratumTanahLiatImg2,false)

        if(pathkarakteristikSubstratumTanahLiatImg3!="") Local.isVisible(btn_karakteristikSubstratumTanahLiatImg3,btn_delete_karakteristikSubstratumTanahLiatImg3,true)
        else Local.isVisible(btn_karakteristikSubstratumTanahLiatImg3,btn_delete_karakteristikSubstratumTanahLiatImg3,false)

        if(pathkarakteristikSubstratumTanahLiatImg4!="") Local.isVisible(btn_karakteristikSubstratumTanahLiatImg4,btn_delete_karakteristikSubstratumTanahLiatImg4,true)
        else Local.isVisible(btn_karakteristikSubstratumTanahLiatImg4,btn_delete_karakteristikSubstratumTanahLiatImg4,false)

        if(pathkarakteristikSubstratumTanahLiatImg5!="") Local.isVisible(btn_karakteristikSubstratumTanahLiatImg5,btn_delete_karakteristikSubstratumTanahLiatImg5,true)
        else Local.isVisible(btn_karakteristikSubstratumTanahLiatImg5,btn_delete_karakteristikSubstratumTanahLiatImg5,false)

        if(pathkarakteristikSubstratumTanahLiatImg6!="") Local.isVisible(btn_karakteristikSubstratumTanahLiatImg6,btn_delete_karakteristikSubstratumTanahLiatImg6,true)
        else Local.isVisible(btn_karakteristikSubstratumTanahLiatImg6,btn_delete_karakteristikSubstratumTanahLiatImg6,false)
    }

    fun klik(){
        btn_simpan.setOnClickListener {
            survey.karakteristikSubstratumTanahNa = spn_karakteristikSubstratumTanahNa.selectedItem.toString()
            survey.karakteristikSubstratumTanahNaJenis = spn_karakteristikSubstratumTanahNaJenis.selectedItem.toString()
            survey.karakteristikSubstratumTanahLiatPh = txt_karakteristikSubstratumTanahPh.text.toString()
            survey.karakteristikSubstratumTanahLiatEc = txt_karakteristikSubstratumTanahEc.text.toString()

            survey.karakteristikSubstratumTanahLiatImg1 = pathkarakteristikSubstratumTanahLiatImg1
            survey.karakteristikSubstratumTanahLiatImg2 = pathkarakteristikSubstratumTanahLiatImg2
            survey.karakteristikSubstratumTanahLiatImg3 = pathkarakteristikSubstratumTanahLiatImg3
            survey.karakteristikSubstratumTanahLiatImg4 = pathkarakteristikSubstratumTanahLiatImg4
            survey.karakteristikSubstratumTanahLiatImg5 = pathkarakteristikSubstratumTanahLiatImg5
            survey.karakteristikSubstratumTanahLiatImg6 = pathkarakteristikSubstratumTanahLiatImg6

            survey.update()
            start<EntryTipeLuapanActivity> {
                putExtra("id", survey.id?.toInt())
            }

        }

        btn_kembali.setOnClickListener {
            survey.karakteristikSubstratumTanahNa = spn_karakteristikSubstratumTanahNa.selectedItem.toString()
            survey.karakteristikSubstratumTanahNaJenis = spn_karakteristikSubstratumTanahNaJenis.selectedItem.toString()
            survey.karakteristikSubstratumTanahLiatPh = txt_karakteristikSubstratumTanahPh.text.toString()
            survey.karakteristikSubstratumTanahLiatEc = txt_karakteristikSubstratumTanahEc.text.toString()

            survey.karakteristikSubstratumTanahLiatImg1 = pathkarakteristikSubstratumTanahLiatImg1
            survey.karakteristikSubstratumTanahLiatImg2 = pathkarakteristikSubstratumTanahLiatImg2
            survey.karakteristikSubstratumTanahLiatImg3 = pathkarakteristikSubstratumTanahLiatImg3
            survey.karakteristikSubstratumTanahLiatImg4 = pathkarakteristikSubstratumTanahLiatImg4
            survey.karakteristikSubstratumTanahLiatImg5 = pathkarakteristikSubstratumTanahLiatImg5
            survey.karakteristikSubstratumTanahLiatImg6 = pathkarakteristikSubstratumTanahLiatImg6

            survey.update()
            onBackPressed()
        }

        btn_karakteristikSubstratumTanahLiatImg1.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(143) }
        btn_karakteristikSubstratumTanahLiatImg2.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(144) }
        btn_karakteristikSubstratumTanahLiatImg3.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(145) }
        btn_karakteristikSubstratumTanahLiatImg4.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(146) }
        btn_karakteristikSubstratumTanahLiatImg5.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(147) }
        btn_karakteristikSubstratumTanahLiatImg6.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(148) }


        //delete image
        deleteImage(btn_delete_karakteristikSubstratumTanahLiatImg1,img_karakteristikSubstratumTanahLiatImg1,1)
        deleteImage(btn_delete_karakteristikSubstratumTanahLiatImg2,img_karakteristikSubstratumTanahLiatImg2,2)
        deleteImage(btn_delete_karakteristikSubstratumTanahLiatImg3,img_karakteristikSubstratumTanahLiatImg3,3)
        deleteImage(btn_delete_karakteristikSubstratumTanahLiatImg4,img_karakteristikSubstratumTanahLiatImg4,4)
        deleteImage(btn_delete_karakteristikSubstratumTanahLiatImg5,img_karakteristikSubstratumTanahLiatImg5,5)
        deleteImage(btn_delete_karakteristikSubstratumTanahLiatImg6,img_karakteristikSubstratumTanahLiatImg6,6)

        //navigasi
        selectNavigasi(btn_navigasi_1,1)
        selectNavigasi(btn_navigasi_2,2)
        selectNavigasi(btn_navigasi_3,3)
        selectNavigasi(btn_navigasi_4,4)
        selectNavigasi(btn_navigasi_5,5)
        selectNavigasi(btn_navigasi_6,6)
        selectNavigasi(btn_navigasi_7,7)
        selectNavigasi(btn_navigasi_8,8)
        selectNavigasi(btn_navigasi_9,9)
        selectNavigasi(btn_navigasi_10,10)
        selectNavigasi(btn_navigasi_11,11)
        selectNavigasi(btn_navigasi_12,12)
        selectNavigasi(btn_navigasi_13,13)
        selectNavigasi(btn_navigasi_14,14)
        selectNavigasi(btn_navigasi_15,15)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode==143){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_karakteristikSubstratumTanahLiatImg1.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_karakteristikSubstratumTanahLiatImg1)
                survey.karakteristikSubstratumTanahLiatImg1 = filePath
                pathkarakteristikSubstratumTanahLiatImg1 = filePath
            }
        }

        if (requestCode==144){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_karakteristikSubstratumTanahLiatImg2.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_karakteristikSubstratumTanahLiatImg2)
                survey.karakteristikSubstratumTanahLiatImg2 = filePath
                pathkarakteristikSubstratumTanahLiatImg2 = filePath
            }
        }

        if (requestCode==145){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_karakteristikSubstratumTanahLiatImg3.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_karakteristikSubstratumTanahLiatImg3)
                survey.karakteristikSubstratumTanahLiatImg3 = filePath
                pathkarakteristikSubstratumTanahLiatImg3 = filePath
            }
        }

        if (requestCode==146){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_karakteristikSubstratumTanahLiatImg4.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_karakteristikSubstratumTanahLiatImg4)
                survey.karakteristikSubstratumTanahLiatImg4 = filePath
                pathkarakteristikSubstratumTanahLiatImg4 = filePath
            }
        }

        if (requestCode==147){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_karakteristikSubstratumTanahLiatImg5.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_karakteristikSubstratumTanahLiatImg5)
                survey.karakteristikSubstratumTanahLiatImg5 = filePath
                pathkarakteristikSubstratumTanahLiatImg5 = filePath
            }
        }

        if (requestCode==148){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_karakteristikSubstratumTanahLiatImg6.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_karakteristikSubstratumTanahLiatImg6)
                survey.karakteristikSubstratumTanahLiatImg6 = filePath
                pathkarakteristikSubstratumTanahLiatImg6 = filePath
            }
        }

    }

    fun returnData(id:Int){
        if (survey.getData(id)) {

        } else {

        }
    }

    fun selectNavigasi(button: Button, code:Int){
        button.setOnClickListener {
            when(code){
                1 -> navigasiIntent(EntryKordinatActivity::class.java)
                2 -> navigasiIntent(EntryElevasiActivity::class.java)
                3 -> navigasiIntent(EntryGenanganActivity::class.java)
                4 -> navigasiIntent(EntryTutupanLahanActivity::class.java)
                5 -> navigasiIntent(EntryFloraFaunaActivity::class.java)
                6 ->  navigasiIntent(EntryDrainaseActivity::class.java)
                7 -> navigasiIntent(EntryKualitasAirTanahActivity::class.java)
                //8 -> navigasiIntent(EntryKarakteristikSubstratumTanahActivity::class.java)
                9 -> navigasiIntent(EntryTipeLuapanActivity::class.java)
                10 -> navigasiIntent(EntryKetebalanGambutActivity::class.java)
                11 -> navigasiIntent(EntryKarakteristikSubstratumDibawahLapisanGambutActivity::class.java)
                12 -> navigasiIntent(EntryTingkatKerusakanLahanActivity::class.java)
                13 -> navigasiIntent(EntryKebakaranLahanActivity::class.java)
                14 -> navigasiIntent(EntryPorositasKelengasanActivity::class.java)
                15 -> navigasiIntent(EntryDataGambarActivity::class.java)
            }
        }
    }

    private fun navigasiIntent(classtujuan: Class<*>) {
        survey.karakteristikSubstratumTanahNa = spn_karakteristikSubstratumTanahNa.selectedItem.toString()
        survey.karakteristikSubstratumTanahNaJenis = spn_karakteristikSubstratumTanahNaJenis.selectedItem.toString()
        survey.karakteristikSubstratumTanahLiatPh = txt_karakteristikSubstratumTanahPh.text.toString()
        survey.karakteristikSubstratumTanahLiatEc = txt_karakteristikSubstratumTanahEc.text.toString()

        survey.karakteristikSubstratumTanahLiatImg1 = pathkarakteristikSubstratumTanahLiatImg1
        survey.karakteristikSubstratumTanahLiatImg2 = pathkarakteristikSubstratumTanahLiatImg2
        survey.karakteristikSubstratumTanahLiatImg3 = pathkarakteristikSubstratumTanahLiatImg3
        survey.karakteristikSubstratumTanahLiatImg4 = pathkarakteristikSubstratumTanahLiatImg4
        survey.karakteristikSubstratumTanahLiatImg5 = pathkarakteristikSubstratumTanahLiatImg5
        survey.karakteristikSubstratumTanahLiatImg6 = pathkarakteristikSubstratumTanahLiatImg6
        survey.update()
        val intent = Intent(activity,classtujuan)
        intent.putExtra("id", survey.id?.toInt())
        startActivity(intent)

    }

    fun deleteImage(button: ImageButton, imageView: ImageView, code: Int){
        button.setOnClickListener {
            when(code){
                1-> pathkarakteristikSubstratumTanahLiatImg1 = ""
                2-> pathkarakteristikSubstratumTanahLiatImg2 = ""
                3-> pathkarakteristikSubstratumTanahLiatImg3 = ""
                4-> pathkarakteristikSubstratumTanahLiatImg4 = ""
                5-> pathkarakteristikSubstratumTanahLiatImg5 = ""
                6-> pathkarakteristikSubstratumTanahLiatImg6 = ""
            }

            Glide.with(activity).load("").into(imageView)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
    override fun onResume() {
        if(id != null){
            survey.getData(id)
        }
        super.onResume()
    }

}