package synergy.project.tallysheet.feature.survey.detail.tab

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_surveyfoto_tab.view.*
import synergy.project.tallysheet.R
import synergy.project.tallysheet.model.Survey

class SurveyFotoTabFragment : Fragment() {

    lateinit var root : View
    var nomorBoring:String?=null
    var id_survey = 0
    lateinit var survey: Survey
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_surveyfoto_tab, container, false)
        survey = Survey(requireContext())
        survey.getData(id_survey)

        initView()

        return  root
    }

    private fun initView() {
        /**
        Glide.with(requireContext()).load(survey.imgBatasTimur).into(root.img_batas_timur)
        Glide.with(requireContext()).load(survey.imgBatasSelatan).into(root.img_batas_selatan)
        Glide.with(requireContext()).load(survey.imgBatasBarat).into(root.img_batas_barat)
        Glide.with(requireContext()).load(survey.imgBatasUtara).into(root.img_batas_utara)
        Glide.with(requireContext()).load(survey.imgSketsa).into(root.img_sketsa)
        **/
    }


}