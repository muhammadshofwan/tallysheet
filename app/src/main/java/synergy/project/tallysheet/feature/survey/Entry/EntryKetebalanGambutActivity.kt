package synergy.project.tallysheet.feature.survey.Entry

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import kotlinx.android.synthetic.main.layout_genangan.*
import kotlinx.android.synthetic.main.layout_header.btn_simpan
import kotlinx.android.synthetic.main.layout_karakteristik_substratum.*
import kotlinx.android.synthetic.main.layout_ketebalan_gambut.*
import kotlinx.android.synthetic.main.layout_ketebalan_gambut.btn_kembali
import kotlinx.android.synthetic.main.layout_ketebalan_gambut.btn_navigasi_1
import kotlinx.android.synthetic.main.layout_ketebalan_gambut.btn_navigasi_10
import kotlinx.android.synthetic.main.layout_ketebalan_gambut.btn_navigasi_11
import kotlinx.android.synthetic.main.layout_ketebalan_gambut.btn_navigasi_12
import kotlinx.android.synthetic.main.layout_ketebalan_gambut.btn_navigasi_13
import kotlinx.android.synthetic.main.layout_ketebalan_gambut.btn_navigasi_14
import kotlinx.android.synthetic.main.layout_ketebalan_gambut.btn_navigasi_15
import kotlinx.android.synthetic.main.layout_ketebalan_gambut.btn_navigasi_2
import kotlinx.android.synthetic.main.layout_ketebalan_gambut.btn_navigasi_3
import kotlinx.android.synthetic.main.layout_ketebalan_gambut.btn_navigasi_4
import kotlinx.android.synthetic.main.layout_ketebalan_gambut.btn_navigasi_5
import kotlinx.android.synthetic.main.layout_ketebalan_gambut.btn_navigasi_6
import kotlinx.android.synthetic.main.layout_ketebalan_gambut.btn_navigasi_7
import kotlinx.android.synthetic.main.layout_ketebalan_gambut.btn_navigasi_8
import kotlinx.android.synthetic.main.layout_ketebalan_gambut.btn_navigasi_9
import kotlinx.android.synthetic.main.layout_koordinat_titik_survey.btn_kordinatImg1
import kotlinx.android.synthetic.main.layout_koordinat_titik_survey.btn_kordinatImg2
import kotlinx.android.synthetic.main.layout_koordinat_titik_survey.btn_kordinatImg3
import kotlinx.android.synthetic.main.layout_koordinat_titik_survey.btn_kordinatImg4
import splitties.activities.start
import splitties.toast.toast
import synergy.project.tallysheet.R
import synergy.project.tallysheet.model.Survey
import synergy.project.tallysheet.utils.DatePickerFragment
import synergy.project.tallysheet.utils.Helper
import synergy.project.tallysheet.utils.Local
import synergy.project.tallysheet.utils.PrefManager

class  EntryKetebalanGambutActivity : AppCompatActivity() {
    lateinit var survey: Survey
    lateinit var prefManager: PrefManager
    var id : Int = 0

    var pathketebalanGambutImg1 = ""
    var pathketebalanGambutImg2 = ""
    var pathketebalanGambutImg3 = ""
    var pathketebalanGambutImg4 = ""
    var pathketebalanGambutImg5 = ""
    var pathketebalanGambutImg6 = ""

    val activity = EntryKetebalanGambutActivity@this


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_ketebalan_gambut)
        val bundle = intent.extras
        if (bundle != null) {
            id = bundle["id"] as Int
        }
        survey = Survey(applicationContext)
        survey.getData(id)
        prefManager = PrefManager(applicationContext)

        // ini untuk ini value spinner
        val dataAdapter_ketebalanGambutJenis: ArrayAdapter<String> = ArrayAdapter<String>(
            this@EntryKetebalanGambutActivity,// ganti jadi this@Nama Activity
            android.R.layout.simple_dropdown_item_1line,
            Helper.ketebalanGambutJenis
        )

        val dataAdapter_keteblanGambutNaJenis: ArrayAdapter<String> = ArrayAdapter<String>(
            this@EntryKetebalanGambutActivity,// ganti jadi this@Nama Activity
            android.R.layout.simple_dropdown_item_1line,
            Helper.ketebalanGambutNaJenis
        )

        val dataAdapter_ketebalanGambutNa: ArrayAdapter<String> = ArrayAdapter<String>(
            this@EntryKetebalanGambutActivity,// ganti jadi this@Nama Activity
            android.R.layout.simple_dropdown_item_1line,
            Helper.ketebalanGambutNa
        )

        spn_ketebalanGambutJenis.adapter = dataAdapter_ketebalanGambutJenis
        spn_ketebalanGambutNaJenis.adapter = dataAdapter_keteblanGambutNaJenis
        spn_ketebalanGambutNa.adapter = dataAdapter_ketebalanGambutNa

        if (survey.getData(id)) {
            val ketebalanGambutNaAdp = spn_ketebalanGambutNa.adapter as ArrayAdapter<String>
            val ketebalanGambutNaPosition = ketebalanGambutNaAdp.getPosition(survey.ketebalanGambutNa)
            spn_ketebalanGambutNa.setSelection(ketebalanGambutNaPosition)
            val ketebalanGambutNaJenisAdp = spn_ketebalanGambutNaJenis.adapter as ArrayAdapter<String>
            val ketebalanGambutNaJenisPosition = ketebalanGambutNaJenisAdp.getPosition(survey.ketebalanGambutNaJenis)
            spn_ketebalanGambutNaJenis.setSelection(ketebalanGambutNaJenisPosition)
            txt_ketebalanGambutTinggi.setText(survey.ketebalanGambutTinggi)
            txt_ketebalanGambut.setText(survey.ketebalanGambut)
            val ketebalanGambutJenisAdp = spn_ketebalanGambutJenis.adapter as ArrayAdapter<String>
            val ketebalanGambutJenisPosition = ketebalanGambutJenisAdp.getPosition(survey.ketebalanGambutJenis)
            spn_ketebalanGambutJenis.setSelection(ketebalanGambutJenisPosition)


            Glide.with(activity).load(survey.ketebalanGambutImg1).into(img_ketebalanGambut1)
            Glide.with(activity).load(survey.ketebalanGambutImg2).into(img_ketebalanGambutImg2)
            Glide.with(activity).load(survey.ketebalanGambutImg3).into(img_ketebalanGambut3)
            Glide.with(activity).load(survey.ketebalanGambutImg4).into(img_ketebalanGambutImg4)
            Glide.with(activity).load(survey.ketebalanGambutImg5).into(img_ketebalanGambutImg5)
            Glide.with(activity).load(survey.ketebalanGambutImg6).into(img_ketebalanGambutImg6)

            pathketebalanGambutImg1 = survey.ketebalanGambutImg1.toString()
            pathketebalanGambutImg2 = survey.ketebalanGambutImg2.toString()
            pathketebalanGambutImg3 = survey.ketebalanGambutImg3.toString()
            pathketebalanGambutImg4 = survey.ketebalanGambutImg4.toString()
            pathketebalanGambutImg5 = survey.ketebalanGambutImg5.toString()
            pathketebalanGambutImg6 = survey.ketebalanGambutImg6.toString()
        }



        supportActionBar?.title = "Isi Survey"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayUseLogoEnabled(false)

        initView()
        klik()
    }

    private fun initView() {
        if(pathketebalanGambutImg1!="") Local.isVisible(btn_ketebalanGambut1,btn_delete_ketebalanGambut1,true)
        else Local.isVisible(btn_ketebalanGambut1,btn_delete_ketebalanGambut1,false)

        if(pathketebalanGambutImg2!="") Local.isVisible(btn_ketebalanGambut2,btn_delete_ketebalanGambut2,true)
        else Local.isVisible(btn_ketebalanGambut2,btn_delete_ketebalanGambut2,false)

        if(pathketebalanGambutImg3!="") Local.isVisible(btn_ketebalanGambut3,btn_delete_btn_ketebalanGambut3,true)
        else Local.isVisible(btn_ketebalanGambut3,btn_delete_btn_ketebalanGambut3,false)

        if(pathketebalanGambutImg4!="") Local.isVisible(btn_ketebalanGambutImg4,btn_delete_ketebalanGambutImg4,true)
        else Local.isVisible(btn_ketebalanGambutImg4,btn_delete_ketebalanGambutImg4,false)

        if(pathketebalanGambutImg5!="") Local.isVisible(btn_ketebalanGambutImg5,btn_delete_ketebalanGambutImg5,true)
        else Local.isVisible(btn_ketebalanGambutImg5,btn_delete_ketebalanGambutImg5,false)

        if(pathketebalanGambutImg6!="") Local.isVisible(btn_ketebalanGambutImg6,btn_delete_ketebalanGambutImg6,true)
        else Local.isVisible(btn_ketebalanGambutImg6,btn_delete_ketebalanGambutImg6,false)
    }

    fun klik(){
        btn_simpan.setOnClickListener {
            survey.ketebalanGambut = txt_ketebalanGambut.text.toString()
            survey.ketebalanGambutNa = spn_ketebalanGambutNa.selectedItem.toString()
            survey.ketebalanGambutNaJenis = spn_ketebalanGambutNaJenis.selectedItem.toString()
            survey.ketebalanGambutTinggi = txt_ketebalanGambutTinggi.text.toString()
            survey.ketebalanGambutJenis = spn_ketebalanGambutJenis.selectedItem.toString()

            survey.ketebalanGambutImg1 = pathketebalanGambutImg1
            survey.ketebalanGambutImg2 = pathketebalanGambutImg2
            survey.ketebalanGambutImg3 = pathketebalanGambutImg3
            survey.ketebalanGambutImg4 = pathketebalanGambutImg4
            survey.ketebalanGambutImg5 = pathketebalanGambutImg5
            survey.ketebalanGambutImg6 = pathketebalanGambutImg6

            survey.update()
            start<EntryKarakteristikSubstratumDibawahLapisanGambutActivity> {
                putExtra("id", survey.id?.toInt())
            }

        }

        btn_kembali.setOnClickListener {
            survey.ketebalanGambut = txt_ketebalanGambut.text.toString()
            survey.ketebalanGambutNa = spn_ketebalanGambutNa.selectedItem.toString()
            survey.ketebalanGambutNaJenis = spn_ketebalanGambutNaJenis.selectedItem.toString()
            survey.ketebalanGambutTinggi = txt_ketebalanGambutTinggi.text.toString()
            survey.ketebalanGambutJenis = spn_ketebalanGambutJenis.selectedItem.toString()

            survey.ketebalanGambutImg1 = pathketebalanGambutImg1
            survey.ketebalanGambutImg2 = pathketebalanGambutImg2
            survey.ketebalanGambutImg3 = pathketebalanGambutImg3
            survey.ketebalanGambutImg4 = pathketebalanGambutImg4
            survey.ketebalanGambutImg5 = pathketebalanGambutImg5
            survey.ketebalanGambutImg6 = pathketebalanGambutImg6

            survey.update()
            onBackPressed()
        }

        btn_ketebalanGambut1.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(155) }
        btn_ketebalanGambut2.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(156) }
        btn_ketebalanGambut3.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(157) }
        btn_ketebalanGambutImg4.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(158) }
        btn_ketebalanGambutImg5.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(159) }
        btn_ketebalanGambutImg6.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(160) }

        //delete image
        deleteImage(btn_delete_ketebalanGambut1,img_ketebalanGambut1,1)
        deleteImage(btn_delete_ketebalanGambut2,img_ketebalanGambutImg2,2)
        deleteImage(btn_delete_btn_ketebalanGambut3,img_ketebalanGambut3,3)
        deleteImage(btn_delete_ketebalanGambutImg4,img_ketebalanGambutImg4,4)
        deleteImage(btn_delete_ketebalanGambutImg5,img_ketebalanGambutImg5,5)
        deleteImage(btn_delete_ketebalanGambutImg6,img_ketebalanGambutImg6,6)


        //navigasi
        selectNavigasi(btn_navigasi_1,1)
        selectNavigasi(btn_navigasi_2,2)
        selectNavigasi(btn_navigasi_3,3)
        selectNavigasi(btn_navigasi_4,4)
        selectNavigasi(btn_navigasi_5,5)
        selectNavigasi(btn_navigasi_6,6)
        selectNavigasi(btn_navigasi_7,7)
        selectNavigasi(btn_navigasi_8,8)
        selectNavigasi(btn_navigasi_9,9)
        selectNavigasi(btn_navigasi_10,10)
        selectNavigasi(btn_navigasi_11,11)
        selectNavigasi(btn_navigasi_12,12)
        selectNavigasi(btn_navigasi_13,13)
        selectNavigasi(btn_navigasi_14,14)
        selectNavigasi(btn_navigasi_15,15)

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode==155){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_ketebalanGambut1.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_ketebalanGambut1)
                survey.ketebalanGambutImg1 = filePath
                pathketebalanGambutImg1 = filePath
            }
        }

        if (requestCode==156){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_ketebalanGambutImg2.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_ketebalanGambutImg2)
                survey.ketebalanGambutImg2 = filePath
                pathketebalanGambutImg2 = filePath
            }
        }

        if (requestCode==157){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_ketebalanGambut3.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_ketebalanGambut3)
                survey.ketebalanGambutImg3 =  filePath
                pathketebalanGambutImg3 = filePath
            }
        }

        if (requestCode==158){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_ketebalanGambutImg4.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_ketebalanGambutImg4)
                survey.ketebalanGambutImg4 = filePath
                pathketebalanGambutImg4 = filePath
            }
        }

        if (requestCode==159){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_ketebalanGambutImg5.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_ketebalanGambutImg5)
                survey.ketebalanGambutImg5 = filePath
                pathketebalanGambutImg5 = filePath
            }
        }

        if (requestCode==160){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_ketebalanGambutImg6.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_ketebalanGambutImg6)
                survey.ketebalanGambutImg6 = filePath
                pathketebalanGambutImg6 = filePath
            }
        }

    }

    fun returnData(id:Int){
        if (survey.getData(id)) {

        } else {

        }
    }

    fun selectNavigasi(button: Button, code:Int){
        button.setOnClickListener {
            when(code){
                1 -> navigasiIntent(EntryKordinatActivity::class.java)
                2 -> navigasiIntent(EntryElevasiActivity::class.java)
                3 -> navigasiIntent(EntryGenanganActivity::class.java)
                4 -> navigasiIntent(EntryTutupanLahanActivity::class.java)
                5 -> navigasiIntent(EntryFloraFaunaActivity::class.java)
                6 ->  navigasiIntent(EntryDrainaseActivity::class.java)
                7 -> navigasiIntent(EntryKualitasAirTanahActivity::class.java)
                8 -> navigasiIntent(EntryKarakteristikSubstratumTanahActivity::class.java)
                9 -> navigasiIntent(EntryTipeLuapanActivity::class.java)
                //10 -> navigasiIntent(EntryKetebalanGambutActivity::class.java)
                11 -> navigasiIntent(EntryKarakteristikSubstratumDibawahLapisanGambutActivity::class.java)
                12 -> navigasiIntent(EntryTingkatKerusakanLahanActivity::class.java)
                13 -> navigasiIntent(EntryKebakaranLahanActivity::class.java)
                14 -> navigasiIntent(EntryPorositasKelengasanActivity::class.java)
                15 -> navigasiIntent(EntryDataGambarActivity::class.java)
            }
        }
    }

    private fun navigasiIntent(classtujuan: Class<*>) {
        survey.ketebalanGambut = txt_ketebalanGambut.text.toString()
        survey.ketebalanGambutNa = spn_ketebalanGambutNa.selectedItem.toString()
        survey.ketebalanGambutNaJenis = spn_ketebalanGambutNaJenis.selectedItem.toString()
        survey.ketebalanGambutTinggi = txt_ketebalanGambutTinggi.text.toString()
        survey.ketebalanGambutJenis = spn_ketebalanGambutJenis.selectedItem.toString()

        survey.ketebalanGambutImg1 = pathketebalanGambutImg1
        survey.ketebalanGambutImg2 = pathketebalanGambutImg2
        survey.ketebalanGambutImg3 = pathketebalanGambutImg3
        survey.ketebalanGambutImg4 = pathketebalanGambutImg4
        survey.ketebalanGambutImg5 = pathketebalanGambutImg5
        survey.ketebalanGambutImg6 = pathketebalanGambutImg6

        survey.update()
        val intent = Intent(activity,classtujuan)
        intent.putExtra("id", survey.id?.toInt())
        startActivity(intent)

    }

    fun deleteImage(button: ImageButton, imageView: ImageView, code: Int){
        button.setOnClickListener {
            when(code){
                1-> pathketebalanGambutImg1 = ""
                2-> pathketebalanGambutImg2 = ""
                3-> pathketebalanGambutImg3 = ""
                4-> pathketebalanGambutImg4 = ""
                5-> pathketebalanGambutImg5 = ""
                6-> pathketebalanGambutImg6 = ""
            }

            Glide.with(activity).load("").into(imageView)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
    override fun onResume() {
        if(id != null){
            survey.getData(id)
        }
        super.onResume()
    }

}