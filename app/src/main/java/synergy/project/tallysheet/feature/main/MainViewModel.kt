package synergy.project.tallysheet.feature.main

import android.app.Activity
import android.content.Intent
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.androidnetworking.interfaces.UploadProgressListener
import okhttp3.OkHttpClient
import org.json.JSONObject
import splitties.alertdialog.alertDialog
import splitties.alertdialog.message
import splitties.alertdialog.okButton
import splitties.alertdialog.title
import splitties.toast.toast
import synergy.project.tallysheet.R
import synergy.project.tallysheet.model.Survey
import synergy.project.tallysheet.utils.Helper
import synergy.project.tallysheet.utils.PrefManager
import synergy.project.tallysheet.utils.Progress
import java.io.File
import java.util.concurrent.TimeUnit


class MainViewModel: ViewModel() {
    lateinit var activity: Activity
    lateinit var prefManager: PrefManager
    lateinit var progress: Progress
    lateinit var survey: Survey
    lateinit var mainActivity: MainActivity
    lateinit var surveyData: Survey

    var status_ = MutableLiveData<String>()
    var host = ""
    val params = HashMap<String, String>()
    val paramImage1 = HashMap<String, File>()
    val paramImage2 = HashMap<String, File>()
    val paramImage3 = HashMap<String, File>()
    val paramImage4 = HashMap<String, File>()
    val paramImage5 = HashMap<String, File>()
    val paramImage6 = HashMap<String, File>()
    val paramImage7 = HashMap<String, File>()
    val paramImage8 = HashMap<String, File>()
    val paramImage9 = HashMap<String, File>()
    val paramImage10 = HashMap<String, File>()
    val paramImage11 = HashMap<String, File>()
    val paramImage12 = HashMap<String, File>()
    val paramImage13 = HashMap<String, File>()
    val paramImage14 = HashMap<String, File>()
    val paramImage15 = HashMap<String, File>()
    val okHttpClient = OkHttpClient().newBuilder()
        .connectTimeout(60, TimeUnit.SECONDS)
        .readTimeout(60, TimeUnit.SECONDS)
        .writeTimeout(60, TimeUnit.SECONDS)
        .build()

    fun setData(activity: Activity, mainActivity: MainActivity){
        this.activity = activity
        this.mainActivity=mainActivity
        prefManager = PrefManager(activity.applicationContext)
        progress = Progress(activity)
        survey = Survey(activity.applicationContext)
        AndroidNetworking.initialize(activity.applicationContext)

    }

    fun downloadData(){
        progress.progress("Download Data")

        val host = "${Helper.host}surveys?access-token=${prefManager.token}"

        AndroidNetworking.get(host).setPriority(Priority.HIGH).setTag(activity).setOkHttpClient(this.okHttpClient)
            .build().getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    progress.dismiss()
                    val jsonArray = response?.getJSONArray("items")
                    survey.importData(jsonArray!!)

                    status_.postValue("1")
                    activity.alertDialog {
                        title = "Berhasil"
                        message = "download data berhasil"
                        setIcon(R.drawable.ic_success)
                        okButton {
                            activity.finish()
                            activity.overridePendingTransition(0, 0)
                            activity.startActivity(activity.intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION))
                            activity.overridePendingTransition(0, 0)
                            //   mainActivity.initView()
                            it.dismiss()
                        }
                    }.show()
                }

                override fun onError(anError: ANError?) {
                    progress.dismiss()
                    status_.postValue(null)
                  //  errorHandler(anError!!, " ")

                    if (anError!!.errorCode!=0){
                        dialogError("Error Upload Data Server ","Gangguan pada server " + anError!!.errorDetail  + " " + anError.errorBody + anError.errorCode + " " + anError.message)
                    }else{
                        if (anError!!.errorDetail == "connectionError"){
                            dialogError("Error Upload Data Server ","Gangguan Jaringan, Silahkan cek jaringan anda " + anError!!.errorDetail  + " " + anError.errorBody + anError.errorCode + " " + anError.message)
                        }else{
                            dialogError("Error Upload Data Server ",
                                    "Silahkan coba lagi "+ anError.errorDetail  + " " + anError!!.errorBody + anError.errorCode + " " + anError.message)
                        }
                    }
                }

            })
    }

    fun uploadData(postionUpload: Int){
        progress.progress("Upload Data")
        var data = ArrayList<HashMap<String, String>>()
        survey = Survey(activity.applicationContext)
        data = survey.getForUpload()
        if (data.size > 0){
            val dataSurvey = data[postionUpload]
            var id = dataSurvey["id"] as String
            surveyData = Survey(activity.applicationContext)
            surveyData.getData(id.toInt())


            host = "${Helper.host}surveys/${id}?access-token=${prefManager.token}"

            params["id"] = surveyData.id!!
            params["nama_khg"] = surveyData.nama_khg!!
            params["nomorBoring"] = surveyData.nomorBoring!!
            params["tanggal"] = surveyData.tanggal!!
            params["provinsi"] = surveyData.provinsi!!
            params["kota_kabupaten"] = surveyData.kota_kabupaten!!
            params["kecamatan"] = surveyData.kecamatan!!
            params["kelurahan"] = surveyData.kelurahan!!
            params["jenis_tanah"] = surveyData.jenis_tanah!!
            params["titik_p10"] = surveyData.titik_p10!!
            params["sample_p10"] = surveyData.sample_p10!!
            params["jenis_sample"] = surveyData.jenis_sample!!
            params["longitude"] = surveyData.longitude!!
            params["latitude"] = surveyData.latitude!!
            params["longitude_target"] = surveyData.longitude_target!!
            params["latitude_target"] = surveyData.latitude_target!!
            params["longitude_hp"] = surveyData.longitude_hp!!
            params["latitude_hp"] = surveyData.latitude_hp!!
            params["elevasi"] = surveyData.elevasi!!
            params["kedalamanAirTanah"] = surveyData.kedalamanAirTanah!!
            params["genangan"] = surveyData.genangan!!
            params["banjirBulan"] = surveyData.banjirBulan!!
            params["banjirLamanya"] = surveyData.banjirLamanya!!
            params["banjirKetinggianAir"] = surveyData.banjirKetinggianAir!!
            params["banjirSumberGenangan"] = surveyData.banjirSumberGenangan!!
            params["tutupanLahanJenisTanaman"] = surveyData.tutupanLahanJenisTanaman!!

            params["banjirSumberGenangan1"] = surveyData.banjirSumberGenangan1!!
            params["banjirSumberGenangan2"] = surveyData.banjirSumberGenangan2!!
            params["banjirSumberGenangan3"] = surveyData.banjirSumberGenangan3!!
            params["banjirSumberGenangan4"] = surveyData.banjirSumberGenangan4!!
            params["banjirSumberGenanganLainnya"] = surveyData.banjirSumberGenanganLainnya!!


            params["tutupanLahanStatus"] = surveyData.tutupanLahanStatus!!
            params["tutupanLahanNamaPerusahaan"] = surveyData.tutupanLahanNamaPerusahaan!!
            params["tutupanLahanLuas"] = surveyData.tutupanLahanLuas!!
            params["tutupanLahanPenggunaanLahan"] = surveyData.tutupanLahanPenggunaanLahan!!
            params["flora"] = surveyData.flora!!
            params["floraJenis"] = surveyData.floraJenis!!
            params["fauna"] = surveyData.fauna!!
            params["faunaJenis"] = surveyData.faunaJenis!!
            params["drainaseAlami"] = surveyData.drainaseAlami!!
            params["drainaseBuatan"] = surveyData.drainaseBuatan!!
            params["drainaseBuatanJenis"] = surveyData.drainaseBuatanJenis!!
            params["drainaseBuatanTinggiMukaAir"] = surveyData.drainaseBuatanTinggiMukaAir!!
            params["kualitasAirTanahPh"] = surveyData.kualitasAirTanahPh!!
            params["kualitasAirTanahEc"] = surveyData.kualitasAirTanahEc!!
            params["kualitasAirTanahTds"] = surveyData.kualitasAirTanahTds!!
            params["karakteristikSubstratumTanahNa"] = surveyData.karakteristikSubstratumTanahNa!!
            params["karakteristikSubstratumTanahNaJenis"] = surveyData.karakteristikSubstratumTanahNaJenis!!
            params["karakteristikSubstratumTanahLiatPh"] = surveyData.karakteristikSubstratumTanahLiatPh!!
            params["karakteristikSubstratumTanahLiatEc"] = surveyData.karakteristikSubstratumTanahLiatEc!!

            params["kualitasAirTanahPhAt"] = surveyData.kualitasAirTanahPhAt!!
            params["kualitasAirTanahPhAs"] = surveyData.kualitasAirTanahPhAs!!
            params["kualitasAirTanahEcAt"] = surveyData.kualitasAirTanahEcAt!!
            params["kualitasAirTanahEcAs"] = surveyData.kualitasAirTanahEcAs!!
            params["kualitasAirTanahTdsAt"] = surveyData.kualitasAirTanahTdsAt!!
            params["kualitasAirTanahTdsAs"] = surveyData.kualitasAirTanahTdsAs!!

            params["tipeLuapanKemarau"] = surveyData.tipeLuapanKemarau!!
            params["tipeLuapanHujan"] = surveyData.tipeLuapanHujan!!
            params["ketebalanGambut"] = surveyData.ketebalanGambut!!
            params["ketebalanGambutNa"] = surveyData.ketebalanGambutNa!!
            params["ketebalanGambutNaJenis"] = surveyData.ketebalanGambutNaJenis!!
            params["ketebalanGambutTinggi"] = surveyData.ketebalanGambutTinggi!!
            params["ketebalanGambutJenis"] = surveyData.ketebalanGambutJenis!!
            params["karakteristikSubstratumDibawahLapisanGambutNa"] = surveyData.karakteristikSubstratumDibawahLapisanGambutNa!!
            params["karakteristikSubstratumDibawahLapisanGambutNaJenis"] = surveyData.karakteristikSubstratumDibawahLapisanGambutNaJenis!!
            params["karakteristikSubstratumDibawahLapisanGambutJenis"] = surveyData.karakteristikSubstratumDibawahLapisanGambutJenis!!
            params["karakteristikSubstratumDibawahLapisanGambutJenis1"] = surveyData.karakteristikSubstratumDibawahLapisanGambutJenis1!!
            params["karakteristikSubstratumDibawahLapisanGambutJenis2"] = surveyData.karakteristikSubstratumDibawahLapisanGambutJenis2!!
            params["karakteristikSubstratumDibawahLapisanGambutJenis3"] = surveyData.karakteristikSubstratumDibawahLapisanGambutJenis3!!
            params["karakteristikSubstratumDibawahLapisanGambutJenis4"] = surveyData.karakteristikSubstratumDibawahLapisanGambutJenis4!!
            params["karakteristikSubstratumDibawahLapisanGambutJenis5"] = surveyData.karakteristikSubstratumDibawahLapisanGambutJenis5!!

            params["karakteristikSubstratumDibawahLapisanGambutLainnya"] = surveyData.karakteristikSubstratumDibawahLapisanGambutLainnya!!
            params["tingkatKerusakanLahanGambutDrainaseBuatan"] = surveyData.tingkatKerusakanLahanGambutDrainaseBuatan!!
            params["tingkatKerusakanLahanGambutSedimen"] = surveyData.tingkatKerusakanLahanGambutSedimen!!
            params["tingkatKerusakanLahanKondisiTanaman"] = surveyData.tingkatKerusakanLahanKondisiTanaman!!

            params["tingkatKerusakanLahanKondisiTanaman1"] = surveyData.tingkatKerusakanLahanKondisiTanaman1!!
            params["tingkatKerusakanLahanKondisiTanaman2"] = surveyData.tingkatKerusakanLahanKondisiTanaman2!!
            params["tingkatKerusakanLahanKondisiTanaman3"] = surveyData.tingkatKerusakanLahanKondisiTanaman3!!
            params["tingkatKerusakanLahanKondisiTanaman4"] = surveyData.tingkatKerusakanLahanKondisiTanaman4!!

            params["tingkatKerusakanLahanSubsiden"] = surveyData.tingkatKerusakanLahanSubsiden!!
            params["tingkatKerusakanLahanKerapatanTajuk"] = surveyData.tingkatKerusakanLahanKerapatanTajuk!!
            params["kebakaranLahanTahun"] = surveyData.kebakaranLahanTahun!!
            params["kebakaranLahanBulan"] = surveyData.kebakaranLahanBulan!!
            params["kebakaranLahanTanggal"] = surveyData.kebakaranLahanTanggal!!
            params["kebakaranLahanLamaKejadian"] = surveyData.kebakaranLahanLamaKejadian!!
            params["kebakaranLahanLamaKejadianHariMInggu"] = surveyData.kebakaranLahanLamaKejadianHariMInggu!!
            params["kebakaranLahanUpayaPemadamanSwadayaMasyarakat"] = surveyData.kebakaranLahanUpayaPemadamanSwadayaMasyarakat!!
            params["kebakaranLahanUpayaPemadamanPemerintah"] = surveyData.kebakaranLahanUpayaPemadamanPemerintah!!
            params["hujanTerakhir"] = surveyData.hujanTerakhir!!
            params["hujanLamaKejadian"] = surveyData.hujanLamaKejadian!!
            params["hujanLamaKejadianJamHari"] = surveyData.hujanLamaKejadianJamHari!!
            params["hujanIntensitas"] = surveyData.hujanIntensitas!!
            params["porositasKelengasanNa"] = surveyData.porositasKelengasanNa!!
            params["porositasKelengasanNaJenis"] = surveyData.porositasKelengasanNaJenis!!
            params["porositasKelengasan1"] = surveyData.porositasKelengasan1!!
            params["porositasKelengasan2"] = surveyData.porositasKelengasan2!!
            params["porositasKelengasanKeterangan"] = surveyData.porositasKelengasanKeterangan!!
            params["keterangan"] = surveyData.keterangan!!
            params["sketsaLokasi"] = surveyData.sketsaLokasi!!
            params["imgBatasBarat"] = surveyData.imgBatasBarat!!
            params["imgBatasTimur"] = surveyData.imgBatasTimur!!
            params["imgBatasSelatan"] = surveyData.imgBatasSelatan!!
            params["imgBatasUtara"] = surveyData.imgBatasUtara!!
            params["dusun"]  = surveyData.dusun!!

            if(surveyData.kordinatImg1!="" || surveyData.kordinatImg1!="null" || surveyData.kordinatImg1!=null){
                if(File(surveyData.kordinatImg1).exists()){
                    paramImage1["kordinatImg1"] = File(surveyData.kordinatImg1)

                }
            }
            if(surveyData.kordinatImg2!="" || surveyData.kordinatImg2!="null" || surveyData.kordinatImg2!=null){
                if(File(surveyData.kordinatImg2).exists()){
                    paramImage1["kordinatImg2"] = File(surveyData.kordinatImg2)

                }
            }
            if(surveyData.kordinatImg3!="" || surveyData.kordinatImg3!="null" || surveyData.kordinatImg3!=null){
                if(File(surveyData.kordinatImg3).exists()){
                    paramImage1["kordinatImg3"] = File(surveyData.kordinatImg3)

                }
            }
            if(surveyData.kordinatImg4!="" || surveyData.kordinatImg4!="null" || surveyData.kordinatImg4!=null){
                if(File(surveyData.kordinatImg4).exists()){
                    paramImage1["kordinatImg4"] = File(surveyData.kordinatImg4)

                }
            }
            if(surveyData.kordinatImg5!="" || surveyData.kordinatImg5!="null" || surveyData.kordinatImg5!=null){
                if(File(surveyData.kordinatImg5).exists()){
                    paramImage1["kordinatImg5"] = File(surveyData.kordinatImg5)

                }
            }
            if(surveyData.kordinatImg6!="" || surveyData.kordinatImg6!="null" || surveyData.kordinatImg6!=null){
                if(File(surveyData.kordinatImg6).exists()){
                    paramImage1["kordinatImg6"] = File(surveyData.kordinatImg6)

                }
            }
            if(surveyData.elevasiImg1!="" || surveyData.elevasiImg1!="null" || surveyData.elevasiImg1!=null){
                if(File(surveyData.elevasiImg1).exists()){
                    paramImage2["elevasiImg1"] = File(surveyData.elevasiImg1)

                }
            }
            if(surveyData.elevasiImg2!="" || surveyData.elevasiImg2!="null" || surveyData.elevasiImg2!=null){
                if(File(surveyData.elevasiImg2).exists()){
                    paramImage2["elevasiImg2"] = File(surveyData.elevasiImg2)

                }
            }
            if(surveyData.elevasiImg3!="" || surveyData.elevasiImg3!="null" || surveyData.elevasiImg3!=null){
                if(File(surveyData.elevasiImg3).exists()){
                    paramImage2["elevasiImg3"] = File(surveyData.elevasiImg3)

                }
            }
            if(surveyData.elevasiImg4!="" || surveyData.elevasiImg4!="null" || surveyData.elevasiImg4!=null){
                if(File(surveyData.elevasiImg4).exists()){
                    paramImage2["elevasiImg4"] = File(surveyData.elevasiImg4)

                }
            }
            if(surveyData.elevasiImg5!="" || surveyData.elevasiImg5!="null" || surveyData.elevasiImg5!=null){
                if(File(surveyData.elevasiImg5).exists()){
                    paramImage4["elevasiImg5"] = File(surveyData.elevasiImg5)

                }
            }
            if(surveyData.elevasiImg6!="" || surveyData.elevasiImg6!="null" || surveyData.elevasiImg6!=null){
                if(File(surveyData.elevasiImg6).exists()){
                    paramImage4["elevasiImg6"] = File(surveyData.elevasiImg6)

                }
            }
            if(surveyData.banjirImg1!="" || surveyData.banjirImg1!="null" || surveyData.banjirImg1!=null){
                if(File(surveyData.banjirImg1).exists()){
                    paramImage3["banjirImg1"] = File(surveyData.banjirImg1)

                }
            }
            if(surveyData.banjirImg2!="" || surveyData.banjirImg2!="null" || surveyData.banjirImg2!=null){
                if(File(surveyData.banjirImg2).exists()){
                    paramImage3["banjirImg2"] = File(surveyData.banjirImg2)

                }
            }
            if(surveyData.banjirImg3!="" || surveyData.banjirImg3!="null" || surveyData.banjirImg3!=null){
                if(File(surveyData.banjirImg3).exists()){
                    paramImage3["banjirImg3"] = File(surveyData.banjirImg3)

                }
            }
            if(surveyData.banjirImg4!="" || surveyData.banjirImg4!="null" || surveyData.banjirImg4!=null){
                if(File(surveyData.banjirImg4).exists()){
                    paramImage3["banjirImg4"] = File(surveyData.banjirImg4)

                }
            }
            if(surveyData.banjirImg5!="" || surveyData.banjirImg5!="null" || surveyData.banjirImg5!=null){
                if(File(surveyData.banjirImg5).exists()){
                    paramImage3["banjirImg5"] = File(surveyData.banjirImg5)

                }
            }
            if(surveyData.banjirImg6!="" || surveyData.banjirImg6!="null" || surveyData.banjirImg6!=null){
                if(File(surveyData.banjirImg6).exists()){
                    paramImage3["banjirImg6"] = File(surveyData.banjirImg6)

                }
            }
            if(surveyData.tutupanLahanImg1!="" || surveyData.tutupanLahanImg1!="null" || surveyData.tutupanLahanImg1!=null){
                if(File(surveyData.tutupanLahanImg1).exists()){
                    paramImage4["tutupanLahanImg1"] = File(surveyData.tutupanLahanImg1)

                }
            }
            if(surveyData.tutupanLahanImg2!="" || surveyData.tutupanLahanImg2!="null" || surveyData.tutupanLahanImg2!=null){
                if(File(surveyData.tutupanLahanImg2).exists()){
                    paramImage4["tutupanLahanImg2"] = File(surveyData.tutupanLahanImg2)

                }
            }
            if(surveyData.tutupanLahanImg3!="" || surveyData.tutupanLahanImg3!="null" || surveyData.tutupanLahanImg3!=null){
                if(File(surveyData.tutupanLahanImg3).exists()){
                    paramImage4["tutupanLahanImg3"] = File(surveyData.tutupanLahanImg3)

                }
            }
            if(surveyData.tutupanLahanImg4!="" || surveyData.tutupanLahanImg4!="null" || surveyData.tutupanLahanImg4!=null){
                if(File(surveyData.tutupanLahanImg4).exists()){
                    paramImage4["tutupanLahanImg4"] = File(surveyData.tutupanLahanImg4)

                }
            }
            if(surveyData.tutupanLahanImg5!="" || surveyData.tutupanLahanImg5!="null" || surveyData.tutupanLahanImg5!=null){
                if(File(surveyData.tutupanLahanImg5).exists()){
                    paramImage4["tutupanLahanImg5"] = File(surveyData.tutupanLahanImg5)

                }
            }
            if(surveyData.tutupanLahanImg6!="" || surveyData.tutupanLahanImg6!="null" || surveyData.tutupanLahanImg6!=null){
                if(File(surveyData.tutupanLahanImg6).exists()){
                    paramImage4["tutupanLahanImg6"] = File(surveyData.tutupanLahanImg6)

                }
            }
            if(surveyData.floraFaunaImg1!="" || surveyData.floraFaunaImg1!="null" || surveyData.floraFaunaImg1!=null){
                if(File(surveyData.floraFaunaImg1).exists()){
                    paramImage5["floraFaunaImg1"] = File(surveyData.floraFaunaImg1)

                }
            }
            if(surveyData.floraFaunaImg2!="" || surveyData.floraFaunaImg2!="null" || surveyData.floraFaunaImg2!=null){
                if(File(surveyData.floraFaunaImg2).exists()){
                    paramImage5["floraFaunaImg2"] = File(surveyData.floraFaunaImg2)

                }
            }
            if(surveyData.floraFaunaImg3!="" || surveyData.floraFaunaImg3!="null" || surveyData.floraFaunaImg3!=null){
                if(File(surveyData.floraFaunaImg3).exists()){
                    paramImage5["floraFaunaImg3"] = File(surveyData.floraFaunaImg3)

                }
            }
            if(surveyData.floraFaunaImg4!="" || surveyData.floraFaunaImg4!="null" || surveyData.floraFaunaImg4!=null){
                if(File(surveyData.floraFaunaImg4).exists()){
                    paramImage5["floraFaunaImg4"] = File(surveyData.floraFaunaImg4)

                }
            }
            if(surveyData.floraFaunaImg5!="" || surveyData.floraFaunaImg5!="null" || surveyData.floraFaunaImg5!=null){
                if(File(surveyData.floraFaunaImg5).exists()){
                    paramImage5["floraFaunaImg5"] = File(surveyData.floraFaunaImg5)

                }
            }
            if(surveyData.floraFaunaImg6!="" || surveyData.floraFaunaImg6!="null" || surveyData.floraFaunaImg6!=null){
                if(File(surveyData.floraFaunaImg6).exists()){
                    paramImage5["floraFaunaImg6"] = File(surveyData.floraFaunaImg6)

                }
            }
            if(surveyData.drainaseImg1!="" || surveyData.drainaseImg1!="null" || surveyData.drainaseImg1!=null){
                if(File(surveyData.drainaseImg1).exists()){
                    paramImage6["drainaseImg1"] = File(surveyData.drainaseImg1)

                }
            }
            if(surveyData.drainaseImg2!="" || surveyData.drainaseImg2!="null" || surveyData.drainaseImg2!=null){
                if(File(surveyData.drainaseImg2).exists()){
                    paramImage6["drainaseImg2"] = File(surveyData.drainaseImg2)

                }
            }
            if(surveyData.drainaseImg3!="" || surveyData.drainaseImg3!="null" || surveyData.drainaseImg3!=null){
                if(File(surveyData.drainaseImg3).exists()){
                    paramImage6["drainaseImg3"] = File(surveyData.drainaseImg3)

                }
            }
            if(surveyData.drainaseImg4!="" || surveyData.drainaseImg4!="null" || surveyData.drainaseImg4!=null){
                if(File(surveyData.drainaseImg4).exists()){
                    paramImage6["drainaseImg4"] = File(surveyData.drainaseImg4)

                }
            }
            if(surveyData.drainaseImg5!="" || surveyData.drainaseImg5!="null" || surveyData.drainaseImg5!=null){
                if(File(surveyData.drainaseImg5).exists()){
                    paramImage6["drainaseImg5"] = File(surveyData.drainaseImg5)

                }
            }
            if(surveyData.drainaseImg6!="" || surveyData.drainaseImg6!="null" || surveyData.drainaseImg6!=null){
                if(File(surveyData.drainaseImg6).exists()){
                    paramImage6["drainaseImg6"] = File(surveyData.drainaseImg6)

                }
            }
            if(surveyData.kualitasiAirTanahImg1!="" || surveyData.kualitasiAirTanahImg1!="null" || surveyData.kualitasiAirTanahImg1!=null){
                if(File(surveyData.kualitasiAirTanahImg1).exists()){
                    paramImage7["kualitasiAirTanahImg1"] = File(surveyData.kualitasiAirTanahImg1)

                }
            }
            if(surveyData.kualitasiAirTanahImg2!="" || surveyData.kualitasiAirTanahImg2!="null" || surveyData.kualitasiAirTanahImg2!=null){
                if(File(surveyData.kualitasiAirTanahImg2).exists()){
                    paramImage7["kualitasiAirTanahImg2"] = File(surveyData.kualitasiAirTanahImg2)

                }
            }
            if(surveyData.kualitasiAirTanahImg3!="" || surveyData.kualitasiAirTanahImg3!="null" || surveyData.kualitasiAirTanahImg3!=null){
                if(File(surveyData.kualitasiAirTanahImg3).exists()){
                    paramImage7["kualitasiAirTanahImg3"] = File(surveyData.kualitasiAirTanahImg3)

                }
            }
            if(surveyData.kualitasiAirTanahImg4!="" || surveyData.kualitasiAirTanahImg4!="null" || surveyData.kualitasiAirTanahImg4!=null){
                if(File(surveyData.kualitasiAirTanahImg4).exists()){
                    paramImage7["kualitasiAirTanahImg4"] = File(surveyData.kualitasiAirTanahImg4)

                }
            }
            if(surveyData.kualitasiAirTanahImg5!="" || surveyData.kualitasiAirTanahImg5!="null" || surveyData.kualitasiAirTanahImg5!=null){
                if(File(surveyData.kualitasiAirTanahImg5).exists()){
                    paramImage7["kualitasiAirTanahImg5"] = File(surveyData.kualitasiAirTanahImg5)

                }
            }
            if(surveyData.kualitasiAirTanahImg6!="" || surveyData.kualitasiAirTanahImg6!="null" || surveyData.kualitasiAirTanahImg6!=null){
                if(File(surveyData.kualitasiAirTanahImg6).exists()){
                    paramImage7["kualitasiAirTanahImg6"] = File(surveyData.kualitasiAirTanahImg6)

                }
            }
            if(surveyData.karakteristikSubstratumTanahLiatImg1!="" || surveyData.karakteristikSubstratumTanahLiatImg1!="null" || surveyData.karakteristikSubstratumTanahLiatImg1!=null){
                if(File(surveyData.karakteristikSubstratumTanahLiatImg1).exists()){
                    paramImage8["karakteristikSubstratumTanahLiatImg1"] = File(surveyData.karakteristikSubstratumTanahLiatImg1)

                }
            }
            if(surveyData.karakteristikSubstratumTanahLiatImg2!="" || surveyData.karakteristikSubstratumTanahLiatImg2!="null" || surveyData.karakteristikSubstratumTanahLiatImg2!=null){
                if(File(surveyData.karakteristikSubstratumTanahLiatImg2).exists()){
                    paramImage8["karakteristikSubstratumTanahLiatImg2"] = File(surveyData.karakteristikSubstratumTanahLiatImg2)

                }
            }
            if(surveyData.karakteristikSubstratumTanahLiatImg3!="" || surveyData.karakteristikSubstratumTanahLiatImg3!="null" || surveyData.karakteristikSubstratumTanahLiatImg3!=null){
                if(File(surveyData.karakteristikSubstratumTanahLiatImg3).exists()){
                    paramImage8["karakteristikSubstratumTanahLiatImg3"] = File(surveyData.karakteristikSubstratumTanahLiatImg3)

                }
            }
            if(surveyData.karakteristikSubstratumTanahLiatImg4!="" || surveyData.karakteristikSubstratumTanahLiatImg4!="null" || surveyData.karakteristikSubstratumTanahLiatImg4!=null){
                if(File(surveyData.karakteristikSubstratumTanahLiatImg4).exists()){
                    paramImage8["karakteristikSubstratumTanahLiatImg4"] = File(surveyData.karakteristikSubstratumTanahLiatImg4)

                }
            }
            if(surveyData.karakteristikSubstratumTanahLiatImg5!="" || surveyData.karakteristikSubstratumTanahLiatImg5!="null" || surveyData.karakteristikSubstratumTanahLiatImg5!=null){
                if(File(surveyData.karakteristikSubstratumTanahLiatImg5).exists()){
                    paramImage8["karakteristikSubstratumTanahLiatImg5"] = File(surveyData.karakteristikSubstratumTanahLiatImg5)

                }
            }
            if(surveyData.karakteristikSubstratumTanahLiatImg6!="" || surveyData.karakteristikSubstratumTanahLiatImg6!="null" || surveyData.karakteristikSubstratumTanahLiatImg6!=null){
                if(File(surveyData.karakteristikSubstratumTanahLiatImg6).exists()){
                    paramImage8["karakteristikSubstratumTanahLiatImg6"] = File(surveyData.karakteristikSubstratumTanahLiatImg6)

                }
            }
            if(surveyData.tipeLuapanImg1!="" || surveyData.tipeLuapanImg1!="null" || surveyData.tipeLuapanImg1!=null){
                if(File(surveyData.tipeLuapanImg1).exists()){
                    paramImage9["tipeLuapanImg1"] = File(surveyData.tipeLuapanImg1)

                }
            }
            if(surveyData.tipeLuapanImg2!="" || surveyData.tipeLuapanImg2!="null" || surveyData.tipeLuapanImg2!=null){
                if(File(surveyData.tipeLuapanImg2).exists()){
                    paramImage9["tipeLuapanImg2"] = File(surveyData.tipeLuapanImg2)

                }
            }
            if(surveyData.tipeLuapanImg3!="" || surveyData.tipeLuapanImg3!="null" || surveyData.tipeLuapanImg3!=null){
                if(File(surveyData.tipeLuapanImg3).exists()){
                    paramImage9["tipeLuapanImg3"] = File(surveyData.tipeLuapanImg3)

                }
            }
            if(surveyData.tipeLuapanImg4!="" || surveyData.tipeLuapanImg4!="null" || surveyData.tipeLuapanImg4!=null){
                if(File(surveyData.tipeLuapanImg4).exists()){
                    paramImage9["tipeLuapanImg4"] = File(surveyData.tipeLuapanImg4)

                }
            }
            if(surveyData.tipeLuapanImg5!="" || surveyData.tipeLuapanImg5!="null" || surveyData.tipeLuapanImg5!=null){
                if(File(surveyData.tipeLuapanImg5).exists()){
                    paramImage9["tipeLuapanImg5"] = File(surveyData.tipeLuapanImg5)

                }
            }
            if(surveyData.tipeLuapanImg6!="" || surveyData.tipeLuapanImg6!="null" || surveyData.tipeLuapanImg6!=null){
                if(File(surveyData.tipeLuapanImg6).exists()){
                    paramImage9["tipeLuapanImg6"] = File(surveyData.tipeLuapanImg6)

                }
            }
            if(surveyData.ketebalanGambutImg1!="" || surveyData.ketebalanGambutImg1!="null" || surveyData.ketebalanGambutImg1!=null){
                if(File(surveyData.ketebalanGambutImg1).exists()){
                    paramImage10["ketebalanGambutImg1"] = File(surveyData.ketebalanGambutImg1)

                }
            }
            if(surveyData.ketebalanGambutImg2!="" || surveyData.ketebalanGambutImg2!="null" || surveyData.ketebalanGambutImg2!=null){
                if(File(surveyData.ketebalanGambutImg2).exists()){
                    paramImage10["ketebalanGambutImg2"] = File(surveyData.ketebalanGambutImg2)

                }
            }
            if(surveyData.ketebalanGambutImg3!="" || surveyData.ketebalanGambutImg3!="null" || surveyData.ketebalanGambutImg3!=null){
                if(File(surveyData.ketebalanGambutImg3).exists()){
                    paramImage10["ketebalanGambutImg3"] = File(surveyData.ketebalanGambutImg3)

                }
            }
            if(surveyData.ketebalanGambutImg4!="" || surveyData.ketebalanGambutImg4!="null" || surveyData.ketebalanGambutImg4!=null){
                if(File(surveyData.ketebalanGambutImg4).exists()){
                    paramImage10["ketebalanGambutImg4"] = File(surveyData.ketebalanGambutImg4)

                }
            }
            if(surveyData.ketebalanGambutImg5!="" || surveyData.ketebalanGambutImg5!="null" || surveyData.ketebalanGambutImg5!=null){
                if(File(surveyData.ketebalanGambutImg5).exists()){
                    paramImage10["ketebalanGambutImg5"] = File(surveyData.ketebalanGambutImg5)

                }
            }
            if(surveyData.ketebalanGambutImg6!="" || surveyData.ketebalanGambutImg6!="null" || surveyData.ketebalanGambutImg6!=null){
                if(File(surveyData.ketebalanGambutImg6).exists()){
                    paramImage10["ketebalanGambutImg6"] = File(surveyData.ketebalanGambutImg6)

                }
            }
            if(surveyData.karakteristikSubstratumDibawahLapisanGambutImg1!="" || surveyData.karakteristikSubstratumDibawahLapisanGambutImg1!="null" || surveyData.karakteristikSubstratumDibawahLapisanGambutImg1!=null){
                if(File(surveyData.karakteristikSubstratumDibawahLapisanGambutImg1).exists()){
                    paramImage11["karakteristikSubstratumDibawahLapisanGambutImg1"] = File(surveyData.karakteristikSubstratumDibawahLapisanGambutImg1)

                }
            }
            if(surveyData.karakteristikSubstratumDibawahLapisanGambutImg2!="" || surveyData.karakteristikSubstratumDibawahLapisanGambutImg2!="null" || surveyData.karakteristikSubstratumDibawahLapisanGambutImg2!=null){
                if(File(surveyData.karakteristikSubstratumDibawahLapisanGambutImg2).exists()){
                    paramImage11["karakteristikSubstratumDibawahLapisanGambutImg2"] = File(surveyData.karakteristikSubstratumDibawahLapisanGambutImg2)

                }
            }
            if(surveyData.karakteristikSubstratumDibawahLapisanGambutImg3!="" || surveyData.karakteristikSubstratumDibawahLapisanGambutImg3!="null" || surveyData.karakteristikSubstratumDibawahLapisanGambutImg3!=null){
                if(File(surveyData.karakteristikSubstratumDibawahLapisanGambutImg3).exists()){
                    paramImage11["karakteristikSubstratumDibawahLapisanGambutImg3"] = File(surveyData.karakteristikSubstratumDibawahLapisanGambutImg3)

                }
            }
            if(surveyData.karakteristikSubstratumDibawahLapisanGambutImg4!="" || surveyData.karakteristikSubstratumDibawahLapisanGambutImg4!="null" || surveyData.karakteristikSubstratumDibawahLapisanGambutImg4!=null){
                if(File(surveyData.karakteristikSubstratumDibawahLapisanGambutImg4).exists()){
                    paramImage11["karakteristikSubstratumDibawahLapisanGambutImg4"] = File(surveyData.karakteristikSubstratumDibawahLapisanGambutImg4)

                }
            }
            if(surveyData.karakteristikSubstratumDibawahLapisanGambutImg5!="" || surveyData.karakteristikSubstratumDibawahLapisanGambutImg5!="null" || surveyData.karakteristikSubstratumDibawahLapisanGambutImg5!=null){
                if(File(surveyData.karakteristikSubstratumDibawahLapisanGambutImg5).exists()){
                    paramImage11["karakteristikSubstratumDibawahLapisanGambutImg5"] = File(surveyData.karakteristikSubstratumDibawahLapisanGambutImg5)

                }
            }
            if(surveyData.karakteristikSubstratumDibawahLapisanGambutImg6!="" || surveyData.karakteristikSubstratumDibawahLapisanGambutImg6!="null" || surveyData.karakteristikSubstratumDibawahLapisanGambutImg6!=null){
                if(File(surveyData.karakteristikSubstratumDibawahLapisanGambutImg6).exists()){
                    paramImage11["karakteristikSubstratumDibawahLapisanGambutImg6"] = File(surveyData.karakteristikSubstratumDibawahLapisanGambutImg6)

                }
            }
            if(surveyData.tingkatKerusakanLahanImg1!="" || surveyData.tingkatKerusakanLahanImg1!="null" || surveyData.tingkatKerusakanLahanImg1!=null){
                if(File(surveyData.tingkatKerusakanLahanImg1).exists()){
                    paramImage12["tingkatKerusakanLahanImg1"] = File(surveyData.tingkatKerusakanLahanImg1)

                }
            }
            if(surveyData.tingkatKerusakanLahanImg2!="" || surveyData.tingkatKerusakanLahanImg2!="null" || surveyData.tingkatKerusakanLahanImg2!=null){
                if(File(surveyData.tingkatKerusakanLahanImg2).exists()){
                    paramImage12["tingkatKerusakanLahanImg2"] = File(surveyData.tingkatKerusakanLahanImg2)

                }
            }
            if(surveyData.tingkatKerusakanLahanImg3!="" || surveyData.tingkatKerusakanLahanImg3!="null" || surveyData.tingkatKerusakanLahanImg3!=null){
                if(File(surveyData.tingkatKerusakanLahanImg3).exists()){
                    paramImage12["tingkatKerusakanLahanImg3"] = File(surveyData.tingkatKerusakanLahanImg3)

                }
            }
            if(surveyData.tingkatKerusakanLahanImg4!="" || surveyData.tingkatKerusakanLahanImg4!="null" || surveyData.tingkatKerusakanLahanImg4!=null){
                if(File(surveyData.tingkatKerusakanLahanImg4).exists()){
                    paramImage12["tingkatKerusakanLahanImg4"] = File(surveyData.tingkatKerusakanLahanImg4)

                }
            }
            if(surveyData.tingkatKerusakanLahanImg5!="" || surveyData.tingkatKerusakanLahanImg5!="null" || surveyData.tingkatKerusakanLahanImg5!=null){
                if(File(surveyData.tingkatKerusakanLahanImg5).exists()){
                    paramImage12["tingkatKerusakanLahanImg5"] = File(surveyData.tingkatKerusakanLahanImg5)

                }
            }
            if(surveyData.tingkatKerusakanLahanImg6!="" || surveyData.tingkatKerusakanLahanImg6!="null" || surveyData.tingkatKerusakanLahanImg6!=null){
                if(File(surveyData.tingkatKerusakanLahanImg6).exists()){
                    paramImage12["tingkatKerusakanLahanImg6"] = File(surveyData.tingkatKerusakanLahanImg6)

                }
            }
            if(surveyData.kebakaranHujanImg1!="" || surveyData.kebakaranHujanImg1!="null" || surveyData.kebakaranHujanImg1!=null){
                if(File(surveyData.kebakaranHujanImg1).exists()){
                    paramImage13["kebakaranHujanImg1"] = File(surveyData.kebakaranHujanImg1)

                }
            }
            if(surveyData.kebakaranHujanImg2!="" || surveyData.kebakaranHujanImg2!="null" || surveyData.kebakaranHujanImg2!=null){
                if(File(surveyData.kebakaranHujanImg2).exists()){
                    paramImage13["kebakaranHujanImg2"] = File(surveyData.kebakaranHujanImg2)

                }
            }
            if(surveyData.kebakaranHujanImg3!="" || surveyData.kebakaranHujanImg3!="null" || surveyData.kebakaranHujanImg3!=null){
                if(File(surveyData.kebakaranHujanImg3).exists()){
                    paramImage13["kebakaranHujanImg3"] = File(surveyData.kebakaranHujanImg3)

                }
            }
            if(surveyData.kebakaranHujanImg4!="" || surveyData.kebakaranHujanImg4!="null" || surveyData.kebakaranHujanImg4!=null){
                if(File(surveyData.kebakaranHujanImg4).exists()){
                    paramImage13["kebakaranHujanImg4"] = File(surveyData.kebakaranHujanImg4)

                }
            }
            if(surveyData.kebakaranHujanImg5!="" || surveyData.kebakaranHujanImg5!="null" || surveyData.kebakaranHujanImg5!=null){
                if(File(surveyData.kebakaranHujanImg5).exists()){
                    paramImage13["kebakaranHujanImg5"] = File(surveyData.kebakaranHujanImg5)

                }
            }
            if(surveyData.kebakaranHujanImg6!="" || surveyData.kebakaranHujanImg6!="null" || surveyData.kebakaranHujanImg6!=null){
                if(File(surveyData.kebakaranHujanImg6).exists()){
                    paramImage13["kebakaranHujanImg6"] = File(surveyData.kebakaranHujanImg6)

                }
            }
            if(surveyData.porositasKelengasanImg1!="" || surveyData.porositasKelengasanImg1!="null" || surveyData.porositasKelengasanImg1!=null){
                if(File(surveyData.porositasKelengasanImg1).exists()){
                    paramImage14["porositasKelengasanImg1"] = File(surveyData.porositasKelengasanImg1)

                }
            }
            if(surveyData.porositasKelengasanImg2!="" || surveyData.porositasKelengasanImg2!="null" || surveyData.porositasKelengasanImg2!=null){
                if(File(surveyData.porositasKelengasanImg2).exists()){
                    paramImage14["porositasKelengasanImg2"] = File(surveyData.porositasKelengasanImg2)

                }
            }
            if(surveyData.porositasKelengasanImg3!="" || surveyData.porositasKelengasanImg3!="null" || surveyData.porositasKelengasanImg3!=null){
                if(File(surveyData.porositasKelengasanImg3).exists()){
                    paramImage14["porositasKelengasanImg3"] = File(surveyData.porositasKelengasanImg3)

                }
            }
            if(surveyData.porositasKelengasanImg4!="" || surveyData.porositasKelengasanImg4!="null" || surveyData.porositasKelengasanImg4!=null){
                if(File(surveyData.porositasKelengasanImg4).exists()){
                    paramImage14["porositasKelengasanImg4"] = File(surveyData.porositasKelengasanImg4)

                }
            }
            if(surveyData.porositasKelengasanImg5!="" || surveyData.porositasKelengasanImg5!="null" || surveyData.porositasKelengasanImg5!=null){
                if(File(surveyData.porositasKelengasanImg5).exists()){
                    paramImage14["porositasKelengasanImg5"] = File(surveyData.porositasKelengasanImg5)

                }
            }
            if(surveyData.porositasKelengasanImg6!="" || surveyData.porositasKelengasanImg6!="null" || surveyData.porositasKelengasanImg6!=null){
                if(File(surveyData.porositasKelengasanImg6).exists()){
                    paramImage14["porositasKelengasanImg6"] = File(surveyData.porositasKelengasanImg6)

                }
            }
            if(surveyData.sketsaLokasi!="" || surveyData.sketsaLokasi!="null" || surveyData.sketsaLokasi!=null){
                if(File(surveyData.sketsaLokasi).exists()){
                    paramImage15["sketsaLokasi"] = File(surveyData.sketsaLokasi)

                }
            }
            if(surveyData.imgBatasBarat!="" || surveyData.imgBatasBarat!="null" || surveyData.imgBatasBarat!=null){
                if(File(surveyData.imgBatasBarat).exists()){
                    paramImage15["imgBatasBarat"] = File(surveyData.imgBatasBarat)

                }
            }
            if(surveyData.imgBatasTimur!="" || surveyData.imgBatasTimur!="null" || surveyData.imgBatasTimur!=null){
                if(File(surveyData.imgBatasTimur).exists()){
                    paramImage15["imgBatasTimur"] = File(surveyData.imgBatasTimur)

                }
            }
            if(surveyData.imgBatasSelatan!="" || surveyData.imgBatasSelatan!="null" || surveyData.imgBatasSelatan!=null){
                if(File(surveyData.imgBatasSelatan).exists()){
                    paramImage15["imgBatasSelatan"] = File(surveyData.imgBatasSelatan)

                }
            }
            if(surveyData.imgBatasUtara!="" || surveyData.imgBatasUtara!="null" || surveyData.imgBatasUtara!=null){
                if(File(surveyData.imgBatasUtara).exists()){
                    paramImage15["imgBatasUtara"] = File(surveyData.imgBatasUtara)

                }
            }

            networkingUploadData()
        }else{
            progress.dismiss()
            //survey.deleteAll()
            activity.alertDialog {
                title = "Berhasil"
                message = "Data telah terupload ke server"
                setIcon(R.drawable.ic_success)
                okButton {
                    activity.finish()
                    activity.overridePendingTransition(0, 0)
                    activity.startActivity(activity.intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION))
                    activity.overridePendingTransition(0, 0)
                    //   mainActivity.initView()
                    it.dismiss()
                }
            }.show()
        }
    }

    fun networkingUpload(
        host: String,
        params: HashMap<String, String>,
        paramImage: HashMap<String, File>,
        postionUpload: Int,
        data: ArrayList<HashMap<String, String>>
    ) {
        AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartParameter(params).addMultipartFile(paramImage)
            .setPriority(Priority.HIGH).setTag(activity).build()
            .setUploadProgressListener(object : UploadProgressListener {
                override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                }
            })
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    val dataSurvey = data[postionUpload]
                    var id = dataSurvey["id"] as String
                    val surveyData = Survey(activity.applicationContext)
                    surveyData.getData(id.toInt())
                    surveyData.status = 2
                    surveyData.update()

                    if (postionUpload < data.size) {
                        var position = postionUpload
                        position++
                        uploadData(position)
                    } else {
                        progress.dismiss()
                        //survey.deleteAll()
                        activity.alertDialog {
                            title = "Berhasil"
                            message = "Data telah terupload ke server"
                            okButton {
                                // listDaftarbarangFragment.init();
                                activity.finish()
                                activity.overridePendingTransition(0, 0)
                                activity.startActivity(activity.intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION))
                                activity.overridePendingTransition(0, 0)
                                it.dismiss()
                            }
                        }.show()
                    }


                }

                override fun onError(anError: ANError?) {
                    progress.dismiss()
                  /*  activity.finish()
                    activity.overridePendingTransition(0, 0)
                    activity.startActivity(activity.intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION))
                    activity.overridePendingTransition(0, 0)
                    toast("Gagal Upload, Silahkan coba beberapa saat lagi")*/

                    errorHandler(anError!!,"Error Upload Data")
                }
            })
    }

    fun networkingUploadData() {
        AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartParameter(params)
            .setPriority(Priority.HIGH).setTag(activity).build()
            .setUploadProgressListener(object : UploadProgressListener {
                override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                }
            })
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    uploadkordinatImg1()
                }

                override fun onError(anError: ANError?) {
                    progress.dismiss()
                    //survey.deleteAll()
                   /* activity.alertDialog {
                        title = "Error"
                        message =
                            "Error upload data " + anError!!.message + anError!!.errorDetail + " " + anError!!.errorBody + " " + anError!!.errorCode
                        setIcon(R.drawable.ic_failed)
                        okButton {
                            activity.finish()
                            activity.overridePendingTransition(0, 0)
                            activity.startActivity(activity.intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION))
                            activity.overridePendingTransition(0, 0)
                            //   mainActivity.initView()
                            it.dismiss()
                        }
                    }.show()*/

                    errorHandler(anError!!, "Error Upload Data")
                }
            })
    }

    fun uploadkordinatImg1(){

        if (surveyData.kordinatImg1!="" && surveyData.kordinatImg1!=null){
            if(File(surveyData.kordinatImg1.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar kordinatImg1")

                val paramkordinatImg1 = HashMap<String, File>()
                paramkordinatImg1["kordinatImg1"] = File(surveyData.kordinatImg1.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramkordinatImg1)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadkordinatImg2()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar kordinatImg1")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar kordinatImg1 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadkordinatImg2()
        }
    }
    fun uploadkordinatImg2(){

        if (surveyData.kordinatImg2!="" && surveyData.kordinatImg2!=null){
            if(File(surveyData.kordinatImg2.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar kordinatImg2")

                val paramkordinatImg2 = HashMap<String, File>()
                paramkordinatImg2["kordinatImg2"] = File(surveyData.kordinatImg2.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramkordinatImg2)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadkordinatImg3()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar kordinatImg2")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar kordinatImg2 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadkordinatImg3()
        }
    }
    fun uploadkordinatImg3(){

        if (surveyData.kordinatImg3!="" && surveyData.kordinatImg3!=null){
            if(File(surveyData.kordinatImg3.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar kordinatImg3")

                val paramkordinatImg3 = HashMap<String, File>()
                paramkordinatImg3["kordinatImg3"] = File(surveyData.kordinatImg3.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramkordinatImg3)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadkordinatImg4()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar kordinatImg3")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar kordinatImg3 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadkordinatImg4()
        }
    }
    fun uploadkordinatImg4(){

        if (surveyData.kordinatImg4!="" && surveyData.kordinatImg4!=null){
            if(File(surveyData.kordinatImg4.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar kordinatImg4")

                val paramkordinatImg4 = HashMap<String, File>()
                paramkordinatImg4["kordinatImg4"] = File(surveyData.kordinatImg4.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramkordinatImg4)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadkordinatImg5()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar kordinatImg4")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar kordinatImg4 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadkordinatImg5()
        }
    }
    fun uploadkordinatImg5(){

        if (surveyData.kordinatImg5!="" && surveyData.kordinatImg5!=null){
            if(File(surveyData.kordinatImg5.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar kordinatImg5")

                val paramkordinatImg5 = HashMap<String, File>()
                paramkordinatImg5["kordinatImg5"] = File(surveyData.kordinatImg5.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramkordinatImg5)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadkordinatImg6()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar kordinatImg5")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar kordinatImg5 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadkordinatImg6()
        }
    }
    fun uploadkordinatImg6(){

        if (surveyData.kordinatImg6!="" && surveyData.kordinatImg6!=null){
            if(File(surveyData.kordinatImg6.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar kordinatImg6")

                val paramkordinatImg6 = HashMap<String, File>()
                paramkordinatImg6["kordinatImg6"] = File(surveyData.kordinatImg6.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramkordinatImg6)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadelevasiImg1()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar kordinatImg6")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar kordinatImg6 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadelevasiImg1()
        }
    }
    fun uploadelevasiImg1(){

        if (surveyData.elevasiImg1!="" && surveyData.elevasiImg1!=null){
            if(File(surveyData.elevasiImg1.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar elevasiImg1")

                val paramelevasiImg1 = HashMap<String, File>()
                paramelevasiImg1["elevasiImg1"] = File(surveyData.elevasiImg1.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramelevasiImg1)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadelevasiImg2()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar elevasiImg1")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar elevasiImg1 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadelevasiImg2()
        }
    }
    fun uploadelevasiImg2(){

        if (surveyData.elevasiImg2!="" && surveyData.elevasiImg2!=null){
            if(File(surveyData.elevasiImg2.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar elevasiImg2")

                val paramelevasiImg2 = HashMap<String, File>()
                paramelevasiImg2["elevasiImg2"] = File(surveyData.elevasiImg2.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramelevasiImg2)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadelevasiImg3()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar elevasiImg2")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar elevasiImg2 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadelevasiImg3()
        }
    }
    fun uploadelevasiImg3(){

        if (surveyData.elevasiImg3!="" && surveyData.elevasiImg3!=null){
            if(File(surveyData.elevasiImg3.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar elevasiImg3")

                val paramelevasiImg3 = HashMap<String, File>()
                paramelevasiImg3["elevasiImg3"] = File(surveyData.elevasiImg3.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramelevasiImg3)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadelevasiImg4()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar elevasiImg3")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar elevasiImg3 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadelevasiImg4()
        }
    }
    fun uploadelevasiImg4(){

        if (surveyData.elevasiImg4!="" && surveyData.elevasiImg4!=null){
            if(File(surveyData.elevasiImg4.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar elevasiImg4")

                val paramelevasiImg4 = HashMap<String, File>()
                paramelevasiImg4["elevasiImg4"] = File(surveyData.elevasiImg4.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramelevasiImg4)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadelevasiImg5()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar elevasiImg4")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar elevasiImg4 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadelevasiImg5()
        }
    }
    fun uploadelevasiImg5(){

        if (surveyData.elevasiImg5!="" && surveyData.elevasiImg5!=null){
            if(File(surveyData.elevasiImg5.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar elevasiImg5")

                val paramelevasiImg5 = HashMap<String, File>()
                paramelevasiImg5["elevasiImg5"] = File(surveyData.elevasiImg5.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramelevasiImg5)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadelevasiImg6()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar elevasiImg5")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar elevasiImg5 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadelevasiImg6()
        }
    }
    fun uploadelevasiImg6(){

        if (surveyData.elevasiImg6!="" && surveyData.elevasiImg6!=null){
            if(File(surveyData.elevasiImg6.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar elevasiImg6")

                val paramelevasiImg6 = HashMap<String, File>()
                paramelevasiImg6["elevasiImg6"] = File(surveyData.elevasiImg6.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramelevasiImg6)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadbanjirImg1()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar elevasiImg6")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar elevasiImg6 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadbanjirImg1()
        }
    }
    fun uploadbanjirImg1(){

        if (surveyData.banjirImg1!="" && surveyData.banjirImg1!=null){
            if(File(surveyData.banjirImg1.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar banjirImg1")

                val parambanjirImg1 = HashMap<String, File>()
                parambanjirImg1["banjirImg1"] = File(surveyData.banjirImg1.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(parambanjirImg1)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadbanjirImg2()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar banjirImg1")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar banjirImg1 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadbanjirImg2()
        }
    }
    fun uploadbanjirImg2(){

        if (surveyData.banjirImg2!="" && surveyData.banjirImg2!=null){
            if(File(surveyData.banjirImg2.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar banjirImg2")

                val parambanjirImg2 = HashMap<String, File>()
                parambanjirImg2["banjirImg2"] = File(surveyData.banjirImg2.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(parambanjirImg2)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadbanjirImg3()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar banjirImg2")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar banjirImg2 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadbanjirImg3()
        }
    }
    fun uploadbanjirImg3(){

        if (surveyData.banjirImg3!="" && surveyData.banjirImg3!=null){
            if(File(surveyData.banjirImg3.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar banjirImg3")

                val parambanjirImg3 = HashMap<String, File>()
                parambanjirImg3["banjirImg3"] = File(surveyData.banjirImg3.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(parambanjirImg3)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadbanjirImg4()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar banjirImg3")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar banjirImg3 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadbanjirImg4()
        }
    }
    fun uploadbanjirImg4(){

        if (surveyData.banjirImg4!="" && surveyData.banjirImg4!=null){
            if(File(surveyData.banjirImg4.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar banjirImg4")

                val parambanjirImg4 = HashMap<String, File>()
                parambanjirImg4["banjirImg4"] = File(surveyData.banjirImg4.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(parambanjirImg4)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadbanjirImg5()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar banjirImg4")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar banjirImg4 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadbanjirImg5()
        }
    }
    fun uploadbanjirImg5(){

        if (surveyData.banjirImg5!="" && surveyData.banjirImg5!=null){
            if(File(surveyData.banjirImg5.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar banjirImg5")

                val parambanjirImg5 = HashMap<String, File>()
                parambanjirImg5["banjirImg5"] = File(surveyData.banjirImg5.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(parambanjirImg5)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadbanjirImg6()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar banjirImg5")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar banjirImg5 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadbanjirImg6()
        }
    }
    fun uploadbanjirImg6(){

        if (surveyData.banjirImg6!="" && surveyData.banjirImg6!=null){
            if(File(surveyData.banjirImg6.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar banjirImg6")

                val parambanjirImg6 = HashMap<String, File>()
                parambanjirImg6["banjirImg6"] = File(surveyData.banjirImg6.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(parambanjirImg6)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadtutupanLahanImg1()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar banjirImg6")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar banjirImg6 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadtutupanLahanImg1()
        }
    }
    fun uploadtutupanLahanImg1(){

        if (surveyData.tutupanLahanImg1!="" && surveyData.tutupanLahanImg1!=null){
            if(File(surveyData.tutupanLahanImg1.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar tutupanLahanImg1")

                val paramtutupanLahanImg1 = HashMap<String, File>()
                paramtutupanLahanImg1["tutupanLahanImg1"] = File(surveyData.tutupanLahanImg1.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramtutupanLahanImg1)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadtutupanLahanImg2()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar tutupanLahanImg1")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar tutupanLahanImg1 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadtutupanLahanImg2()
        }
    }
    fun uploadtutupanLahanImg2(){

        if (surveyData.tutupanLahanImg2!="" && surveyData.tutupanLahanImg2!=null){
            if(File(surveyData.tutupanLahanImg2.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar tutupanLahanImg2")

                val paramtutupanLahanImg2 = HashMap<String, File>()
                paramtutupanLahanImg2["tutupanLahanImg2"] = File(surveyData.tutupanLahanImg2.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramtutupanLahanImg2)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadtutupanLahanImg3()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar tutupanLahanImg2")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar tutupanLahanImg2 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadtutupanLahanImg3()
        }
    }
    fun uploadtutupanLahanImg3(){

        if (surveyData.tutupanLahanImg3!="" && surveyData.tutupanLahanImg3!=null){
            if(File(surveyData.tutupanLahanImg3.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar tutupanLahanImg3")

                val paramtutupanLahanImg3 = HashMap<String, File>()
                paramtutupanLahanImg3["tutupanLahanImg3"] = File(surveyData.tutupanLahanImg3.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramtutupanLahanImg3)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadtutupanLahanImg4()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar tutupanLahanImg3")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar tutupanLahanImg3 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadtutupanLahanImg4()
        }
    }
    fun uploadtutupanLahanImg4(){

        if (surveyData.tutupanLahanImg4!="" && surveyData.tutupanLahanImg4!=null){
            if(File(surveyData.tutupanLahanImg4.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar tutupanLahanImg4")

                val paramtutupanLahanImg4 = HashMap<String, File>()
                paramtutupanLahanImg4["tutupanLahanImg4"] = File(surveyData.tutupanLahanImg4.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramtutupanLahanImg4)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadtutupanLahanImg5()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar tutupanLahanImg4")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar tutupanLahanImg4 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadtutupanLahanImg5()
        }
    }
    fun uploadtutupanLahanImg5(){

        if (surveyData.tutupanLahanImg5!="" && surveyData.tutupanLahanImg5!=null){
            if(File(surveyData.tutupanLahanImg5.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar tutupanLahanImg5")

                val paramtutupanLahanImg5 = HashMap<String, File>()
                paramtutupanLahanImg5["tutupanLahanImg5"] = File(surveyData.tutupanLahanImg5.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramtutupanLahanImg5)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadtutupanLahanImg6()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar tutupanLahanImg5")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar tutupanLahanImg5 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadtutupanLahanImg6()
        }
    }
    fun uploadtutupanLahanImg6(){

        if (surveyData.tutupanLahanImg6!="" && surveyData.tutupanLahanImg6!=null){
            if(File(surveyData.tutupanLahanImg6.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar tutupanLahanImg6")

                val paramtutupanLahanImg6 = HashMap<String, File>()
                paramtutupanLahanImg6["tutupanLahanImg6"] = File(surveyData.tutupanLahanImg6.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramtutupanLahanImg6)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadfloraFaunaImg1()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar tutupanLahanImg6")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar tutupanLahanImg6 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadfloraFaunaImg1()
        }
    }
    fun uploadfloraFaunaImg1(){

        if (surveyData.floraFaunaImg1!="" && surveyData.floraFaunaImg1!=null){
            if(File(surveyData.floraFaunaImg1.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar floraFaunaImg1")

                val paramfloraFaunaImg1 = HashMap<String, File>()
                paramfloraFaunaImg1["floraFaunaImg1"] = File(surveyData.floraFaunaImg1.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramfloraFaunaImg1)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadfloraFaunaImg2()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar floraFaunaImg1")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar floraFaunaImg1 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadfloraFaunaImg2()
        }
    }
    fun uploadfloraFaunaImg2(){

        if (surveyData.floraFaunaImg2!="" && surveyData.floraFaunaImg2!=null){
            if(File(surveyData.floraFaunaImg2.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar floraFaunaImg2")

                val paramfloraFaunaImg2 = HashMap<String, File>()
                paramfloraFaunaImg2["floraFaunaImg2"] = File(surveyData.floraFaunaImg2.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramfloraFaunaImg2)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadfloraFaunaImg3()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar floraFaunaImg2")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar floraFaunaImg2 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadfloraFaunaImg3()
        }
    }
    fun uploadfloraFaunaImg3(){

        if (surveyData.floraFaunaImg3!="" && surveyData.floraFaunaImg3!=null){
            if(File(surveyData.floraFaunaImg3.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar floraFaunaImg3")

                val paramfloraFaunaImg3 = HashMap<String, File>()
                paramfloraFaunaImg3["floraFaunaImg3"] = File(surveyData.floraFaunaImg3.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramfloraFaunaImg3)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadfloraFaunaImg4()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar floraFaunaImg3")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar floraFaunaImg3 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadfloraFaunaImg4()
        }
    }
    fun uploadfloraFaunaImg4(){

        if (surveyData.floraFaunaImg4!="" && surveyData.floraFaunaImg4!=null){
            if(File(surveyData.floraFaunaImg4.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar floraFaunaImg4")

                val paramfloraFaunaImg4 = HashMap<String, File>()
                paramfloraFaunaImg4["floraFaunaImg4"] = File(surveyData.floraFaunaImg4.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramfloraFaunaImg4)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadfloraFaunaImg5()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar floraFaunaImg4")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar floraFaunaImg4 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadfloraFaunaImg5()
        }
    }
    fun uploadfloraFaunaImg5(){

        if (surveyData.floraFaunaImg5!="" && surveyData.floraFaunaImg5!=null){
            if(File(surveyData.floraFaunaImg5.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar floraFaunaImg5")

                val paramfloraFaunaImg5 = HashMap<String, File>()
                paramfloraFaunaImg5["floraFaunaImg5"] = File(surveyData.floraFaunaImg5.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramfloraFaunaImg5)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadfloraFaunaImg6()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar floraFaunaImg5")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar floraFaunaImg5 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadfloraFaunaImg6()
        }
    }
    fun uploadfloraFaunaImg6(){

        if (surveyData.floraFaunaImg6!="" && surveyData.floraFaunaImg6!=null){
            if(File(surveyData.floraFaunaImg6.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar floraFaunaImg6")

                val paramfloraFaunaImg6 = HashMap<String, File>()
                paramfloraFaunaImg6["floraFaunaImg6"] = File(surveyData.floraFaunaImg6.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramfloraFaunaImg6)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploaddrainaseImg1()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar floraFaunaImg6")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar floraFaunaImg6 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploaddrainaseImg1()
        }
    }
    fun uploaddrainaseImg1(){

        if (surveyData.drainaseImg1!="" && surveyData.drainaseImg1!=null){
            if(File(surveyData.drainaseImg1.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar drainaseImg1")

                val paramdrainaseImg1 = HashMap<String, File>()
                paramdrainaseImg1["drainaseImg1"] = File(surveyData.drainaseImg1.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramdrainaseImg1)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploaddrainaseImg2()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar drainaseImg1")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar drainaseImg1 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploaddrainaseImg2()
        }
    }
    fun uploaddrainaseImg2(){

        if (surveyData.drainaseImg2!="" && surveyData.drainaseImg2!=null){
            if(File(surveyData.drainaseImg2.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar drainaseImg2")

                val paramdrainaseImg2 = HashMap<String, File>()
                paramdrainaseImg2["drainaseImg2"] = File(surveyData.drainaseImg2.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramdrainaseImg2)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploaddrainaseImg3()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar drainaseImg2")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar drainaseImg2 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploaddrainaseImg3()
        }
    }
    fun uploaddrainaseImg3(){

        if (surveyData.drainaseImg3!="" && surveyData.drainaseImg3!=null){
            if(File(surveyData.drainaseImg3.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar drainaseImg3")

                val paramdrainaseImg3 = HashMap<String, File>()
                paramdrainaseImg3["drainaseImg3"] = File(surveyData.drainaseImg3.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramdrainaseImg3)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploaddrainaseImg4()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar drainaseImg3")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar drainaseImg3 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploaddrainaseImg4()
        }
    }
    fun uploaddrainaseImg4(){

        if (surveyData.drainaseImg4!="" && surveyData.drainaseImg4!=null){
            if(File(surveyData.drainaseImg4.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar drainaseImg4")

                val paramdrainaseImg4 = HashMap<String, File>()
                paramdrainaseImg4["drainaseImg4"] = File(surveyData.drainaseImg4.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramdrainaseImg4)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploaddrainaseImg5()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar drainaseImg4")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar drainaseImg4 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploaddrainaseImg5()
        }
    }
    fun uploaddrainaseImg5(){

        if (surveyData.drainaseImg5!="" && surveyData.drainaseImg5!=null){
            if(File(surveyData.drainaseImg5.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar drainaseImg5")

                val paramdrainaseImg5 = HashMap<String, File>()
                paramdrainaseImg5["drainaseImg5"] = File(surveyData.drainaseImg5.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramdrainaseImg5)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploaddrainaseImg6()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar drainaseImg5")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar drainaseImg5 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploaddrainaseImg6()
        }
    }
    fun uploaddrainaseImg6(){

        if (surveyData.drainaseImg6!="" && surveyData.drainaseImg6!=null){
            if(File(surveyData.drainaseImg6.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar drainaseImg6")

                val paramdrainaseImg6 = HashMap<String, File>()
                paramdrainaseImg6["drainaseImg6"] = File(surveyData.drainaseImg6.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramdrainaseImg6)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadkualitasiAirTanahImg1()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar drainaseImg6")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar drainaseImg6 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadkualitasiAirTanahImg1()
        }
    }
    fun uploadkualitasiAirTanahImg1(){

        if (surveyData.kualitasiAirTanahImg1!="" && surveyData.kualitasiAirTanahImg1!=null){
            if(File(surveyData.kualitasiAirTanahImg1.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar kualitasiAirTanahImg1")

                val paramkualitasiAirTanahImg1 = HashMap<String, File>()
                paramkualitasiAirTanahImg1["kualitasiAirTanahImg1"] = File(surveyData.kualitasiAirTanahImg1.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramkualitasiAirTanahImg1)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadkualitasiAirTanahImg2()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar kualitasiAirTanahImg1")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar kualitasiAirTanahImg1 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadkualitasiAirTanahImg2()
        }
    }
    fun uploadkualitasiAirTanahImg2(){

        if (surveyData.kualitasiAirTanahImg2!="" && surveyData.kualitasiAirTanahImg2!=null){
            if(File(surveyData.kualitasiAirTanahImg2.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar kualitasiAirTanahImg2")

                val paramkualitasiAirTanahImg2 = HashMap<String, File>()
                paramkualitasiAirTanahImg2["kualitasiAirTanahImg2"] = File(surveyData.kualitasiAirTanahImg2.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramkualitasiAirTanahImg2)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadkualitasiAirTanahImg3()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar kualitasiAirTanahImg2")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar kualitasiAirTanahImg2 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadkualitasiAirTanahImg3()
        }
    }
    fun uploadkualitasiAirTanahImg3(){

        if (surveyData.kualitasiAirTanahImg3!="" && surveyData.kualitasiAirTanahImg3!=null){
            if(File(surveyData.kualitasiAirTanahImg3.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar kualitasiAirTanahImg3")

                val paramkualitasiAirTanahImg3 = HashMap<String, File>()
                paramkualitasiAirTanahImg3["kualitasiAirTanahImg3"] = File(surveyData.kualitasiAirTanahImg3.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramkualitasiAirTanahImg3)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadkualitasiAirTanahImg4()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar kualitasiAirTanahImg3")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar kualitasiAirTanahImg3 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadkualitasiAirTanahImg4()
        }
    }
    fun uploadkualitasiAirTanahImg4(){

        if (surveyData.kualitasiAirTanahImg4!="" && surveyData.kualitasiAirTanahImg4!=null){
            if(File(surveyData.kualitasiAirTanahImg4.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar kualitasiAirTanahImg4")

                val paramkualitasiAirTanahImg4 = HashMap<String, File>()
                paramkualitasiAirTanahImg4["kualitasiAirTanahImg4"] = File(surveyData.kualitasiAirTanahImg4.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramkualitasiAirTanahImg4)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadkualitasiAirTanahImg5()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar kualitasiAirTanahImg4")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar kualitasiAirTanahImg4 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadkualitasiAirTanahImg5()
        }
    }
    fun uploadkualitasiAirTanahImg5(){

        if (surveyData.kualitasiAirTanahImg5!="" && surveyData.kualitasiAirTanahImg5!=null){
            if(File(surveyData.kualitasiAirTanahImg5.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar kualitasiAirTanahImg5")

                val paramkualitasiAirTanahImg5 = HashMap<String, File>()
                paramkualitasiAirTanahImg5["kualitasiAirTanahImg5"] = File(surveyData.kualitasiAirTanahImg5.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramkualitasiAirTanahImg5)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadkualitasiAirTanahImg6()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar kualitasiAirTanahImg5")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar kualitasiAirTanahImg5 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadkualitasiAirTanahImg6()
        }
    }
    fun uploadkualitasiAirTanahImg6(){

        if (surveyData.kualitasiAirTanahImg6!="" && surveyData.kualitasiAirTanahImg6!=null){
            if(File(surveyData.kualitasiAirTanahImg6.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar kualitasiAirTanahImg6")

                val paramkualitasiAirTanahImg6 = HashMap<String, File>()
                paramkualitasiAirTanahImg6["kualitasiAirTanahImg6"] = File(surveyData.kualitasiAirTanahImg6.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramkualitasiAirTanahImg6)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadkarakteristikSubstratumTanahLiatImg1()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar kualitasiAirTanahImg6")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar kualitasiAirTanahImg6 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadkarakteristikSubstratumTanahLiatImg1()
        }
    }
    fun uploadkarakteristikSubstratumTanahLiatImg1(){

        if (surveyData.karakteristikSubstratumTanahLiatImg1!="" && surveyData.karakteristikSubstratumTanahLiatImg1!=null){
            if(File(surveyData.karakteristikSubstratumTanahLiatImg1.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar karakteristikSubstratumTanahLiatImg1")

                val paramkarakteristikSubstratumTanahLiatImg1 = HashMap<String, File>()
                paramkarakteristikSubstratumTanahLiatImg1["karakteristikSubstratumTanahLiatImg1"] = File(surveyData.karakteristikSubstratumTanahLiatImg1.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramkarakteristikSubstratumTanahLiatImg1)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadkarakteristikSubstratumTanahLiatImg2()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar karakteristikSubstratumTanahLiatImg1")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar karakteristikSubstratumTanahLiatImg1 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadkarakteristikSubstratumTanahLiatImg2()
        }
    }
    fun uploadkarakteristikSubstratumTanahLiatImg2(){

        if (surveyData.karakteristikSubstratumTanahLiatImg2!="" && surveyData.karakteristikSubstratumTanahLiatImg2!=null){
            if(File(surveyData.karakteristikSubstratumTanahLiatImg2.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar karakteristikSubstratumTanahLiatImg2")

                val paramkarakteristikSubstratumTanahLiatImg2 = HashMap<String, File>()
                paramkarakteristikSubstratumTanahLiatImg2["karakteristikSubstratumTanahLiatImg2"] = File(surveyData.karakteristikSubstratumTanahLiatImg2.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramkarakteristikSubstratumTanahLiatImg2)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadkarakteristikSubstratumTanahLiatImg3()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar karakteristikSubstratumTanahLiatImg2")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar karakteristikSubstratumTanahLiatImg2 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadkarakteristikSubstratumTanahLiatImg3()
        }
    }
    fun uploadkarakteristikSubstratumTanahLiatImg3(){

        if (surveyData.karakteristikSubstratumTanahLiatImg3!="" && surveyData.karakteristikSubstratumTanahLiatImg3!=null){
            if(File(surveyData.karakteristikSubstratumTanahLiatImg3.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar karakteristikSubstratumTanahLiatImg3")

                val paramkarakteristikSubstratumTanahLiatImg3 = HashMap<String, File>()
                paramkarakteristikSubstratumTanahLiatImg3["karakteristikSubstratumTanahLiatImg3"] = File(surveyData.karakteristikSubstratumTanahLiatImg3.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramkarakteristikSubstratumTanahLiatImg3)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadkarakteristikSubstratumTanahLiatImg4()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar karakteristikSubstratumTanahLiatImg3")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar karakteristikSubstratumTanahLiatImg3 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadkarakteristikSubstratumTanahLiatImg4()
        }
    }
    fun uploadkarakteristikSubstratumTanahLiatImg4(){

        if (surveyData.karakteristikSubstratumTanahLiatImg4!="" && surveyData.karakteristikSubstratumTanahLiatImg4!=null){
            if(File(surveyData.karakteristikSubstratumTanahLiatImg4.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar karakteristikSubstratumTanahLiatImg4")

                val paramkarakteristikSubstratumTanahLiatImg4 = HashMap<String, File>()
                paramkarakteristikSubstratumTanahLiatImg4["karakteristikSubstratumTanahLiatImg4"] = File(surveyData.karakteristikSubstratumTanahLiatImg4.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramkarakteristikSubstratumTanahLiatImg4)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadkarakteristikSubstratumTanahLiatImg5()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar karakteristikSubstratumTanahLiatImg4")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar karakteristikSubstratumTanahLiatImg4 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadkarakteristikSubstratumTanahLiatImg5()
        }
    }
    fun uploadkarakteristikSubstratumTanahLiatImg5(){

        if (surveyData.karakteristikSubstratumTanahLiatImg5!="" && surveyData.karakteristikSubstratumTanahLiatImg5!=null){
            if(File(surveyData.karakteristikSubstratumTanahLiatImg5.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar karakteristikSubstratumTanahLiatImg5")

                val paramkarakteristikSubstratumTanahLiatImg5 = HashMap<String, File>()
                paramkarakteristikSubstratumTanahLiatImg5["karakteristikSubstratumTanahLiatImg5"] = File(surveyData.karakteristikSubstratumTanahLiatImg5.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramkarakteristikSubstratumTanahLiatImg5)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadkarakteristikSubstratumTanahLiatImg6()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar karakteristikSubstratumTanahLiatImg5")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar karakteristikSubstratumTanahLiatImg5 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadkarakteristikSubstratumTanahLiatImg6()
        }
    }
    fun uploadkarakteristikSubstratumTanahLiatImg6(){

        if (surveyData.karakteristikSubstratumTanahLiatImg6!="" && surveyData.karakteristikSubstratumTanahLiatImg6!=null){
            if(File(surveyData.karakteristikSubstratumTanahLiatImg6.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar karakteristikSubstratumTanahLiatImg6")

                val paramkarakteristikSubstratumTanahLiatImg6 = HashMap<String, File>()
                paramkarakteristikSubstratumTanahLiatImg6["karakteristikSubstratumTanahLiatImg6"] = File(surveyData.karakteristikSubstratumTanahLiatImg6.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramkarakteristikSubstratumTanahLiatImg6)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadtipeLuapanImg1()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar karakteristikSubstratumTanahLiatImg6")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar karakteristikSubstratumTanahLiatImg6 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadtipeLuapanImg1()
        }
    }
    fun uploadtipeLuapanImg1(){

        if (surveyData.tipeLuapanImg1!="" && surveyData.tipeLuapanImg1!=null){
            if(File(surveyData.tipeLuapanImg1.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar tipeLuapanImg1")

                val paramtipeLuapanImg1 = HashMap<String, File>()
                paramtipeLuapanImg1["tipeLuapanImg1"] = File(surveyData.tipeLuapanImg1.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramtipeLuapanImg1)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadtipeLuapanImg2()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar tipeLuapanImg1")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar tipeLuapanImg1 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadtipeLuapanImg2()
        }
    }
    fun uploadtipeLuapanImg2(){

        if (surveyData.tipeLuapanImg2!="" && surveyData.tipeLuapanImg2!=null){
            if(File(surveyData.tipeLuapanImg2.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar tipeLuapanImg2")

                val paramtipeLuapanImg2 = HashMap<String, File>()
                paramtipeLuapanImg2["tipeLuapanImg2"] = File(surveyData.tipeLuapanImg2.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramtipeLuapanImg2)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadtipeLuapanImg3()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar tipeLuapanImg2")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar tipeLuapanImg2 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadtipeLuapanImg3()
        }
    }
    fun uploadtipeLuapanImg3(){

        if (surveyData.tipeLuapanImg3!="" && surveyData.tipeLuapanImg3!=null){
            if(File(surveyData.tipeLuapanImg3.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar tipeLuapanImg3")

                val paramtipeLuapanImg3 = HashMap<String, File>()
                paramtipeLuapanImg3["tipeLuapanImg3"] = File(surveyData.tipeLuapanImg3.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramtipeLuapanImg3)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadtipeLuapanImg4()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar tipeLuapanImg3")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar tipeLuapanImg3 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadtipeLuapanImg4()
        }
    }
    fun uploadtipeLuapanImg4(){

        if (surveyData.tipeLuapanImg4!="" && surveyData.tipeLuapanImg4!=null){
            if(File(surveyData.tipeLuapanImg4.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar tipeLuapanImg4")

                val paramtipeLuapanImg4 = HashMap<String, File>()
                paramtipeLuapanImg4["tipeLuapanImg4"] = File(surveyData.tipeLuapanImg4.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramtipeLuapanImg4)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadtipeLuapanImg5()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar tipeLuapanImg4")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar tipeLuapanImg4 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadtipeLuapanImg5()
        }
    }
    fun uploadtipeLuapanImg5(){

        if (surveyData.tipeLuapanImg5!="" && surveyData.tipeLuapanImg5!=null){
            if(File(surveyData.tipeLuapanImg5.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar tipeLuapanImg5")

                val paramtipeLuapanImg5 = HashMap<String, File>()
                paramtipeLuapanImg5["tipeLuapanImg5"] = File(surveyData.tipeLuapanImg5.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramtipeLuapanImg5)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadtipeLuapanImg6()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar tipeLuapanImg5")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar tipeLuapanImg5 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadtipeLuapanImg6()
        }
    }
    fun uploadtipeLuapanImg6(){

        if (surveyData.tipeLuapanImg6!="" && surveyData.tipeLuapanImg6!=null){
            if(File(surveyData.tipeLuapanImg6.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar tipeLuapanImg6")

                val paramtipeLuapanImg6 = HashMap<String, File>()
                paramtipeLuapanImg6["tipeLuapanImg6"] = File(surveyData.tipeLuapanImg6.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramtipeLuapanImg6)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadketebalanGambutImg1()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar tipeLuapanImg6")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar tipeLuapanImg6 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadketebalanGambutImg1()
        }
    }
    fun uploadketebalanGambutImg1(){

        if (surveyData.ketebalanGambutImg1!="" && surveyData.ketebalanGambutImg1!=null){
            if(File(surveyData.ketebalanGambutImg1.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar ketebalanGambutImg1")

                val paramketebalanGambutImg1 = HashMap<String, File>()
                paramketebalanGambutImg1["ketebalanGambutImg1"] = File(surveyData.ketebalanGambutImg1.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramketebalanGambutImg1)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadketebalanGambutImg2()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar ketebalanGambutImg1")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar ketebalanGambutImg1 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadketebalanGambutImg2()
        }
    }
    fun uploadketebalanGambutImg2(){

        if (surveyData.ketebalanGambutImg2!="" && surveyData.ketebalanGambutImg2!=null){
            if(File(surveyData.ketebalanGambutImg2.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar ketebalanGambutImg2")

                val paramketebalanGambutImg2 = HashMap<String, File>()
                paramketebalanGambutImg2["ketebalanGambutImg2"] = File(surveyData.ketebalanGambutImg2.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramketebalanGambutImg2)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadketebalanGambutImg3()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar ketebalanGambutImg2")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar ketebalanGambutImg2 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadketebalanGambutImg3()
        }
    }
    fun uploadketebalanGambutImg3(){

        if (surveyData.ketebalanGambutImg3!="" && surveyData.ketebalanGambutImg3!=null){
            if(File(surveyData.ketebalanGambutImg3.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar ketebalanGambutImg3")

                val paramketebalanGambutImg3 = HashMap<String, File>()
                paramketebalanGambutImg3["ketebalanGambutImg3"] = File(surveyData.ketebalanGambutImg3.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramketebalanGambutImg3)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadketebalanGambutImg4()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar ketebalanGambutImg3")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar ketebalanGambutImg3 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadketebalanGambutImg4()
        }
    }
    fun uploadketebalanGambutImg4(){

        if (surveyData.ketebalanGambutImg4!="" && surveyData.ketebalanGambutImg4!=null){
            if(File(surveyData.ketebalanGambutImg4.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar ketebalanGambutImg4")

                val paramketebalanGambutImg4 = HashMap<String, File>()
                paramketebalanGambutImg4["ketebalanGambutImg4"] = File(surveyData.ketebalanGambutImg4.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramketebalanGambutImg4)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadketebalanGambutImg5()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar ketebalanGambutImg4")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar ketebalanGambutImg4 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadketebalanGambutImg5()
        }
    }
    fun uploadketebalanGambutImg5(){

        if (surveyData.ketebalanGambutImg5!="" && surveyData.ketebalanGambutImg5!=null){
            if(File(surveyData.ketebalanGambutImg5.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar ketebalanGambutImg5")

                val paramketebalanGambutImg5 = HashMap<String, File>()
                paramketebalanGambutImg5["ketebalanGambutImg5"] = File(surveyData.ketebalanGambutImg5.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramketebalanGambutImg5)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadketebalanGambutImg6()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar ketebalanGambutImg5")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar ketebalanGambutImg5 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadketebalanGambutImg6()
        }
    }
    fun uploadketebalanGambutImg6(){

        if (surveyData.ketebalanGambutImg6!="" && surveyData.ketebalanGambutImg6!=null){
            if(File(surveyData.ketebalanGambutImg6.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar ketebalanGambutImg6")

                val paramketebalanGambutImg6 = HashMap<String, File>()
                paramketebalanGambutImg6["ketebalanGambutImg6"] = File(surveyData.ketebalanGambutImg6.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramketebalanGambutImg6)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadkarakteristikSubstratumDibawahLapisanGambutImg1()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar ketebalanGambutImg6")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar ketebalanGambutImg6 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadkarakteristikSubstratumDibawahLapisanGambutImg1()
        }
    }
    fun uploadkarakteristikSubstratumDibawahLapisanGambutImg1(){

        if (surveyData.karakteristikSubstratumDibawahLapisanGambutImg1!="" && surveyData.karakteristikSubstratumDibawahLapisanGambutImg1!=null){
            if(File(surveyData.karakteristikSubstratumDibawahLapisanGambutImg1.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar karakteristikSubstratumDibawahLapisanGambutImg1")

                val paramkarakteristikSubstratumDibawahLapisanGambutImg1 = HashMap<String, File>()
                paramkarakteristikSubstratumDibawahLapisanGambutImg1["karakteristikSubstratumDibawahLapisanGambutImg1"] = File(surveyData.karakteristikSubstratumDibawahLapisanGambutImg1.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramkarakteristikSubstratumDibawahLapisanGambutImg1)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadkarakteristikSubstratumDibawahLapisanGambutImg2()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar karakteristikSubstratumDibawahLapisanGambutImg1")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar karakteristikSubstratumDibawahLapisanGambutImg1 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadkarakteristikSubstratumDibawahLapisanGambutImg2()
        }
    }
    fun uploadkarakteristikSubstratumDibawahLapisanGambutImg2(){

        if (surveyData.karakteristikSubstratumDibawahLapisanGambutImg2!="" && surveyData.karakteristikSubstratumDibawahLapisanGambutImg2!=null){
            if(File(surveyData.karakteristikSubstratumDibawahLapisanGambutImg2.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar karakteristikSubstratumDibawahLapisanGambutImg2")

                val paramkarakteristikSubstratumDibawahLapisanGambutImg2 = HashMap<String, File>()
                paramkarakteristikSubstratumDibawahLapisanGambutImg2["karakteristikSubstratumDibawahLapisanGambutImg2"] = File(surveyData.karakteristikSubstratumDibawahLapisanGambutImg2.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramkarakteristikSubstratumDibawahLapisanGambutImg2)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadkarakteristikSubstratumDibawahLapisanGambutImg3()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar karakteristikSubstratumDibawahLapisanGambutImg2")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar karakteristikSubstratumDibawahLapisanGambutImg2 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadkarakteristikSubstratumDibawahLapisanGambutImg3()
        }
    }
    fun uploadkarakteristikSubstratumDibawahLapisanGambutImg3(){

        if (surveyData.karakteristikSubstratumDibawahLapisanGambutImg3!="" && surveyData.karakteristikSubstratumDibawahLapisanGambutImg3!=null){
            if(File(surveyData.karakteristikSubstratumDibawahLapisanGambutImg3.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar karakteristikSubstratumDibawahLapisanGambutImg3")

                val paramkarakteristikSubstratumDibawahLapisanGambutImg3 = HashMap<String, File>()
                paramkarakteristikSubstratumDibawahLapisanGambutImg3["karakteristikSubstratumDibawahLapisanGambutImg3"] = File(surveyData.karakteristikSubstratumDibawahLapisanGambutImg3.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramkarakteristikSubstratumDibawahLapisanGambutImg3)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadkarakteristikSubstratumDibawahLapisanGambutImg4()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar karakteristikSubstratumDibawahLapisanGambutImg3")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar karakteristikSubstratumDibawahLapisanGambutImg3 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadkarakteristikSubstratumDibawahLapisanGambutImg4()
        }
    }
    fun uploadkarakteristikSubstratumDibawahLapisanGambutImg4(){

        if (surveyData.karakteristikSubstratumDibawahLapisanGambutImg4!="" && surveyData.karakteristikSubstratumDibawahLapisanGambutImg4!=null){
            if(File(surveyData.karakteristikSubstratumDibawahLapisanGambutImg4.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar karakteristikSubstratumDibawahLapisanGambutImg4")

                val paramkarakteristikSubstratumDibawahLapisanGambutImg4 = HashMap<String, File>()
                paramkarakteristikSubstratumDibawahLapisanGambutImg4["karakteristikSubstratumDibawahLapisanGambutImg4"] = File(surveyData.karakteristikSubstratumDibawahLapisanGambutImg4.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramkarakteristikSubstratumDibawahLapisanGambutImg4)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadkarakteristikSubstratumDibawahLapisanGambutImg5()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar karakteristikSubstratumDibawahLapisanGambutImg4")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar karakteristikSubstratumDibawahLapisanGambutImg4 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadkarakteristikSubstratumDibawahLapisanGambutImg5()
        }
    }
    fun uploadkarakteristikSubstratumDibawahLapisanGambutImg5(){

        if (surveyData.karakteristikSubstratumDibawahLapisanGambutImg5!="" && surveyData.karakteristikSubstratumDibawahLapisanGambutImg5!=null){
            if(File(surveyData.karakteristikSubstratumDibawahLapisanGambutImg5.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar karakteristikSubstratumDibawahLapisanGambutImg5")

                val paramkarakteristikSubstratumDibawahLapisanGambutImg5 = HashMap<String, File>()
                paramkarakteristikSubstratumDibawahLapisanGambutImg5["karakteristikSubstratumDibawahLapisanGambutImg5"] = File(surveyData.karakteristikSubstratumDibawahLapisanGambutImg5.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramkarakteristikSubstratumDibawahLapisanGambutImg5)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadkarakteristikSubstratumDibawahLapisanGambutImg6()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar karakteristikSubstratumDibawahLapisanGambutImg5")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar karakteristikSubstratumDibawahLapisanGambutImg5 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadkarakteristikSubstratumDibawahLapisanGambutImg6()
        }
    }
    fun uploadkarakteristikSubstratumDibawahLapisanGambutImg6(){

        if (surveyData.karakteristikSubstratumDibawahLapisanGambutImg6!="" && surveyData.karakteristikSubstratumDibawahLapisanGambutImg6!=null){
            if(File(surveyData.karakteristikSubstratumDibawahLapisanGambutImg6.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar karakteristikSubstratumDibawahLapisanGambutImg6")

                val paramkarakteristikSubstratumDibawahLapisanGambutImg6 = HashMap<String, File>()
                paramkarakteristikSubstratumDibawahLapisanGambutImg6["karakteristikSubstratumDibawahLapisanGambutImg6"] = File(surveyData.karakteristikSubstratumDibawahLapisanGambutImg6.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramkarakteristikSubstratumDibawahLapisanGambutImg6)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadtingkatKerusakanLahanImg1()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar karakteristikSubstratumDibawahLapisanGambutImg6")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar karakteristikSubstratumDibawahLapisanGambutImg6 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadtingkatKerusakanLahanImg1()
        }
    }
    fun uploadtingkatKerusakanLahanImg1(){

        if (surveyData.tingkatKerusakanLahanImg1!="" && surveyData.tingkatKerusakanLahanImg1!=null){
            if(File(surveyData.tingkatKerusakanLahanImg1.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar tingkatKerusakanLahanImg1")

                val paramtingkatKerusakanLahanImg1 = HashMap<String, File>()
                paramtingkatKerusakanLahanImg1["tingkatKerusakanLahanImg1"] = File(surveyData.tingkatKerusakanLahanImg1.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramtingkatKerusakanLahanImg1)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadtingkatKerusakanLahanImg2()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar tingkatKerusakanLahanImg1")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar tingkatKerusakanLahanImg1 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadtingkatKerusakanLahanImg2()
        }
    }
    fun uploadtingkatKerusakanLahanImg2(){

        if (surveyData.tingkatKerusakanLahanImg2!="" && surveyData.tingkatKerusakanLahanImg2!=null){
            if(File(surveyData.tingkatKerusakanLahanImg2.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar tingkatKerusakanLahanImg2")

                val paramtingkatKerusakanLahanImg2 = HashMap<String, File>()
                paramtingkatKerusakanLahanImg2["tingkatKerusakanLahanImg2"] = File(surveyData.tingkatKerusakanLahanImg2.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramtingkatKerusakanLahanImg2)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadtingkatKerusakanLahanImg3()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar tingkatKerusakanLahanImg2")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar tingkatKerusakanLahanImg2 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadtingkatKerusakanLahanImg3()
        }
    }
    fun uploadtingkatKerusakanLahanImg3(){

        if (surveyData.tingkatKerusakanLahanImg3!="" && surveyData.tingkatKerusakanLahanImg3!=null){
            if(File(surveyData.tingkatKerusakanLahanImg3.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar tingkatKerusakanLahanImg3")

                val paramtingkatKerusakanLahanImg3 = HashMap<String, File>()
                paramtingkatKerusakanLahanImg3["tingkatKerusakanLahanImg3"] = File(surveyData.tingkatKerusakanLahanImg3.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramtingkatKerusakanLahanImg3)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadtingkatKerusakanLahanImg4()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar tingkatKerusakanLahanImg3")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar tingkatKerusakanLahanImg3 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadtingkatKerusakanLahanImg4()
        }
    }
    fun uploadtingkatKerusakanLahanImg4(){

        if (surveyData.tingkatKerusakanLahanImg4!="" && surveyData.tingkatKerusakanLahanImg4!=null){
            if(File(surveyData.tingkatKerusakanLahanImg4.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar tingkatKerusakanLahanImg4")

                val paramtingkatKerusakanLahanImg4 = HashMap<String, File>()
                paramtingkatKerusakanLahanImg4["tingkatKerusakanLahanImg4"] = File(surveyData.tingkatKerusakanLahanImg4.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramtingkatKerusakanLahanImg4)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadtingkatKerusakanLahanImg5()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar tingkatKerusakanLahanImg4")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar tingkatKerusakanLahanImg4 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadtingkatKerusakanLahanImg5()
        }
    }
    fun uploadtingkatKerusakanLahanImg5(){

        if (surveyData.tingkatKerusakanLahanImg5!="" && surveyData.tingkatKerusakanLahanImg5!=null){
            if(File(surveyData.tingkatKerusakanLahanImg5.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar tingkatKerusakanLahanImg5")

                val paramtingkatKerusakanLahanImg5 = HashMap<String, File>()
                paramtingkatKerusakanLahanImg5["tingkatKerusakanLahanImg5"] = File(surveyData.tingkatKerusakanLahanImg5.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramtingkatKerusakanLahanImg5)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadtingkatKerusakanLahanImg6()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar tingkatKerusakanLahanImg5")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar tingkatKerusakanLahanImg5 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadtingkatKerusakanLahanImg6()
        }
    }
    fun uploadtingkatKerusakanLahanImg6(){

        if (surveyData.tingkatKerusakanLahanImg6!="" && surveyData.tingkatKerusakanLahanImg6!=null){
            if(File(surveyData.tingkatKerusakanLahanImg6.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar tingkatKerusakanLahanImg6")

                val paramtingkatKerusakanLahanImg6 = HashMap<String, File>()
                paramtingkatKerusakanLahanImg6["tingkatKerusakanLahanImg6"] = File(surveyData.tingkatKerusakanLahanImg6.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramtingkatKerusakanLahanImg6)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadkebakaranHujanImg1()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar tingkatKerusakanLahanImg6")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar tingkatKerusakanLahanImg6 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadkebakaranHujanImg1()
        }
    }
    fun uploadkebakaranHujanImg1(){

        if (surveyData.kebakaranHujanImg1!="" && surveyData.kebakaranHujanImg1!=null){
            if(File(surveyData.kebakaranHujanImg1.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar kebakaranHujanImg1")

                val paramkebakaranHujanImg1 = HashMap<String, File>()
                paramkebakaranHujanImg1["kebakaranHujanImg1"] = File(surveyData.kebakaranHujanImg1.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramkebakaranHujanImg1)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadkebakaranHujanImg2()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar kebakaranHujanImg1")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar kebakaranHujanImg1 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadkebakaranHujanImg2()
        }
    }
    fun uploadkebakaranHujanImg2(){

        if (surveyData.kebakaranHujanImg2!="" && surveyData.kebakaranHujanImg2!=null){
            if(File(surveyData.kebakaranHujanImg2.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar kebakaranHujanImg2")

                val paramkebakaranHujanImg2 = HashMap<String, File>()
                paramkebakaranHujanImg2["kebakaranHujanImg2"] = File(surveyData.kebakaranHujanImg2.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramkebakaranHujanImg2)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadkebakaranHujanImg3()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar kebakaranHujanImg2")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar kebakaranHujanImg2 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadkebakaranHujanImg3()
        }
    }
    fun uploadkebakaranHujanImg3(){

        if (surveyData.kebakaranHujanImg3!="" && surveyData.kebakaranHujanImg3!=null){
            if(File(surveyData.kebakaranHujanImg3.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar kebakaranHujanImg3")

                val paramkebakaranHujanImg3 = HashMap<String, File>()
                paramkebakaranHujanImg3["kebakaranHujanImg3"] = File(surveyData.kebakaranHujanImg3.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramkebakaranHujanImg3)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadkebakaranHujanImg4()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar kebakaranHujanImg3")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar kebakaranHujanImg3 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadkebakaranHujanImg4()
        }
    }
    fun uploadkebakaranHujanImg4(){

        if (surveyData.kebakaranHujanImg4!="" && surveyData.kebakaranHujanImg4!=null){
            if(File(surveyData.kebakaranHujanImg4.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar kebakaranHujanImg4")

                val paramkebakaranHujanImg4 = HashMap<String, File>()
                paramkebakaranHujanImg4["kebakaranHujanImg4"] = File(surveyData.kebakaranHujanImg4.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramkebakaranHujanImg4)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadkebakaranHujanImg5()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar kebakaranHujanImg4")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar kebakaranHujanImg4 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadkebakaranHujanImg5()
        }
    }
    fun uploadkebakaranHujanImg5(){

        if (surveyData.kebakaranHujanImg5!="" && surveyData.kebakaranHujanImg5!=null){
            if(File(surveyData.kebakaranHujanImg5.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar kebakaranHujanImg5")

                val paramkebakaranHujanImg5 = HashMap<String, File>()
                paramkebakaranHujanImg5["kebakaranHujanImg5"] = File(surveyData.kebakaranHujanImg5.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramkebakaranHujanImg5)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadkebakaranHujanImg6()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar kebakaranHujanImg5")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar kebakaranHujanImg5 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadkebakaranHujanImg6()
        }
    }
    fun uploadkebakaranHujanImg6(){

        if (surveyData.kebakaranHujanImg6!="" && surveyData.kebakaranHujanImg6!=null){
            if(File(surveyData.kebakaranHujanImg6.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar kebakaranHujanImg6")

                val paramkebakaranHujanImg6 = HashMap<String, File>()
                paramkebakaranHujanImg6["kebakaranHujanImg6"] = File(surveyData.kebakaranHujanImg6.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramkebakaranHujanImg6)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadporositasKelengasanImg1()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar kebakaranHujanImg6")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar kebakaranHujanImg6 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadporositasKelengasanImg1()
        }
    }
    fun uploadporositasKelengasanImg1(){

        if (surveyData.porositasKelengasanImg1!="" && surveyData.porositasKelengasanImg1!=null){
            if(File(surveyData.porositasKelengasanImg1.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar porositasKelengasanImg1")

                val paramporositasKelengasanImg1 = HashMap<String, File>()
                paramporositasKelengasanImg1["porositasKelengasanImg1"] = File(surveyData.porositasKelengasanImg1.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramporositasKelengasanImg1)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadporositasKelengasanImg2()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar porositasKelengasanImg1")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar porositasKelengasanImg1 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadporositasKelengasanImg2()
        }
    }
    fun uploadporositasKelengasanImg2(){

        if (surveyData.porositasKelengasanImg2!="" && surveyData.porositasKelengasanImg2!=null){
            if(File(surveyData.porositasKelengasanImg2.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar porositasKelengasanImg2")

                val paramporositasKelengasanImg2 = HashMap<String, File>()
                paramporositasKelengasanImg2["porositasKelengasanImg2"] = File(surveyData.porositasKelengasanImg2.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramporositasKelengasanImg2)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadporositasKelengasanImg3()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar porositasKelengasanImg2")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar porositasKelengasanImg2 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadporositasKelengasanImg3()
        }
    }
    fun uploadporositasKelengasanImg3(){

        if (surveyData.porositasKelengasanImg3!="" && surveyData.porositasKelengasanImg3!=null){
            if(File(surveyData.porositasKelengasanImg3.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar porositasKelengasanImg3")

                val paramporositasKelengasanImg3 = HashMap<String, File>()
                paramporositasKelengasanImg3["porositasKelengasanImg3"] = File(surveyData.porositasKelengasanImg3.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramporositasKelengasanImg3)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadporositasKelengasanImg4()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar porositasKelengasanImg3")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar porositasKelengasanImg3 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadporositasKelengasanImg4()
        }
    }
    fun uploadporositasKelengasanImg4(){

        if (surveyData.porositasKelengasanImg4!="" && surveyData.porositasKelengasanImg4!=null){
            if(File(surveyData.porositasKelengasanImg4.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar porositasKelengasanImg4")

                val paramporositasKelengasanImg4 = HashMap<String, File>()
                paramporositasKelengasanImg4["porositasKelengasanImg4"] = File(surveyData.porositasKelengasanImg4.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramporositasKelengasanImg4)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadporositasKelengasanImg5()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar porositasKelengasanImg4")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar porositasKelengasanImg4 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadporositasKelengasanImg5()
        }
    }
    fun uploadporositasKelengasanImg5(){

        if (surveyData.porositasKelengasanImg5!="" && surveyData.porositasKelengasanImg5!=null){
            if(File(surveyData.porositasKelengasanImg5.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar porositasKelengasanImg5")

                val paramporositasKelengasanImg5 = HashMap<String, File>()
                paramporositasKelengasanImg5["porositasKelengasanImg5"] = File(surveyData.porositasKelengasanImg5.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramporositasKelengasanImg5)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadporositasKelengasanImg6()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar porositasKelengasanImg5")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar porositasKelengasanImg5 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadporositasKelengasanImg6()
        }
    }
    fun uploadporositasKelengasanImg6(){

        if (surveyData.porositasKelengasanImg6!="" && surveyData.porositasKelengasanImg6!=null){
            if(File(surveyData.porositasKelengasanImg6.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar porositasKelengasanImg6")

                val paramporositasKelengasanImg6 = HashMap<String, File>()
                paramporositasKelengasanImg6["porositasKelengasanImg6"] = File(surveyData.porositasKelengasanImg6.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramporositasKelengasanImg6)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadsketsaLokasi()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar porositasKelengasanImg6")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar porositasKelengasanImg6 Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadsketsaLokasi()
        }
    }
    fun uploadsketsaLokasi(){

        if (surveyData.sketsaLokasi!="" && surveyData.sketsaLokasi!=null){
            if(File(surveyData.sketsaLokasi.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar sketsaLokasi")

                val paramsketsaLokasi = HashMap<String, File>()
                paramsketsaLokasi["sketsaLokasi"] = File(surveyData.sketsaLokasi.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramsketsaLokasi)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadimgBatasBarat()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar sketsaLokasi")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar sketsaLokasi Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadimgBatasBarat()
        }
    }
    fun uploadimgBatasBarat(){

        if (surveyData.imgBatasBarat!="" && surveyData.imgBatasBarat!=null){
            if(File(surveyData.imgBatasBarat.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar imgBatasBarat")

                val paramimgBatasBarat = HashMap<String, File>()
                paramimgBatasBarat["imgBatasBarat"] = File(surveyData.imgBatasBarat.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramimgBatasBarat)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadimgBatasTimur()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar imgBatasBarat")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar imgBatasBarat Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadimgBatasTimur()
        }
    }
    fun uploadimgBatasTimur(){

        if (surveyData.imgBatasTimur!="" && surveyData.imgBatasTimur!=null){
            if(File(surveyData.imgBatasTimur.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar imgBatasTimur")

                val paramimgBatasTimur = HashMap<String, File>()
                paramimgBatasTimur["imgBatasTimur"] = File(surveyData.imgBatasTimur.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramimgBatasTimur)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadimgBatasSelatan()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar imgBatasTimur")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar imgBatasTimur Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadimgBatasSelatan()
        }
    }
    fun uploadimgBatasSelatan(){

        if (surveyData.imgBatasSelatan!="" && surveyData.imgBatasSelatan!=null){
            if(File(surveyData.imgBatasSelatan.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar imgBatasSelatan")

                val paramimgBatasSelatan = HashMap<String, File>()
                paramimgBatasSelatan["imgBatasSelatan"] = File(surveyData.imgBatasSelatan.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramimgBatasSelatan)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            uploadimgBatasUtara()
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar imgBatasSelatan")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar imgBatasSelatan Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            uploadimgBatasUtara()
        }
    }
    fun uploadimgBatasUtara(){

        if (surveyData.imgBatasUtara!="" && surveyData.imgBatasUtara!=null){
            if(File(surveyData.imgBatasUtara.toString()).exists()){
                progress.dismiss()
                progress.progress("Upload Data Gambar imgBatasUtara")

                val paramimgBatasUtara = HashMap<String, File>()
                paramimgBatasUtara["imgBatasUtara"] = File(surveyData.imgBatasUtara.toString())
                AndroidNetworking.upload(host).setOkHttpClient(this.okHttpClient).addMultipartFile(paramimgBatasUtara)
                    .setPriority(Priority.HIGH).setTag(activity).build()
                    .setUploadProgressListener(object : UploadProgressListener {
                        override fun onProgress(bytesUploaded: Long, totalBytes: Long) {

                        }
                    })
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            surveyData.status = 2
                            surveyData.update()
                            uploadData(0)
                        }

                        override fun onError(anError: ANError?) {
                            progress.dismiss()
                            //survey.deleteAll()

                            errorHandler(anError!!,"Error Upload Gambar imgBatasUtara")
                        }
                    })
            }else{
                dialogError("Error Upload Gambar","Gambar imgBatasUtara Tidak tersediaPeriksa kembali gambar kordinatImg1 di form dan di memory HP.")

            }
        }else{
            surveyData.status = 2
            surveyData.update()
            uploadData(0)
        }
    }
     
    internal fun status() : LiveData<String>{
        return status_
    }

    fun errorHandler(anError: ANError, title:String){
        if (anError.errorCode!=0){
            dialogError(title,"Gangguan pada server \n" + anError.errorDetail  + " " + anError.errorBody + anError.errorCode + " " + anError.message)
        }else{
            if (anError.errorDetail == "connectionError"){
                dialogError(title,"Gangguan Jaringan, Silahkan cek jaringan anda \n" + anError.errorDetail  + " " + anError.errorBody + anError.errorCode + " " + anError.message)
            }else{
                dialogError(title,"Gagal pada KHG ${surveyData.nama_khg} ${surveyData.nomorBoring}, " +
                        "Silahkan coba lagi \n"+ anError.errorDetail  + " " + anError.errorBody + anError.errorCode + " " + anError.message)
            }
        }
    }
    fun dialogError(judul:String, pesan:String){
        activity.alertDialog {
            title = judul
            message = pesan
            setIcon(R.drawable.ic_failed)
            okButton {
                activity.finish()
                activity.overridePendingTransition(0, 0)
                activity.startActivity(activity.intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION))
                activity.overridePendingTransition(0, 0)
                //   mainActivity.initView()
                it.dismiss()
            }
        }.show()
    }
}