package synergy.project.tallysheet.feature.survey.Entry

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import kotlinx.android.synthetic.main.layout_genangan.*
import kotlinx.android.synthetic.main.layout_header.btn_simpan
import kotlinx.android.synthetic.main.layout_tipe_luapan.*
import kotlinx.android.synthetic.main.layout_tutupan_lahan.*
import kotlinx.android.synthetic.main.layout_tutupan_lahan.btn_kembali
import kotlinx.android.synthetic.main.layout_tutupan_lahan.btn_navigasi_1
import kotlinx.android.synthetic.main.layout_tutupan_lahan.btn_navigasi_10
import kotlinx.android.synthetic.main.layout_tutupan_lahan.btn_navigasi_11
import kotlinx.android.synthetic.main.layout_tutupan_lahan.btn_navigasi_12
import kotlinx.android.synthetic.main.layout_tutupan_lahan.btn_navigasi_13
import kotlinx.android.synthetic.main.layout_tutupan_lahan.btn_navigasi_14
import kotlinx.android.synthetic.main.layout_tutupan_lahan.btn_navigasi_15
import kotlinx.android.synthetic.main.layout_tutupan_lahan.btn_navigasi_2
import kotlinx.android.synthetic.main.layout_tutupan_lahan.btn_navigasi_3
import kotlinx.android.synthetic.main.layout_tutupan_lahan.btn_navigasi_4
import kotlinx.android.synthetic.main.layout_tutupan_lahan.btn_navigasi_5
import kotlinx.android.synthetic.main.layout_tutupan_lahan.btn_navigasi_6
import kotlinx.android.synthetic.main.layout_tutupan_lahan.btn_navigasi_7
import kotlinx.android.synthetic.main.layout_tutupan_lahan.btn_navigasi_8
import kotlinx.android.synthetic.main.layout_tutupan_lahan.btn_navigasi_9
import splitties.activities.start
import splitties.toast.toast
import synergy.project.tallysheet.R
import synergy.project.tallysheet.model.Survey
import synergy.project.tallysheet.utils.DatePickerFragment
import synergy.project.tallysheet.utils.Helper
import synergy.project.tallysheet.utils.Local
import synergy.project.tallysheet.utils.PrefManager

class  EntryTutupanLahanActivity : AppCompatActivity() {
    lateinit var survey: Survey
    lateinit var prefManager: PrefManager
    var id : Int= 0

    var pathtutupanLahanImg1 = ""
    var pathtutupanLahanImg2 = ""
    var pathtutupanLahanImg3 = ""
    var pathtutupanLahanImg4 = ""
    var pathtutupanLahanImg5 = ""
    var pathtutupanLahanImg6 = ""

    val activity = EntryTutupanLahanActivity@this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_tutupan_lahan)
        val bundle = intent.extras
        if (bundle != null) {
            id = bundle["id"] as Int
        }
        survey = Survey(applicationContext)
        survey.getData(id)
        prefManager = PrefManager(applicationContext)

        // ini untuk ini value spinner
        val dataAdapter_tutupanLahanPenggunaanLahan: ArrayAdapter<String> = ArrayAdapter<String>(
            this@EntryTutupanLahanActivity,// ganti jadi this@Nama Activity
            android.R.layout.simple_dropdown_item_1line,
            Helper.tutupanLahanPenggunaanLahan
        )

        val dataAdapter_tutupanLahanStatus: ArrayAdapter<String> = ArrayAdapter<String>(
            this@EntryTutupanLahanActivity,// ganti jadi this@Nama Activity
            android.R.layout.simple_dropdown_item_1line,
            Helper.tutupanLahanStatus
        )
        spn_tutupanLahanStatus.adapter = dataAdapter_tutupanLahanStatus
        spn_tutupanLahanPenggunaanLahan.adapter = dataAdapter_tutupanLahanPenggunaanLahan

        if (survey.getData(id)) {
            txt_tutupanLahanJenisTanaman.setText(survey.tutupanLahanJenisTanaman)
            val tutupanLahanStatusAdp = spn_tutupanLahanStatus.adapter as ArrayAdapter<String>
            val tutupanLahanStatusPosition = tutupanLahanStatusAdp.getPosition(survey.tutupanLahanStatus)
            spn_tutupanLahanStatus.setSelection(tutupanLahanStatusPosition)
            txt_tutupanLahanNamaPerusahaan.setText(survey.tutupanLahanNamaPerusahaan)
            txt_tutupanLahanLuas.setText(survey.tutupanLahanLuas)
            val tutupanLahanPenggunaanLahanAdp = spn_tutupanLahanPenggunaanLahan.adapter as ArrayAdapter<String>
            val tutupanLahanPenggunaanLahanPosition = tutupanLahanPenggunaanLahanAdp.getPosition(survey.tutupanLahanPenggunaanLahan)
            spn_tutupanLahanPenggunaanLahan.setSelection(tutupanLahanPenggunaanLahanPosition)

            Glide.with(activity).load(survey.tutupanLahanImg1).into(img_tutupanLahanImg1)
            Glide.with(activity).load(survey.tutupanLahanImg2).into(img_tutupanLahanImg2)
            Glide.with(activity).load(survey.tutupanLahanImg3).into(img_tutupanLahanImg3)
            Glide.with(activity).load(survey.tutupanLahanImg4).into(img_tutupanLahanImg4)
            Glide.with(activity).load(survey.tutupanLahanImg5).into(img_tutupanLahanImg5)
            Glide.with(activity).load(survey.tutupanLahanImg6).into(img_tutupanLahanImg6)

            pathtutupanLahanImg1 = survey.tutupanLahanImg1.toString()
            pathtutupanLahanImg2 = survey.tutupanLahanImg2.toString()
            pathtutupanLahanImg3 = survey.tutupanLahanImg3.toString()
            pathtutupanLahanImg4 = survey.tutupanLahanImg4.toString()
            pathtutupanLahanImg5 = survey.tutupanLahanImg5.toString()
            pathtutupanLahanImg6 = survey.tutupanLahanImg6.toString()
        }

        supportActionBar?.title = "Isi Survey"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayUseLogoEnabled(false)

        initView()
        klik()
    }

    private fun initView() {
        if(pathtutupanLahanImg1!="") Local.isVisible(btn_tutupanLahanImg1,btn_delete_tutupanLahanImg1,true)
        else Local.isVisible(btn_tutupanLahanImg1,btn_delete_tutupanLahanImg1,false)

        if(pathtutupanLahanImg2!="") Local.isVisible(btn_tutupanLahanImg2,btn_delete_tutupanLahanImg2,true)
        else Local.isVisible(btn_tutupanLahanImg2,btn_delete_tutupanLahanImg2,false)

        if(pathtutupanLahanImg3!="") Local.isVisible(btn_tutupanLahanImg3,btn_delete_tutupanLahanImg3,true)
        else Local.isVisible(btn_tutupanLahanImg3,btn_delete_tutupanLahanImg3,false)

        if(pathtutupanLahanImg4!="") Local.isVisible(btn_tutupanLahanImg4,btn_delete_tutupanLahanImg4,true)
        else Local.isVisible(btn_tutupanLahanImg4,btn_delete_tutupanLahanImg4,false)

        if(pathtutupanLahanImg5!="") Local.isVisible(btn_tutupanLahanImg5,btn_delete_tutupanLahanImg5,true)
        else Local.isVisible(btn_tutupanLahanImg5,btn_delete_tutupanLahanImg5,false)

        if(pathtutupanLahanImg6!="") Local.isVisible(btn_tutupanLahanImg6,btn_delete_tutupanLahanImg6,true)
        else Local.isVisible(btn_tutupanLahanImg6,btn_delete_tutupanLahanImg6,false)
    }

    fun klik(){
        btn_simpan.setOnClickListener {
            survey.tutupanLahanJenisTanaman = txt_tutupanLahanJenisTanaman.text.toString()
            survey.tutupanLahanStatus = spn_tutupanLahanStatus.selectedItem.toString()
            survey.tutupanLahanNamaPerusahaan = txt_tutupanLahanNamaPerusahaan.text.toString()
            survey.tutupanLahanLuas = txt_tutupanLahanLuas.text.toString()
            survey.tutupanLahanPenggunaanLahan = spn_tutupanLahanPenggunaanLahan.selectedItem.toString()

            survey.tutupanLahanImg1 = pathtutupanLahanImg1
            survey.tutupanLahanImg2 = pathtutupanLahanImg2
            survey.tutupanLahanImg3 = pathtutupanLahanImg3
            survey.tutupanLahanImg4 = pathtutupanLahanImg4
            survey.tutupanLahanImg5 = pathtutupanLahanImg5
            survey.tutupanLahanImg6 = pathtutupanLahanImg6

            survey.update()
            start<EntryFloraFaunaActivity> {
                putExtra("id", survey.id?.toInt())
            }

        }

        btn_kembali.setOnClickListener {
            survey.tutupanLahanJenisTanaman = txt_tutupanLahanJenisTanaman.text.toString()
            survey.tutupanLahanStatus = spn_tutupanLahanStatus.selectedItem.toString()
            survey.tutupanLahanNamaPerusahaan = txt_tutupanLahanNamaPerusahaan.text.toString()
            survey.tutupanLahanLuas = txt_tutupanLahanLuas.text.toString()
            survey.tutupanLahanPenggunaanLahan = spn_tutupanLahanPenggunaanLahan.selectedItem.toString()

            survey.tutupanLahanImg1 = pathtutupanLahanImg1
            survey.tutupanLahanImg2 = pathtutupanLahanImg2
            survey.tutupanLahanImg3 = pathtutupanLahanImg3
            survey.tutupanLahanImg4 = pathtutupanLahanImg4
            survey.tutupanLahanImg5 = pathtutupanLahanImg5
            survey.tutupanLahanImg6 = pathtutupanLahanImg6

            survey.update()
            onBackPressed()
        }

        btn_tutupanLahanImg1.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(119) }
        btn_tutupanLahanImg2.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(120) }
        btn_tutupanLahanImg3.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(121) }
        btn_tutupanLahanImg4.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(122) }
        btn_tutupanLahanImg5.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(123) }
        btn_tutupanLahanImg6.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(124) }


        //delete image
        deleteImage(btn_delete_tutupanLahanImg1,img_tutupanLahanImg1,1)
        deleteImage(btn_delete_tutupanLahanImg2,img_tutupanLahanImg2,2)
        deleteImage(btn_delete_tutupanLahanImg3,img_tutupanLahanImg3,3)
        deleteImage(btn_delete_tutupanLahanImg4,img_tutupanLahanImg4,4)
        deleteImage(btn_delete_tutupanLahanImg5,img_tutupanLahanImg5,5)
        deleteImage(btn_delete_tutupanLahanImg6,img_tutupanLahanImg6,6)

        //navigasi
        selectNavigasi(btn_navigasi_1,1)
        selectNavigasi(btn_navigasi_2,2)
        selectNavigasi(btn_navigasi_3,3)
        selectNavigasi(btn_navigasi_4,4)
        selectNavigasi(btn_navigasi_5,5)
        selectNavigasi(btn_navigasi_6,6)
        selectNavigasi(btn_navigasi_7,7)
        selectNavigasi(btn_navigasi_8,8)
        selectNavigasi(btn_navigasi_9,9)
        selectNavigasi(btn_navigasi_10,10)
        selectNavigasi(btn_navigasi_11,11)
        selectNavigasi(btn_navigasi_12,12)
        selectNavigasi(btn_navigasi_13,13)
        selectNavigasi(btn_navigasi_14,14)
        selectNavigasi(btn_navigasi_15,15)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode==119){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_tutupanLahanImg1.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_tutupanLahanImg1)
                survey.tutupanLahanImg1 = filePath
                pathtutupanLahanImg1 = filePath
            }
        }

        if (requestCode==120){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_tutupanLahanImg2.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_tutupanLahanImg2)
                survey.tutupanLahanImg2 = filePath
                pathtutupanLahanImg2 = filePath
            }
        }

        if (requestCode==121){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_tutupanLahanImg3.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_tutupanLahanImg3)
                survey.tutupanLahanImg3 = filePath
                pathtutupanLahanImg3 = filePath
            }
        }

        if (requestCode==122){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_tutupanLahanImg4.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_tutupanLahanImg4)
                survey.tutupanLahanImg4 = filePath
                pathtutupanLahanImg4 = filePath
            }
        }

        if (requestCode==123){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_tutupanLahanImg5.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_tutupanLahanImg5)
                survey.tutupanLahanImg5 = filePath
                pathtutupanLahanImg5 = filePath
            }
        }

        if (requestCode==124){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_tutupanLahanImg6.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_tutupanLahanImg6)
                survey.tutupanLahanImg6 = filePath
                pathtutupanLahanImg6 = filePath
            }
        }

    }

    fun returnData(id:Int){
        if (survey.getData(id)) {

        } else {

        }
    }

    fun selectNavigasi(button: Button, code:Int){
        button.setOnClickListener {
            when(code){
                1 -> navigasiIntent(EntryKordinatActivity::class.java)
                2 -> navigasiIntent(EntryElevasiActivity::class.java)
                3 -> navigasiIntent(EntryGenanganActivity::class.java)
                //4 -> navigasiIntent(EntryTutupanLahanActivity::class.java)
                5 -> navigasiIntent(EntryFloraFaunaActivity::class.java)
                6 ->  navigasiIntent(EntryDrainaseActivity::class.java)
                7 -> navigasiIntent(EntryKualitasAirTanahActivity::class.java)
                8 -> navigasiIntent(EntryKarakteristikSubstratumTanahActivity::class.java)
                9 -> navigasiIntent(EntryTipeLuapanActivity::class.java)
                10 -> navigasiIntent(EntryKetebalanGambutActivity::class.java)
                11 -> navigasiIntent(EntryKarakteristikSubstratumDibawahLapisanGambutActivity::class.java)
                12 -> navigasiIntent(EntryTingkatKerusakanLahanActivity::class.java)
                13 -> navigasiIntent(EntryKebakaranLahanActivity::class.java)
                14 -> navigasiIntent(EntryPorositasKelengasanActivity::class.java)
                15 -> navigasiIntent(EntryDataGambarActivity::class.java)
            }
        }
    }

    private fun navigasiIntent(classtujuan: Class<*>) {
        survey.tutupanLahanJenisTanaman = txt_tutupanLahanJenisTanaman.text.toString()
        survey.tutupanLahanStatus = spn_tutupanLahanStatus.selectedItem.toString()
        survey.tutupanLahanNamaPerusahaan = txt_tutupanLahanNamaPerusahaan.text.toString()
        survey.tutupanLahanLuas = txt_tutupanLahanLuas.text.toString()
        survey.tutupanLahanPenggunaanLahan = spn_tutupanLahanPenggunaanLahan.selectedItem.toString()

        survey.tutupanLahanImg1 = pathtutupanLahanImg1
        survey.tutupanLahanImg2 = pathtutupanLahanImg2
        survey.tutupanLahanImg3 = pathtutupanLahanImg3
        survey.tutupanLahanImg4 = pathtutupanLahanImg4
        survey.tutupanLahanImg5 = pathtutupanLahanImg5
        survey.tutupanLahanImg6 = pathtutupanLahanImg6
        survey.update()
        val intent = Intent(activity,classtujuan)
        intent.putExtra("id", survey.id?.toInt())
        startActivity(intent)

    }

    fun deleteImage(button: ImageButton, imageView: ImageView, code: Int){
        button.setOnClickListener {
            when(code){
                1-> pathtutupanLahanImg1 = ""
                2-> pathtutupanLahanImg2 = ""
                3-> pathtutupanLahanImg3 = ""
                4-> pathtutupanLahanImg4 = ""
                5-> pathtutupanLahanImg5 = ""
                6-> pathtutupanLahanImg6 = ""
            }

            Glide.with(activity).load("").into(imageView)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
    override fun onResume() {
        if(id != null){
            survey.getData(id)
        }
        super.onResume()
    }

}