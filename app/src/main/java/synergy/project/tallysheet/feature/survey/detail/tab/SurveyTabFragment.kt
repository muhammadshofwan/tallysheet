package synergy.project.tallysheet.feature.survey.detail.tab

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.fragment_survey_tab.view.*
import synergy.project.tallysheet.R
import synergy.project.tallysheet.model.Survey
import synergy.project.tallysheet.utils.Helper

class SurveyTabFragment : Fragment() {
    lateinit var root:View
    var nomorBoring:String?=null
    var id_survey = 0
    lateinit var survey: Survey
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_survey_tab, container, false)
        survey = Survey(requireContext())


        survey.getData(id_survey)

        initView()

        return root
    }

    private fun initView() {
        /**
        root.txt_tanggal.setText(survey.tanggal)
        root.txt_surveyorId.setText(survey.surveyorId)
        root.txt_kelurahan.setText(survey.desa)
        root.txt_kecamatan.setText(survey.kecamatan)
        root.lbl_longitude.setText(survey.longitude)
        root.lbl_latitude.setText(survey.latitude)
        root.txt_suhu.setText(survey.suhu)
        root.txt_elevasi.setText(survey.elevasi)
        root.txt_kedalamanAirTanah.setText(survey.kedalamanAirTanah)
        root.txt_genangan.setText(survey.genangan)
        root.txt_banjirLamaFrekuensi.setText(survey.banjirLamaFrekuensi)
        root.txt_banjirKetinggian.setText(survey.banjirKetinggian)
        root.txt_drainaseAlamiTinggiTebing.setText(survey.drainaseAlamiTinggiTebing)
        root.txt_drainaseAlamiKedalamanAirSaluran.setText(survey.drainaseAlamiKedalamanAirSaluran)
        root.txt_drainaseAlamiLebarSaluran.setText(survey.drainaseAlamiKedalamanAirSaluran)
        root.txt_drainaseAlamiKerapatanDrainase.setText(survey.drainaseAlamiKerapatanDrainase)
        root.txt_drainaseBuatanTinggiTebing.setText(survey.drainaseBuatanTinggiTebing)
        root.txt_drainaseBuatanKedalamanAirSaluran.setText(survey.drainaseBuatanKedalamanAirSaluran)
        root.txt_drainaseBuatanLebarSaluran.setText(survey.drainaseBuatanLebarSaluran)
        root.txt_drainaseBuatanKerapatanDrainase.setText(survey.drainaseBuatanKerapatanDrainase)
        root.txt_tutupanLahanJenisTanaman1.setText(survey.tutupanLahanJenisTanaman1)
        root.txt_tutupanLahanJenisTanaman2.setText(survey.tutupanLahanJenisTanaman2)
        root.txt_tutupanLahanJenisTanaman3.setText(survey.tutupanLahanJenisTanaman3)
        root.txt_tutupanLahanJenisTanaman4.setText(survey.tutupanLahanJenisTanaman4)
        root.txt_tutupanLahanJenisTanaman5.setText(survey.tutupanLahanJenisTanaman5)
        root.txt_floraNama1.setText(survey.floraNama1)
        root.txt_floraNama2.setText(survey.floraNama2)
        root.txt_floraNama3.setText(survey.floraNama3)
        root.txt_floraNama4.setText(survey.floraNama4)
        root.txt_floraNama5.setText(survey.floraNama5)
        root. txt_faunaSeringDitemuiNama1.setText(survey.faunaSeringDitemuiNama1)
        root.txt_faunaSeringDitemuiNama2.setText(survey.faunaSeringDitemuiNama2)
        root.txt_faunaSeringDitemuiNama3.setText(survey.faunaSeringDitemuiNama3)
        root.txt_faunaSeringDitemuiNama4.setText(survey.faunaSeringDitemuiNama4)
        root.txt_faunaSeringDitemuiNama5.setText(survey.faunaSeringDitemuiNama5)
        root.txt_faunaJarangDitemuiNama1.setText(survey.faunaJarangDitemuiNama1)
        root.txt_faunaJarangDitemuiNama2.setText(survey.faunaJarangDitemuiNama2)
        root.txt_faunaJarangDitemuiNama3.setText(survey.faunaJarangDitemuiNama3)
        root.txt_faunaJarangDitemuiNama4.setText(survey.faunaJarangDitemuiNama4)
        root.txt_faunaJarangDitemuiNama5.setText(survey.faunaJarangDitemuiNama5)
        root.txt_kualitasAirDrainaseAlamiPh.setText(survey.kualitasAirDrainaseAlamiPh)
        root.txt_kualitasAirDrainaseAlamiEc.setText(survey.kualitasAirDrainaseAlamiEc)
        root.txt_kualitasAirDrainaseAlamiTds.setText(survey.kualitasAirDrainaseAlamiTds)
        root.txt_kualitasAirDrainaseAlamiTemperatur.setText(survey.kualitasAirDrainaseAlamiTemperatur)
        root.txt_kualitasAirDrainaseBuatanPh.setText(survey.kualitasAirDrainaseBuatanPh)
        root.txt_kualitasAirDrainaseBuatanEc.setText(survey.kualitasAirDrainaseBuatanEc)
        root.txt_kualitasAirDrainaseBuatanTds.setText(survey.kualitasAirDrainaseBuatanTds)
        root.txt_kualitasAirDrainaseBuatanTemperatur.setText(survey.kualitasAirDrainaseBuatanTemperatur)
        root.txt_kualitasAirTanahPh.setText(survey.kualitasAirTanahPh)
        root.txt_kualitasAirTanahEc.setText(survey.kualitasAirTanahEc)
        root.txt_kualitasAirTanahTds.setText(survey.kualitasAirTanahTds)
        root.txt_kualitasAirTanahTemperatur.setText(survey.kualitasAirTanahTemperatur)
        root.txt_ketebalanGambut.setText(survey.ketebalanGambut)
        root.txt_karakteristikSubstratumLainnya.setText(survey.karakteristikSubstratumLainnya)
        root.txt_tingkatKerusakanJenisVegetasi1.setText(survey.tingkatKerusakanJenisVegetasi1)
        root.txt_tingkatKerusakanJenisVegetasi2.setText(survey.tingkatKerusakanJenisVegetasi2)
        root.txt_tingkatKerusakanJenisVegetasi3.setText(survey.tingkatKerusakanJenisVegetasi3)
        root.txt_tingkatKerusakanJenisVegetasi4.setText(survey.tingkatKerusakanJenisVegetasi4)
        root.txt_tingkatKerusakanJenisVegetasi5.setText(survey.tingkatKerusakanJenisVegetasi5)
        root.txt_karakteristikTanahPh1.setText(survey.karakteristikTanahPh1)
        root.txt_karakteristikTanahWarna1.setText(survey.karakteristikTanahWarna1)
        root.txt_karakteristikTanahPh2.setText(survey.karakteristikTanahPh2)
        root.txt_karakteristikTanahWarna2.setText(survey.karakteristikTanahWarna2)
        root.txt_karakteristikTanahPh3.setText(survey.karakteristikTanahPh3)
        root.txt_karakteristikTanahWarna3.setText(survey.karakteristikTanahWarna3)
        root.txt_karakteristikTanahPh4.setText(survey.karakteristikTanahPh4)
        root.txt_karakteristikTanahWarna4.setText(survey.karakteristikTanahWarna4)
        root.txt_substratumKedalaman.setText(survey.substratumKedalaman)
        root.txt_substratumWarna.setText(survey.substratumWarna)
        root.txt_substratumKonsistensi.setText(survey.substratumKonsistensi)
        root.txt_substratumPh.setText(survey.substratumPh)
        root.txt_substratumEc.setText(survey.substratumEc)
        root.txt_substratumLainya2.setText(survey.substratumLainya2)
        root.txt_bulkDensity1.setText(survey.bulkDensity1)
        root.txt_kadarAbu1.setText(survey.kadarAbu1)
        root.txt_bulkDensity2.setText(survey.bulkDensity2)
        root.txt_kadarAbu2.setText(survey.kadarAbu2)
        root.txt_bulkDensity3.setText(survey.bulkDensity3)
        root.txt_kadarAbu3.setText(survey.kadarAbu3)
        root.txt_bulkDensity4.setText(survey.bulkDensity4)
        root.txt_kadarAbu4.setText(survey.kadarAbu4)
        root.txt_kebakaranTanggal.setText(survey.kebakaranTanggal)
        root.txt_kebakaranLama.setText(survey.kebakaranLama)
        root.txt_hujanLama.setText(survey.hujanLama)

        root.txt_lahanKedalaman1.setText(survey.lahanKedalaman1)
        root.txt_lahanSpecialFeature1.setText(survey.lahanSpecialFeature1)
        root.txt_lahanKedalaman2.setText(survey.lahanKedalaman2)
        root.txt_lahanSpecialFeature2.setText(survey.lahanSpecialFeature2)
        root.txt_lahanKedalaman3.setText(survey.lahanKedalaman3)
        root.txt_lahanSpecialFeature3.setText(survey.lahanSpecialFeature3)
        root.txt_lahanKedalaman4.setText(survey.lahanKedalaman4)
        root.txt_lahanSpecialFeature4.setText(survey.lahanSpecialFeature4)
        root.txt_lahanKedalaman5.setText(survey.lahanKedalaman5)
        root.txt_lahanSpecialFeature5.setText(survey.lahanSpecialFeature5)

        dataSpinner()
            */
    }

    private fun dataSpinner() {
//data spinner
        /**
        val dataAdapter_kondisiCuaca: ArrayAdapter<String> = ArrayAdapter<String>(requireActivity(), android.R.layout.simple_dropdown_item_1line, Helper.valueKondisiCuaca)
        val dataAdapter_drainaseAlami: ArrayAdapter<String> = ArrayAdapter<String>(
            requireActivity(),
            android.R.layout.simple_dropdown_item_1line,
            Helper.valueDrainaseAlami
        )
        val dataAdapter_drainaseAlamiKondisiSaluran: ArrayAdapter<String> =
            ArrayAdapter<String>(
                requireActivity(),
                android.R.layout.simple_dropdown_item_1line,
                Helper.valueDrainaseAlamiKondisiSaluran
            )
        val dataAdapter_drainaseBuatan: ArrayAdapter<String> = ArrayAdapter<String>(
            requireActivity(),
            android.R.layout.simple_dropdown_item_1line,
            Helper.valueDrainaseBuatan
        )
        val dataAdapter_drainaseBuatanKondisiSaluran: ArrayAdapter<String> =
            ArrayAdapter<String>(
                requireActivity(),
                android.R.layout.simple_dropdown_item_1line,
                Helper.valueDrainaseBuatanKondisiSaluran
            )
        val dataAdapter_tutupanLahan: ArrayAdapter<String> = ArrayAdapter<String>(
            requireActivity(),
            android.R.layout.simple_dropdown_item_1line,
            Helper.valueTutupanLahan
        )
        val dataAdapter_tutupanLahanKondisiTumbuhan: ArrayAdapter<String> =
            ArrayAdapter<String>(
                requireActivity(),
                android.R.layout.simple_dropdown_item_1line,
                Helper.valueTutupanLahanKondisiTumbuhan
            )
        val dataAdapter_tutupanLahanKondisiTegakan: ArrayAdapter<String> =
            ArrayAdapter<String>(
                requireActivity(),
                android.R.layout.simple_dropdown_item_1line,
                Helper.valueTutupanLahanKondisiTegakan
            )
        val dataAdapter_tutupanLahanSebelumnya: ArrayAdapter<String> =
            ArrayAdapter<String>(
                requireActivity(),
                android.R.layout.simple_dropdown_item_1line,
                Helper.valueTutupanLahanSebelumnya
            )
        val dataAdapter_flora: ArrayAdapter<String> = ArrayAdapter<String>(
            requireActivity(),
            android.R.layout.simple_dropdown_item_1line,
            Helper.valueFlora
        )
        val dataAdapter_fauna: ArrayAdapter<String> = ArrayAdapter<String>(
            requireActivity(),
            android.R.layout.simple_dropdown_item_1line,
            Helper.valueFauna
        )
        val dataAdapter_faunaSeringDitemuiStatus1: ArrayAdapter<String> =
            ArrayAdapter<String>(
                requireActivity(),
                android.R.layout.simple_dropdown_item_1line,
                Helper.valueFaunaSeringDitemuiStatus1
            )
        val dataAdapter_faunaSeringDitemuiStatus2: ArrayAdapter<String> =
            ArrayAdapter<String>(
                requireActivity(),
                android.R.layout.simple_dropdown_item_1line,
                Helper.valueFaunaSeringDitemuiStatus2
            )
        val dataAdapter_faunaSeringDitemuiStatus3: ArrayAdapter<String> =
            ArrayAdapter<String>(
                requireActivity(),
                android.R.layout.simple_dropdown_item_1line,
                Helper.valueFaunaSeringDitemuiStatus3
            )
        val dataAdapter_faunaSeringDitemuiStatus4: ArrayAdapter<String> =
            ArrayAdapter<String>(
                requireActivity(),
                android.R.layout.simple_dropdown_item_1line,
                Helper.valueFaunaSeringDitemuiStatus4
            )
        val dataAdapter_faunaSeringDitemuiStatus5: ArrayAdapter<String> =
            ArrayAdapter<String>(
                requireActivity(),
                android.R.layout.simple_dropdown_item_1line,
                Helper.valueFaunaSeringDitemuiStatus5
            )
        val dataAdapter_faunaJarangDitemuiStatus1: ArrayAdapter<String> =
            ArrayAdapter<String>(
                requireActivity(),
                android.R.layout.simple_dropdown_item_1line,
                Helper.valueFaunaJarangDitemuiStatus1
            )
        val dataAdapter_faunaJarangDitemuiStatus2: ArrayAdapter<String> =
            ArrayAdapter<String>(
                requireActivity(),
                android.R.layout.simple_dropdown_item_1line,
                Helper.valueFaunaJarangDitemuiStatus2
            )
        val dataAdapter_faunaJarangDitemuiStatus3: ArrayAdapter<String> =
            ArrayAdapter<String>(
                requireActivity(),
                android.R.layout.simple_dropdown_item_1line,
                Helper.valueFaunaJarangDitemuiStatus3
            )
        val dataAdapter_faunaJarangDitemuiStatus4: ArrayAdapter<String> =
            ArrayAdapter<String>(
                requireActivity(),
                android.R.layout.simple_dropdown_item_1line,
                Helper.valueFaunaJarangDitemuiStatus4
            )
        val dataAdapter_faunaJarangDitemuiStatus5: ArrayAdapter<String> =
            ArrayAdapter<String>(
                requireActivity(),
                android.R.layout.simple_dropdown_item_1line,

                Helper.valueFaunaJarangDitemuiStatus5
            )
        val dataAdapter_tipeLuapan: ArrayAdapter<String> = ArrayAdapter<String>(
            requireActivity(),
            android.R.layout.simple_dropdown_item_1line,
            Helper.valueTipeLuapan
        )
        val dataAdapter_kematanganGambut1: ArrayAdapter<String> =
            ArrayAdapter<String>(
                requireActivity(),
                android.R.layout.simple_dropdown_item_1line,
                Helper.valueKematanganGambut1
            )
        val dataAdapter_kematanganGambut2: ArrayAdapter<String> =
            ArrayAdapter<String>(
                requireActivity(),
                android.R.layout.simple_dropdown_item_1line,
                Helper.valueKematanganGambut2
            )
        val dataAdapter_kematanganGambut3: ArrayAdapter<String> =
            ArrayAdapter<String>(
                requireActivity(),
                android.R.layout.simple_dropdown_item_1line,
                Helper.valueKematanganGambut3
            )
        val dataAdapter_kematanganGambut4: ArrayAdapter<String> =
            ArrayAdapter<String>(
                requireActivity(),
                android.R.layout.simple_dropdown_item_1line,
                Helper.valueKematanganGambut4
            )
        val dataAdapter_karakteristikSubstratum: ArrayAdapter<String> =
            ArrayAdapter<String>(
                requireActivity(),
                android.R.layout.simple_dropdown_item_1line,
                Helper.valueKarakteristikSubstratum
            )
        val dataAdapter_tingkatKerusakan: ArrayAdapter<String> = ArrayAdapter<String>(
            requireActivity(),
            android.R.layout.simple_dropdown_item_1line,
            Helper.valueTingkatKerusakan
        )
        val dataAdapter_tingkatKerusakanKerapatanTajuk: ArrayAdapter<String> =
            ArrayAdapter<String>(
                requireActivity(),
                android.R.layout.simple_dropdown_item_1line,
                Helper.valueTingkatKerusakanKerapatanTajuk
            )
        val dataAdapter_karakteristikTanahTingkatDekomposisi1: ArrayAdapter<String> =
            ArrayAdapter<String>(
                requireActivity(),
                android.R.layout.simple_dropdown_item_1line,
                Helper.valueKarakteristikTanahTingkatDekomposisi1
            )
        val dataAdapter_karakteristikTanahPirit1: ArrayAdapter<String> =
            ArrayAdapter<String>(
                requireActivity(),
                android.R.layout.simple_dropdown_item_1line,
                Helper.valueKarakteristikTanahPirit1
            )
        val dataAdapter_karakteristikTanahTingkatDekomposisi2: ArrayAdapter<String> =
            ArrayAdapter<String>(
                requireActivity(),
                android.R.layout.simple_dropdown_item_1line,
                Helper.valueKarakteristikTanahTingkatDekomposisi2
            )
        val dataAdapter_karakteristikTanahPirit2: ArrayAdapter<String> =
            ArrayAdapter<String>(
                requireActivity(),
                android.R.layout.simple_dropdown_item_1line,
                Helper.valueKarakteristikTanahPirit2
            )
        val dataAdapter_karakteristikTanahTingkatDekomposisi3: ArrayAdapter<String> =
            ArrayAdapter<String>(
                requireActivity(),
                android.R.layout.simple_dropdown_item_1line,
                Helper.valueKarakteristikTanahTingkatDekomposisi3
            )
        val dataAdapter_karakteristikTanahPirit3: ArrayAdapter<String> =
            ArrayAdapter<String>(
                requireActivity(),
                android.R.layout.simple_dropdown_item_1line,
                Helper.valueKarakteristikTanahPirit3
            )
        val dataAdapter_karakteristikTanahTingkatDekomposisi4: ArrayAdapter<String> =
            ArrayAdapter<String>(
                requireActivity(),
                android.R.layout.simple_dropdown_item_1line,
                Helper.valueKarakteristikTanahTingkatDekomposisi4
            )
        val dataAdapter_karakteristikTanahPirit4: ArrayAdapter<String> =
            ArrayAdapter<String>(
                requireActivity(),
                android.R.layout.simple_dropdown_item_1line,
                Helper.valueKarakteristikTanahPirit4
            )
        val dataAdapter_substratumJenis: ArrayAdapter<String> = ArrayAdapter<String>(
            requireActivity(),
            android.R.layout.simple_dropdown_item_1line,
            Helper.valueSubstratumJenis
        )
        val dataAdapter_substratumTekstur: ArrayAdapter<String> =
            ArrayAdapter<String>(
                requireActivity(),
                android.R.layout.simple_dropdown_item_1line,
                Helper.valueSubstratumTekstur
            )
        val dataAdapter_substratumPirit: ArrayAdapter<String> = ArrayAdapter<String>(
            requireActivity(),
            android.R.layout.simple_dropdown_item_1line,
            Helper.valueSubstratumPirit
        )
        val dataAdapter_substratumJenis2: ArrayAdapter<String> = ArrayAdapter<String>(
            requireActivity(),
            android.R.layout.simple_dropdown_item_1line,
            Helper.valueSubstratumJenis2
        )
        val dataAdapter_kebakaranFaktor: ArrayAdapter<String> = ArrayAdapter<String>(
            requireActivity(),
            android.R.layout.simple_dropdown_item_1line,
            Helper.valueKebakaranFaktor
        )
        val dataAdapter_kebakaranPenanganan: ArrayAdapter<String> =
            ArrayAdapter<String>(
                requireActivity(),
                android.R.layout.simple_dropdown_item_1line,
                Helper.valueKebakaranPenanganan
            )
        val dataAdapter_hujanTerakhir: ArrayAdapter<String> = ArrayAdapter<String>(
            requireActivity(),
            android.R.layout.simple_dropdown_item_1line,
            Helper.valueHujanTerakhir
        )
        val dataAdapter_hujanIntensitas: ArrayAdapter<String> = ArrayAdapter<String>(
            requireActivity(),
            android.R.layout.simple_dropdown_item_1line,
            Helper.valueHujanIntensitas
        )

        //setting adapter
        root.spn_kondisiCuaca.adapter = dataAdapter_kondisiCuaca
        root.spn_drainaseAlami.adapter = dataAdapter_drainaseAlami
        root.spn_drainaseAlamiKondisiSaluran.adapter = dataAdapter_drainaseAlamiKondisiSaluran
        root.spn_drainaseBuatan.adapter = dataAdapter_drainaseBuatan
        root.spn_drainaseBuatanKondisiSaluran.adapter = dataAdapter_drainaseBuatanKondisiSaluran
        root.spn_tutupanLahan.adapter = dataAdapter_tutupanLahan
        root.spn_tutupanLahanKondisiTumbuhan.adapter = dataAdapter_tutupanLahanKondisiTumbuhan
        root.spn_tutupanLahanKondisiTegakan.adapter = dataAdapter_tutupanLahanKondisiTegakan
        root.spn_tutupanLahanSebelumnya.adapter = dataAdapter_tutupanLahanSebelumnya
        root.spn_flora.adapter = dataAdapter_flora
        root.spn_fauna.adapter = dataAdapter_fauna
        root.spn_faunaSeringDitemuiStatus1.adapter = dataAdapter_faunaSeringDitemuiStatus1
        root.spn_faunaSeringDitemuiStatus2.adapter = dataAdapter_faunaSeringDitemuiStatus2
        root.spn_faunaSeringDitemuiStatus3.adapter = dataAdapter_faunaSeringDitemuiStatus3
        root.spn_faunaSeringDitemuiStatus4.adapter = dataAdapter_faunaSeringDitemuiStatus4
        root.spn_faunaSeringDitemuiStatus5.adapter = dataAdapter_faunaSeringDitemuiStatus5
        root.spn_faunaJarangDitemuiStatus1.adapter = dataAdapter_faunaJarangDitemuiStatus1
        root.spn_faunaJarangDitemuiStatus2.adapter = dataAdapter_faunaJarangDitemuiStatus2
        root.spn_faunaJarangDitemuiStatus3.adapter = dataAdapter_faunaJarangDitemuiStatus3
        root.spn_faunaJarangDitemuiStatus4.adapter = dataAdapter_faunaJarangDitemuiStatus4
        root.spn_faunaJarangDitemuiStatus5.adapter = dataAdapter_faunaJarangDitemuiStatus5
        root.spn_tipeLuapan.adapter = dataAdapter_tipeLuapan
        root.spn_kematanganGambut1.adapter = dataAdapter_kematanganGambut1
        root.spn_kematanganGambut2.adapter = dataAdapter_kematanganGambut2
        root.spn_kematanganGambut3.adapter = dataAdapter_kematanganGambut3
        root.spn_kematanganGambut4.adapter = dataAdapter_kematanganGambut4
        root.spn_karakteristikSubstratum.adapter = dataAdapter_karakteristikSubstratum
        root.spn_tingkatKerusakan.adapter = dataAdapter_tingkatKerusakan
        root.spn_tingkatKerusakanKerapatanTajuk.adapter = dataAdapter_tingkatKerusakanKerapatanTajuk
        root.spn_karakteristikTanahTingkatDekomposisi1.adapter =
            dataAdapter_karakteristikTanahTingkatDekomposisi1
        root.spn_karakteristikTanahPirit1.adapter = dataAdapter_karakteristikTanahPirit1
        root.spn_karakteristikTanahTingkatDekomposisi2.adapter =
            dataAdapter_karakteristikTanahTingkatDekomposisi2
        root.spn_karakteristikTanahPirit2.adapter = dataAdapter_karakteristikTanahPirit2
        root.spn_karakteristikTanahTingkatDekomposisi3.adapter =
            dataAdapter_karakteristikTanahTingkatDekomposisi3
        root.spn_karakteristikTanahPirit3.adapter = dataAdapter_karakteristikTanahPirit3
        root.spn_karakteristikTanahTingkatDekomposisi4.adapter =
            dataAdapter_karakteristikTanahTingkatDekomposisi4
        root.spn_karakteristikTanahPirit4.adapter = dataAdapter_karakteristikTanahPirit4
        root.spn_substratumJenis.adapter = dataAdapter_substratumJenis
        root.spn_substratumTekstur.adapter = dataAdapter_substratumTekstur
        root.spn_substratumPirit.adapter = dataAdapter_substratumPirit
        root.spn_substratumJenis2.adapter = dataAdapter_substratumJenis2
        root.spn_kebakaranFaktor.adapter = dataAdapter_kebakaranFaktor
        root.spn_kebakaranPenanganan.adapter = dataAdapter_kebakaranPenanganan
        root.spn_hujanTerakhir.adapter = dataAdapter_hujanTerakhir
        root.spn_hujanIntensitas.adapter = dataAdapter_hujanIntensitas

        //selection data

        val kondisiCuacaAdp =
            root.spn_kondisiCuaca.adapter as ArrayAdapter<String>
        val drainaseAlamiAdp =
            root.spn_drainaseAlami.adapter as ArrayAdapter<String>
        val drainaseAlamiKondisiSaluranAdp =
            root.spn_drainaseAlamiKondisiSaluran.adapter as ArrayAdapter<String>
        val drainaseBuatanAdp =
            root.spn_drainaseBuatan.adapter as ArrayAdapter<String>
        val drainaseBuatanKondisiSaluranAdp =
            root.spn_drainaseBuatanKondisiSaluran.adapter as ArrayAdapter<String>
        val tutupanLahanAdp =
            root.spn_tutupanLahan.adapter as ArrayAdapter<String>
        val tutupanLahanKondisiTumbuhanAdp =
            root.spn_tutupanLahanKondisiTumbuhan.adapter as ArrayAdapter<String>
        val tutupanLahanKondisiTegakanAdp =
            root.spn_tutupanLahanKondisiTegakan.adapter as ArrayAdapter<String>
        val tutupanLahanSebelumnyaAdp =
            root.spn_tutupanLahanSebelumnya.adapter as ArrayAdapter<String>
        val floraAdp = root.spn_flora.adapter as ArrayAdapter<String>
        val faunaAdp = root.spn_fauna.adapter as ArrayAdapter<String>
        val faunaSeringDitemuiStatus1Adp =
            root.spn_faunaSeringDitemuiStatus1.adapter as ArrayAdapter<String>
        val faunaSeringDitemuiStatus2Adp =
            root.spn_faunaSeringDitemuiStatus2.adapter as ArrayAdapter<String>
        val faunaSeringDitemuiStatus3Adp =
            root.spn_faunaSeringDitemuiStatus3.adapter as ArrayAdapter<String>
        val faunaSeringDitemuiStatus4Adp =
            root.spn_faunaSeringDitemuiStatus4.adapter as ArrayAdapter<String>
        val faunaSeringDitemuiStatus5Adp =
            root.spn_faunaSeringDitemuiStatus5.adapter as ArrayAdapter<String>
        val faunaJarangDitemuiStatus1Adp =
            root.spn_faunaJarangDitemuiStatus1.adapter as ArrayAdapter<String>
        val faunaJarangDitemuiStatus2Adp =
            root.spn_faunaJarangDitemuiStatus2.adapter as ArrayAdapter<String>
        val faunaJarangDitemuiStatus3Adp =
            root.spn_faunaJarangDitemuiStatus3.adapter as ArrayAdapter<String>
        val faunaJarangDitemuiStatus4Adp =
            root.spn_faunaJarangDitemuiStatus4.adapter as ArrayAdapter<String>
        val faunaJarangDitemuiStatus5Adp =
            root.spn_faunaJarangDitemuiStatus5.adapter as ArrayAdapter<String>
        val tipeLuapanAdp =
            root.spn_tipeLuapan.adapter as ArrayAdapter<String>
        val kematanganGambut1Adp =
            root.spn_kematanganGambut1.adapter as ArrayAdapter<String>
        val kematanganGambut2Adp =
            root.spn_kematanganGambut2.adapter as ArrayAdapter<String>
        val kematanganGambut3Adp =
            root.spn_kematanganGambut3.adapter as ArrayAdapter<String>
        val kematanganGambut4Adp =
            root.spn_kematanganGambut4.adapter as ArrayAdapter<String>
        val karakteristikSubstratumAdp =
            root.spn_karakteristikSubstratum.adapter as ArrayAdapter<String>
        val tingkatKerusakanAdp =
            root.spn_tingkatKerusakan.adapter as ArrayAdapter<String>
        val tingkatKerusakanKerapatanTajukAdp =
            root.spn_tingkatKerusakanKerapatanTajuk.adapter as ArrayAdapter<String>
        val karakteristikTanahTingkatDekomposisi1Adp =
            root.spn_karakteristikTanahTingkatDekomposisi1.adapter as ArrayAdapter<String>
        val karakteristikTanahPirit1Adp =
            root.spn_karakteristikTanahPirit1.adapter as ArrayAdapter<String>
        val karakteristikTanahTingkatDekomposisi2Adp =
            root.spn_karakteristikTanahTingkatDekomposisi2.adapter as ArrayAdapter<String>
        val karakteristikTanahPirit2Adp =
            root.spn_karakteristikTanahPirit2.adapter as ArrayAdapter<String>
        val karakteristikTanahTingkatDekomposisi3Adp =
            root.spn_karakteristikTanahTingkatDekomposisi3.adapter as ArrayAdapter<String>
        val karakteristikTanahPirit3Adp =
            root.spn_karakteristikTanahPirit3.adapter as ArrayAdapter<String>
        val karakteristikTanahTingkatDekomposisi4Adp =
            root.spn_karakteristikTanahTingkatDekomposisi4.adapter as ArrayAdapter<String>
        val karakteristikTanahPirit4Adp =
            root.spn_karakteristikTanahPirit4.adapter as ArrayAdapter<String>
        val substratumJenisAdp =
            root.spn_substratumJenis.adapter as ArrayAdapter<String>
        val substratumTeksturAdp =
            root.spn_substratumTekstur.adapter as ArrayAdapter<String>
        val substratumPiritAdp =
            root.spn_substratumPirit.adapter as ArrayAdapter<String>
        val substratumJenis2Adp =
            root.spn_substratumJenis2.adapter as ArrayAdapter<String>
        val kebakaranFaktorAdp =
            root.spn_kebakaranFaktor.adapter as ArrayAdapter<String>
        val kebakaranPenangananAdp =
            root.spn_kebakaranPenanganan.adapter as ArrayAdapter<String>
        val hujanTerakhirAdp =
            root.spn_hujanTerakhir.adapter as ArrayAdapter<String>
        val hujanIntensitasAdp =
            root.spn_hujanIntensitas.adapter as ArrayAdapter<String>

        val kondisiCuacaPosition =
            kondisiCuacaAdp.getPosition(survey.kondisiCuaca)
        val drainaseAlamiPosition =
            drainaseAlamiAdp.getPosition(survey.drainaseAlami)
        val drainaseAlamiKondisiSaluranPosition =
            drainaseAlamiKondisiSaluranAdp.getPosition(survey.drainaseAlamiKondisiSaluran)
        val drainaseBuatanPosition =
            drainaseBuatanAdp.getPosition(survey.drainaseBuatan)
        val drainaseBuatanKondisiSaluranPosition =
            drainaseBuatanKondisiSaluranAdp.getPosition(survey.drainaseBuatanKondisiSaluran)
        val tutupanLahanPosition =
            tutupanLahanAdp.getPosition(survey.tutupanLahan)
        val tutupanLahanKondisiTumbuhanPosition =
            tutupanLahanKondisiTumbuhanAdp.getPosition(survey.tutupanLahanKondisiTumbuhan)
        val tutupanLahanKondisiTegakanPosition =
            tutupanLahanKondisiTegakanAdp.getPosition(survey.tutupanLahanKondisiTegakan)
        val tutupanLahanSebelumnyaPosition =
            tutupanLahanSebelumnyaAdp.getPosition(survey.tutupanLahanSebelumnya)
        val floraPosition = floraAdp.getPosition(survey.flora)
        val faunaPosition = faunaAdp.getPosition(survey.fauna)
        val faunaSeringDitemuiStatus1Position =
            faunaSeringDitemuiStatus1Adp.getPosition(survey.faunaSeringDitemuiStatus1)
        val faunaSeringDitemuiStatus2Position =
            faunaSeringDitemuiStatus2Adp.getPosition(survey.faunaSeringDitemuiStatus2)
        val faunaSeringDitemuiStatus3Position =
            faunaSeringDitemuiStatus3Adp.getPosition(survey.faunaSeringDitemuiStatus3)
        val faunaSeringDitemuiStatus4Position =
            faunaSeringDitemuiStatus4Adp.getPosition(survey.faunaSeringDitemuiStatus4)
        val faunaSeringDitemuiStatus5Position =
            faunaSeringDitemuiStatus5Adp.getPosition(survey.faunaSeringDitemuiStatus5)
        val faunaJarangDitemuiStatus1Position =
            faunaJarangDitemuiStatus1Adp.getPosition(survey.faunaJarangDitemuiStatus1)
        val faunaJarangDitemuiStatus2Position =
            faunaJarangDitemuiStatus2Adp.getPosition(survey.faunaJarangDitemuiStatus2)
        val faunaJarangDitemuiStatus3Position =
            faunaJarangDitemuiStatus3Adp.getPosition(survey.faunaJarangDitemuiStatus3)
        val faunaJarangDitemuiStatus4Position =
            faunaJarangDitemuiStatus4Adp.getPosition(survey.faunaJarangDitemuiStatus4)
        val faunaJarangDitemuiStatus5Position =
            faunaJarangDitemuiStatus5Adp.getPosition(survey.faunaJarangDitemuiStatus5)
        val tipeLuapanPosition = tipeLuapanAdp.getPosition(survey.tipeLuapan)
        val kematanganGambut1Position =
            kematanganGambut1Adp.getPosition(survey.kematanganGambut1)
        val kematanganGambut2Position =
            kematanganGambut2Adp.getPosition(survey.kematanganGambut2)
        val kematanganGambut3Position =
            kematanganGambut3Adp.getPosition(survey.kematanganGambut3)
        val kematanganGambut4Position =
            kematanganGambut4Adp.getPosition(survey.kematanganGambut4)
        val karakteristikSubstratumPosition =
            karakteristikSubstratumAdp.getPosition(survey.karakteristikSubstratum)
        val tingkatKerusakanPosition =
            tingkatKerusakanAdp.getPosition(survey.tingkatKerusakan)
        val tingkatKerusakanKerapatanTajukPosition =
            tingkatKerusakanKerapatanTajukAdp.getPosition(survey.tingkatKerusakanKerapatanTajuk)
        val karakteristikTanahTingkatDekomposisi1Position =
            karakteristikTanahTingkatDekomposisi1Adp.getPosition(survey.karakteristikTanahTingkatDekomposisi1)
        val karakteristikTanahPirit1Position =
            karakteristikTanahPirit1Adp.getPosition(survey.karakteristikTanahPirit1)
        val karakteristikTanahTingkatDekomposisi2Position =
            karakteristikTanahTingkatDekomposisi2Adp.getPosition(survey.karakteristikTanahTingkatDekomposisi2)
        val karakteristikTanahPirit2Position =
            karakteristikTanahPirit2Adp.getPosition(survey.karakteristikTanahPirit2)
        val karakteristikTanahTingkatDekomposisi3Position =
            karakteristikTanahTingkatDekomposisi3Adp.getPosition(survey.karakteristikTanahTingkatDekomposisi3)
        val karakteristikTanahPirit3Position =
            karakteristikTanahPirit3Adp.getPosition(survey.karakteristikTanahPirit3)
        val karakteristikTanahTingkatDekomposisi4Position =
            karakteristikTanahTingkatDekomposisi4Adp.getPosition(survey.karakteristikTanahTingkatDekomposisi4)
        val karakteristikTanahPirit4Position =
            karakteristikTanahPirit4Adp.getPosition(survey.karakteristikTanahPirit4)
        val substratumJenisPosition =
            substratumJenisAdp.getPosition(survey.substratumJenis)
        val substratumTeksturPosition =
            substratumTeksturAdp.getPosition(survey.substratumTekstur)
        val substratumPiritPosition =
            substratumPiritAdp.getPosition(survey.substratumPirit)
        val substratumJenis2Position =
            substratumJenis2Adp.getPosition(survey.substratumJenis2)
        val kebakaranFaktorPosition =
            kebakaranFaktorAdp.getPosition(survey.kebakaranFaktor)
        val kebakaranPenangananPosition =
            kebakaranPenangananAdp.getPosition(survey.kebakaranPenanganan)
        val hujanTerakhirPosition =
            hujanTerakhirAdp.getPosition(survey.hujanTerakhir)
        val hujanIntensitasPosition =
            hujanIntensitasAdp.getPosition(survey.hujanIntensitas)

        root.spn_kondisiCuaca.setSelection(kondisiCuacaPosition)
        root.spn_drainaseAlami.setSelection(drainaseAlamiPosition)
        root.spn_drainaseAlamiKondisiSaluran.setSelection(drainaseAlamiKondisiSaluranPosition)
        root.spn_drainaseBuatan.setSelection(drainaseBuatanPosition)
        root.spn_drainaseBuatanKondisiSaluran.setSelection(
            drainaseBuatanKondisiSaluranPosition
        )
        root.spn_tutupanLahan.setSelection(tutupanLahanPosition)
        root.spn_tutupanLahanKondisiTumbuhan.setSelection(tutupanLahanKondisiTumbuhanPosition)
        root.spn_tutupanLahanKondisiTegakan.setSelection(tutupanLahanKondisiTegakanPosition)
        root.spn_tutupanLahanSebelumnya.setSelection(tutupanLahanSebelumnyaPosition)
        root.spn_flora.setSelection(floraPosition)
        root.spn_fauna.setSelection(faunaPosition)
        root.spn_faunaSeringDitemuiStatus1.setSelection(faunaSeringDitemuiStatus1Position)
        root.spn_faunaSeringDitemuiStatus2.setSelection(faunaSeringDitemuiStatus2Position)
        root.spn_faunaSeringDitemuiStatus3.setSelection(faunaSeringDitemuiStatus3Position)
        root.spn_faunaSeringDitemuiStatus4.setSelection(faunaSeringDitemuiStatus4Position)
        root.spn_faunaSeringDitemuiStatus5.setSelection(faunaSeringDitemuiStatus5Position)
        root.spn_faunaJarangDitemuiStatus1.setSelection(faunaJarangDitemuiStatus1Position)
        root.spn_faunaJarangDitemuiStatus2.setSelection(faunaJarangDitemuiStatus2Position)
        root.spn_faunaJarangDitemuiStatus3.setSelection(faunaJarangDitemuiStatus3Position)
        root.spn_faunaJarangDitemuiStatus4.setSelection(faunaJarangDitemuiStatus4Position)
        root.spn_faunaJarangDitemuiStatus5.setSelection(faunaJarangDitemuiStatus5Position)
        root.spn_tipeLuapan.setSelection(tipeLuapanPosition)
        root.spn_kematanganGambut1.setSelection(kematanganGambut1Position)
        root.spn_kematanganGambut2.setSelection(kematanganGambut2Position)
        root.spn_kematanganGambut3.setSelection(kematanganGambut3Position)
        root.spn_kematanganGambut4.setSelection(kematanganGambut4Position)
        root.spn_karakteristikSubstratum.setSelection(karakteristikSubstratumPosition)
        root.spn_tingkatKerusakan.setSelection(tingkatKerusakanPosition)
        root.spn_tingkatKerusakanKerapatanTajuk.setSelection(
            tingkatKerusakanKerapatanTajukPosition
        )
        root.spn_karakteristikTanahTingkatDekomposisi1.setSelection(
            karakteristikTanahTingkatDekomposisi1Position
        )
        root.spn_karakteristikTanahPirit1.setSelection(karakteristikTanahPirit1Position)
        root.spn_karakteristikTanahTingkatDekomposisi2.setSelection(
            karakteristikTanahTingkatDekomposisi2Position
        )
        root.spn_karakteristikTanahPirit2.setSelection(karakteristikTanahPirit2Position)
        root.spn_karakteristikTanahTingkatDekomposisi3.setSelection(
            karakteristikTanahTingkatDekomposisi3Position
        )
        root.spn_karakteristikTanahPirit3.setSelection(karakteristikTanahPirit3Position)
        root.spn_karakteristikTanahTingkatDekomposisi4.setSelection(
            karakteristikTanahTingkatDekomposisi4Position
        )
        root.spn_karakteristikTanahPirit4.setSelection(karakteristikTanahPirit4Position)
        root.spn_substratumJenis.setSelection(substratumJenisPosition)
        root.spn_substratumTekstur.setSelection(substratumTeksturPosition)
        root.spn_substratumPirit.setSelection(substratumPiritPosition)
        root.spn_substratumJenis2.setSelection(substratumJenis2Position)
        root.spn_kebakaranFaktor.setSelection(kebakaranFaktorPosition)
        root.spn_kebakaranPenanganan.setSelection(kebakaranPenangananPosition)
        root.spn_hujanTerakhir.setSelection(hujanTerakhirPosition)
        root.spn_hujanIntensitas.setSelection(hujanIntensitasPosition)
        **/
    }


}