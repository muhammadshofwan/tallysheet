package synergy.project.tallysheet.feature

import android.content.Intent
import android.content.pm.PackageInfo
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_splash.*
import splitties.activities.start
import splitties.alertdialog.alertDialog
import splitties.alertdialog.message
import splitties.alertdialog.okButton
import synergy.project.tallysheet.R
import synergy.project.tallysheet.feature.login.LoginActivity
import synergy.project.tallysheet.feature.main.MainActivity
import synergy.project.tallysheet.model.DatabaseHelper
import synergy.project.tallysheet.utils.PrefManager


class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val pInfo: PackageInfo =
            getPackageManager().getPackageInfo(getPackageName(), 0)
        val version = pInfo.versionName
        txt_version.text = "Versi $version"



        Handler().postDelayed(Runnable {
            val prefManager = PrefManager(applicationContext)
            if (prefManager.token.equals("")) {
                start<LoginActivity> { flags = Intent.FLAG_ACTIVITY_CLEAR_TOP }
                finish()
            } else {

                start<MainActivity> { flags = Intent.FLAG_ACTIVITY_CLEAR_TOP }
                finish()
            }
        }, 2000)

    }

}