package synergy.project.tallysheet.feature.survey.Entry

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import kotlinx.android.synthetic.main.layout_header.btn_simpan
import kotlinx.android.synthetic.main.layout_koordinat_titik_survey.*
import splitties.activities.start
import splitties.toast.toast
import synergy.project.tallysheet.R
import synergy.project.tallysheet.model.Survey
import synergy.project.tallysheet.utils.DatePickerFragment
import synergy.project.tallysheet.utils.Local
import synergy.project.tallysheet.utils.PrefManager
import java.io.File

class  EntryKordinatActivity : AppCompatActivity() {
    lateinit var survey: Survey
    lateinit var prefManager: PrefManager
    var id : Int= 0

    var pathkordinatImg1 = ""
    var pathkordinatImg2 = ""
    var pathkordinatImg3 = ""
    var pathkordinatImg4 = ""
    var pathkordinatImg5 = ""
    var pathkordinatImg6 = ""


    val activity = EntryKordinatActivity@this


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_koordinat_titik_survey)
        val bundle = intent.extras
        if (bundle != null) {
            id = bundle["id"] as Int
        }
        survey = Survey(applicationContext)
        prefManager = PrefManager(applicationContext)

        // ini untuk menampilkan data
        if (survey.getData(id)) {
            txt_longitude.setText(survey.longitude)
            txt_latitude.setText(survey.latitude)
            Glide.with(activity).load(survey.kordinatImg1).into(img_kordinatImg1)
            Glide.with(activity).load(survey.kordinatImg2).into(img_kordinatImg2)
            Glide.with(activity).load(survey.kordinatImg3).into(img_kordinatImg3)
            Glide.with(activity).load(survey.kordinatImg4).into(img_kordinatImg4)
            Glide.with(activity).load(survey.kordinatImg5).into(img_kordinatImg5)
            Glide.with(activity).load(survey.kordinatImg6).into(img_kordinatImg6)

            pathkordinatImg1 = survey.kordinatImg1.toString()
            pathkordinatImg2 = survey.kordinatImg2.toString()
            pathkordinatImg3 = survey.kordinatImg3.toString()
            pathkordinatImg4 = survey.kordinatImg4.toString()
            pathkordinatImg5 = survey.kordinatImg5.toString()
            pathkordinatImg6 = survey.kordinatImg6.toString()
        }

        supportActionBar?.title = "Isi Survey"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayUseLogoEnabled(false)

        initView()
        klik()
    }

    private fun initView() {
        if(pathkordinatImg1!="") Local.isVisible(btn_kordinatImg1,btn_delete_kordinatImg1,true)
        else Local.isVisible(btn_kordinatImg1,btn_delete_kordinatImg1,false)

        if(pathkordinatImg2!="") Local.isVisible(btn_kordinatImg2,btn_delete_kordinatImg2,true)
        else Local.isVisible(btn_kordinatImg2,btn_delete_kordinatImg2,false)

        if(pathkordinatImg3!="") Local.isVisible(btn_kordinatImg3,btn_delete_kordinatImg3,true)
        else Local.isVisible(btn_kordinatImg3,btn_delete_kordinatImg3,false)

        if(pathkordinatImg4!="") Local.isVisible(btn_kordinatImg4,btn_delete_kordinatImg4,true)
        else Local.isVisible(btn_kordinatImg4,btn_delete_kordinatImg4,false)

        if(pathkordinatImg5!="") Local.isVisible(btn_kordinatImg5,btn_delete_kordinatImg5,true)
        else Local.isVisible(btn_kordinatImg5,btn_delete_kordinatImg5,false)

        if(pathkordinatImg6!="") Local.isVisible(btn_kordinatImg6,btn_delete_kordinatImg6,true)
        else Local.isVisible(btn_kordinatImg6,btn_delete_kordinatImg6,false)
    }

    fun klik(){
         btn_simpan.setOnClickListener {
             survey.latitude = txt_latitude.text.toString()
             survey.longitude = txt_longitude.text.toString()

             survey.kordinatImg1 = pathkordinatImg1
             survey.kordinatImg2 = pathkordinatImg2
             survey.kordinatImg3 = pathkordinatImg3
             survey.kordinatImg4 = pathkordinatImg4
             survey.kordinatImg5 = pathkordinatImg5
             survey.kordinatImg6 = pathkordinatImg6


             survey.update()
            start<EntryElevasiActivity> {
                putExtra("id", survey.id?.toInt())
            }
        }
        btn_kembali.setOnClickListener {
            survey.latitude = txt_latitude.text.toString()
            survey.longitude = txt_longitude.text.toString()

            survey.kordinatImg1 = pathkordinatImg1
            survey.kordinatImg2 = pathkordinatImg2
            survey.kordinatImg3 = pathkordinatImg3
            survey.kordinatImg4 = pathkordinatImg4
            survey.kordinatImg5 = pathkordinatImg5
            survey.kordinatImg6 = pathkordinatImg6

            survey.update()
            onBackPressed()
        }

        //image
        btn_kordinatImg1.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(101) }
        btn_kordinatImg2.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(102) }
        btn_kordinatImg3.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(103) }
        btn_kordinatImg4.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(104) }
        btn_kordinatImg5.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(105) }
        btn_kordinatImg6.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(106) }

        //delete image
       deleteImage(btn_delete_kordinatImg1,img_kordinatImg1,1)
        deleteImage(btn_delete_kordinatImg2,img_kordinatImg2,2)
        deleteImage(btn_delete_kordinatImg3,img_kordinatImg3,3)
        deleteImage(btn_delete_kordinatImg4,img_kordinatImg4,4)
        deleteImage(btn_delete_kordinatImg5,img_kordinatImg5,5)
        deleteImage(btn_delete_kordinatImg6,img_kordinatImg6,6)


        //navigasi
        selectNavigasi(btn_navigasi_1,1)
        selectNavigasi(btn_navigasi_2,2)
        selectNavigasi(btn_navigasi_3,3)
        selectNavigasi(btn_navigasi_4,4)
        selectNavigasi(btn_navigasi_5,5)
        selectNavigasi(btn_navigasi_6,6)
        selectNavigasi(btn_navigasi_7,7)
        selectNavigasi(btn_navigasi_8,8)
        selectNavigasi(btn_navigasi_9,9)
        selectNavigasi(btn_navigasi_10,10)
        selectNavigasi(btn_navigasi_11,11)
        selectNavigasi(btn_navigasi_12,12)
        selectNavigasi(btn_navigasi_13,13)
        selectNavigasi(btn_navigasi_14,14)
        selectNavigasi(btn_navigasi_15,15)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)


        if (requestCode==101){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_kordinatImg1.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_kordinatImg1)
                survey.kordinatImg1 = filePath
                pathkordinatImg1 = filePath
            }
        }

        if (requestCode==102){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_kordinatImg2.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_kordinatImg2)
                survey.kordinatImg2 = filePath
                pathkordinatImg2 = filePath
            }
        }

        if (requestCode==103){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_kordinatImg3.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_kordinatImg3)
                survey.kordinatImg3 = filePath
                pathkordinatImg3 = filePath
            }
        }

        if (requestCode==104){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_kordinatImg4.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_kordinatImg4)
                survey.kordinatImg4 = filePath
                pathkordinatImg4 = filePath
            }
        }

        if (requestCode==105){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_kordinatImg5.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_kordinatImg5)
                survey.kordinatImg5 = filePath
                pathkordinatImg5 = filePath
            }
        }

        if (requestCode==106){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_kordinatImg6.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_kordinatImg6)
                survey.kordinatImg6 = filePath
                pathkordinatImg6 = filePath
            }
        }


    }

    fun returnData(id:Int){
        if (survey.getData(id)) {

        } else {

        }
    }

    fun selectNavigasi(button: Button,code:Int){
        button.setOnClickListener {
            when(code){
                //1 -> navigasiIntent(EntryKordinatActivity::class.java)
                2 -> navigasiIntent(EntryElevasiActivity::class.java)
                3 -> navigasiIntent(EntryGenanganActivity::class.java)
                4 -> navigasiIntent(EntryTutupanLahanActivity::class.java)
                5 -> navigasiIntent(EntryFloraFaunaActivity::class.java)
                6 ->  navigasiIntent(EntryDrainaseActivity::class.java)
                7 -> navigasiIntent(EntryKualitasAirTanahActivity::class.java)
                8 -> navigasiIntent(EntryKarakteristikSubstratumTanahActivity::class.java)
                9 -> navigasiIntent(EntryTipeLuapanActivity::class.java)
                10 -> navigasiIntent(EntryKetebalanGambutActivity::class.java)
                11 -> navigasiIntent(EntryKarakteristikSubstratumDibawahLapisanGambutActivity::class.java)
                12 -> navigasiIntent(EntryTingkatKerusakanLahanActivity::class.java)
                13 -> navigasiIntent(EntryKebakaranLahanActivity::class.java)
                14 -> navigasiIntent(EntryPorositasKelengasanActivity::class.java)
                15 -> navigasiIntent(EntryDataGambarActivity::class.java)
            }
        }
    }


    private fun navigasiIntent(classtujuan: Class<*>) {
        survey.latitude = txt_latitude.text.toString()
        survey.longitude = txt_longitude.text.toString()

        survey.kordinatImg1 = pathkordinatImg1
        survey.kordinatImg2 = pathkordinatImg2
        survey.kordinatImg3 = pathkordinatImg3
        survey.kordinatImg4 = pathkordinatImg4
        survey.kordinatImg5 = pathkordinatImg5
        survey.kordinatImg6 = pathkordinatImg6
        survey.update()
        val intent = Intent(activity,classtujuan)
        intent.putExtra("id", survey.id?.toInt())
        startActivity(intent)

    }

    fun deleteImage(button:ImageButton,imageView: ImageView,code: Int){
       button.setOnClickListener {
           when(code){
               1-> pathkordinatImg1 = ""
               2-> pathkordinatImg2 = ""
               3-> pathkordinatImg3 = ""
               4-> pathkordinatImg4 = ""
               5-> pathkordinatImg5 = ""
               6-> pathkordinatImg6 = ""
           }

           Glide.with(activity).load("").into(imageView)
       }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        if(id != null){
            survey.getData(id)
        }
        initView()
        super.onResume()
    }
}