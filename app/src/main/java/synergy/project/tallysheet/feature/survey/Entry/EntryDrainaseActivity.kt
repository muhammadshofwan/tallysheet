package synergy.project.tallysheet.feature.survey.Entry

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import kotlinx.android.synthetic.main.layout_drainase.*
import kotlinx.android.synthetic.main.layout_drainase.btn_kembali
import kotlinx.android.synthetic.main.layout_drainase.btn_navigasi_1
import kotlinx.android.synthetic.main.layout_drainase.btn_navigasi_10
import kotlinx.android.synthetic.main.layout_drainase.btn_navigasi_11
import kotlinx.android.synthetic.main.layout_drainase.btn_navigasi_12
import kotlinx.android.synthetic.main.layout_drainase.btn_navigasi_13
import kotlinx.android.synthetic.main.layout_drainase.btn_navigasi_14
import kotlinx.android.synthetic.main.layout_drainase.btn_navigasi_15
import kotlinx.android.synthetic.main.layout_drainase.btn_navigasi_2
import kotlinx.android.synthetic.main.layout_drainase.btn_navigasi_3
import kotlinx.android.synthetic.main.layout_drainase.btn_navigasi_4
import kotlinx.android.synthetic.main.layout_drainase.btn_navigasi_5
import kotlinx.android.synthetic.main.layout_drainase.btn_navigasi_6
import kotlinx.android.synthetic.main.layout_drainase.btn_navigasi_7
import kotlinx.android.synthetic.main.layout_drainase.btn_navigasi_8
import kotlinx.android.synthetic.main.layout_drainase.btn_navigasi_9
import kotlinx.android.synthetic.main.layout_header.btn_simpan
import kotlinx.android.synthetic.main.layout_koordinat_titik_survey.*
import splitties.activities.start
import splitties.toast.toast
import synergy.project.tallysheet.R
import synergy.project.tallysheet.feature.survey.detail.DetailSurveyActivity
import synergy.project.tallysheet.model.Survey
import synergy.project.tallysheet.utils.DatePickerFragment
import synergy.project.tallysheet.utils.Helper
import synergy.project.tallysheet.utils.Local
import synergy.project.tallysheet.utils.PrefManager

class   EntryDrainaseActivity : AppCompatActivity() {
    lateinit var survey: Survey
    lateinit var prefManager: PrefManager
    var id : Int= 0

    var pathdrainaseImg1 = ""
    var pathdrainaseImg2 = ""
    var pathdrainaseImg3 = ""
    var pathdrainaseImg4 = ""
    var pathdrainaseImg5 = ""
    var pathdrainaseImg6 = ""

    val activity = EntryDrainaseActivity@this


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_drainase)

        val bundle = intent.extras
        if (bundle != null) {
            id = bundle["id"] as Int
        }
        survey = Survey(applicationContext)
        survey.getData(id)

        prefManager = PrefManager(applicationContext)
        // ini untuk ini value spinner
        val dataAdapter_drainaseBuatan: ArrayAdapter<String> = ArrayAdapter<String>(
            this@EntryDrainaseActivity,// ganti jadi this@Nama Activity
            android.R.layout.simple_dropdown_item_1line,
            Helper.drainaseBuatan
        )

        val dataAdapter_drainaseAlami: ArrayAdapter<String> = ArrayAdapter<String>(
            this@EntryDrainaseActivity,// ganti jadi this@Nama Activity
            android.R.layout.simple_dropdown_item_1line,
            Helper.drainaseAlami
        )

        val dataAdapter_drainaseBuatanJenis: ArrayAdapter<String> = ArrayAdapter<String>(
            this@EntryDrainaseActivity,// ganti jadi this@Nama Activity
            android.R.layout.simple_dropdown_item_1line,
            Helper.drainaseBuatanJenis
        )
        spn_drainaseBuatan.adapter = dataAdapter_drainaseBuatan
        spn_drainaseAlami.adapter = dataAdapter_drainaseAlami
        spn_drainaseBuatanJenis.adapter = dataAdapter_drainaseBuatanJenis


        // ini untuk menampilkan data
        if (survey.getData(id)) {
            val drainaseAlamiAdp = spn_drainaseAlami.adapter as ArrayAdapter<String>
            val drainaseAlamiPosition = drainaseAlamiAdp.getPosition(survey.drainaseAlami)
            spn_drainaseAlami.setSelection(drainaseAlamiPosition)
            val drainaseBuatanAdp = spn_drainaseBuatan.adapter as ArrayAdapter<String>
            val drainaseBuatanPosition = drainaseBuatanAdp.getPosition(survey.drainaseBuatan)
            spn_drainaseBuatan.setSelection(drainaseBuatanPosition)
            val drainaseBuatanJenisAdp = spn_drainaseBuatanJenis.adapter as ArrayAdapter<String>
            val drainaseBuatanJenisPosition =
                drainaseBuatanJenisAdp.getPosition(survey.drainaseBuatanJenis)
            spn_drainaseBuatanJenis.setSelection(drainaseBuatanJenisPosition)
            txt_drainaseBuatanTinggiMukaAir.setText(survey.drainaseBuatanTinggiMukaAir)

            Glide.with(activity).load(survey.drainaseImg1).into(img_drainaseImg1)
            Glide.with(activity).load(survey.drainaseImg2).into(img_drainaseImg2)
            Glide.with(activity).load(survey.drainaseImg3).into(img_drainaseImg3)
            Glide.with(activity).load(survey.drainaseImg4).into(img_drainaseImg4)
            Glide.with(activity).load(survey.drainaseImg5).into(img_drainaseImg5)
            Glide.with(activity).load(survey.drainaseImg6).into(img_drainaseImg6)

            pathdrainaseImg1 = survey.drainaseImg1.toString()
            pathdrainaseImg2 = survey.drainaseImg2.toString()
            pathdrainaseImg3 = survey.drainaseImg3.toString()
            pathdrainaseImg4 = survey.drainaseImg4.toString()
            pathdrainaseImg5 = survey.drainaseImg5.toString()
            pathdrainaseImg6 = survey.drainaseImg6.toString()
        }

        supportActionBar?.title = "Isi Survey"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayUseLogoEnabled(false)

        initView()
        klik()
    }

    private fun initView() {
        if(pathdrainaseImg1!="") Local.isVisible(btn_drainaseImg1,btn_delete_drainaseImg1,true)
        else Local.isVisible(btn_drainaseImg1,btn_delete_drainaseImg1,false)

        if(pathdrainaseImg2!="") Local.isVisible(btn_drainaseImg2,btn_delete_drainaseImg2,true)
        else Local.isVisible(btn_drainaseImg2,btn_delete_drainaseImg2,false)

        if(pathdrainaseImg3!="") Local.isVisible(btn_drainaseImg3,btn_delete_drainaseImg3,true)
        else Local.isVisible(btn_drainaseImg3,btn_delete_drainaseImg3,false)

        if(pathdrainaseImg4!="") Local.isVisible(btn_drainaseImg4,btn_delete_drainaseImg4,true)
        else Local.isVisible(btn_drainaseImg4,btn_delete_drainaseImg4,false)

        if(pathdrainaseImg5!="") Local.isVisible(btn_drainaseImg5,btn_delete_drainaseImg5,true)
        else Local.isVisible(btn_drainaseImg5,btn_delete_drainaseImg5,false)

        if(pathdrainaseImg6!="") Local.isVisible(btn_drainaseImg6,btn_delete_drainaseImg6,true)
        else Local.isVisible(btn_drainaseImg6,btn_delete_drainaseImg6,false)
    }

    fun klik(){
        btn_simpan.setOnClickListener {
            survey.drainaseAlami = spn_drainaseAlami.selectedItem.toString()
            survey.drainaseBuatan = spn_drainaseBuatan.selectedItem.toString()
            survey.drainaseBuatanJenis = spn_drainaseBuatanJenis.selectedItem.toString()
            survey.drainaseBuatanTinggiMukaAir = txt_drainaseBuatanTinggiMukaAir.text.toString()

            survey.drainaseImg1 = pathdrainaseImg1
            survey.drainaseImg2 = pathdrainaseImg2
            survey.drainaseImg3 = pathdrainaseImg3
            survey.drainaseImg4 = pathdrainaseImg4
            survey.drainaseImg5 = pathdrainaseImg5
            survey.drainaseImg6 = pathdrainaseImg6

            survey.update()
            start<EntryKualitasAirTanahActivity> {
                putExtra("id", survey.id?.toInt())
            }

        }

        btn_kembali.setOnClickListener {
            survey.drainaseAlami = spn_drainaseAlami.selectedItem.toString()
            survey.drainaseBuatan = spn_drainaseBuatan.selectedItem.toString()
            survey.drainaseBuatanJenis = spn_drainaseBuatanJenis.selectedItem.toString()
            survey.drainaseBuatanTinggiMukaAir = txt_drainaseBuatanTinggiMukaAir.text.toString()

            survey.drainaseImg1 = pathdrainaseImg1
            survey.drainaseImg2 = pathdrainaseImg2
            survey.drainaseImg3 = pathdrainaseImg3
            survey.drainaseImg4 = pathdrainaseImg4
            survey.drainaseImg5 = pathdrainaseImg5
            survey.drainaseImg6 = pathdrainaseImg6

            survey.update()
           onBackPressed()
        }



        btn_drainaseImg1.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(131) }
        btn_drainaseImg2.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(132) }
        btn_drainaseImg3.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(133) }
        btn_drainaseImg4.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(134) }
        btn_drainaseImg5.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(135) }
        btn_drainaseImg6.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(136) }

        //delete image
        deleteImage(btn_delete_drainaseImg1,img_drainaseImg1,1)
        deleteImage(btn_delete_drainaseImg2,img_drainaseImg2,2)
        deleteImage(btn_delete_drainaseImg3,img_drainaseImg3,3)
        deleteImage(btn_delete_drainaseImg4,img_drainaseImg4,4)
        deleteImage(btn_delete_drainaseImg5,img_drainaseImg5,5)
        deleteImage(btn_delete_drainaseImg6,img_drainaseImg6,6)


        //navigasi
        selectNavigasi(btn_navigasi_1,1)
        selectNavigasi(btn_navigasi_2,2)
        selectNavigasi(btn_navigasi_3,3)
        selectNavigasi(btn_navigasi_4,4)
        selectNavigasi(btn_navigasi_5,5)
        selectNavigasi(btn_navigasi_6,6)
        selectNavigasi(btn_navigasi_7,7)
        selectNavigasi(btn_navigasi_8,8)
        selectNavigasi(btn_navigasi_9,9)
        selectNavigasi(btn_navigasi_10,10)
        selectNavigasi(btn_navigasi_11,11)
        selectNavigasi(btn_navigasi_12,12)
        selectNavigasi(btn_navigasi_13,13)
        selectNavigasi(btn_navigasi_14,14)
        selectNavigasi(btn_navigasi_15,15)


    }

    fun returnData(id:Int){
        if (survey.getData(id)) {

        } else {

        }
    }

    fun selectNavigasi(button: Button, code:Int){
        button.setOnClickListener {
            when(code){
                1 -> navigasiIntent(EntryKordinatActivity::class.java)
                2 -> navigasiIntent(EntryElevasiActivity::class.java)
                3 -> navigasiIntent(EntryGenanganActivity::class.java)
                4 -> navigasiIntent(EntryTutupanLahanActivity::class.java)
                5 -> navigasiIntent(EntryFloraFaunaActivity::class.java)
                //6 ->  navigasiIntent(EntryDrainaseActivity::class.java)
                7 -> navigasiIntent(EntryKualitasAirTanahActivity::class.java)
                8 -> navigasiIntent(EntryKarakteristikSubstratumTanahActivity::class.java)
                9 -> navigasiIntent(EntryTipeLuapanActivity::class.java)
                10 -> navigasiIntent(EntryKetebalanGambutActivity::class.java)
                11 -> navigasiIntent(EntryKarakteristikSubstratumDibawahLapisanGambutActivity::class.java)
                12 -> navigasiIntent(EntryTingkatKerusakanLahanActivity::class.java)
                13 -> navigasiIntent(EntryKebakaranLahanActivity::class.java)
                14 -> navigasiIntent(EntryPorositasKelengasanActivity::class.java)
                15 -> navigasiIntent(EntryDataGambarActivity::class.java)
            }
        }
    }

    private fun navigasiIntent(classtujuan: Class<*>) {
        survey.drainaseAlami = spn_drainaseAlami.selectedItem.toString()
        survey.drainaseBuatan = spn_drainaseBuatan.selectedItem.toString()
        survey.drainaseBuatanJenis = spn_drainaseBuatanJenis.selectedItem.toString()
        survey.drainaseBuatanTinggiMukaAir = txt_drainaseBuatanTinggiMukaAir.text.toString()
        survey.drainaseImg1 = pathdrainaseImg1
        survey.drainaseImg2 = pathdrainaseImg2
        survey.drainaseImg3 = pathdrainaseImg3
        survey.drainaseImg4 = pathdrainaseImg4
        survey.drainaseImg5 = pathdrainaseImg5
        survey.drainaseImg6 = pathdrainaseImg6
        survey.update()
        val intent = Intent(activity,classtujuan)
        intent.putExtra("id", survey.id?.toInt())
        startActivity(intent)

    }

    fun deleteImage(button: ImageButton, imageView: ImageView, code: Int){
        button.setOnClickListener {
            when(code){
                1-> pathdrainaseImg1 = ""
                2-> pathdrainaseImg2 = ""
                3-> pathdrainaseImg3 = ""
                4-> pathdrainaseImg4 = ""
                5-> pathdrainaseImg5 = ""
                6-> pathdrainaseImg6 = ""
            }

            Glide.with(activity).load("").into(imageView)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode==131){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_drainaseImg1.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_drainaseImg1)
                survey.drainaseImg1 = filePath
                pathdrainaseImg1 = filePath
            }
        }

        if (requestCode==132){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_drainaseImg2.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_drainaseImg2)
                survey.drainaseImg2 = filePath
                pathdrainaseImg2 = filePath
            }
        }

        if (requestCode==133){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_drainaseImg3.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_drainaseImg3)
                survey.drainaseImg3 = filePath
                pathdrainaseImg3 = filePath
            }
        }

        if (requestCode==134){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_drainaseImg4.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_drainaseImg4)
                survey.drainaseImg4 = filePath
                pathdrainaseImg4 = filePath
            }
        }

        if (requestCode==135){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_drainaseImg5.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_drainaseImg5)
                survey.drainaseImg5 = filePath
                pathdrainaseImg5 = filePath
            }
        }

        if (requestCode==136){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_drainaseImg6.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_drainaseImg6)
                survey.drainaseImg6 = filePath
                pathdrainaseImg6 = filePath
            }
        }

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
    override fun onResume() {
        if(id != null){
            survey.getData(id)
        }
        super.onResume()
    }

}