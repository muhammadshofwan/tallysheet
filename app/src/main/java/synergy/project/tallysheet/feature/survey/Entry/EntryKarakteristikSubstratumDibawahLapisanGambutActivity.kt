package synergy.project.tallysheet.feature.survey.Entry

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import kotlinx.android.synthetic.main.layout_genangan.*
import kotlinx.android.synthetic.main.layout_header.btn_simpan
import kotlinx.android.synthetic.main.layout_karakteristik_substratum_dibawah_lapisan_gambut.*
import kotlinx.android.synthetic.main.layout_karakteristik_substratum_dibawah_lapisan_gambut.btn_kembali
import kotlinx.android.synthetic.main.layout_karakteristik_substratum_dibawah_lapisan_gambut.btn_navigasi_1
import kotlinx.android.synthetic.main.layout_karakteristik_substratum_dibawah_lapisan_gambut.btn_navigasi_10
import kotlinx.android.synthetic.main.layout_karakteristik_substratum_dibawah_lapisan_gambut.btn_navigasi_11
import kotlinx.android.synthetic.main.layout_karakteristik_substratum_dibawah_lapisan_gambut.btn_navigasi_12
import kotlinx.android.synthetic.main.layout_karakteristik_substratum_dibawah_lapisan_gambut.btn_navigasi_13
import kotlinx.android.synthetic.main.layout_karakteristik_substratum_dibawah_lapisan_gambut.btn_navigasi_14
import kotlinx.android.synthetic.main.layout_karakteristik_substratum_dibawah_lapisan_gambut.btn_navigasi_15
import kotlinx.android.synthetic.main.layout_karakteristik_substratum_dibawah_lapisan_gambut.btn_navigasi_2
import kotlinx.android.synthetic.main.layout_karakteristik_substratum_dibawah_lapisan_gambut.btn_navigasi_3
import kotlinx.android.synthetic.main.layout_karakteristik_substratum_dibawah_lapisan_gambut.btn_navigasi_4
import kotlinx.android.synthetic.main.layout_karakteristik_substratum_dibawah_lapisan_gambut.btn_navigasi_5
import kotlinx.android.synthetic.main.layout_karakteristik_substratum_dibawah_lapisan_gambut.btn_navigasi_6
import kotlinx.android.synthetic.main.layout_karakteristik_substratum_dibawah_lapisan_gambut.btn_navigasi_7
import kotlinx.android.synthetic.main.layout_karakteristik_substratum_dibawah_lapisan_gambut.btn_navigasi_8
import kotlinx.android.synthetic.main.layout_karakteristik_substratum_dibawah_lapisan_gambut.btn_navigasi_9
import kotlinx.android.synthetic.main.layout_koordinat_titik_survey.btn_kordinatImg1
import kotlinx.android.synthetic.main.layout_koordinat_titik_survey.btn_kordinatImg2
import kotlinx.android.synthetic.main.layout_koordinat_titik_survey.btn_kordinatImg3
import kotlinx.android.synthetic.main.layout_koordinat_titik_survey.btn_kordinatImg4
import splitties.activities.start
import splitties.toast.toast
import synergy.project.tallysheet.R
import synergy.project.tallysheet.model.Survey
import synergy.project.tallysheet.utils.DatePickerFragment
import synergy.project.tallysheet.utils.Helper
import synergy.project.tallysheet.utils.Local
import synergy.project.tallysheet.utils.PrefManager

class  EntryKarakteristikSubstratumDibawahLapisanGambutActivity : AppCompatActivity() {
    lateinit var survey: Survey
    lateinit var prefManager: PrefManager
    var id : Int= 0

    var pathkarakteristikSubstratumDibawahLapisanGambutImg1 = ""
    var pathkarakteristikSubstratumDibawahLapisanGambutImg2 = ""
    var pathkarakteristikSubstratumDibawahLapisanGambutImg3 = ""
    var pathkarakteristikSubstratumDibawahLapisanGambutImg4 = ""
    var pathkarakteristikSubstratumDibawahLapisanGambutImg5 = ""
    var pathkarakteristikSubstratumDibawahLapisanGambutImg6 = ""

    var karakteristikSubstratumDibawahLapisanGambutJenis1 = 0
    var karakteristikSubstratumDibawahLapisanGambutJenis2 = 0
    var karakteristikSubstratumDibawahLapisanGambutJenis3 = 0
    var karakteristikSubstratumDibawahLapisanGambutJenis4 = 0
    var karakteristikSubstratumDibawahLapisanGambutJenis5 = 0

    val activity = EntryKarakteristikSubstratumDibawahLapisanGambutActivity@this


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_karakteristik_substratum_dibawah_lapisan_gambut)
        val bundle = intent.extras
        if (bundle != null) {
            id = bundle["id"] as Int
        }
        survey = Survey(applicationContext)
        survey.getData(id)
        prefManager = PrefManager(applicationContext)

        // ini untuk ini value spinner
        val dataAdapter_karakteristikSubstratumDibawahLapisanGambutNa: ArrayAdapter<String> = ArrayAdapter<String>(
            this@EntryKarakteristikSubstratumDibawahLapisanGambutActivity,// ganti jadi this@Nama Activity
            android.R.layout.simple_dropdown_item_1line,
            Helper.karakteristikSubstratumDibawahLapisanGambutNa
        )

        val dataAdapter_karakteristikSubstratumDibawahLapisanGambutNaJenis: ArrayAdapter<String> = ArrayAdapter<String>(
            this@EntryKarakteristikSubstratumDibawahLapisanGambutActivity,// ganti jadi this@Nama Activity
            android.R.layout.simple_dropdown_item_1line,
            Helper.karakteristikSubstratumDibawahLapisanGambutNaJenis
        )

        val dataAdapter_karakteristikSubstratumDibawahLapisanGambutJenis: ArrayAdapter<String> = ArrayAdapter<String>(
            this@EntryKarakteristikSubstratumDibawahLapisanGambutActivity,// ganti jadi this@Nama Activity
            android.R.layout.simple_dropdown_item_1line,
            Helper.karakteristikSubstratumDibawahLapisanGambutJenis
        )
        spn_karakteristikSubstratumDibawahLapisanGambutNa.adapter = dataAdapter_karakteristikSubstratumDibawahLapisanGambutNa
        spn_karakteristikSubstratumDibawahLapisanGambutNaJenis.adapter = dataAdapter_karakteristikSubstratumDibawahLapisanGambutNaJenis
        spn_karakteristikSubstratumDibawahLapisanGambutJenis.adapter = dataAdapter_karakteristikSubstratumDibawahLapisanGambutJenis

        if (survey.getData(id)) {
            val karakteristikSubstratumDibawahLapisanGambutNaAdp = spn_karakteristikSubstratumDibawahLapisanGambutNa.adapter as ArrayAdapter<String>
            val karakteristikSubstratumDibawahLapisanGambutNaPosition = karakteristikSubstratumDibawahLapisanGambutNaAdp.getPosition(survey.karakteristikSubstratumDibawahLapisanGambutNa)
            spn_karakteristikSubstratumDibawahLapisanGambutNa.setSelection(karakteristikSubstratumDibawahLapisanGambutNaPosition)
            val karakteristikSubstratumDibawahLapisanGambutNaJenisAdp = spn_karakteristikSubstratumDibawahLapisanGambutNaJenis.adapter as ArrayAdapter<String>
            val karakteristikSubstratumDibawahLapisanGambutNaJenisPosition = karakteristikSubstratumDibawahLapisanGambutNaJenisAdp.getPosition(survey.karakteristikSubstratumDibawahLapisanGambutNaJenis)
            spn_karakteristikSubstratumDibawahLapisanGambutNaJenis.setSelection(karakteristikSubstratumDibawahLapisanGambutNaJenisPosition)
            val karakteristikSubstratumDibawahLapisanGambutJenisAdp = spn_karakteristikSubstratumDibawahLapisanGambutJenis.adapter as ArrayAdapter<String>
            val karakteristikSubstratumDibawahLapisanGambutJenisPosition = karakteristikSubstratumDibawahLapisanGambutJenisAdp.getPosition(survey.karakteristikSubstratumDibawahLapisanGambutJenis)
            spn_karakteristikSubstratumDibawahLapisanGambutJenis.setSelection(karakteristikSubstratumDibawahLapisanGambutJenisPosition)
            txt_karakteristikSubstratumDibawahLapisanGambutLainnya.setText(survey.karakteristikSubstratumDibawahLapisanGambutLainnya)

            Glide.with(activity).load(survey.karakteristikSubstratumDibawahLapisanGambutImg1).into(img_karakteristikSubstratumDibawahLapisanGambutImg1)
            Glide.with(activity).load(survey.karakteristikSubstratumDibawahLapisanGambutImg2).into(img_karakteristikSubstratumDibawahLapisanGambutImg2)
            Glide.with(activity).load(survey.karakteristikSubstratumDibawahLapisanGambutImg3).into(img_karakteristikSubstratumDibawahLapisanGambutImg3)
            Glide.with(activity).load(survey.karakteristikSubstratumDibawahLapisanGambutImg4).into(img_karakteristikSubstratumDibawahLapisanGambutImg4)
            Glide.with(activity).load(survey.karakteristikSubstratumDibawahLapisanGambutImg5).into(img_karakteristikSubstratumDibawahLapisanGambutImg5)
            Glide.with(activity).load(survey.karakteristikSubstratumDibawahLapisanGambutImg6).into(img_karakteristikSubstratumDibawahLapisanGambutImg6)

            pathkarakteristikSubstratumDibawahLapisanGambutImg1 = survey.karakteristikSubstratumDibawahLapisanGambutImg1.toString()
            pathkarakteristikSubstratumDibawahLapisanGambutImg2 = survey.karakteristikSubstratumDibawahLapisanGambutImg2.toString()
            pathkarakteristikSubstratumDibawahLapisanGambutImg3 = survey.karakteristikSubstratumDibawahLapisanGambutImg3.toString()
            pathkarakteristikSubstratumDibawahLapisanGambutImg4 = survey.karakteristikSubstratumDibawahLapisanGambutImg4.toString()
            pathkarakteristikSubstratumDibawahLapisanGambutImg5 = survey.karakteristikSubstratumDibawahLapisanGambutImg5.toString()
            pathkarakteristikSubstratumDibawahLapisanGambutImg6 = survey.karakteristikSubstratumDibawahLapisanGambutImg6.toString()

            checkBoxSet(survey.karakteristikSubstratumDibawahLapisanGambutJenis1!!,chk_karakteristikSubstratumDibawahLapisanGambutJenis1)
            checkBoxSet(survey.karakteristikSubstratumDibawahLapisanGambutJenis2!!,chk_karakteristikSubstratumDibawahLapisanGambutJenis2)
            checkBoxSet(survey.karakteristikSubstratumDibawahLapisanGambutJenis3!!,chk_karakteristikSubstratumDibawahLapisanGambutJenis3)
            checkBoxSet(survey.karakteristikSubstratumDibawahLapisanGambutJenis4!!,chk_karakteristikSubstratumDibawahLapisanGambutJenis4)
            checkBoxSet(survey.karakteristikSubstratumDibawahLapisanGambutJenis5!!,chk_karakteristikSubstratumDibawahLapisanGambutJenis5)


        }

        supportActionBar?.title = "Isi Survey"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayUseLogoEnabled(false)

        initView()
        klik()
    }

    private fun initView() {
        if(pathkarakteristikSubstratumDibawahLapisanGambutImg1!="") Local.isVisible(btn_karakteristikSubstratumDibawahLapisanGambut1,btn_delete_karakteristikSubstratumDibawahLapisanGambut1,true)
        else Local.isVisible(btn_karakteristikSubstratumDibawahLapisanGambut1,btn_delete_karakteristikSubstratumDibawahLapisanGambut1,false)

        if(pathkarakteristikSubstratumDibawahLapisanGambutImg2!="") Local.isVisible(btn_karakteristikSubstratumDibawahLapisanGambut2,btn_delete_karakteristikSubstratumDibawahLapisanGambut2,true)
        else Local.isVisible(btn_karakteristikSubstratumDibawahLapisanGambut2,btn_delete_karakteristikSubstratumDibawahLapisanGambut2,false)

        if(pathkarakteristikSubstratumDibawahLapisanGambutImg3!="") Local.isVisible(btn_karakteristikSubstratumDibawahLapisanGambut3,btn_delete_karakteristikSubstratumDibawahLapisanGambut3,true)
        else Local.isVisible(btn_karakteristikSubstratumDibawahLapisanGambut3,btn_delete_karakteristikSubstratumDibawahLapisanGambut3,false)

        if(pathkarakteristikSubstratumDibawahLapisanGambutImg4!="") Local.isVisible(btn_karakteristikSubstratumDibawahLapisanGambut4,btn_delete_karakteristikSubstratumDibawahLapisanGambut4,true)
        else Local.isVisible(btn_karakteristikSubstratumDibawahLapisanGambut4,btn_delete_karakteristikSubstratumDibawahLapisanGambut4,false)

        if(pathkarakteristikSubstratumDibawahLapisanGambutImg5!="") Local.isVisible(btn_karakteristikSubstratumDibawahLapisanGambut5,btn_delete_karakteristikSubstratumDibawahLapisanGambut5,true)
        else Local.isVisible(btn_karakteristikSubstratumDibawahLapisanGambut5,btn_delete_karakteristikSubstratumDibawahLapisanGambut5,false)

        if(pathkarakteristikSubstratumDibawahLapisanGambutImg6!="") Local.isVisible(btn_karakteristikSubstratumDibawahLapisanGambut6,btn_delete_karakteristikSubstratumDibawahLapisanGambut6,true)
        else Local.isVisible(btn_karakteristikSubstratumDibawahLapisanGambut6,btn_delete_karakteristikSubstratumDibawahLapisanGambut6,false)
    }

    fun klik(){
        btn_simpan.setOnClickListener {
            //check box
            selectCheckBox(chk_karakteristikSubstratumDibawahLapisanGambutJenis1,1)
            selectCheckBox(chk_karakteristikSubstratumDibawahLapisanGambutJenis2,2)
            selectCheckBox(chk_karakteristikSubstratumDibawahLapisanGambutJenis3,3)
            selectCheckBox(chk_karakteristikSubstratumDibawahLapisanGambutJenis4,4)
            selectCheckBox(chk_karakteristikSubstratumDibawahLapisanGambutJenis5,5)

            survey.karakteristikSubstratumDibawahLapisanGambutJenis1 = karakteristikSubstratumDibawahLapisanGambutJenis1.toString()
            survey.karakteristikSubstratumDibawahLapisanGambutJenis2 = karakteristikSubstratumDibawahLapisanGambutJenis2.toString()
            survey.karakteristikSubstratumDibawahLapisanGambutJenis3 = karakteristikSubstratumDibawahLapisanGambutJenis3.toString()
            survey.karakteristikSubstratumDibawahLapisanGambutJenis4 = karakteristikSubstratumDibawahLapisanGambutJenis4.toString()
            survey.karakteristikSubstratumDibawahLapisanGambutJenis5 = karakteristikSubstratumDibawahLapisanGambutJenis5.toString()

            survey.karakteristikSubstratumDibawahLapisanGambutNa = spn_karakteristikSubstratumDibawahLapisanGambutNa.selectedItem.toString()
            survey.karakteristikSubstratumDibawahLapisanGambutNaJenis = spn_karakteristikSubstratumDibawahLapisanGambutNaJenis.selectedItem.toString()
            //survey.karakteristikSubstratumDibawahLapisanGambutJenis = spn_karakteristikSubstratumDibawahLapisanGambutJenis.selectedItem.toString()
            survey.karakteristikSubstratumDibawahLapisanGambutLainnya = txt_karakteristikSubstratumDibawahLapisanGambutLainnya.text.toString()

            survey.karakteristikSubstratumDibawahLapisanGambutImg1 = pathkarakteristikSubstratumDibawahLapisanGambutImg1
            survey.karakteristikSubstratumDibawahLapisanGambutImg2 = pathkarakteristikSubstratumDibawahLapisanGambutImg2
            survey.karakteristikSubstratumDibawahLapisanGambutImg3 = pathkarakteristikSubstratumDibawahLapisanGambutImg3
            survey.karakteristikSubstratumDibawahLapisanGambutImg4 = pathkarakteristikSubstratumDibawahLapisanGambutImg4
            survey.karakteristikSubstratumDibawahLapisanGambutImg5 = pathkarakteristikSubstratumDibawahLapisanGambutImg5
            survey.karakteristikSubstratumDibawahLapisanGambutImg6 = pathkarakteristikSubstratumDibawahLapisanGambutImg6

            survey.update()
            start<EntryTingkatKerusakanLahanActivity> {
                putExtra("id", survey.id?.toInt())
            }

        }

        btn_kembali.setOnClickListener {
            //check box
            selectCheckBox(chk_karakteristikSubstratumDibawahLapisanGambutJenis1,1)
            selectCheckBox(chk_karakteristikSubstratumDibawahLapisanGambutJenis2,2)
            selectCheckBox(chk_karakteristikSubstratumDibawahLapisanGambutJenis3,3)
            selectCheckBox(chk_karakteristikSubstratumDibawahLapisanGambutJenis4,4)
            selectCheckBox(chk_karakteristikSubstratumDibawahLapisanGambutJenis5,5)

            survey.karakteristikSubstratumDibawahLapisanGambutJenis1 = karakteristikSubstratumDibawahLapisanGambutJenis1.toString()
            survey.karakteristikSubstratumDibawahLapisanGambutJenis2 = karakteristikSubstratumDibawahLapisanGambutJenis2.toString()
            survey.karakteristikSubstratumDibawahLapisanGambutJenis3 = karakteristikSubstratumDibawahLapisanGambutJenis3.toString()
            survey.karakteristikSubstratumDibawahLapisanGambutJenis4 = karakteristikSubstratumDibawahLapisanGambutJenis4.toString()
            survey.karakteristikSubstratumDibawahLapisanGambutJenis5 = karakteristikSubstratumDibawahLapisanGambutJenis5.toString()

            survey.karakteristikSubstratumDibawahLapisanGambutNa = spn_karakteristikSubstratumDibawahLapisanGambutNa.selectedItem.toString()
            survey.karakteristikSubstratumDibawahLapisanGambutNaJenis = spn_karakteristikSubstratumDibawahLapisanGambutNaJenis.selectedItem.toString()
            //survey.karakteristikSubstratumDibawahLapisanGambutJenis = spn_karakteristikSubstratumDibawahLapisanGambutJenis.selectedItem.toString()
            survey.karakteristikSubstratumDibawahLapisanGambutLainnya = txt_karakteristikSubstratumDibawahLapisanGambutLainnya.text.toString()

            survey.karakteristikSubstratumDibawahLapisanGambutImg1 = pathkarakteristikSubstratumDibawahLapisanGambutImg1
            survey.karakteristikSubstratumDibawahLapisanGambutImg2 = pathkarakteristikSubstratumDibawahLapisanGambutImg2
            survey.karakteristikSubstratumDibawahLapisanGambutImg3 = pathkarakteristikSubstratumDibawahLapisanGambutImg3
            survey.karakteristikSubstratumDibawahLapisanGambutImg4 = pathkarakteristikSubstratumDibawahLapisanGambutImg4
            survey.karakteristikSubstratumDibawahLapisanGambutImg5 = pathkarakteristikSubstratumDibawahLapisanGambutImg5
            survey.karakteristikSubstratumDibawahLapisanGambutImg6 = pathkarakteristikSubstratumDibawahLapisanGambutImg6

            survey.update()
            onBackPressed()
        }

        btn_karakteristikSubstratumDibawahLapisanGambut1.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(161) }
        btn_karakteristikSubstratumDibawahLapisanGambut2.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(162) }
        btn_karakteristikSubstratumDibawahLapisanGambut3.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(163) }
        btn_karakteristikSubstratumDibawahLapisanGambut4.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(164) }
        btn_karakteristikSubstratumDibawahLapisanGambut5.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(165) }
        btn_karakteristikSubstratumDibawahLapisanGambut6.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(166) }


        //delete image
        deleteImage(btn_delete_karakteristikSubstratumDibawahLapisanGambut1,img_karakteristikSubstratumDibawahLapisanGambutImg1,1)
        deleteImage(btn_delete_karakteristikSubstratumDibawahLapisanGambut2,img_karakteristikSubstratumDibawahLapisanGambutImg2,2)
        deleteImage(btn_delete_karakteristikSubstratumDibawahLapisanGambut3,img_karakteristikSubstratumDibawahLapisanGambutImg3,3)
        deleteImage(btn_delete_karakteristikSubstratumDibawahLapisanGambut4,img_karakteristikSubstratumDibawahLapisanGambutImg4,4)
        deleteImage(btn_delete_karakteristikSubstratumDibawahLapisanGambut5,img_karakteristikSubstratumDibawahLapisanGambutImg5,5)
        deleteImage(btn_delete_karakteristikSubstratumDibawahLapisanGambut6,img_karakteristikSubstratumDibawahLapisanGambutImg6,6)

        //navigasi
        selectNavigasi(btn_navigasi_1,1)
        selectNavigasi(btn_navigasi_2,2)
        selectNavigasi(btn_navigasi_3,3)
        selectNavigasi(btn_navigasi_4,4)
        selectNavigasi(btn_navigasi_5,5)
        selectNavigasi(btn_navigasi_6,6)
        selectNavigasi(btn_navigasi_7,7)
        selectNavigasi(btn_navigasi_8,8)
        selectNavigasi(btn_navigasi_9,9)
        selectNavigasi(btn_navigasi_10,10)
        selectNavigasi(btn_navigasi_11,11)
        selectNavigasi(btn_navigasi_12,12)
        selectNavigasi(btn_navigasi_13,13)
        selectNavigasi(btn_navigasi_14,14)
        selectNavigasi(btn_navigasi_15,15)


    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)


        if (requestCode==161){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_karakteristikSubstratumDibawahLapisanGambutImg1.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_karakteristikSubstratumDibawahLapisanGambutImg1)
                survey.karakteristikSubstratumDibawahLapisanGambutImg1 = filePath
                pathkarakteristikSubstratumDibawahLapisanGambutImg1 = filePath
            }
        }

        if (requestCode==162){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_karakteristikSubstratumDibawahLapisanGambutImg2.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_karakteristikSubstratumDibawahLapisanGambutImg2)
                survey.karakteristikSubstratumDibawahLapisanGambutImg2 = filePath
                pathkarakteristikSubstratumDibawahLapisanGambutImg2 = filePath
            }
        }

        if (requestCode==163){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_karakteristikSubstratumDibawahLapisanGambutImg3.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_karakteristikSubstratumDibawahLapisanGambutImg3)
                survey.karakteristikSubstratumDibawahLapisanGambutImg3 = filePath
                pathkarakteristikSubstratumDibawahLapisanGambutImg3 = filePath
            }
        }

        if (requestCode==164){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_karakteristikSubstratumDibawahLapisanGambutImg4.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_karakteristikSubstratumDibawahLapisanGambutImg4)
                survey.karakteristikSubstratumDibawahLapisanGambutImg4 = filePath
                pathkarakteristikSubstratumDibawahLapisanGambutImg4 = filePath
            }
        }

        if (requestCode==165){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_karakteristikSubstratumDibawahLapisanGambutImg5.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_karakteristikSubstratumDibawahLapisanGambutImg5)
                survey.karakteristikSubstratumDibawahLapisanGambutImg5 = filePath
                pathkarakteristikSubstratumDibawahLapisanGambutImg5 = filePath
            }
        }

        if (requestCode==166){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_karakteristikSubstratumDibawahLapisanGambutImg6.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_karakteristikSubstratumDibawahLapisanGambutImg6)
                survey.karakteristikSubstratumDibawahLapisanGambutImg6 = filePath
                pathkarakteristikSubstratumDibawahLapisanGambutImg6 = filePath
            }
        }



    }

    fun returnData(id:Int){
        if (survey.getData(id)) {

        } else {

        }
    }

    fun selectNavigasi(button: Button, code:Int){
        button.setOnClickListener {
            when(code){
                1 -> navigasiIntent(EntryKordinatActivity::class.java)
                2 -> navigasiIntent(EntryElevasiActivity::class.java)
                3 -> navigasiIntent(EntryGenanganActivity::class.java)
                4 -> navigasiIntent(EntryTutupanLahanActivity::class.java)
                5 -> navigasiIntent(EntryFloraFaunaActivity::class.java)
                6 ->  navigasiIntent(EntryDrainaseActivity::class.java)
                7 -> navigasiIntent(EntryKualitasAirTanahActivity::class.java)
                8 -> navigasiIntent(EntryKarakteristikSubstratumTanahActivity::class.java)
                9 -> navigasiIntent(EntryTipeLuapanActivity::class.java)
                10 -> navigasiIntent(EntryKetebalanGambutActivity::class.java)
                //11 -> navigasiIntent(EntryKarakteristikSubstratumDibawahLapisanGambutActivity::class.java)
                12 -> navigasiIntent(EntryTingkatKerusakanLahanActivity::class.java)
                13 -> navigasiIntent(EntryKebakaranLahanActivity::class.java)
                14 -> navigasiIntent(EntryPorositasKelengasanActivity::class.java)
                15 -> navigasiIntent(EntryDataGambarActivity::class.java)

            }
        }
    }

    fun selectCheckBox(checkBox: CheckBox, status:Int){
        if (checkBox.isChecked){
            when(status){
                1->{karakteristikSubstratumDibawahLapisanGambutJenis1 = 1}
                2->{karakteristikSubstratumDibawahLapisanGambutJenis2 = 1}
                3->{karakteristikSubstratumDibawahLapisanGambutJenis3 = 1}
                4->{karakteristikSubstratumDibawahLapisanGambutJenis4 = 1}
                5->{karakteristikSubstratumDibawahLapisanGambutJenis5 = 1}
            }
        }
    }

    fun checkBoxSet(value:String,checkBox: CheckBox)
    {
        checkBox.isChecked = value=="1"
    }
    private fun navigasiIntent(classtujuan: Class<*>) {
        //check box
        selectCheckBox(chk_karakteristikSubstratumDibawahLapisanGambutJenis1,1)
        selectCheckBox(chk_karakteristikSubstratumDibawahLapisanGambutJenis2,2)
        selectCheckBox(chk_karakteristikSubstratumDibawahLapisanGambutJenis3,3)
        selectCheckBox(chk_karakteristikSubstratumDibawahLapisanGambutJenis4,4)
        selectCheckBox(chk_karakteristikSubstratumDibawahLapisanGambutJenis5,5)

        survey.karakteristikSubstratumDibawahLapisanGambutJenis2 = karakteristikSubstratumDibawahLapisanGambutJenis2.toString()
        survey.karakteristikSubstratumDibawahLapisanGambutJenis3 = karakteristikSubstratumDibawahLapisanGambutJenis3.toString()
        survey.karakteristikSubstratumDibawahLapisanGambutJenis4 = karakteristikSubstratumDibawahLapisanGambutJenis4.toString()
        survey.karakteristikSubstratumDibawahLapisanGambutJenis5 = karakteristikSubstratumDibawahLapisanGambutJenis5.toString()

        survey.karakteristikSubstratumDibawahLapisanGambutNa = spn_karakteristikSubstratumDibawahLapisanGambutNa.selectedItem.toString()
        survey.karakteristikSubstratumDibawahLapisanGambutNaJenis = spn_karakteristikSubstratumDibawahLapisanGambutNaJenis.selectedItem.toString()
        //survey.karakteristikSubstratumDibawahLapisanGambutJenis = spn_karakteristikSubstratumDibawahLapisanGambutJenis.selectedItem.toString()
        survey.karakteristikSubstratumDibawahLapisanGambutLainnya = txt_karakteristikSubstratumDibawahLapisanGambutLainnya.text.toString()

        survey.karakteristikSubstratumDibawahLapisanGambutImg1 = pathkarakteristikSubstratumDibawahLapisanGambutImg1
        survey.karakteristikSubstratumDibawahLapisanGambutImg2 = pathkarakteristikSubstratumDibawahLapisanGambutImg2
        survey.karakteristikSubstratumDibawahLapisanGambutImg3 = pathkarakteristikSubstratumDibawahLapisanGambutImg3
        survey.karakteristikSubstratumDibawahLapisanGambutImg4 = pathkarakteristikSubstratumDibawahLapisanGambutImg4
        survey.karakteristikSubstratumDibawahLapisanGambutImg5 = pathkarakteristikSubstratumDibawahLapisanGambutImg5
        survey.karakteristikSubstratumDibawahLapisanGambutImg6 = pathkarakteristikSubstratumDibawahLapisanGambutImg6

        survey.update()
        val intent = Intent(activity,classtujuan)
        intent.putExtra("id", survey.id?.toInt())
        startActivity(intent)

    }

    fun deleteImage(button: ImageButton, imageView: ImageView, code: Int){
        button.setOnClickListener {
            when(code){
                1-> pathkarakteristikSubstratumDibawahLapisanGambutImg1 = ""
                2-> pathkarakteristikSubstratumDibawahLapisanGambutImg2 = ""
                3-> pathkarakteristikSubstratumDibawahLapisanGambutImg3 = ""
                4-> pathkarakteristikSubstratumDibawahLapisanGambutImg4 = ""
                5-> pathkarakteristikSubstratumDibawahLapisanGambutImg5 = ""
                6-> pathkarakteristikSubstratumDibawahLapisanGambutImg6 = ""
            }

            Glide.with(activity).load("").into(imageView)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        if(id != null){
            survey.getData(id)
        }
        super.onResume()
    }
}