package synergy.project.tallysheet.feature.survey.Entry

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import kotlinx.android.synthetic.main.layout_flora_fauna.*
import kotlinx.android.synthetic.main.layout_genangan.*
import kotlinx.android.synthetic.main.layout_genangan.btn_kembali
import kotlinx.android.synthetic.main.layout_genangan.btn_navigasi_1
import kotlinx.android.synthetic.main.layout_genangan.btn_navigasi_10
import kotlinx.android.synthetic.main.layout_genangan.btn_navigasi_11
import kotlinx.android.synthetic.main.layout_genangan.btn_navigasi_12
import kotlinx.android.synthetic.main.layout_genangan.btn_navigasi_13
import kotlinx.android.synthetic.main.layout_genangan.btn_navigasi_14
import kotlinx.android.synthetic.main.layout_genangan.btn_navigasi_15
import kotlinx.android.synthetic.main.layout_genangan.btn_navigasi_2
import kotlinx.android.synthetic.main.layout_genangan.btn_navigasi_3
import kotlinx.android.synthetic.main.layout_genangan.btn_navigasi_4
import kotlinx.android.synthetic.main.layout_genangan.btn_navigasi_5
import kotlinx.android.synthetic.main.layout_genangan.btn_navigasi_6
import kotlinx.android.synthetic.main.layout_genangan.btn_navigasi_7
import kotlinx.android.synthetic.main.layout_genangan.btn_navigasi_8
import kotlinx.android.synthetic.main.layout_genangan.btn_navigasi_9
import kotlinx.android.synthetic.main.layout_header.btn_simpan
import kotlinx.android.synthetic.main.layout_karakteristik_substratum_dibawah_lapisan_gambut.*
import splitties.activities.start
import splitties.toast.toast
import synergy.project.tallysheet.R
import synergy.project.tallysheet.model.Survey
import synergy.project.tallysheet.utils.DatePickerFragment
import synergy.project.tallysheet.utils.Helper
import synergy.project.tallysheet.utils.Local
import synergy.project.tallysheet.utils.PrefManager

class  EntryGenanganActivity : AppCompatActivity() {
    lateinit var survey: Survey
    lateinit var prefManager: PrefManager
    var id : Int= 0

    var pathbanjirImg1 = ""
    var pathbanjirImg2 = ""
    var pathbanjirImg3 = ""
    var pathbanjirImg4 = ""
    var pathbanjirImg5 = ""
    var pathbanjirImg6 = ""

    var banjirSumberGenangan1 = 0
    var banjirSumberGenangan2 = 0
    var banjirSumberGenangan3 = 0
    var banjirSumberGenangan4 = 0
    var banjirSumberGenanganLainnya = 0

    val activity = EntryGenanganActivity@this


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_genangan)
        val bundle = intent.extras
        if (bundle != null) {
            id = bundle["id"] as Int
        }
        survey = Survey(applicationContext)
        prefManager = PrefManager(applicationContext)

        // ini untuk ini value spinner
        val dataAdapter_banjirBulan: ArrayAdapter<String> = ArrayAdapter<String>(
            this@EntryGenanganActivity,// ganti jadi this@Nama Activity
            android.R.layout.simple_dropdown_item_1line,
            Helper.banjirBulan
        )
        spn_banjirBulan.adapter = dataAdapter_banjirBulan


        // ini untuk menampilkan data
        if (survey.getData(id)) {
            txt_kedalamanAirTanah.setText(survey.kedalamanAirTanah)
            txt_genangan.setText(survey.genangan)
            spn_banjirBulan.selectedItem.toString()
            txt_banjirLamanya.setText(survey.banjirLamanya)
            txt_banjirKetinggianAir.setText(survey.banjirKetinggianAir)
            txt_banjirSumberGenanganLainnya.setText(survey.banjirSumberGenanganLainnya)

            val banjirBulanAdp = spn_banjirBulan.adapter as ArrayAdapter<String>
            val banjirBulanPosition = banjirBulanAdp.getPosition(survey.banjirBulan)
            spn_banjirBulan.setSelection(banjirBulanPosition)

            Glide.with(activity).load(survey.banjirImg1).into(img_banjirImg1)
            Glide.with(activity).load(survey.banjirImg2).into(img_banjirImg2)
            Glide.with(activity).load(survey.banjirImg3).into(img_banjirImg3)
            Glide.with(activity).load(survey.banjirImg4).into(img_banjirImg4)
            Glide.with(activity).load(survey.banjirImg5).into(img_banjirImg5)
            Glide.with(activity).load(survey.banjirImg6).into(img_banjirImg6)

            pathbanjirImg1 = survey.banjirImg1.toString()
            pathbanjirImg2 = survey.banjirImg2.toString()
            pathbanjirImg3 = survey.banjirImg3.toString()
            pathbanjirImg4 = survey.banjirImg4.toString()
            pathbanjirImg5 = survey.banjirImg5.toString()
            pathbanjirImg6 = survey.banjirImg6.toString()


            checkBoxSet(survey.banjirSumberGenangan1!!,chk_banjirSumberGenangan1)
            checkBoxSet(survey.banjirSumberGenangan2!!,chk_banjirSumberGenangan2)
            checkBoxSet(survey.banjirSumberGenangan3!!,chk_banjirSumberGenangan3)
            checkBoxSet(survey.banjirSumberGenangan4!!,chk_banjirSumberGenangan4)


        }

        supportActionBar?.title = "Isi Survey"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayUseLogoEnabled(false)

        initView()
        klik()
    }

    private fun initView() {
        if(pathbanjirImg1!="") Local.isVisible(btn_banjirImg1,btn_delete_banjirImg1,true)
        else Local.isVisible(btn_banjirImg1,btn_delete_banjirImg1,false)

        if(pathbanjirImg2!="") Local.isVisible(btn_banjirImg2,btn_delete_banjirImg2,true)
        else Local.isVisible(btn_banjirImg2,btn_delete_banjirImg2,false)

        if(pathbanjirImg3!="") Local.isVisible(btn_banjirImg3,btn_delete_banjirImg3,true)
        else Local.isVisible(btn_banjirImg3,btn_delete_banjirImg3,false)

        if(pathbanjirImg4!="") Local.isVisible(btn_banjirImg4,btn_delete_banjirImg4,true)
        else Local.isVisible(btn_banjirImg4,btn_delete_banjirImg4,false)

        if(pathbanjirImg5!="") Local.isVisible(btn_banjirImg5,btn_delete_banjirImg5,true)
        else Local.isVisible(btn_banjirImg5,btn_delete_banjirImg5,false)

        if(pathbanjirImg6!="") Local.isVisible(btn_banjirImg6,btn_delete_banjirImg6,true)
        else Local.isVisible(btn_banjirImg6,btn_delete_banjirImg6,false)
    }

    fun klik(){
        btn_simpan.setOnClickListener {
            //check box
            selectCheckBox(chk_banjirSumberGenangan1,1)
            selectCheckBox(chk_banjirSumberGenangan2,2)
            selectCheckBox(chk_banjirSumberGenangan3,3)
            selectCheckBox(chk_banjirSumberGenangan4,4)

            survey.banjirSumberGenangan1 = banjirSumberGenangan1.toString()
            survey.banjirSumberGenangan2 = banjirSumberGenangan2.toString()
            survey.banjirSumberGenangan3 = banjirSumberGenangan3.toString()
            survey.banjirSumberGenangan4 = banjirSumberGenangan4.toString()
           // survey.banjirSumberGenanganLainnya = banjirSumberGenanganLainnya.toString()

            survey.kedalamanAirTanah = txt_kedalamanAirTanah.text.toString()
            survey.genangan = txt_genangan.text.toString()
            survey.banjirBulan = spn_banjirBulan.selectedItem.toString()
            survey.banjirLamanya = txt_banjirLamanya.text.toString()
            survey.banjirKetinggianAir = txt_banjirKetinggianAir.text.toString()
            survey.banjirSumberGenanganLainnya = txt_banjirSumberGenanganLainnya.text.toString()

            survey.banjirImg1 = pathbanjirImg1
            survey.banjirImg2 = pathbanjirImg2
            survey.banjirImg3 = pathbanjirImg3
            survey.banjirImg4 = pathbanjirImg4
            survey.banjirImg5 = pathbanjirImg5
            survey.banjirImg6 = pathbanjirImg6

            survey.update()
            start<EntryTutupanLahanActivity> {
                putExtra("id", survey.id?.toInt())
            }

        }

        btn_kembali.setOnClickListener {
            //check box
            selectCheckBox(chk_banjirSumberGenangan1,1)
            selectCheckBox(chk_banjirSumberGenangan2,2)
            selectCheckBox(chk_banjirSumberGenangan3,3)
            selectCheckBox(chk_banjirSumberGenangan4,4)

            survey.banjirSumberGenangan1 = banjirSumberGenangan1.toString()
            survey.banjirSumberGenangan2 = banjirSumberGenangan2.toString()
            survey.banjirSumberGenangan3 = banjirSumberGenangan3.toString()
            survey.banjirSumberGenangan4 = banjirSumberGenangan4.toString()

            survey.kedalamanAirTanah = txt_kedalamanAirTanah.text.toString()
            survey.genangan = txt_genangan.text.toString()
            survey.banjirBulan = spn_banjirBulan.selectedItem.toString()
            survey.banjirLamanya = txt_banjirLamanya.text.toString()
            survey.banjirKetinggianAir = txt_banjirKetinggianAir.text.toString()
            survey.banjirSumberGenanganLainnya = txt_banjirSumberGenanganLainnya.text.toString()

            survey.banjirImg1 = pathbanjirImg1
            survey.banjirImg2 = pathbanjirImg2
            survey.banjirImg3 = pathbanjirImg3
            survey.banjirImg4 = pathbanjirImg4
            survey.banjirImg5 = pathbanjirImg5
            survey.banjirImg6 = pathbanjirImg6
            survey.update()
            onBackPressed()
        }

        btn_banjirImg1.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(113) }
        btn_banjirImg2.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(114) }
        btn_banjirImg3.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(115) }
        btn_banjirImg4.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(116) }
        btn_banjirImg5.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(117) }
        btn_banjirImg6.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(118) }


        //delete image
        deleteImage(btn_delete_banjirImg1,img_banjirImg1,1)
        deleteImage(btn_delete_banjirImg2,img_banjirImg2,2)
        deleteImage(btn_delete_banjirImg3,img_banjirImg3,3)
        deleteImage(btn_delete_banjirImg4,img_banjirImg4,4)
        deleteImage(btn_delete_banjirImg5,img_banjirImg5,5)
        deleteImage(btn_delete_banjirImg6,img_banjirImg6,6)

        //navigasi
        selectNavigasi(btn_navigasi_1,1)
        selectNavigasi(btn_navigasi_2,2)
        selectNavigasi(btn_navigasi_3,3)
        selectNavigasi(btn_navigasi_4,4)
        selectNavigasi(btn_navigasi_5,5)
        selectNavigasi(btn_navigasi_6,6)
        selectNavigasi(btn_navigasi_7,7)
        selectNavigasi(btn_navigasi_8,8)
        selectNavigasi(btn_navigasi_9,9)
        selectNavigasi(btn_navigasi_10,10)
        selectNavigasi(btn_navigasi_11,11)
        selectNavigasi(btn_navigasi_12,12)
        selectNavigasi(btn_navigasi_13,13)
        selectNavigasi(btn_navigasi_14,14)
        selectNavigasi(btn_navigasi_15,15)
    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode==113){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_banjirImg1.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_banjirImg1)
                survey.banjirImg1 = filePath
                pathbanjirImg1 = filePath
            }
        }

        if (requestCode==114){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_banjirImg2.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_banjirImg2)
                survey.banjirImg2 = filePath
                pathbanjirImg2 = filePath
            }
        }

        if (requestCode==115){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_banjirImg3.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_banjirImg3)
                survey.banjirImg3 = filePath
                pathbanjirImg3 = filePath
            }
        }

        if (requestCode==116){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_banjirImg4.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_banjirImg4)
                survey.banjirImg4 = filePath
                pathbanjirImg4 = filePath
            }
        }

        if (requestCode==117){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_banjirImg5.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_banjirImg5)
                survey.banjirImg5 = filePath
                pathbanjirImg5 = filePath
            }
        }

        if (requestCode==118){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_banjirImg6.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_banjirImg6)
                survey.banjirImg6 = filePath
                pathbanjirImg6 = filePath

            }
        }

    }

    fun returnData(id:Int){
        if (survey.getData(id)) {

        } else {

        }
    }

    fun selectNavigasi(button: Button, code:Int){
        button.setOnClickListener {
            when(code){
                1 -> navigasiIntent(EntryKordinatActivity::class.java)
                2 -> navigasiIntent(EntryElevasiActivity::class.java)
                //3 -> navigasiIntent(EntryGenanganActivity::class.java)
                4 -> navigasiIntent(EntryTutupanLahanActivity::class.java)
                5 -> navigasiIntent(EntryFloraFaunaActivity::class.java)
                6 ->  navigasiIntent(EntryDrainaseActivity::class.java)
                7 -> navigasiIntent(EntryKualitasAirTanahActivity::class.java)
                8 -> navigasiIntent(EntryKarakteristikSubstratumTanahActivity::class.java)
                9 -> navigasiIntent(EntryTipeLuapanActivity::class.java)
                10 -> navigasiIntent(EntryKetebalanGambutActivity::class.java)
                11 -> navigasiIntent(EntryKarakteristikSubstratumDibawahLapisanGambutActivity::class.java)
                12 -> navigasiIntent(EntryTingkatKerusakanLahanActivity::class.java)
                13 -> navigasiIntent(EntryKebakaranLahanActivity::class.java)
                14 -> navigasiIntent(EntryPorositasKelengasanActivity::class.java)
                15 -> navigasiIntent(EntryDataGambarActivity::class.java)

            }
        }
    }

    fun selectCheckBox(checkBox: CheckBox, status:Int){
        if (checkBox.isChecked){
            when(status){
                1->{banjirSumberGenangan1 = 1}
                2->{banjirSumberGenangan2 = 1}
                3->{banjirSumberGenangan3 = 1}
                4->{banjirSumberGenangan4 = 1}
            }
        }
    }

    fun checkBoxSet(value:String,checkBox: CheckBox)
    {
        checkBox.isChecked = value=="1"
    }

    private fun navigasiIntent(classtujuan: Class<*>) {
        //check box
        selectCheckBox(chk_banjirSumberGenangan1,1)
        selectCheckBox(chk_banjirSumberGenangan2,2)
        selectCheckBox(chk_banjirSumberGenangan3,3)
        selectCheckBox(chk_banjirSumberGenangan4,4)

        survey.banjirImg1 = pathbanjirImg1
        survey.banjirImg2 = pathbanjirImg2
        survey.banjirImg3 = pathbanjirImg3
        survey.banjirImg4 = pathbanjirImg4
        survey.banjirImg5 = pathbanjirImg5
        survey.banjirImg6 = pathbanjirImg6

        survey.banjirSumberGenangan1 = banjirSumberGenangan1.toString()
        survey.banjirSumberGenangan2 = banjirSumberGenangan2.toString()
        survey.banjirSumberGenangan3 = banjirSumberGenangan3.toString()
        survey.banjirSumberGenangan4 = banjirSumberGenangan4.toString()

        survey.kedalamanAirTanah = txt_kedalamanAirTanah.text.toString()
        survey.genangan = txt_genangan.text.toString()
        survey.banjirBulan = spn_banjirBulan.selectedItem.toString()
        survey.banjirLamanya = txt_banjirLamanya.text.toString()
        survey.banjirKetinggianAir = txt_banjirKetinggianAir.text.toString()
        survey.banjirSumberGenanganLainnya = txt_banjirSumberGenanganLainnya.text.toString()
        survey.update()
        val intent = Intent(activity,classtujuan)
        intent.putExtra("id", survey.id?.toInt())
        startActivity(intent)

    }

    fun deleteImage(button: ImageButton, imageView: ImageView, code: Int){
        button.setOnClickListener {
            when(code){
                1-> pathbanjirImg1 = ""
                2-> pathbanjirImg2 = ""
                3-> pathbanjirImg3 = ""
                4-> pathbanjirImg4 = ""
                5-> pathbanjirImg5 = ""
                6-> pathbanjirImg6 = ""
            }

            Glide.with(activity).load("").into(imageView)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
    override fun onResume() {
        if(id != null){
            survey.getData(id)
        }
        super.onResume()
    }

}