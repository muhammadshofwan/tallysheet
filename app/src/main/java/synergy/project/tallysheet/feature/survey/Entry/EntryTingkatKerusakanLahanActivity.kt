package synergy.project.tallysheet.feature.survey.Entry

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import kotlinx.android.synthetic.main.layout_genangan.*
import kotlinx.android.synthetic.main.layout_header.btn_simpan
import kotlinx.android.synthetic.main.layout_koordinat_titik_survey.btn_kordinatImg1
import kotlinx.android.synthetic.main.layout_koordinat_titik_survey.btn_kordinatImg2
import kotlinx.android.synthetic.main.layout_koordinat_titik_survey.btn_kordinatImg3
import kotlinx.android.synthetic.main.layout_koordinat_titik_survey.btn_kordinatImg4
import kotlinx.android.synthetic.main.layout_porositas_kelengasan.*
import kotlinx.android.synthetic.main.layout_tingkat_kerusakan_lahan_gambut.*
import kotlinx.android.synthetic.main.layout_tingkat_kerusakan_lahan_gambut.btn_kembali
import kotlinx.android.synthetic.main.layout_tingkat_kerusakan_lahan_gambut.btn_navigasi_1
import kotlinx.android.synthetic.main.layout_tingkat_kerusakan_lahan_gambut.btn_navigasi_10
import kotlinx.android.synthetic.main.layout_tingkat_kerusakan_lahan_gambut.btn_navigasi_11
import kotlinx.android.synthetic.main.layout_tingkat_kerusakan_lahan_gambut.btn_navigasi_12
import kotlinx.android.synthetic.main.layout_tingkat_kerusakan_lahan_gambut.btn_navigasi_13
import kotlinx.android.synthetic.main.layout_tingkat_kerusakan_lahan_gambut.btn_navigasi_14
import kotlinx.android.synthetic.main.layout_tingkat_kerusakan_lahan_gambut.btn_navigasi_15
import kotlinx.android.synthetic.main.layout_tingkat_kerusakan_lahan_gambut.btn_navigasi_2
import kotlinx.android.synthetic.main.layout_tingkat_kerusakan_lahan_gambut.btn_navigasi_3
import kotlinx.android.synthetic.main.layout_tingkat_kerusakan_lahan_gambut.btn_navigasi_4
import kotlinx.android.synthetic.main.layout_tingkat_kerusakan_lahan_gambut.btn_navigasi_5
import kotlinx.android.synthetic.main.layout_tingkat_kerusakan_lahan_gambut.btn_navigasi_6
import kotlinx.android.synthetic.main.layout_tingkat_kerusakan_lahan_gambut.btn_navigasi_7
import kotlinx.android.synthetic.main.layout_tingkat_kerusakan_lahan_gambut.btn_navigasi_8
import kotlinx.android.synthetic.main.layout_tingkat_kerusakan_lahan_gambut.btn_navigasi_9
import splitties.activities.start
import splitties.toast.toast
import synergy.project.tallysheet.R
import synergy.project.tallysheet.model.Survey
import synergy.project.tallysheet.utils.DatePickerFragment
import synergy.project.tallysheet.utils.Helper
import synergy.project.tallysheet.utils.Local
import synergy.project.tallysheet.utils.PrefManager

class  EntryTingkatKerusakanLahanActivity : AppCompatActivity() {
    lateinit var survey: Survey
    lateinit var prefManager: PrefManager
    var id : Int= 0

    var pathtingkatKerusakanLahanImg1 = ""
    var pathtingkatKerusakanLahanImg2 = ""
    var pathtingkatKerusakanLahanImg3 = ""
    var pathtingkatKerusakanLahanImg4 = ""
    var pathtingkatKerusakanLahanImg5 = ""
    var pathtingkatKerusakanLahanImg6 = ""

    var tingkatKerusakanLahanKondisiTanaman1 = 0
    var tingkatKerusakanLahanKondisiTanaman2 = 0
    var tingkatKerusakanLahanKondisiTanaman3 = 0
    var tingkatKerusakanLahanKondisiTanaman4 = 0

    val activity = EntryTingkatKerusakanLahanActivity@this


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_tingkat_kerusakan_lahan_gambut)
        val bundle = intent.extras
        if (bundle != null) {
            id = bundle["id"] as Int
        }
        survey = Survey(applicationContext)
        survey.getData(id)
        prefManager = PrefManager(applicationContext)

        // ini untuk ini value spinner
        val dataAdapter_tingkatKerusakanLahanGambutDrainaseBuatan: ArrayAdapter<String> = ArrayAdapter<String>(
            this@EntryTingkatKerusakanLahanActivity,// ganti jadi this@Nama Activity
            android.R.layout.simple_dropdown_item_1line,
            Helper.tingkatKerusakanLahanGambutDrainaseBuatan
        )
        // ini untuk ini value spinner
        val dataAdapter_tingkatKerusakanLahanGambutSedimen: ArrayAdapter<String> = ArrayAdapter<String>(
            this@EntryTingkatKerusakanLahanActivity,// ganti jadi this@Nama Activity
            android.R.layout.simple_dropdown_item_1line,
            Helper.tingkatKerusakanLahanGambutSedimen
        )
        // ini untuk ini value spinner
        val dataAdapter_tingkatKerusakanLahanKondisiTanaman: ArrayAdapter<String> = ArrayAdapter<String>(
            this@EntryTingkatKerusakanLahanActivity,// ganti jadi this@Nama Activity
            android.R.layout.simple_dropdown_item_1line,
            Helper.tingkatKerusakanLahanKondisiTanaman
        )
        // ini untuk ini value spinner
        val dataAdapter_tingkatKerusakanLahanKerapatanTajuk: ArrayAdapter<String> = ArrayAdapter<String>(
            this@EntryTingkatKerusakanLahanActivity,// ganti jadi this@Nama Activity
            android.R.layout.simple_dropdown_item_1line,
            Helper.tingkatKerusakanLahanKerapatanTajuk
        )


        spn_tingkatKerusakanLahanGambutDrainaseBuatan.adapter = dataAdapter_tingkatKerusakanLahanGambutDrainaseBuatan
        spn_tingkatKerusakanLahanGambutSedimen.adapter = dataAdapter_tingkatKerusakanLahanGambutSedimen
        spn_tingkatKerusakanLahanKondisiTanaman.adapter = dataAdapter_tingkatKerusakanLahanKondisiTanaman
        spn_tingkatKerusakanLahanKerapatanTajuk.adapter = dataAdapter_tingkatKerusakanLahanKerapatanTajuk

        if (survey.getData(id)) {
            val tingkatKerusakanLahanGambutDrainaseBuatanAdp = spn_tingkatKerusakanLahanGambutDrainaseBuatan.adapter as ArrayAdapter<String>
            val tingkatKerusakanLahanGambutDrainaseBuatanPosition = tingkatKerusakanLahanGambutDrainaseBuatanAdp.getPosition(survey.tingkatKerusakanLahanGambutDrainaseBuatan)
            spn_tingkatKerusakanLahanGambutDrainaseBuatan.setSelection(tingkatKerusakanLahanGambutDrainaseBuatanPosition)
            val tingkatKerusakanLahanGambutSedimenAdp = spn_tingkatKerusakanLahanGambutSedimen.adapter as ArrayAdapter<String>
            val tingkatKerusakanLahanGambutSedimenPosition = tingkatKerusakanLahanGambutSedimenAdp.getPosition(survey.tingkatKerusakanLahanGambutSedimen)
            spn_tingkatKerusakanLahanGambutSedimen.setSelection(tingkatKerusakanLahanGambutSedimenPosition)
            val tingkatKerusakanLahanKondisiTanamanAdp = spn_tingkatKerusakanLahanKondisiTanaman.adapter as ArrayAdapter<String>
            val tingkatKerusakanLahanKondisiTanamanPosition = tingkatKerusakanLahanKondisiTanamanAdp.getPosition(survey.tingkatKerusakanLahanKondisiTanaman)
            spn_tingkatKerusakanLahanKondisiTanaman.setSelection(tingkatKerusakanLahanKondisiTanamanPosition)
            txt_tingkatKerusakanLahanSubsiden.setText(survey.tingkatKerusakanLahanSubsiden)
            val tingkatKerusakanLahanKerapatanTajukAdp = spn_tingkatKerusakanLahanKerapatanTajuk.adapter as ArrayAdapter<String>
            val tingkatKerusakanLahanKerapatanTajukPosition = tingkatKerusakanLahanKerapatanTajukAdp.getPosition(survey.tingkatKerusakanLahanKerapatanTajuk)
            spn_tingkatKerusakanLahanKerapatanTajuk.setSelection(tingkatKerusakanLahanKerapatanTajukPosition)

            Glide.with(activity).load(survey.tingkatKerusakanLahanImg1).into(img_tingkatKerusakanLahanImg1)
            Glide.with(activity).load(survey.tingkatKerusakanLahanImg2).into(img_tingkatKerusakanLahanImg2)
            Glide.with(activity).load(survey.tingkatKerusakanLahanImg3).into(img_tingkatKerusakanLahanImg3)
            Glide.with(activity).load(survey.tingkatKerusakanLahanImg4).into(img_tingkatKerusakanLahanImg4)
            Glide.with(activity).load(survey.tingkatKerusakanLahanImg5).into(img_tingkatKerusakanLahanImg5)
            Glide.with(activity).load(survey.tingkatKerusakanLahanImg6).into(img_tingkatKerusakanLahanImg6)

            checkBoxSet(survey.tingkatKerusakanLahanKondisiTanaman1!!,chk_tingkatKerusakanLahanKondisiTanaman1)
            checkBoxSet(survey.tingkatKerusakanLahanKondisiTanaman2!!,chk_tingkatKerusakanLahanKondisiTanaman2)
            checkBoxSet(survey.tingkatKerusakanLahanKondisiTanaman3!!,chk_tingkatKerusakanLahanKondisiTanaman3)
            checkBoxSet(survey.tingkatKerusakanLahanKondisiTanaman4!!,chk_tingkatKerusakanLahanKondisiTanaman4)

            pathtingkatKerusakanLahanImg1 = survey.tingkatKerusakanLahanImg1.toString()
            pathtingkatKerusakanLahanImg2 = survey.tingkatKerusakanLahanImg2.toString()
            pathtingkatKerusakanLahanImg3 = survey.tingkatKerusakanLahanImg3.toString()
            pathtingkatKerusakanLahanImg4 = survey.tingkatKerusakanLahanImg4.toString()
            pathtingkatKerusakanLahanImg5 = survey.tingkatKerusakanLahanImg5.toString()
            pathtingkatKerusakanLahanImg6 = survey.tingkatKerusakanLahanImg6.toString()
        }


        supportActionBar?.title = "Isi Survey"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayUseLogoEnabled(false)

        initView()
        klik()
    }

    private fun initView() {
        if(pathtingkatKerusakanLahanImg1!="") Local.isVisible(btn_tingkatKerusakanLahan1,btn_delete_tingkatKerusakanLahan1,true)
        else Local.isVisible(btn_tingkatKerusakanLahan1,btn_delete_tingkatKerusakanLahan1,false)

        if(pathtingkatKerusakanLahanImg2!="") Local.isVisible(btn_tingkatKerusakanLahan2,btn_delete_tingkatKerusakanLahan2,true)
        else Local.isVisible(btn_tingkatKerusakanLahan2,btn_delete_tingkatKerusakanLahan2,false)

        if(pathtingkatKerusakanLahanImg3!="") Local.isVisible(btn_tingkatKerusakanLahan3,btn_delete_tingkatKerusakanLahan3,true)
        else Local.isVisible(btn_tingkatKerusakanLahan3,btn_delete_tingkatKerusakanLahan3,false)

        if(pathtingkatKerusakanLahanImg4!="") Local.isVisible(btn_tingkatKerusakanLahan4,btn_delete_tingkatKerusakanLahan4,true)
        else Local.isVisible(btn_tingkatKerusakanLahan4,btn_delete_tingkatKerusakanLahan4,false)

        if(pathtingkatKerusakanLahanImg5!="") Local.isVisible(btn_tingkatKerusakanLahan5,btn_delete_tingkatKerusakanLahan5,true)
        else Local.isVisible(btn_tingkatKerusakanLahan5,btn_delete_tingkatKerusakanLahan5,false)

        if(pathtingkatKerusakanLahanImg6!="") Local.isVisible(btn_tingkatKerusakanLahan6,btn_delete_tingkatKerusakanLahan6,true)
        else Local.isVisible(btn_tingkatKerusakanLahan6,btn_delete_tingkatKerusakanLahan6,false)
    }

    fun klik(){
        btn_simpan.setOnClickListener {
            selectCheckBox(chk_tingkatKerusakanLahanKondisiTanaman1,1)
            selectCheckBox(chk_tingkatKerusakanLahanKondisiTanaman2,2)
            selectCheckBox(chk_tingkatKerusakanLahanKondisiTanaman3,3)
            selectCheckBox(chk_tingkatKerusakanLahanKondisiTanaman4,4)

            survey.tingkatKerusakanLahanKondisiTanaman1 = tingkatKerusakanLahanKondisiTanaman1.toString()
            survey.tingkatKerusakanLahanKondisiTanaman2 = tingkatKerusakanLahanKondisiTanaman2.toString()
            survey.tingkatKerusakanLahanKondisiTanaman3 = tingkatKerusakanLahanKondisiTanaman3.toString()
            survey.tingkatKerusakanLahanKondisiTanaman4 = tingkatKerusakanLahanKondisiTanaman4.toString()

            survey.tingkatKerusakanLahanGambutDrainaseBuatan = spn_tingkatKerusakanLahanGambutDrainaseBuatan.selectedItem.toString()
            survey.tingkatKerusakanLahanGambutSedimen = spn_tingkatKerusakanLahanGambutSedimen.selectedItem.toString()
//            survey.tingkatKerusakanLahanKondisiTanaman = spn_tingkatKerusakanLahanKondisiTanaman.selectedItem.toString()
            survey.tingkatKerusakanLahanSubsiden = txt_tingkatKerusakanLahanSubsiden.text.toString()
            survey.tingkatKerusakanLahanKerapatanTajuk = spn_tingkatKerusakanLahanKerapatanTajuk.selectedItem.toString()

            survey.tingkatKerusakanLahanImg1 = pathtingkatKerusakanLahanImg1
            survey.tingkatKerusakanLahanImg2 = pathtingkatKerusakanLahanImg2
            survey.tingkatKerusakanLahanImg3 = pathtingkatKerusakanLahanImg3
            survey.tingkatKerusakanLahanImg4 = pathtingkatKerusakanLahanImg4
            survey.tingkatKerusakanLahanImg5 = pathtingkatKerusakanLahanImg5
            survey.tingkatKerusakanLahanImg6 = pathtingkatKerusakanLahanImg6

            survey.update()
            start<EntryKebakaranLahanActivity> {
                putExtra("id", survey.id?.toInt())
            }

        }

        btn_kembali.setOnClickListener {

            selectCheckBox(chk_tingkatKerusakanLahanKondisiTanaman1,1)
            selectCheckBox(chk_tingkatKerusakanLahanKondisiTanaman2,2)
            selectCheckBox(chk_tingkatKerusakanLahanKondisiTanaman3,3)
            selectCheckBox(chk_tingkatKerusakanLahanKondisiTanaman4,4)

            survey.tingkatKerusakanLahanKondisiTanaman1 = tingkatKerusakanLahanKondisiTanaman1.toString()
            survey.tingkatKerusakanLahanKondisiTanaman2 = tingkatKerusakanLahanKondisiTanaman2.toString()
            survey.tingkatKerusakanLahanKondisiTanaman3 = tingkatKerusakanLahanKondisiTanaman3.toString()
            survey.tingkatKerusakanLahanKondisiTanaman4 = tingkatKerusakanLahanKondisiTanaman4.toString()

            survey.tingkatKerusakanLahanGambutDrainaseBuatan = spn_tingkatKerusakanLahanGambutDrainaseBuatan.selectedItem.toString()
            survey.tingkatKerusakanLahanGambutSedimen = spn_tingkatKerusakanLahanGambutSedimen.selectedItem.toString()
          //  survey.tingkatKerusakanLahanKondisiTanaman = spn_tingkatKerusakanLahanKondisiTanaman.selectedItem.toString()
            survey.tingkatKerusakanLahanSubsiden = txt_tingkatKerusakanLahanSubsiden.text.toString()
            survey.tingkatKerusakanLahanKerapatanTajuk = spn_tingkatKerusakanLahanKerapatanTajuk.selectedItem.toString()

            survey.tingkatKerusakanLahanImg1 = pathtingkatKerusakanLahanImg1
            survey.tingkatKerusakanLahanImg2 = pathtingkatKerusakanLahanImg2
            survey.tingkatKerusakanLahanImg3 = pathtingkatKerusakanLahanImg3
            survey.tingkatKerusakanLahanImg4 = pathtingkatKerusakanLahanImg4
            survey.tingkatKerusakanLahanImg5 = pathtingkatKerusakanLahanImg5
            survey.tingkatKerusakanLahanImg6 = pathtingkatKerusakanLahanImg6

            survey.update()
            onBackPressed()
        }

        btn_tingkatKerusakanLahan1.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(167) }
        btn_tingkatKerusakanLahan2.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(168) }
        btn_tingkatKerusakanLahan3.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(169) }
        btn_tingkatKerusakanLahan4.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(170) }
        btn_tingkatKerusakanLahan5.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(171) }
        btn_tingkatKerusakanLahan6.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(172) }


        //delete image
        deleteImage(btn_delete_tingkatKerusakanLahan1,img_tingkatKerusakanLahanImg1,1)
        deleteImage(btn_delete_tingkatKerusakanLahan2,img_tingkatKerusakanLahanImg2,2)
        deleteImage(btn_delete_tingkatKerusakanLahan3,img_tingkatKerusakanLahanImg3,3)
        deleteImage(btn_delete_tingkatKerusakanLahan4,img_tingkatKerusakanLahanImg4,4)
        deleteImage(btn_delete_tingkatKerusakanLahan5,img_tingkatKerusakanLahanImg5,5)
        deleteImage(btn_delete_tingkatKerusakanLahan6,img_tingkatKerusakanLahanImg6,6)

        //navigasi
        selectNavigasi(btn_navigasi_1,1)
        selectNavigasi(btn_navigasi_2,2)
        selectNavigasi(btn_navigasi_3,3)
        selectNavigasi(btn_navigasi_4,4)
        selectNavigasi(btn_navigasi_5,5)
        selectNavigasi(btn_navigasi_6,6)
        selectNavigasi(btn_navigasi_7,7)
        selectNavigasi(btn_navigasi_8,8)
        selectNavigasi(btn_navigasi_9,9)
        selectNavigasi(btn_navigasi_10,10)
        selectNavigasi(btn_navigasi_11,11)
        selectNavigasi(btn_navigasi_12,12)
        selectNavigasi(btn_navigasi_13,13)
        selectNavigasi(btn_navigasi_14,14)
        selectNavigasi(btn_navigasi_15,15)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode==167){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_tingkatKerusakanLahanImg1.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_tingkatKerusakanLahanImg1)
                survey.tingkatKerusakanLahanImg1 = filePath
                pathtingkatKerusakanLahanImg1 = filePath

            }
        }

        if (requestCode==168){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_tingkatKerusakanLahanImg2.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_tingkatKerusakanLahanImg2)
                survey.tingkatKerusakanLahanImg2 = filePath
                pathtingkatKerusakanLahanImg2 = filePath
            }
        }

        if (requestCode==169){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_tingkatKerusakanLahanImg3.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_tingkatKerusakanLahanImg3)
                survey.tingkatKerusakanLahanImg3 = filePath
                pathtingkatKerusakanLahanImg3 = filePath
            }
        }

        if (requestCode==170){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_tingkatKerusakanLahanImg4.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_tingkatKerusakanLahanImg4)
                survey.tingkatKerusakanLahanImg4 = filePath
                pathtingkatKerusakanLahanImg4 = filePath
            }
        }

        if (requestCode==171){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_tingkatKerusakanLahanImg5.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_tingkatKerusakanLahanImg5)
                survey.tingkatKerusakanLahanImg5 = filePath
                pathtingkatKerusakanLahanImg5 = filePath
            }
        }

        if (requestCode==172){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_tingkatKerusakanLahanImg6.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_tingkatKerusakanLahanImg6)
                survey.tingkatKerusakanLahanImg6 = filePath
                pathtingkatKerusakanLahanImg6 = filePath
            }
        }
    }

    fun returnData(id:Int){
        if (survey.getData(id)) {

        } else {

        }
    }

    fun selectNavigasi(button: Button, code:Int){
        button.setOnClickListener {
            when(code){
                1 -> navigasiIntent(EntryKordinatActivity::class.java)
                2 -> navigasiIntent(EntryElevasiActivity::class.java)
                3 -> navigasiIntent(EntryGenanganActivity::class.java)
                4 -> navigasiIntent(EntryTutupanLahanActivity::class.java)
                5 -> navigasiIntent(EntryFloraFaunaActivity::class.java)
                6 ->  navigasiIntent(EntryDrainaseActivity::class.java)
                7 -> navigasiIntent(EntryKualitasAirTanahActivity::class.java)
                8 -> navigasiIntent(EntryKarakteristikSubstratumTanahActivity::class.java)
                9 -> navigasiIntent(EntryTipeLuapanActivity::class.java)
                10 -> navigasiIntent(EntryKetebalanGambutActivity::class.java)
                11 -> navigasiIntent(EntryKarakteristikSubstratumDibawahLapisanGambutActivity::class.java)
                //12 -> navigasiIntent(EntryTingkatKerusakanLahanActivity::class.java)
                13 -> navigasiIntent(EntryKebakaranLahanActivity::class.java)
                14 -> navigasiIntent(EntryPorositasKelengasanActivity::class.java)
                15 -> navigasiIntent(EntryDataGambarActivity::class.java)
            }
        }
    }

    fun selectCheckBox(checkBox: CheckBox, status:Int){
        if (checkBox.isChecked){
            when(status){
                1->{tingkatKerusakanLahanKondisiTanaman1 = 1}
                2->{tingkatKerusakanLahanKondisiTanaman2 = 1}
                3->{tingkatKerusakanLahanKondisiTanaman3 = 1}
                4->{tingkatKerusakanLahanKondisiTanaman4 = 1}
            }
        }
    }

    fun checkBoxSet(value:String,checkBox: CheckBox)
    {
        checkBox.isChecked = value=="1"
    }


    private fun navigasiIntent(classtujuan: Class<*>) {
        survey.tingkatKerusakanLahanKondisiTanaman1 = tingkatKerusakanLahanKondisiTanaman1.toString()
        survey.tingkatKerusakanLahanKondisiTanaman2 = tingkatKerusakanLahanKondisiTanaman2.toString()
        survey.tingkatKerusakanLahanKondisiTanaman3 = tingkatKerusakanLahanKondisiTanaman3.toString()
        survey.tingkatKerusakanLahanKondisiTanaman4 = tingkatKerusakanLahanKondisiTanaman4.toString()

        survey.tingkatKerusakanLahanGambutDrainaseBuatan = spn_tingkatKerusakanLahanGambutDrainaseBuatan.selectedItem.toString()
        survey.tingkatKerusakanLahanGambutSedimen = spn_tingkatKerusakanLahanGambutSedimen.selectedItem.toString()
      //  survey.tingkatKerusakanLahanKondisiTanaman = spn_tingkatKerusakanLahanKondisiTanaman.selectedItem.toString()
        survey.tingkatKerusakanLahanSubsiden = txt_tingkatKerusakanLahanSubsiden.text.toString()
        survey.tingkatKerusakanLahanKerapatanTajuk = spn_tingkatKerusakanLahanKerapatanTajuk.selectedItem.toString()

        survey.tingkatKerusakanLahanImg1 = pathtingkatKerusakanLahanImg1
        survey.tingkatKerusakanLahanImg2 = pathtingkatKerusakanLahanImg2
        survey.tingkatKerusakanLahanImg3 = pathtingkatKerusakanLahanImg3
        survey.tingkatKerusakanLahanImg4 = pathtingkatKerusakanLahanImg4
        survey.tingkatKerusakanLahanImg5 = pathtingkatKerusakanLahanImg5
        survey.tingkatKerusakanLahanImg6 = pathtingkatKerusakanLahanImg6
        survey.update()
        val intent = Intent(activity,classtujuan)
        intent.putExtra("id", survey.id?.toInt())
        startActivity(intent)

    }

    fun deleteImage(button: ImageButton, imageView: ImageView, code: Int){
        button.setOnClickListener {
            when(code){
                1-> pathtingkatKerusakanLahanImg1 = ""
                2-> pathtingkatKerusakanLahanImg2 = ""
                3-> pathtingkatKerusakanLahanImg3 = ""
                4-> pathtingkatKerusakanLahanImg4 = ""
                5-> pathtingkatKerusakanLahanImg5 = ""
                6-> pathtingkatKerusakanLahanImg6 = ""
            }

            Glide.with(activity).load("").into(imageView)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
    override fun onResume() {
        if(id != null){
            survey.getData(id)
        }
        super.onResume()
    }

}