package synergy.project.tallysheet.feature.survey.Entry

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import kotlinx.android.synthetic.main.layout_genangan.*
import kotlinx.android.synthetic.main.layout_header.btn_simpan
import kotlinx.android.synthetic.main.layout_koordinat_titik_survey.btn_kordinatImg1
import kotlinx.android.synthetic.main.layout_koordinat_titik_survey.btn_kordinatImg2
import kotlinx.android.synthetic.main.layout_koordinat_titik_survey.btn_kordinatImg3
import kotlinx.android.synthetic.main.layout_koordinat_titik_survey.btn_kordinatImg4
import kotlinx.android.synthetic.main.layout_tingkat_kerusakan_lahan_gambut.*
import kotlinx.android.synthetic.main.layout_tipe_luapan.*
import kotlinx.android.synthetic.main.layout_tipe_luapan.btn_kembali
import kotlinx.android.synthetic.main.layout_tipe_luapan.btn_navigasi_1
import kotlinx.android.synthetic.main.layout_tipe_luapan.btn_navigasi_10
import kotlinx.android.synthetic.main.layout_tipe_luapan.btn_navigasi_11
import kotlinx.android.synthetic.main.layout_tipe_luapan.btn_navigasi_12
import kotlinx.android.synthetic.main.layout_tipe_luapan.btn_navigasi_13
import kotlinx.android.synthetic.main.layout_tipe_luapan.btn_navigasi_14
import kotlinx.android.synthetic.main.layout_tipe_luapan.btn_navigasi_15
import kotlinx.android.synthetic.main.layout_tipe_luapan.btn_navigasi_2
import kotlinx.android.synthetic.main.layout_tipe_luapan.btn_navigasi_3
import kotlinx.android.synthetic.main.layout_tipe_luapan.btn_navigasi_4
import kotlinx.android.synthetic.main.layout_tipe_luapan.btn_navigasi_5
import kotlinx.android.synthetic.main.layout_tipe_luapan.btn_navigasi_6
import kotlinx.android.synthetic.main.layout_tipe_luapan.btn_navigasi_7
import kotlinx.android.synthetic.main.layout_tipe_luapan.btn_navigasi_8
import kotlinx.android.synthetic.main.layout_tipe_luapan.btn_navigasi_9
import splitties.activities.start
import splitties.toast.toast
import synergy.project.tallysheet.R
import synergy.project.tallysheet.model.Survey
import synergy.project.tallysheet.utils.DatePickerFragment
import synergy.project.tallysheet.utils.Helper
import synergy.project.tallysheet.utils.Local
import synergy.project.tallysheet.utils.PrefManager

class  EntryTipeLuapanActivity : AppCompatActivity() {
    lateinit var survey: Survey
    lateinit var prefManager: PrefManager
    var id : Int= 0

    var pathtipeLuapanImg1 = ""
    var pathtipeLuapanImg2 = ""
    var pathtipeLuapanImg3 = ""
    var pathtipeLuapanImg4 = ""
    var pathtipeLuapanImg5 = ""
    var pathtipeLuapanImg6 = ""

    val activity = EntryTipeLuapanActivity@this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_tipe_luapan)
        val bundle = intent.extras
        if (bundle != null) {
            id = bundle["id"] as Int
        }
        survey = Survey(applicationContext)
        survey.getData(id)
        prefManager = PrefManager(applicationContext)

        // ini untuk ini value spinner
        val dataAdapter_tipeLuapanKemarau: ArrayAdapter<String> = ArrayAdapter<String>(
            this@EntryTipeLuapanActivity,// ganti jadi this@Nama Activity
            android.R.layout.simple_dropdown_item_1line,
            Helper.tipeLuapanKemarau
        )

        val dataAdapter_tipeLuapanHujan: ArrayAdapter<String> = ArrayAdapter<String>(
            this@EntryTipeLuapanActivity,// ganti jadi this@Nama Activity
            android.R.layout.simple_dropdown_item_1line,
            Helper.tipeLuapanHujan
        )

        spn_tipeLuapanKemarau.adapter = dataAdapter_tipeLuapanKemarau
        spn_tipeLuapanHujan.adapter = dataAdapter_tipeLuapanHujan

        if (survey.getData(id)) {
            val tipeLuapanKemarauAdp = spn_tipeLuapanKemarau.adapter as ArrayAdapter<String>
            val tipeLuapanKemarauPosition = tipeLuapanKemarauAdp.getPosition(survey.tipeLuapanKemarau)
            spn_tipeLuapanKemarau.setSelection(tipeLuapanKemarauPosition)
            val tipeLuapanHujanAdp = spn_tipeLuapanHujan.adapter as ArrayAdapter<String>
            val tipeLuapanHujanPosition = tipeLuapanHujanAdp.getPosition(survey.tipeLuapanHujan)
            spn_tipeLuapanHujan.setSelection(tipeLuapanHujanPosition)

            Glide.with(activity).load(survey.tipeLuapanImg1).into(img_tipeLuapanImg1)
            Glide.with(activity).load(survey.tipeLuapanImg2).into(img_tipeLuapanImg2)
            Glide.with(activity).load(survey.tipeLuapanImg3).into(img_tipeLuapanImg3)
            Glide.with(activity).load(survey.tipeLuapanImg4).into(img_tipeLuapanImg4)
            Glide.with(activity).load(survey.tipeLuapanImg5).into(img_tipeLuapanImg5)
            Glide.with(activity).load(survey.tipeLuapanImg6).into(img_tipeLuapanImg6)

            pathtipeLuapanImg1 = survey.tipeLuapanImg1.toString()
            pathtipeLuapanImg2 = survey.tipeLuapanImg2.toString()
            pathtipeLuapanImg3 = survey.tipeLuapanImg3.toString()
            pathtipeLuapanImg4 = survey.tipeLuapanImg4.toString()
            pathtipeLuapanImg5 = survey.tipeLuapanImg5.toString()
            pathtipeLuapanImg6 = survey.tipeLuapanImg6.toString()
        }

        supportActionBar?.title = "Isi Survey"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayUseLogoEnabled(false)

        initView()
        klik()
    }

    private fun initView() {
        if(pathtipeLuapanImg1!="") Local.isVisible(btn_tipeLuapan1,btn_delete_tipeLuapan1,true)
        else Local.isVisible(btn_tipeLuapan1,btn_delete_tipeLuapan1,false)

        if(pathtipeLuapanImg2!="") Local.isVisible(btn_tipeLuapan2,btn_delete_tipeLuapan2,true)
        else Local.isVisible(btn_tipeLuapan2,btn_delete_tipeLuapan2,false)

        if(pathtipeLuapanImg3!="") Local.isVisible(btn_tipeLuapan3,btn_delete_tipeLuapan3,true)
        else Local.isVisible(btn_tipeLuapan3,btn_delete_tipeLuapan3,false)

        if(pathtipeLuapanImg4!="") Local.isVisible(btn_tipeLuapan4,btn_delete_tipeLuapan4,true)
        else Local.isVisible(btn_tipeLuapan4,btn_delete_tipeLuapan4,false)

        if(pathtipeLuapanImg5!="") Local.isVisible(btn_tipeLuapan5,btn_delete_tipeLuapan5,true)
        else Local.isVisible(btn_tipeLuapan5,btn_delete_tipeLuapan5,false)

        if(pathtipeLuapanImg6!="") Local.isVisible(btn_tipeLuapan6,btn_delete_tipeLuapan6,true)
        else Local.isVisible(btn_tipeLuapan6,btn_delete_tipeLuapan6,false)
    }

    fun klik(){
        btn_simpan.setOnClickListener {
            survey.tipeLuapanKemarau = spn_tipeLuapanKemarau.selectedItem.toString()
            survey.tipeLuapanHujan = spn_tipeLuapanHujan.selectedItem.toString()

            survey.tipeLuapanImg1 = pathtipeLuapanImg1
            survey.tipeLuapanImg2 = pathtipeLuapanImg2
            survey.tipeLuapanImg3 = pathtipeLuapanImg3
            survey.tipeLuapanImg4 = pathtipeLuapanImg4
            survey.tipeLuapanImg5 = pathtipeLuapanImg5
            survey.tipeLuapanImg6 = pathtipeLuapanImg6

            survey.update()
            start<EntryKetebalanGambutActivity> {
                putExtra("id", survey.id?.toInt())
            }

        }

        btn_kembali.setOnClickListener {
            survey.tipeLuapanKemarau = spn_tipeLuapanKemarau.selectedItem.toString()
            survey.tipeLuapanHujan = spn_tipeLuapanHujan.selectedItem.toString()

            survey.tipeLuapanImg1 = pathtipeLuapanImg1
            survey.tipeLuapanImg2 = pathtipeLuapanImg2
            survey.tipeLuapanImg3 = pathtipeLuapanImg3
            survey.tipeLuapanImg4 = pathtipeLuapanImg4
            survey.tipeLuapanImg5 = pathtipeLuapanImg5
            survey.tipeLuapanImg6 = pathtipeLuapanImg6

            survey.update()
            onBackPressed()
        }

        btn_tipeLuapan1.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(149) }
        btn_tipeLuapan2.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(150) }
        btn_tipeLuapan3.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(151) }
        btn_tipeLuapan4.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(152) }
        btn_tipeLuapan5.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(153) }
        btn_tipeLuapan6.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(154) }


        //delete image
        deleteImage(btn_delete_tipeLuapan1,img_tipeLuapanImg1,1)
        deleteImage(btn_delete_tipeLuapan2,img_tipeLuapanImg2,2)
        deleteImage(btn_delete_tipeLuapan3,img_tipeLuapanImg3,3)
        deleteImage(btn_delete_tipeLuapan4,img_tipeLuapanImg4,4)
        deleteImage(btn_delete_tipeLuapan5,img_tipeLuapanImg5,5)
        deleteImage(btn_delete_tipeLuapan6,img_tipeLuapanImg6,6)

        //navigasi
        selectNavigasi(btn_navigasi_1,1)
        selectNavigasi(btn_navigasi_2,2)
        selectNavigasi(btn_navigasi_3,3)
        selectNavigasi(btn_navigasi_4,4)
        selectNavigasi(btn_navigasi_5,5)
        selectNavigasi(btn_navigasi_6,6)
        selectNavigasi(btn_navigasi_7,7)
        selectNavigasi(btn_navigasi_8,8)
        selectNavigasi(btn_navigasi_9,9)
        selectNavigasi(btn_navigasi_10,10)
        selectNavigasi(btn_navigasi_11,11)
        selectNavigasi(btn_navigasi_12,12)
        selectNavigasi(btn_navigasi_13,13)
        selectNavigasi(btn_navigasi_14,14)
        selectNavigasi(btn_navigasi_15,15)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode==149){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_tipeLuapanImg1.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_tipeLuapanImg1)
                survey.tipeLuapanImg1 = filePath
                pathtipeLuapanImg1 = filePath
            }
        }

        if (requestCode==150){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_tipeLuapanImg2.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_tipeLuapanImg2)
                survey.tipeLuapanImg2 = filePath
                pathtipeLuapanImg2 = filePath
            }
        }

        if (requestCode==151){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_tipeLuapanImg3.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_tipeLuapanImg3)
                survey.tipeLuapanImg3 = filePath
                pathtipeLuapanImg3 = filePath
            }
        }

        if (requestCode==152){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_tipeLuapanImg4.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_tipeLuapanImg4)
                survey.tipeLuapanImg4 = filePath
                pathtipeLuapanImg4 = filePath
            }
        }

        if (requestCode==153){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_tipeLuapanImg5.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_tipeLuapanImg5)
                survey.tipeLuapanImg5 = filePath
                pathtipeLuapanImg5 = filePath
            }
        }

        if (requestCode==154){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_tipeLuapanImg6.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_tipeLuapanImg6)
                survey.tipeLuapanImg6 = filePath
                pathtipeLuapanImg6 = filePath
            }
        }

    }

    fun returnData(id:Int){
        if (survey.getData(id)) {

        } else {

        }
    }

    fun selectNavigasi(button: Button, code:Int){
        button.setOnClickListener {
            when(code){
                1 -> navigasiIntent(EntryKordinatActivity::class.java)
                2 -> navigasiIntent(EntryElevasiActivity::class.java)
                3 -> navigasiIntent(EntryGenanganActivity::class.java)
                4 -> navigasiIntent(EntryTutupanLahanActivity::class.java)
                5 -> navigasiIntent(EntryFloraFaunaActivity::class.java)
                6 ->  navigasiIntent(EntryDrainaseActivity::class.java)
                7 -> navigasiIntent(EntryKualitasAirTanahActivity::class.java)
                8 -> navigasiIntent(EntryKarakteristikSubstratumTanahActivity::class.java)
                //9 -> navigasiIntent(EntryTipeLuapanActivity::class.java)
                10 -> navigasiIntent(EntryKetebalanGambutActivity::class.java)
                11 -> navigasiIntent(EntryKarakteristikSubstratumDibawahLapisanGambutActivity::class.java)
                12 -> navigasiIntent(EntryTingkatKerusakanLahanActivity::class.java)
                13 -> navigasiIntent(EntryKebakaranLahanActivity::class.java)
                14 -> navigasiIntent(EntryPorositasKelengasanActivity::class.java)
                15 -> navigasiIntent(EntryDataGambarActivity::class.java)
            }
        }
    }

    private fun navigasiIntent(classtujuan: Class<*>) {
        survey.tipeLuapanKemarau = spn_tipeLuapanKemarau.selectedItem.toString()
        survey.tipeLuapanHujan = spn_tipeLuapanHujan.selectedItem.toString()

        survey.tipeLuapanImg1 = pathtipeLuapanImg1
        survey.tipeLuapanImg2 = pathtipeLuapanImg2
        survey.tipeLuapanImg3 = pathtipeLuapanImg3
        survey.tipeLuapanImg4 = pathtipeLuapanImg4
        survey.tipeLuapanImg5 = pathtipeLuapanImg5
        survey.tipeLuapanImg6 = pathtipeLuapanImg6
        survey.update()
        val intent = Intent(activity,classtujuan)
        intent.putExtra("id", survey.id?.toInt())
        startActivity(intent)

    }

    fun deleteImage(button: ImageButton, imageView: ImageView, code: Int){
        button.setOnClickListener {
            when(code){
                1-> pathtipeLuapanImg1 = ""
                2-> pathtipeLuapanImg2 = ""
                3-> pathtipeLuapanImg3 = ""
                4-> pathtipeLuapanImg4 = ""
                5-> pathtipeLuapanImg5 = ""
                6-> pathtipeLuapanImg6 = ""
            }

            Glide.with(activity).load("").into(imageView)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        if(id != null){
            survey.getData(id)
        }
        super.onResume()
    }
}