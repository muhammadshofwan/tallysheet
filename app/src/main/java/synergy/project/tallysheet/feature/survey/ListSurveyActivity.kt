package synergy.project.tallysheet.feature.survey

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_list_survey.*
import synergy.project.tallysheet.R
import synergy.project.tallysheet.adapter.ListSurveyAdapter
import synergy.project.tallysheet.model.Survey
import synergy.project.tallysheet.utils.Helper

class ListSurveyActivity : AppCompatActivity() {
    val activity = ListSurveyActivity@this
    var data = ArrayList<HashMap<String, String>>()
    lateinit var survey: Survey

    var pesan = ""; var surveyId = ""
    var recordId = ""; var location = ""
    var deleteCode = ""
    var sortColumn = 0
    var sortOrder = 0
    var sortActive = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_survey)

        supportActionBar?.title = "Survey"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayUseLogoEnabled(false)

        survey = Survey(applicationContext)
        data = survey.getSurveyList(0, 0)

        initView()
    }

    private fun initView() {
        recycler_view.layoutManager = LinearLayoutManager(activity)
        if (data.size>0){
            val adapter = ListSurveyAdapter(activity, data)
            recycler_view.adapter = adapter
            txt_info.visibility = View.GONE
            recycler_view.visibility = View.VISIBLE
        }else{
            txt_info.text = getString(R.string.list_survey_no_data)
            txt_info.visibility = View.VISIBLE
            recycler_view.visibility = View.GONE
        }

        //footer
        val connMgr = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo =connMgr.activeNetworkInfo

        if (networkInfo!=null && networkInfo.isConnected){
            Helper.mode = 1
            txt_mode.setText(getString(R.string.login_online_mode))
            footer.setBackgroundResource(R.color.online_mode_text_background)
            img_icon.setImageResource(R.drawable.ic_action_accept)
        }else{

            Helper.mode = 0
            txt_mode.setText(getString(R.string.login_offline_mode))
            footer.setBackgroundResource(R.color.offline_mode_text_background)
            img_icon.setImageResource(R.drawable.ic_action_warning)
        }

    }

    fun dialogSort(){
        var dialogBuilder = AlertDialog.Builder(activity)
        val layoutView = activity.layoutInflater.inflate(R.layout.dialog_sort, null)
        val rl_kode_titik = layoutView.findViewById<RelativeLayout>(R.id.rl_kode_titik)
        val img_kode_titik = layoutView.findViewById<ImageView>(R.id.img_kode_titik)
        val txt_kode_titik = layoutView.findViewById<TextView>(R.id.txt_kode_titik)

        val rl_status = layoutView.findViewById<RelativeLayout>(R.id.rl_status)
        val img_status = layoutView.findViewById<ImageView>(R.id.imgstatus)
        val txt_status = layoutView.findViewById<TextView>(R.id.txt_status)

        var map = HashMap<String, String>()
        if (sortColumn==0){
            map["sort"] = "true"
        }else{
            map["sort"] = "false"
        }
        var map2 = HashMap<String, String>()
        if (sortColumn==1){
            map2["sort"] = "true"
        }else{
            map2["sort"] = "false"
        }
        map2["order"] = sortOrder.toString()

       //

       // map2["order"] = sortOrder.toString()

        if (map["sort"] == "true") {
            txt_kode_titik.setTextColor(activity.resources.getColor(R.color.menu_text_active))
            if (map["order"] == "0") {
                img_kode_titik.setImageResource(R.drawable.ic_sort_asc)
            } else {
                img_kode_titik.setImageResource(R.drawable.ic_sort_desc)
            }
            img_kode_titik.visibility = View.VISIBLE
        } else {
            txt_kode_titik.setTextColor(activity.resources.getColor(R.color.menu_text))
            img_kode_titik.visibility = View.INVISIBLE
        }

        if (map2["sort"] == "true") {
            txt_status.setTextColor(activity.resources.getColor(R.color.menu_text_active))
            if (map2["order"] == "0") {
                img_status.setImageResource(R.drawable.ic_sort_asc)
            } else {
                img_status.setImageResource(R.drawable.ic_sort_desc)
            }
            img_status.visibility = View.VISIBLE
        } else {
            txt_status.setTextColor(activity.resources.getColor(R.color.menu_text))
            img_status.visibility = View.INVISIBLE
        }



        dialogBuilder.setView(layoutView)
        val alertDialog =dialogBuilder.create()
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.WHITE))
        alertDialog.setCancelable(true)
        alertDialog.show()

        rl_kode_titik.setOnClickListener {
            val survey = Survey(applicationContext)
            sort(0)
            data =survey.getSurveyList(sortColumn, sortOrder)
            val adapter = ListSurveyAdapter(activity, data)
            recycler_view.adapter = adapter
            alertDialog.dismiss()
        }

        rl_status.setOnClickListener {
            val survey = Survey(applicationContext)
            sort(1)
            data =survey.getSurveyList(sortColumn, sortOrder)
            val adapter = ListSurveyAdapter(activity, data)
            recycler_view.adapter = adapter
            alertDialog.dismiss()
        }
    }

    fun sort(menu: Int){
        sortColumn = menu
        sortOrder = if (sortActive == menu) {
            if (sortOrder == 0) {
                1
            } else {
                0
            }
        } else {
            0
        }
        sortActive = menu
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_survey, menu)

        val mSearch = menu?.findItem(R.id.action_search)
        val searchView = mSearch?.actionView as SearchView
        searchView.queryHint = "Search"

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                //  mAdapter.getFilter().filter(newText);
                if (newText?.length!! > 0) {
                    val survey = Survey(applicationContext)
                    data = survey.search(newText)
                    val adapter = ListSurveyAdapter(activity, data)
                    recycler_view.adapter = adapter
                } else {
                    val survey = Survey(applicationContext)
                    data = survey.getSurveyList(0, 0)
                    val adapter = ListSurveyAdapter(activity, data)
                    recycler_view.adapter = adapter
                }
                return true
            }
        })


        return super.onCreateOptionsMenu(menu)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            R.id.action_sort -> {
                dialogSort()
                true
            }
            R.id.action_search -> {
                val searchView = item.actionView as SearchView
                searchView.queryHint = "Search"

                searchView.setOnQueryTextFocusChangeListener(object :
                    SearchView.OnQueryTextListener,
                    View.OnFocusChangeListener {
                    override fun onQueryTextSubmit(query: String?): Boolean {
                        return false
                    }

                    override fun onQueryTextChange(newText: String?): Boolean {
                        if (newText?.length!! > 0) {
                            val survey = Survey(applicationContext)
                            data = survey.search(newText)
                            val adapter = ListSurveyAdapter(activity, data)
                            recycler_view.adapter = adapter
                        } else {
                            val survey = Survey(applicationContext)
                            data = survey.getSurveyList(0, 0)
                            val adapter = ListSurveyAdapter(activity, data)
                            recycler_view.adapter = adapter
                        }
                        return true
                    }

                    override fun onFocusChange(v: View?, hasFocus: Boolean) {

                    }

                })

                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}


