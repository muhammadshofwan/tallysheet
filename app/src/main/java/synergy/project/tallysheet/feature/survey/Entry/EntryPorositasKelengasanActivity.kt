package synergy.project.tallysheet.feature.survey.Entry

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import kotlinx.android.synthetic.main.layout_genangan.*
import kotlinx.android.synthetic.main.layout_header.btn_simpan
import kotlinx.android.synthetic.main.layout_koordinat_titik_survey.*
import kotlinx.android.synthetic.main.layout_porositas_kelengasan.*
import kotlinx.android.synthetic.main.layout_porositas_kelengasan.btn_kembali
import kotlinx.android.synthetic.main.layout_porositas_kelengasan.btn_navigasi_1
import kotlinx.android.synthetic.main.layout_porositas_kelengasan.btn_navigasi_10
import kotlinx.android.synthetic.main.layout_porositas_kelengasan.btn_navigasi_11
import kotlinx.android.synthetic.main.layout_porositas_kelengasan.btn_navigasi_12
import kotlinx.android.synthetic.main.layout_porositas_kelengasan.btn_navigasi_13
import kotlinx.android.synthetic.main.layout_porositas_kelengasan.btn_navigasi_14
import kotlinx.android.synthetic.main.layout_porositas_kelengasan.btn_navigasi_15
import kotlinx.android.synthetic.main.layout_porositas_kelengasan.btn_navigasi_2
import kotlinx.android.synthetic.main.layout_porositas_kelengasan.btn_navigasi_3
import kotlinx.android.synthetic.main.layout_porositas_kelengasan.btn_navigasi_4
import kotlinx.android.synthetic.main.layout_porositas_kelengasan.btn_navigasi_5
import kotlinx.android.synthetic.main.layout_porositas_kelengasan.btn_navigasi_6
import kotlinx.android.synthetic.main.layout_porositas_kelengasan.btn_navigasi_7
import kotlinx.android.synthetic.main.layout_porositas_kelengasan.btn_navigasi_8
import kotlinx.android.synthetic.main.layout_porositas_kelengasan.btn_navigasi_9
import splitties.activities.start
import splitties.toast.toast
import synergy.project.tallysheet.R
import synergy.project.tallysheet.feature.main.MainActivity
import synergy.project.tallysheet.model.Survey
import synergy.project.tallysheet.utils.DatePickerFragment
import synergy.project.tallysheet.utils.Helper
import synergy.project.tallysheet.utils.Local
import synergy.project.tallysheet.utils.PrefManager

class  EntryPorositasKelengasanActivity : AppCompatActivity() {
    lateinit var survey: Survey
    lateinit var prefManager: PrefManager
    var id : Int= 0

    var pathporositasKelengasanImg1 = ""
    var pathporositasKelengasanImg2 = ""
    var pathporositasKelengasanImg3 = ""
    var pathporositasKelengasanImg4 = ""
    var pathporositasKelengasanImg5 = ""
    var pathporositasKelengasanImg6 = ""

    val activity = EntryPorositasKelengasanActivity@this


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_porositas_kelengasan)
        val bundle = intent.extras
        if (bundle != null) {
            id = bundle["id"] as Int
        }
        survey = Survey(applicationContext)
        prefManager = PrefManager(applicationContext)

        val dataAdapter_porositasKelengasanNa: ArrayAdapter<String> = ArrayAdapter<String>(
                this@EntryPorositasKelengasanActivity,// ganti jadi this@Nama Activity
                android.R.layout.simple_dropdown_item_1line,
                Helper.porositasKelengasanNa
        )
        spn_porositasKelengasanNa.adapter = dataAdapter_porositasKelengasanNa


        val dataAdapter_porositasKelengasanNaJenis: ArrayAdapter<String> = ArrayAdapter<String>(
                this@EntryPorositasKelengasanActivity,// ganti jadi this@Nama Activity
                android.R.layout.simple_dropdown_item_1line,
                Helper.porositasKelengasanNaJenis
        )
        spn_porositasKelengasanNaJenis.adapter = dataAdapter_porositasKelengasanNaJenis


        // ini untuk menampilkan data
        if (survey.getData(id)) {

            val porositasKelengasanNaAdp = spn_porositasKelengasanNa.adapter as ArrayAdapter<String>
            val porositasKelengasanNaPosition = porositasKelengasanNaAdp.getPosition(survey.porositasKelengasanNa)
            spn_porositasKelengasanNa.setSelection(porositasKelengasanNaPosition)

            val porositasKelengasanNaJenisAdp = spn_porositasKelengasanNaJenis.adapter as ArrayAdapter<String>
            val porositasKelengasanNaJenisPosition = porositasKelengasanNaJenisAdp.getPosition(survey.porositasKelengasanNaJenis)
            spn_porositasKelengasanNaJenis.setSelection(porositasKelengasanNaJenisPosition)
            txt_porositasKelengasan1.setText(survey.porositasKelengasan1)
            txt_porositasKelengasan2.setText(survey.porositasKelengasan2)

            Glide.with(activity).load(survey.porositasKelengasanImg1).into(img_porositasKelengasanImg1)
            Glide.with(activity).load(survey.porositasKelengasanImg2).into(img_porositasKelengasanImg2)
            Glide.with(activity).load(survey.porositasKelengasanImg3).into(img_porositasKelengasanImg3)
            Glide.with(activity).load(survey.porositasKelengasanImg4).into(img_porositasKelengasanImg4)
            Glide.with(activity).load(survey.porositasKelengasanImg5).into(img_porositasKelengasanImg5)
            Glide.with(activity).load(survey.porositasKelengasanImg6).into(img_porositasKelengasanImg6)

            pathporositasKelengasanImg1 = survey.porositasKelengasanImg1.toString()
            pathporositasKelengasanImg2 = survey.porositasKelengasanImg2.toString()
            pathporositasKelengasanImg3 = survey.porositasKelengasanImg3.toString()
            pathporositasKelengasanImg4 = survey.porositasKelengasanImg4.toString()
            pathporositasKelengasanImg5 = survey.porositasKelengasanImg5.toString()
            pathporositasKelengasanImg6 = survey.porositasKelengasanImg6.toString()
        }

        supportActionBar?.title = "Isi Survey"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayUseLogoEnabled(false)

        initView()
        klik()
    }

    private fun initView() {
        if(pathporositasKelengasanImg1!="") Local.isVisible(btn_porositasKelengasan1,btn_delete_porositasKelengasan1,true)
        else Local.isVisible(btn_porositasKelengasan1,btn_delete_porositasKelengasan1,false)

        if(pathporositasKelengasanImg2!="") Local.isVisible(btn_porositasKelengasan2,btn_delete_porositasKelengasan2,true)
        else Local.isVisible(btn_porositasKelengasan2,btn_delete_porositasKelengasan2,false)

        if(pathporositasKelengasanImg3!="") Local.isVisible(btn_porositasKelengasan3,btn_delete_porositasKelengasan3,true)
        else Local.isVisible(btn_porositasKelengasan3,btn_delete_porositasKelengasan3,false)

        if(pathporositasKelengasanImg4!="") Local.isVisible(btn_porositasKelengasan4,btn_delete_porositasKelengasan4,true)
        else Local.isVisible(btn_porositasKelengasan4,btn_delete_porositasKelengasan4,false)

        if(pathporositasKelengasanImg5!="") Local.isVisible(btn_porositasKelengasan5,btn_delete_porositasKelengasan5,true)
        else Local.isVisible(btn_porositasKelengasan5,btn_delete_porositasKelengasan5,false)

        if(pathporositasKelengasanImg6!="") Local.isVisible(btn_porositasKelengasan6,btn_delete_porositasKelengasan6,true)
        else Local.isVisible(btn_porositasKelengasan6,btn_delete_porositasKelengasan6,false)
    }

    fun klik(){
        btn_simpan.setOnClickListener {
            survey.porositasKelengasanNa = spn_porositasKelengasanNa.selectedItem.toString()
            survey.porositasKelengasanNaJenis = spn_porositasKelengasanNaJenis.selectedItem.toString()
            survey.porositasKelengasan1 = txt_porositasKelengasan1.text.toString()
            survey.porositasKelengasan2 = txt_porositasKelengasan2.text.toString()

            survey.porositasKelengasanImg1 = pathporositasKelengasanImg1
            survey.porositasKelengasanImg2 = pathporositasKelengasanImg2
            survey.porositasKelengasanImg3 = pathporositasKelengasanImg3
            survey.porositasKelengasanImg4 = pathporositasKelengasanImg4
            survey.porositasKelengasanImg5 = pathporositasKelengasanImg5
            survey.porositasKelengasanImg6 = pathporositasKelengasanImg6

             survey.status = 1 ;
            survey.update()
            start<EntryDataGambarActivity> {
                putExtra("id", survey.id?.toInt())
            }
        }
        btn_kembali.setOnClickListener {
            survey.porositasKelengasanNa = spn_porositasKelengasanNa.selectedItem.toString()
            survey.porositasKelengasanNaJenis = spn_porositasKelengasanNaJenis.selectedItem.toString()
            survey.porositasKelengasan1 = txt_porositasKelengasan1.text.toString()
            survey.porositasKelengasan2 = txt_porositasKelengasan2.text.toString()

            survey.porositasKelengasanImg1 = pathporositasKelengasanImg1
            survey.porositasKelengasanImg2 = pathporositasKelengasanImg2
            survey.porositasKelengasanImg3 = pathporositasKelengasanImg3
            survey.porositasKelengasanImg4 = pathporositasKelengasanImg4
            survey.porositasKelengasanImg5 = pathporositasKelengasanImg5
            survey.porositasKelengasanImg6 = pathporositasKelengasanImg6

            survey.status = 1 ;
            survey.update()
            onBackPressed()
        }

        btn_porositasKelengasan1.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(179) }
        btn_porositasKelengasan2.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(180) }
        btn_porositasKelengasan3.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(181) }
        btn_porositasKelengasan4.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(182) }
        btn_porositasKelengasan5.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(183) }
        btn_porositasKelengasan6.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(184) }

        //delete image
        deleteImage(btn_delete_porositasKelengasan1,img_porositasKelengasanImg1,1)
        deleteImage(btn_delete_porositasKelengasan2,img_porositasKelengasanImg2,2)
        deleteImage(btn_delete_porositasKelengasan3,img_porositasKelengasanImg3,3)
        deleteImage(btn_delete_porositasKelengasan4,img_porositasKelengasanImg4,4)
        deleteImage(btn_delete_porositasKelengasan5,img_porositasKelengasanImg5,5)
        deleteImage(btn_delete_porositasKelengasan6,img_porositasKelengasanImg6,6)


        //navigasi
        selectNavigasi(btn_navigasi_1,1)
        selectNavigasi(btn_navigasi_2,2)
        selectNavigasi(btn_navigasi_3,3)
        selectNavigasi(btn_navigasi_4,4)
        selectNavigasi(btn_navigasi_5,5)
        selectNavigasi(btn_navigasi_6,6)
        selectNavigasi(btn_navigasi_7,7)
        selectNavigasi(btn_navigasi_8,8)
        selectNavigasi(btn_navigasi_9,9)
        selectNavigasi(btn_navigasi_10,10)
        selectNavigasi(btn_navigasi_11,11)
        selectNavigasi(btn_navigasi_12,12)
        selectNavigasi(btn_navigasi_13,13)
        selectNavigasi(btn_navigasi_14,14)
        selectNavigasi(btn_navigasi_15,15)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode==179){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_porositasKelengasanImg1.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_porositasKelengasanImg1)
                survey.porositasKelengasanImg1 = filePath
                pathporositasKelengasanImg1 = filePath
            }
        }

        if (requestCode==180){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_porositasKelengasanImg2.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_porositasKelengasanImg2)
                survey.porositasKelengasanImg2 = filePath
                pathporositasKelengasanImg2 = filePath
            }
        }

        if (requestCode==181){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_porositasKelengasanImg3.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_porositasKelengasanImg3)
                survey.porositasKelengasanImg3 = filePath
                pathporositasKelengasanImg3 = filePath
            }
        }

        if (requestCode==182){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_porositasKelengasanImg4.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_porositasKelengasanImg4)
                survey.porositasKelengasanImg4 = filePath
                pathporositasKelengasanImg4 = filePath
            }
        }

        if (requestCode==183){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_porositasKelengasanImg5.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_porositasKelengasanImg5)
                survey.porositasKelengasanImg5 = filePath
                pathporositasKelengasanImg5 = filePath
            }
        }

        if (requestCode==184){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_porositasKelengasanImg6.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_porositasKelengasanImg6)
                survey.porositasKelengasanImg6 = filePath
                pathporositasKelengasanImg6 = filePath
            }
        }
    }

    fun returnData(id:Int){
        if (survey.getData(id)) {

        } else {

        }
    }

    fun selectNavigasi(button: Button, code:Int){
        button.setOnClickListener {
            when(code){
                1 -> navigasiIntent(EntryKordinatActivity::class.java)
                2 -> navigasiIntent(EntryElevasiActivity::class.java)
                3 -> navigasiIntent(EntryGenanganActivity::class.java)
                4 -> navigasiIntent(EntryTutupanLahanActivity::class.java)
                5 -> navigasiIntent(EntryFloraFaunaActivity::class.java)
                6 ->  navigasiIntent(EntryDrainaseActivity::class.java)
                7 -> navigasiIntent(EntryKualitasAirTanahActivity::class.java)
                8 -> navigasiIntent(EntryKarakteristikSubstratumTanahActivity::class.java)
                9 -> navigasiIntent(EntryTipeLuapanActivity::class.java)
                10 -> navigasiIntent(EntryKetebalanGambutActivity::class.java)
                11 -> navigasiIntent(EntryKarakteristikSubstratumDibawahLapisanGambutActivity::class.java)
                12 -> navigasiIntent(EntryTingkatKerusakanLahanActivity::class.java)
                13 -> navigasiIntent(EntryKebakaranLahanActivity::class.java)
                //14 -> navigasiIntent(EntryPorositasKelengasanActivity::class.java)
                15 -> navigasiIntent(EntryDataGambarActivity::class.java)
            }
        }
    }

    private fun navigasiIntent(classtujuan: Class<*>) {
        survey.porositasKelengasanNa = spn_porositasKelengasanNa.selectedItem.toString()
        survey.porositasKelengasanNaJenis = spn_porositasKelengasanNaJenis.selectedItem.toString()
        survey.porositasKelengasan1 = txt_porositasKelengasan1.text.toString()
        survey.porositasKelengasan2 = txt_porositasKelengasan2.text.toString()

        survey.porositasKelengasanImg1 = pathporositasKelengasanImg1
        survey.porositasKelengasanImg2 = pathporositasKelengasanImg2
        survey.porositasKelengasanImg3 = pathporositasKelengasanImg3
        survey.porositasKelengasanImg4 = pathporositasKelengasanImg4
        survey.porositasKelengasanImg5 = pathporositasKelengasanImg5
        survey.porositasKelengasanImg6 = pathporositasKelengasanImg6
        survey.status = 1 ;
        survey.update()
        val intent = Intent(activity,classtujuan)
        intent.putExtra("id", survey.id?.toInt())
        startActivity(intent)

    }

    fun deleteImage(button: ImageButton, imageView: ImageView, code: Int){
        button.setOnClickListener {
            when(code){
                1-> pathporositasKelengasanImg1 = ""
                2-> pathporositasKelengasanImg2 = ""
                3-> pathporositasKelengasanImg3 = ""
                4-> pathporositasKelengasanImg4 = ""
                5-> pathporositasKelengasanImg5 = ""
                6-> pathporositasKelengasanImg6 = ""
            }

            Glide.with(activity).load("").into(imageView)
        }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        if(id != null){
            survey.getData(id)
        }
        super.onResume()
    }
}