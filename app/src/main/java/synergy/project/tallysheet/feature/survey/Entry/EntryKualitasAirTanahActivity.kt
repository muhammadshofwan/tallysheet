package synergy.project.tallysheet.feature.survey.Entry

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import kotlinx.android.synthetic.main.layout_genangan.*
import kotlinx.android.synthetic.main.layout_header.btn_simpan
import kotlinx.android.synthetic.main.layout_koordinat_titik_survey.btn_kordinatImg1
import kotlinx.android.synthetic.main.layout_koordinat_titik_survey.btn_kordinatImg2
import kotlinx.android.synthetic.main.layout_koordinat_titik_survey.btn_kordinatImg3
import kotlinx.android.synthetic.main.layout_koordinat_titik_survey.btn_kordinatImg4
import kotlinx.android.synthetic.main.layout_kualitas_air_tanah.*
import kotlinx.android.synthetic.main.layout_kualitas_air_tanah.btn_kembali
import kotlinx.android.synthetic.main.layout_kualitas_air_tanah.btn_navigasi_1
import kotlinx.android.synthetic.main.layout_kualitas_air_tanah.btn_navigasi_10
import kotlinx.android.synthetic.main.layout_kualitas_air_tanah.btn_navigasi_11
import kotlinx.android.synthetic.main.layout_kualitas_air_tanah.btn_navigasi_12
import kotlinx.android.synthetic.main.layout_kualitas_air_tanah.btn_navigasi_13
import kotlinx.android.synthetic.main.layout_kualitas_air_tanah.btn_navigasi_14
import kotlinx.android.synthetic.main.layout_kualitas_air_tanah.btn_navigasi_15
import kotlinx.android.synthetic.main.layout_kualitas_air_tanah.btn_navigasi_2
import kotlinx.android.synthetic.main.layout_kualitas_air_tanah.btn_navigasi_3
import kotlinx.android.synthetic.main.layout_kualitas_air_tanah.btn_navigasi_4
import kotlinx.android.synthetic.main.layout_kualitas_air_tanah.btn_navigasi_5
import kotlinx.android.synthetic.main.layout_kualitas_air_tanah.btn_navigasi_6
import kotlinx.android.synthetic.main.layout_kualitas_air_tanah.btn_navigasi_7
import kotlinx.android.synthetic.main.layout_kualitas_air_tanah.btn_navigasi_8
import kotlinx.android.synthetic.main.layout_kualitas_air_tanah.btn_navigasi_9
import splitties.activities.start
import splitties.toast.toast
import synergy.project.tallysheet.R
import synergy.project.tallysheet.model.Survey
import synergy.project.tallysheet.utils.DatePickerFragment
import synergy.project.tallysheet.utils.Local
import synergy.project.tallysheet.utils.PrefManager

class  EntryKualitasAirTanahActivity : AppCompatActivity() {
    lateinit var survey: Survey
    lateinit var prefManager: PrefManager
    var id : Int= 0

    var pathkualitasiAirTanahImg1 = ""
    var pathkualitasiAirTanahImg2 = ""
    var pathkualitasiAirTanahImg3 = ""
    var pathkualitasiAirTanahImg4 = ""
    var pathkualitasiAirTanahImg5 = ""
    var pathkualitasiAirTanahImg6 = ""

    val activity = EntryKualitasAirTanahActivity@this


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_kualitas_air_tanah)
        val bundle = intent.extras
        if (bundle != null) {
            id = bundle["id"] as Int
        }
        survey = Survey(applicationContext)
        survey.getData(id)
        prefManager = PrefManager(applicationContext)

        if (survey.getData(id)) {
            txt_kualitasAirTanahPh.setText(survey.kualitasAirTanahPh)
            txt_kualitasAirTanahEc.setText(survey.kualitasAirTanahEc)
            txt_kualitasAirTanahTds.setText(survey.kualitasAirTanahTds)

            txt_aitTanah.setText(survey.kualitasAirTanahPhAt)
            txt_saluran.setText(survey.kualitasAirTanahPhAs)
            txt_airTanahEc.setText(survey.kualitasAirTanahEcAt)
            txt_saluranEc.setText(survey.kualitasAirTanahEcAs)
            txt_airTanahTds.setText(survey.kualitasAirTanahTdsAt)
            txt_saluranTds.setText(survey.kualitasAirTanahTdsAs)

            Glide.with(activity).load(survey.kualitasiAirTanahImg1).into(img_kualitasiAirTanahImg1)
            Glide.with(activity).load(survey.kualitasiAirTanahImg2).into(img_kualitasAirTanahImg2)
            Glide.with(activity).load(survey.kualitasiAirTanahImg3).into(img_ketebalanGambutImg3)
            Glide.with(activity).load(survey.kualitasiAirTanahImg4).into(img_kualitasiAirTanahImg4)
            Glide.with(activity).load(survey.kualitasiAirTanahImg5).into(img_kualitasAirTanahImg5)
            Glide.with(activity).load(survey.kualitasiAirTanahImg6).into(img_ketebalanGambutImg6)

            pathkualitasiAirTanahImg1 = survey.kualitasiAirTanahImg1.toString()
            pathkualitasiAirTanahImg2 = survey.kualitasiAirTanahImg2.toString()
            pathkualitasiAirTanahImg3 = survey.kualitasiAirTanahImg3.toString()
            pathkualitasiAirTanahImg4 = survey.kualitasiAirTanahImg4.toString()
            pathkualitasiAirTanahImg5 = survey.kualitasiAirTanahImg5.toString()
            pathkualitasiAirTanahImg6 = survey.kualitasiAirTanahImg6.toString()
        }

        supportActionBar?.title = "Isi Survey"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayUseLogoEnabled(false)

        initView()
        klik()
    }

    private fun initView() {
        if(pathkualitasiAirTanahImg1!="") Local.isVisible(btn_kualitasAirTanahImg1,btn_delete_kualitasAirTanahImg1,true)
        else Local.isVisible(btn_kualitasAirTanahImg1,btn_delete_kualitasAirTanahImg1,false)

        if(pathkualitasiAirTanahImg2!="") Local.isVisible(btn_kualitasAirTanahImg2,btn_delete_kualitasAirTanahImg2,true)
        else Local.isVisible(btn_kualitasAirTanahImg2,btn_delete_kualitasAirTanahImg2,false)

        if(pathkualitasiAirTanahImg3!="") Local.isVisible(btn_kualitasAirTanahImg3,btn_delete_kualitasAirTanahImg3,true)
        else Local.isVisible(btn_kualitasAirTanahImg3,btn_delete_kualitasAirTanahImg3,false)

        if(pathkualitasiAirTanahImg4!="") Local.isVisible(btn_kualitasAirTanahImg4,btn_delete_kualitasAirTanahImg4,true)
        else Local.isVisible(btn_kualitasAirTanahImg4,btn_delete_kualitasAirTanahImg4,false)

        if(pathkualitasiAirTanahImg5!="") Local.isVisible(btn_kualitasAirTanahImg5,btn_delete_kualitasAirTanahImg5,true)
        else Local.isVisible(btn_kualitasAirTanahImg5,btn_delete_kualitasAirTanahImg5,false)

        if(pathkualitasiAirTanahImg6!="") Local.isVisible(btn_kualitasAirTanahImg6,btn_delete_kualitasAirTanahImg6,true)
        else Local.isVisible(btn_kualitasAirTanahImg6,btn_delete_kualitasAirTanahImg6,false)
    }

    fun klik(){
        btn_simpan.setOnClickListener {
            survey.kualitasAirTanahPh = txt_kualitasAirTanahPh.text.toString()
            survey.kualitasAirTanahEc = txt_kualitasAirTanahEc.text.toString()
            survey.kualitasAirTanahTds = txt_kualitasAirTanahTds.text.toString()

            survey.kualitasiAirTanahImg1 = pathkualitasiAirTanahImg1
            survey.kualitasiAirTanahImg2 = pathkualitasiAirTanahImg2
            survey.kualitasiAirTanahImg3 = pathkualitasiAirTanahImg3
            survey.kualitasiAirTanahImg4 = pathkualitasiAirTanahImg4
            survey.kualitasiAirTanahImg5 = pathkualitasiAirTanahImg5
            survey.kualitasiAirTanahImg6 = pathkualitasiAirTanahImg6

            survey.kualitasAirTanahPhAt = txt_aitTanah.text.toString()
            survey.kualitasAirTanahPhAs = txt_saluran.text.toString()
            survey.kualitasAirTanahEcAt = txt_airTanahEc.text.toString()
            survey.kualitasAirTanahEcAs = txt_saluranEc.text.toString()
            survey.kualitasAirTanahTdsAt = txt_airTanahTds.text.toString()
            survey.kualitasAirTanahTdsAs = txt_saluranTds.text.toString()

            survey.update()
            start<EntryKarakteristikSubstratumTanahActivity> {
                putExtra("id", survey.id?.toInt())
            }
        }

        btn_kembali.setOnClickListener {
            survey.kualitasAirTanahPh = txt_kualitasAirTanahPh.text.toString()
            survey.kualitasAirTanahEc = txt_kualitasAirTanahEc.text.toString()
            survey.kualitasAirTanahTds = txt_kualitasAirTanahTds.text.toString()

            survey.kualitasiAirTanahImg1 = pathkualitasiAirTanahImg1
            survey.kualitasiAirTanahImg2 = pathkualitasiAirTanahImg2
            survey.kualitasiAirTanahImg3 = pathkualitasiAirTanahImg3
            survey.kualitasiAirTanahImg4 = pathkualitasiAirTanahImg4
            survey.kualitasiAirTanahImg5 = pathkualitasiAirTanahImg5
            survey.kualitasiAirTanahImg6 = pathkualitasiAirTanahImg6

            survey.kualitasAirTanahPhAt = txt_aitTanah.text.toString()
            survey.kualitasAirTanahPhAs = txt_saluran.text.toString()
            survey.kualitasAirTanahEcAt = txt_airTanahEc.text.toString()
            survey.kualitasAirTanahEcAs = txt_saluranEc.text.toString()
            survey.kualitasAirTanahTdsAt = txt_airTanahTds.text.toString()
            survey.kualitasAirTanahTdsAs = txt_saluranTds.text.toString()

            survey.update()
            onBackPressed()
        }

        btn_kualitasAirTanahImg1.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(137) }
        btn_kualitasAirTanahImg2.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(138) }
        btn_kualitasAirTanahImg3.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(139) }
        btn_kualitasAirTanahImg4.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(140) }
        btn_kualitasAirTanahImg5.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(141) }
        btn_kualitasAirTanahImg6.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(142) }


        //delete image
        deleteImage(btn_delete_kualitasAirTanahImg1,img_kualitasiAirTanahImg1,1)
        deleteImage(btn_delete_kualitasAirTanahImg2,img_kualitasAirTanahImg2,2)
        deleteImage(btn_delete_kualitasAirTanahImg3,img_ketebalanGambutImg3,3)
        deleteImage(btn_delete_kualitasAirTanahImg4,img_kualitasiAirTanahImg4,4)
        deleteImage(btn_delete_kualitasAirTanahImg5,img_kualitasAirTanahImg5,5)
        deleteImage(btn_delete_kualitasAirTanahImg6,img_ketebalanGambutImg6,6)

        //navigasi
        selectNavigasi(btn_navigasi_1,1)
        selectNavigasi(btn_navigasi_2,2)
        selectNavigasi(btn_navigasi_3,3)
        selectNavigasi(btn_navigasi_4,4)
        selectNavigasi(btn_navigasi_5,5)
        selectNavigasi(btn_navigasi_6,6)
        selectNavigasi(btn_navigasi_7,7)
        selectNavigasi(btn_navigasi_8,8)
        selectNavigasi(btn_navigasi_9,9)
        selectNavigasi(btn_navigasi_10,10)
        selectNavigasi(btn_navigasi_11,11)
        selectNavigasi(btn_navigasi_12,12)
        selectNavigasi(btn_navigasi_13,13)
        selectNavigasi(btn_navigasi_14,14)
        selectNavigasi(btn_navigasi_15,15)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode==137){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_kualitasiAirTanahImg1.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_kualitasiAirTanahImg1)
                survey.kualitasiAirTanahImg1 = filePath
                pathkualitasiAirTanahImg1 = filePath
            }
        }

        if (requestCode==138){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_kualitasAirTanahImg2.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_kualitasAirTanahImg2)
                survey.kualitasiAirTanahImg2 = filePath
                pathkualitasiAirTanahImg2 = filePath
            }
        }

        if (requestCode==139){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_ketebalanGambutImg3.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_ketebalanGambutImg3)
                survey.kualitasiAirTanahImg3 = filePath
                pathkualitasiAirTanahImg3 = filePath
            }
        }

        if (requestCode==140){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_kualitasiAirTanahImg4.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_kualitasiAirTanahImg4)
                survey.kualitasiAirTanahImg4 = filePath
                pathkualitasiAirTanahImg4 = filePath
            }
        }

        if (requestCode==141){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_kualitasAirTanahImg5.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_kualitasAirTanahImg5)
                survey.kualitasiAirTanahImg5 = filePath
                pathkualitasiAirTanahImg5 = filePath
            }
        }

        if (requestCode==142){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_ketebalanGambutImg6.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_ketebalanGambutImg6)
                survey.kualitasiAirTanahImg6 = filePath
                pathkualitasiAirTanahImg6 = filePath
            }
        }
    }

    fun returnData(id:Int){
        if (survey.getData(id)) {

        } else {

        }
    }

    fun selectNavigasi(button: Button, code:Int){
        button.setOnClickListener {
            when(code){
                1 -> navigasiIntent(EntryKordinatActivity::class.java)
                2 -> navigasiIntent(EntryElevasiActivity::class.java)
                3 -> navigasiIntent(EntryGenanganActivity::class.java)
                4 -> navigasiIntent(EntryTutupanLahanActivity::class.java)
                5 -> navigasiIntent(EntryFloraFaunaActivity::class.java)
                6 ->  navigasiIntent(EntryDrainaseActivity::class.java)
                //7 -> navigasiIntent(EntryKualitasAirTanahActivity::class.java)
                8 -> navigasiIntent(EntryKarakteristikSubstratumTanahActivity::class.java)
                9 -> navigasiIntent(EntryTipeLuapanActivity::class.java)
                10 -> navigasiIntent(EntryKetebalanGambutActivity::class.java)
                11 -> navigasiIntent(EntryKarakteristikSubstratumDibawahLapisanGambutActivity::class.java)
                12 -> navigasiIntent(EntryTingkatKerusakanLahanActivity::class.java)
                13 -> navigasiIntent(EntryKebakaranLahanActivity::class.java)
                14 -> navigasiIntent(EntryPorositasKelengasanActivity::class.java)
                15 -> navigasiIntent(EntryDataGambarActivity::class.java)
            }
        }
    }

    private fun navigasiIntent(classtujuan: Class<*>) {
        survey.kualitasAirTanahPh = txt_kualitasAirTanahPh.text.toString()
        survey.kualitasAirTanahEc = txt_kualitasAirTanahEc.text.toString()
        survey.kualitasAirTanahTds = txt_kualitasAirTanahTds.text.toString()

        survey.kualitasiAirTanahImg1 = pathkualitasiAirTanahImg1
        survey.kualitasiAirTanahImg2 = pathkualitasiAirTanahImg2
        survey.kualitasiAirTanahImg3 = pathkualitasiAirTanahImg3
        survey.kualitasiAirTanahImg4 = pathkualitasiAirTanahImg4
        survey.kualitasiAirTanahImg5 = pathkualitasiAirTanahImg5
        survey.kualitasiAirTanahImg6 = pathkualitasiAirTanahImg6

        survey.kualitasAirTanahPhAt = txt_aitTanah.text.toString()
        survey.kualitasAirTanahPhAs = txt_saluran.text.toString()
        survey.kualitasAirTanahEcAt = txt_airTanahEc.text.toString()
        survey.kualitasAirTanahEcAs = txt_saluranEc.text.toString()
        survey.kualitasAirTanahTdsAt = txt_airTanahTds.text.toString()
        survey.kualitasAirTanahTdsAs = txt_saluranTds.text.toString()

        survey.update()
        val intent = Intent(activity,classtujuan)
        intent.putExtra("id", survey.id?.toInt())
        startActivity(intent)

    }

    fun deleteImage(button: ImageButton, imageView: ImageView, code: Int){
        button.setOnClickListener {
            when(code){
                1-> pathkualitasiAirTanahImg1 = ""
                2-> pathkualitasiAirTanahImg2 = ""
                3-> pathkualitasiAirTanahImg3 = ""
                4-> pathkualitasiAirTanahImg4 = ""
                5-> pathkualitasiAirTanahImg5 = ""
                6-> pathkualitasiAirTanahImg6 = ""
            }

            Glide.with(activity).load("").into(imageView)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        if(id != null){
            survey.getData(id)
        }
        super.onResume()
    }
}