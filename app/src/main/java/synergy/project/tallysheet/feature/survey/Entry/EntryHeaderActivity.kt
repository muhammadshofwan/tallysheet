package synergy.project.tallysheet.feature.survey.Entry

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.layout_header.*
import splitties.activities.start
import splitties.toast.toast
import synergy.project.tallysheet.R
import synergy.project.tallysheet.model.Survey
import synergy.project.tallysheet.utils.DatePickerFragment
import synergy.project.tallysheet.utils.PrefManager

class  EntryHeaderActivity : AppCompatActivity() {
    val activity = EntryHeaderActivity@this
    lateinit var survey: Survey
    lateinit var prefManager: PrefManager
    var dataNamaKhg = ArrayList<String>()
    //var dataNomorBoring = ArrayList<String>()
    var nomorBoring = ""
    var namaKhg = ""
    var id : Int= 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_header)

        survey = Survey(applicationContext)
        prefManager = PrefManager(applicationContext)

        val  haslmapNamaKhg = survey.getListNamaKhg()
        haslmapNamaKhg.forEach {
            dataNamaKhg.add(it.get("nama_khg")!!)
        }

        val bundle = intent.extras
        if (bundle != null) {
            //survey.nama_khg= bundle["nama_khg"] as String
            //survey.nomorBoring= bundle["nomorBoring"] as String
            id = bundle["id"] as Int
            survey.getData(id)
            returnData(id)
        }

        supportActionBar?.title = "Isi Survey"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayUseLogoEnabled(false)

        txt_namakhg.isEnabled = false
        txt_nomorboring.isEnabled = false
        initView()
        klik()
    }

    private fun initView() {
        //data spinner

        //nama khg
        /**
        val adapterNamaKhg = ArrayAdapter<String>(
            EntrySurveyActivity@ this,
            R.layout.spinner2lines,
            dataNamaKhg
        )
        spn_namakhg.adapter = adapterNamaKhg

        spn_namakhg.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                namaKhg = dataNamaKhg.get(position).toString()
                survey.nama_khg = namaKhg
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }
        **/

        /**
        spn_nomorboring.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                //nomorBoring = dataNomorBoring.get(position).toString()
                survey.nomorBoring = nomorBoring
                returnData(survey.getIdSurvey())
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }
        **/

        /**
         val textWatcherNomorBoring = object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                nomorBoring = s.toString()
                if(nomorBoring!=null && !nomorBoring.equals("")){
                    survey.nomorBoring = nomorBoring
                    returnData(survey.getIdSurvey())
                }
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        }

        txt_nomorboring.addTextChangedListener(
            textWatcherNomorBoring
        )

        if(!survey.nama_khg.equals("")){
            spn_namakhg.setSelection(adapterNamaKhg.getPosition(survey.nama_khg))
            txt_nomorboring.setText(survey.nomorBoring)
            //val adapterNomorBoring = ArrayAdapter<String>(applicationContext,android.R.layout.simple_dropdown_item_1line,dataNomorBoring)
            //spn_nomorboring.setSelection(adapterNomorBoring.getPosition(survey.nomorBoring))
        }
         **/

    }

    fun klik(){
        btn_tanggal.setOnClickListener { pickTanggal(txt_tanggal)}
        btn_simpan.setOnClickListener {

                survey.getData(id)
                survey.tanggal = txt_tanggal.text.toString()
                survey.surveyorId = txt_surveyorId.text.toString()
                survey.kecamatan = txt_kecamatan.text.toString()
                survey.kelurahan = txt_kelurahan.text.toString()
                survey.titik_p10 = txt_titik_p10.text.toString()
                survey.sample_p10 = txt_sample_p10.text.toString()
                survey.dusun = txt_dusun.text.toString()
                survey.status = 1
                survey.update()
                start<EntryKordinatActivity> {
                    putExtra("id", id)
                }

        }

        //navigasi
       /** selectNavigasi(btn_navigasi_1, 1)
        selectNavigasi(btn_navigasi_2, 2)
        selectNavigasi(btn_navigasi_3, 3)
        selectNavigasi(btn_navigasi_4, 4)
        selectNavigasi(btn_navigasi_5, 5)
        selectNavigasi(btn_navigasi_6, 6)
        selectNavigasi(btn_navigasi_7, 7)
        selectNavigasi(btn_navigasi_8, 8)
        selectNavigasi(btn_navigasi_9, 9)
        selectNavigasi(btn_navigasi_10, 10)
        selectNavigasi(btn_navigasi_11, 11)
        selectNavigasi(btn_navigasi_12, 12)
        selectNavigasi(btn_navigasi_13, 13)
        selectNavigasi(btn_navigasi_14, 14)
        selectNavigasi(btn_navigasi_15, 15) **/

    }


    fun pickTanggal(editText: EditText){
        val newFragment = DatePickerFragment(editText)
        newFragment.show(fragmentManager, "")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

    }

    fun returnData(id: Int){
        if (survey.getData(id)) {
            txt_namakhg.setText(survey.nama_khg)
            txt_nomorboring.setText(survey.nomorBoring)
            txt_tanggal.setText(survey.tanggal)
            txt_surveyorId.setText(survey.surveyorId)
            txt_kecamatan.setText(survey.kecamatan)
            txt_kelurahan.setText(survey.kelurahan)
            txt_titik_p10.setText(survey.titik_p10)
            txt_sample_p10.setText(survey.sample_p10)
            this.id = id
            //spiner
            /**
            val drainaseAlamiAdp = spn_namakhg.adapter as ArrayAdapter<String>
            val drainaseAlamiPosition = drainaseAlamiAdp.getPosition(survey.nama_khg)
            spn_namakhg.setSelection(drainaseAlamiPosition)
            **/
        } else {
           // toast("Nomor Boring tidak ditemukan")
            //spn_namakhg.setSelection(0)
            txt_namakhg.setText("")
            txt_nomorboring.setText("")
            txt_tanggal.setText("")
            txt_surveyorId.setText("")
            txt_kecamatan.setText("")
            txt_kelurahan.setText("")
            txt_titik_p10.setText("")
            txt_sample_p10.setText("")
        }
    }

    fun selectNavigasi(button: Button, code: Int){
        button.setOnClickListener {
            if (nomorBoring==null || nomorBoring==""){
                toast("Masukan Nomor Boring terlebih dahulu")
            }else{
                when(code){
                    //1 -> toast("Anda sedang berada di halaman $code")
                    2 -> navigasiIntent(EntryKordinatActivity::class.java)
                    3 -> navigasiIntent(EntryElevasiActivity::class.java)
                    4 -> navigasiIntent(EntryGenanganActivity::class.java)
                    5 -> navigasiIntent(EntryTutupanLahanActivity::class.java)
                    6 -> navigasiIntent(EntryFloraFaunaActivity::class.java)
                    7 -> navigasiIntent(EntryDrainaseActivity::class.java)
                    8 -> navigasiIntent(EntryKualitasAirTanahActivity::class.java)
                    9 -> navigasiIntent(EntryKarakteristikSubstratumTanahActivity::class.java)
                    10 -> navigasiIntent(EntryTipeLuapanActivity::class.java)
                    11 -> navigasiIntent(EntryKetebalanGambutActivity::class.java)
                    12 -> navigasiIntent(EntryKarakteristikSubstratumDibawahLapisanGambutActivity::class.java)
                    13 -> navigasiIntent(EntryTingkatKerusakanLahanActivity::class.java)
                    14 -> navigasiIntent(EntryKebakaranLahanActivity::class.java)
                    15 -> navigasiIntent(EntryPorositasKelengasanActivity::class.java)

                }
            }

        }
    }

    private fun navigasiIntent(classtujuan: Class<*>) {
        survey.getData(id)
        survey.tanggal = txt_tanggal.text.toString()
        survey.surveyorId = txt_surveyorId.text.toString()
        survey.kecamatan = txt_kecamatan.text.toString()
        survey.kelurahan = txt_kelurahan.text.toString()
        survey.titik_p10 = txt_titik_p10.text.toString()
        survey.sample_p10 = txt_sample_p10.text.toString()
        survey.status = 1
        survey.update()
        val intent = Intent(activity,classtujuan)
        intent.putExtra("id", survey.id?.toInt())
        startActivity(intent)

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


}