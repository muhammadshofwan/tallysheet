package synergy.project.tallysheet.feature.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import splitties.toast.toast
import synergy.project.tallysheet.R
import synergy.project.tallysheet.adapter.MainMenuAdapter
import synergy.project.tallysheet.model.MainMenu

class MainActivity : AppCompatActivity() {
    lateinit var viewModel: MainViewModel
    val activity = MainActivity@this

    var menuList = ArrayList<MainMenu>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel = ViewModelProvider(this)[MainViewModel::class.java]
        viewModel.setData(activity,MainActivity@this)

        initView()


    }

     fun initView() {
        menuList.clear()
         txt_nama.text = "Selamat Datang ${viewModel.prefManager.nama}"
        val countUnsync = viewModel.survey.countUnsync()
        val countData = viewModel.survey.countData()
        val countUpload = viewModel.survey.countUpload()

        recycler_view.layoutManager = LinearLayoutManager(activity)
        menuList.add(MainMenu("Ambil Data Plot dari Server","Hanya lakukan di awal sebelum mulai survey", R.drawable.ic_download))
        menuList.add(MainMenu("Isi Survey","Masukkan nomor plot titik lalu lengkapi datanya.", R.drawable.ic_data))
        menuList.add(MainMenu("Lihat Semua Data Titik Survey $countUpload/$countData",
            "$countUpload data survey telah berhasil masuk server dari total $countData titik plot", R.drawable.ic_list))
        menuList.add(MainMenu("Kirim Data ke Server ($countUnsync)",
            "Ada $countUnsync data survey yang perlu segera dikirim. Pastikan datanya sudah lengkap!", R.drawable.ic_upload))
        menuList.add(MainMenu("Unduh Update Aplikasi","Download aplikasi versi terbaru, lalu install.", R.drawable.ic_update))
        menuList.add(MainMenu("Logout Aplikasi","", R.drawable.ic_logout))

        val adapter = MainMenuAdapter(activity,menuList,viewModel)
        recycler_view.adapter = adapter


    }

    override fun onResume() {
        super.onResume()
        initView()
    }
}