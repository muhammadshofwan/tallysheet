package synergy.project.tallysheet.feature.survey.Entry

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import kotlinx.android.synthetic.main.layout_elevasi_lahan.*
import kotlinx.android.synthetic.main.layout_flora_fauna.*
import kotlinx.android.synthetic.main.layout_flora_fauna.btn_kembali
import kotlinx.android.synthetic.main.layout_flora_fauna.btn_navigasi_1
import kotlinx.android.synthetic.main.layout_flora_fauna.btn_navigasi_10
import kotlinx.android.synthetic.main.layout_flora_fauna.btn_navigasi_11
import kotlinx.android.synthetic.main.layout_flora_fauna.btn_navigasi_12
import kotlinx.android.synthetic.main.layout_flora_fauna.btn_navigasi_13
import kotlinx.android.synthetic.main.layout_flora_fauna.btn_navigasi_14
import kotlinx.android.synthetic.main.layout_flora_fauna.btn_navigasi_15
import kotlinx.android.synthetic.main.layout_flora_fauna.btn_navigasi_2
import kotlinx.android.synthetic.main.layout_flora_fauna.btn_navigasi_3
import kotlinx.android.synthetic.main.layout_flora_fauna.btn_navigasi_4
import kotlinx.android.synthetic.main.layout_flora_fauna.btn_navigasi_5
import kotlinx.android.synthetic.main.layout_flora_fauna.btn_navigasi_6
import kotlinx.android.synthetic.main.layout_flora_fauna.btn_navigasi_7
import kotlinx.android.synthetic.main.layout_flora_fauna.btn_navigasi_8
import kotlinx.android.synthetic.main.layout_flora_fauna.btn_navigasi_9
import kotlinx.android.synthetic.main.layout_header.btn_simpan
import splitties.activities.start
import splitties.toast.toast
import synergy.project.tallysheet.R
import synergy.project.tallysheet.model.Survey
import synergy.project.tallysheet.utils.DatePickerFragment
import synergy.project.tallysheet.utils.Helper
import synergy.project.tallysheet.utils.Local
import synergy.project.tallysheet.utils.PrefManager

class  EntryFloraFaunaActivity : AppCompatActivity() {
    lateinit var survey: Survey
    lateinit var prefManager: PrefManager
    var id : Int= 0

    var pathfloraFaunaImg1 = ""
    var pathfloraFaunaImg2 = ""
    var pathfloraFaunaImg3 = ""
    var pathfloraFaunaImg4 = ""
    var pathfloraFaunaImg5 = ""
    var pathfloraFaunaImg6 = ""

    val activity = EntryFloraFaunaActivityActivity@this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_flora_fauna)
        val bundle = intent.extras
        if (bundle != null) {
            id = bundle["id"] as Int
        }
        survey = Survey(applicationContext)
        survey.getData(id)
        prefManager = PrefManager(applicationContext)

        // ini untuk ini value spinner
        val dataAdapter_fauna: ArrayAdapter<String> = ArrayAdapter<String>(
            this@EntryFloraFaunaActivity,// ganti jadi this@Nama Activity
            android.R.layout.simple_dropdown_item_1line,
            Helper.fauna
        )

        val dataAdapter_flora: ArrayAdapter<String> = ArrayAdapter<String>(
            this@EntryFloraFaunaActivity,// ganti jadi this@Nama Activity
            android.R.layout.simple_dropdown_item_1line,
            Helper.flora
        )

        spn_flora.adapter = dataAdapter_flora
        spn_fauna.adapter = dataAdapter_fauna

        if (survey.getData(id)) {
            val floraAdp = spn_flora.adapter as ArrayAdapter<String>
            val floraPosition = floraAdp.getPosition(survey.flora)
            spn_flora.setSelection(floraPosition)
            txt_floraJenis.setText(survey.floraJenis)
            val faunaAdp = spn_fauna.adapter as ArrayAdapter<String>
            val faunaPosition = faunaAdp.getPosition(survey.fauna)
            spn_fauna.setSelection(faunaPosition)
            txt_faunaJenis.setText(survey.faunaJenis)

            Glide.with(activity).load(survey.floraFaunaImg1).into(img_floraFaunaImg1)
            Glide.with(activity).load(survey.floraFaunaImg2).into(img_floraFaunaImg2)
            Glide.with(activity).load(survey.floraFaunaImg3).into(img_floraFaunaImg3)
            Glide.with(activity).load(survey.floraFaunaImg4).into(img_floraFaunaImg4)
            Glide.with(activity).load(survey.floraFaunaImg5).into(img_floraFaunaImg5)
            Glide.with(activity).load(survey.floraFaunaImg6).into(img_floraFaunaImg6)

            pathfloraFaunaImg1 = survey.floraFaunaImg1.toString()
            pathfloraFaunaImg2 = survey.floraFaunaImg2.toString()
            pathfloraFaunaImg3 = survey.floraFaunaImg3.toString()
            pathfloraFaunaImg4 = survey.floraFaunaImg4.toString()
            pathfloraFaunaImg5 = survey.floraFaunaImg5.toString()
            pathfloraFaunaImg6 = survey.floraFaunaImg6.toString()
        }

        supportActionBar?.title = "Isi Survey"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayUseLogoEnabled(false)

        initView()
        klik()
    }

    private fun initView() {
        if(pathfloraFaunaImg1!="") Local.isVisible(btn_floraFaunaImg1,btn_delete_floraFaunaImg1,true)
        else Local.isVisible(btn_floraFaunaImg1,btn_delete_floraFaunaImg1,false)

        if(pathfloraFaunaImg2!="") Local.isVisible(btn_floraFaunaImg2,btn_delete_floraFaunaImg2,true)
        else Local.isVisible(btn_floraFaunaImg2,btn_delete_floraFaunaImg2,false)

        if(pathfloraFaunaImg3!="") Local.isVisible(btn_floraFaunaImg3,btn_delete_floraFaunaImg3,true)
        else Local.isVisible(btn_floraFaunaImg3,btn_delete_floraFaunaImg3,false)

        if(pathfloraFaunaImg4!="") Local.isVisible(btn_floraFaunaImg4,btn_delete_floraFaunaImg4,true)
        else Local.isVisible(btn_floraFaunaImg4,btn_delete_floraFaunaImg4,false)

        if(pathfloraFaunaImg5!="") Local.isVisible(btn_floraFaunaImg5,btn_delete_floraFaunaImg5,true)
        else Local.isVisible(btn_floraFaunaImg5,btn_delete_floraFaunaImg5,false)

        if(pathfloraFaunaImg6!="") Local.isVisible(btn_floraFaunaImg6,btn_delete_floraFaunaImg6,true)
        else Local.isVisible(btn_floraFaunaImg6,btn_delete_floraFaunaImg6,false)
    }

    fun klik(){
        btn_simpan.setOnClickListener {
            survey.flora = spn_flora.selectedItem.toString()
            survey.floraJenis = txt_floraJenis.text.toString()
            survey.fauna = spn_fauna.selectedItem.toString()
            survey.faunaJenis = txt_faunaJenis.text.toString()

            survey.floraFaunaImg1 = pathfloraFaunaImg1
            survey.floraFaunaImg2 = pathfloraFaunaImg2
            survey.floraFaunaImg3 = pathfloraFaunaImg3
            survey.floraFaunaImg4 = pathfloraFaunaImg4
            survey.floraFaunaImg5 = pathfloraFaunaImg5
            survey.floraFaunaImg6 = pathfloraFaunaImg6

            survey.update()
            start<EntryDrainaseActivity> {
                putExtra("id", survey.id?.toInt())
            }

        }

        btn_kembali.setOnClickListener {
            survey.flora = spn_flora.selectedItem.toString()
            survey.floraJenis = txt_floraJenis.text.toString()
            survey.fauna = spn_fauna.selectedItem.toString()
            survey.faunaJenis = txt_faunaJenis.text.toString()

            survey.floraFaunaImg1 = pathfloraFaunaImg1
            survey.floraFaunaImg2 = pathfloraFaunaImg2
            survey.floraFaunaImg3 = pathfloraFaunaImg3
            survey.floraFaunaImg4 = pathfloraFaunaImg4
            survey.floraFaunaImg5 = pathfloraFaunaImg5
            survey.floraFaunaImg6 = pathfloraFaunaImg6

            survey.update()
            onBackPressed()
        }

        btn_floraFaunaImg1.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(125) }
        btn_floraFaunaImg2.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(126) }
        btn_floraFaunaImg3.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(127) }
        btn_floraFaunaImg4.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(128) }
        btn_floraFaunaImg5.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(129) }
        btn_floraFaunaImg6.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(130) }

        //delete image
        deleteImage(btn_delete_floraFaunaImg1,img_floraFaunaImg1,1)
        deleteImage(btn_delete_floraFaunaImg2,img_floraFaunaImg2,2)
        deleteImage(btn_delete_floraFaunaImg3,img_floraFaunaImg3,3)
        deleteImage(btn_delete_floraFaunaImg4,img_floraFaunaImg4,4)
        deleteImage(btn_delete_floraFaunaImg5,img_floraFaunaImg5,5)
        deleteImage(btn_delete_floraFaunaImg6,img_floraFaunaImg6,6)


        //navigasi
        selectNavigasi(btn_navigasi_1,1)
        selectNavigasi(btn_navigasi_2,2)
        selectNavigasi(btn_navigasi_3,3)
        selectNavigasi(btn_navigasi_4,4)
        selectNavigasi(btn_navigasi_5,5)
        selectNavigasi(btn_navigasi_6,6)
        selectNavigasi(btn_navigasi_7,7)
        selectNavigasi(btn_navigasi_8,8)
        selectNavigasi(btn_navigasi_9,9)
        selectNavigasi(btn_navigasi_10,10)
        selectNavigasi(btn_navigasi_11,11)
        selectNavigasi(btn_navigasi_12,12)
        selectNavigasi(btn_navigasi_13,13)
        selectNavigasi(btn_navigasi_14,14)
        selectNavigasi(btn_navigasi_15,15)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode==125){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_floraFaunaImg1.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_floraFaunaImg1)
                survey.floraFaunaImg1 = filePath
                pathfloraFaunaImg1 = filePath
            }
        }

        if (requestCode==126){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_floraFaunaImg2.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_floraFaunaImg2)
                survey.floraFaunaImg2 = filePath
                pathfloraFaunaImg2 = filePath
            }
        }

        if (requestCode==127){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_floraFaunaImg3.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_floraFaunaImg3)
                survey.floraFaunaImg3 = filePath
                pathfloraFaunaImg3 = filePath
            }
        }

        if (requestCode==128){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_floraFaunaImg4.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_floraFaunaImg4)
                survey.floraFaunaImg4 = filePath
                pathfloraFaunaImg4 = filePath
            }
        }

        if (requestCode==129){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_floraFaunaImg5.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_floraFaunaImg5)
                survey.floraFaunaImg5 = filePath
                pathfloraFaunaImg5 = filePath
            }
        }

        if (requestCode==130){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_floraFaunaImg6.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_floraFaunaImg6)
                survey.floraFaunaImg6 = filePath
                pathfloraFaunaImg6 = filePath
            }
        }

    }

    fun returnData(id:Int){
        if (survey.getData(id)) {

        } else {

        }
    }

    fun selectNavigasi(button: Button, code:Int){
        button.setOnClickListener {
            when(code){
                1 -> navigasiIntent(EntryKordinatActivity::class.java)
                2 -> navigasiIntent(EntryElevasiActivity::class.java)
                3 -> navigasiIntent(EntryGenanganActivity::class.java)
                4 -> navigasiIntent(EntryTutupanLahanActivity::class.java)
                //5 -> navigasiIntent(EntryFloraFaunaActivity::class.java)
                6 ->  navigasiIntent(EntryDrainaseActivity::class.java)
                7 -> navigasiIntent(EntryKualitasAirTanahActivity::class.java)
                8 -> navigasiIntent(EntryKarakteristikSubstratumTanahActivity::class.java)
                9 -> navigasiIntent(EntryTipeLuapanActivity::class.java)
                10 -> navigasiIntent(EntryKetebalanGambutActivity::class.java)
                11 -> navigasiIntent(EntryKarakteristikSubstratumDibawahLapisanGambutActivity::class.java)
                12 -> navigasiIntent(EntryTingkatKerusakanLahanActivity::class.java)
                13 -> navigasiIntent(EntryKebakaranLahanActivity::class.java)
                14 -> navigasiIntent(EntryPorositasKelengasanActivity::class.java)
                15 -> navigasiIntent(EntryDataGambarActivity::class.java)

            }
        }
    }

    fun deleteImage(button: ImageButton, imageView: ImageView, code: Int){
        button.setOnClickListener {
            when(code){
                1-> pathfloraFaunaImg1 = ""
                2-> pathfloraFaunaImg2 = ""
                3-> pathfloraFaunaImg3 = ""
                4-> pathfloraFaunaImg4 = ""
                5-> pathfloraFaunaImg5 = ""
                6-> pathfloraFaunaImg6 = ""
            }

            Glide.with(activity).load("").into(imageView)
        }
    }

    private fun navigasiIntent(classtujuan: Class<*>) {
        survey.flora = spn_flora.selectedItem.toString()
        survey.floraJenis = txt_floraJenis.text.toString()
        survey.fauna = spn_fauna.selectedItem.toString()
        survey.faunaJenis = txt_faunaJenis.text.toString()
        survey.floraFaunaImg1 = pathfloraFaunaImg1
        survey.floraFaunaImg2 = pathfloraFaunaImg2
        survey.floraFaunaImg3 = pathfloraFaunaImg3
        survey.floraFaunaImg4 = pathfloraFaunaImg4
        survey.floraFaunaImg5 = pathfloraFaunaImg5
        survey.floraFaunaImg6 = pathfloraFaunaImg6
        survey.update()
        val intent = Intent(activity,classtujuan)
        intent.putExtra("id", survey.id?.toInt())
        startActivity(intent)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        if(id != null){
            survey.getData(id)
        }
        super.onResume()
    }
}