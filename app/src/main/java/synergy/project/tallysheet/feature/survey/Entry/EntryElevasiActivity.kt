package synergy.project.tallysheet.feature.survey.Entry

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import kotlinx.android.synthetic.main.layout_drainase.*
import kotlinx.android.synthetic.main.layout_elevasi_lahan.*
import kotlinx.android.synthetic.main.layout_elevasi_lahan.btn_kembali
import kotlinx.android.synthetic.main.layout_elevasi_lahan.btn_navigasi_1
import kotlinx.android.synthetic.main.layout_elevasi_lahan.btn_navigasi_10
import kotlinx.android.synthetic.main.layout_elevasi_lahan.btn_navigasi_11
import kotlinx.android.synthetic.main.layout_elevasi_lahan.btn_navigasi_12
import kotlinx.android.synthetic.main.layout_elevasi_lahan.btn_navigasi_13
import kotlinx.android.synthetic.main.layout_elevasi_lahan.btn_navigasi_14
import kotlinx.android.synthetic.main.layout_elevasi_lahan.btn_navigasi_15
import kotlinx.android.synthetic.main.layout_elevasi_lahan.btn_navigasi_2
import kotlinx.android.synthetic.main.layout_elevasi_lahan.btn_navigasi_3
import kotlinx.android.synthetic.main.layout_elevasi_lahan.btn_navigasi_4
import kotlinx.android.synthetic.main.layout_elevasi_lahan.btn_navigasi_5
import kotlinx.android.synthetic.main.layout_elevasi_lahan.btn_navigasi_6
import kotlinx.android.synthetic.main.layout_elevasi_lahan.btn_navigasi_7
import kotlinx.android.synthetic.main.layout_elevasi_lahan.btn_navigasi_8
import kotlinx.android.synthetic.main.layout_elevasi_lahan.btn_navigasi_9
import kotlinx.android.synthetic.main.layout_header.btn_simpan
import kotlinx.android.synthetic.main.layout_koordinat_titik_survey.*
import splitties.activities.start
import splitties.toast.toast
import synergy.project.tallysheet.R
import synergy.project.tallysheet.model.Survey
import synergy.project.tallysheet.utils.DatePickerFragment
import synergy.project.tallysheet.utils.Local
import synergy.project.tallysheet.utils.PrefManager

class  EntryElevasiActivity : AppCompatActivity() {
    lateinit var survey: Survey
    lateinit var prefManager: PrefManager
    var id : Int= 0

    var pathelevasiImg1 = ""
    var pathelevasiImg2 = ""
    var pathelevasiImg3 = ""
    var pathelevasiImg4 = ""
    var pathelevasiImg5 = ""
    var pathelevasiImg6 = ""

    val activity = EntryElevasiActivity@this


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_elevasi_lahan)
        val bundle = intent.extras
        if (bundle != null) {
            id = bundle["id"] as Int
        }
        survey = Survey(applicationContext)
        survey.getData(id)
        prefManager = PrefManager(applicationContext)

        if (survey.getData(id)) {
            txt_elevasi.setText(survey.elevasi)
            Glide.with(activity).load(survey.elevasiImg1).into(img_elevasiImg1)
            Glide.with(activity).load(survey.elevasiImg2).into(img_elevasiImg2)
            Glide.with(activity).load(survey.elevasiImg3).into(img_elevasiImg3)
            Glide.with(activity).load(survey.elevasiImg4).into(img_elevasiImg4)
            Glide.with(activity).load(survey.elevasiImg5).into(img_elevasiImg5)
            Glide.with(activity).load(survey.elevasiImg6).into(img_elevasiImg6)

            pathelevasiImg1 = survey.elevasiImg1.toString()
            pathelevasiImg2 = survey.elevasiImg2.toString()
            pathelevasiImg3 = survey.elevasiImg3.toString()
            pathelevasiImg4 = survey.elevasiImg4.toString()
            pathelevasiImg5 = survey.elevasiImg5.toString()
            pathelevasiImg6 = survey.elevasiImg6.toString()
        }

        supportActionBar?.title = "Isi Survey"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayUseLogoEnabled(false)

        initView()
        klik()
    }

    private fun initView() {
        if(pathelevasiImg1!="") Local.isVisible(btn_elevasiImg1,btn_delete_elevasiImg1,true)
        else Local.isVisible(btn_elevasiImg1,btn_delete_elevasiImg1,false)

        if(pathelevasiImg2!="") Local.isVisible(btn_elevasiImg2,btn_delete_elevasiImg2,true)
        else Local.isVisible(btn_elevasiImg2,btn_delete_elevasiImg2,false)

        if(pathelevasiImg3!="") Local.isVisible(btn_elevasiImg3,btn_delete_elevasiImg3,true)
        else Local.isVisible(btn_elevasiImg3,btn_delete_elevasiImg3,false)

        if(pathelevasiImg4!="") Local.isVisible(btn_elevasiImg4,btn_delete_elevasiImg4,true)
        else Local.isVisible(btn_elevasiImg4,btn_delete_elevasiImg4,false)

        if(pathelevasiImg5!="") Local.isVisible(btn_elevasiImg5,btn_delete_elevasiImg5,true)
        else Local.isVisible(btn_elevasiImg5,btn_delete_elevasiImg5,false)

        if(pathelevasiImg6!="") Local.isVisible(btn_elevasiImg6,btn_delete_elevasiImg6,true)
        else Local.isVisible(btn_elevasiImg6,btn_delete_elevasiImg6,false)
    }

    fun klik(){
        btn_simpan.setOnClickListener {
            survey.elevasi = txt_elevasi.text.toString()

            survey.elevasiImg1 = pathelevasiImg1
            survey.elevasiImg2 = pathelevasiImg2
            survey.elevasiImg3 = pathelevasiImg3
            survey.elevasiImg4 = pathelevasiImg4
            survey.elevasiImg5 = pathelevasiImg5
            survey.elevasiImg6 = pathelevasiImg6

            survey.update()
            start<EntryGenanganActivity> {
                putExtra("id", survey.id?.toInt())
            }

        }

        btn_kembali.setOnClickListener {
            survey.elevasi = txt_elevasi.text.toString()

            survey.elevasiImg1 = pathelevasiImg1
            survey.elevasiImg2 = pathelevasiImg2
            survey.elevasiImg3 = pathelevasiImg3
            survey.elevasiImg4 = pathelevasiImg4
            survey.elevasiImg5 = pathelevasiImg5
            survey.elevasiImg6 = pathelevasiImg6

            survey.update()
            onBackPressed()
        }

        btn_elevasiImg1.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(107) }
        btn_elevasiImg2.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(108) }
        btn_elevasiImg3.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(109) }
        btn_elevasiImg4.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(110) }
        btn_elevasiImg5.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(111) }
        btn_elevasiImg6.setOnClickListener { ImagePicker.with(activity).cropSquare().maxResultSize(512,512).compress(512).start(112) }

        //delete image
        deleteImage(btn_delete_elevasiImg1,img_elevasiImg1,1)
        deleteImage(btn_delete_elevasiImg2,img_elevasiImg2,2)
        deleteImage(btn_delete_elevasiImg3,img_elevasiImg3,3)
        deleteImage(btn_delete_elevasiImg4,img_elevasiImg4,4)
        deleteImage(btn_delete_elevasiImg5,img_elevasiImg5,5)
        deleteImage(btn_delete_elevasiImg6,img_elevasiImg6,6)


        //navigasi
        selectNavigasi(btn_navigasi_1,1)
        selectNavigasi(btn_navigasi_2,2)
        selectNavigasi(btn_navigasi_3,3)
        selectNavigasi(btn_navigasi_4,4)
        selectNavigasi(btn_navigasi_5,5)
        selectNavigasi(btn_navigasi_6,6)
        selectNavigasi(btn_navigasi_7,7)
        selectNavigasi(btn_navigasi_8,8)
        selectNavigasi(btn_navigasi_9,9)
        selectNavigasi(btn_navigasi_10,10)
        selectNavigasi(btn_navigasi_11,11)
        selectNavigasi(btn_navigasi_12,12)
        selectNavigasi(btn_navigasi_13,13)
        selectNavigasi(btn_navigasi_14,14)
        selectNavigasi(btn_navigasi_15,15)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode==107){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_elevasiImg1.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_elevasiImg1)
                survey.elevasiImg1 = filePath
                pathelevasiImg1 = filePath
            }
        }

        if (requestCode==108){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_elevasiImg2.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_elevasiImg2)
                survey.elevasiImg2 = filePath
                pathelevasiImg1 = filePath
            }
        }

        if (requestCode==109){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_elevasiImg3.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_elevasiImg3)
                survey.elevasiImg3 = filePath
                pathelevasiImg2 = filePath
            }
        }

        if (requestCode==110){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_elevasiImg4.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_elevasiImg4)
                survey.elevasiImg4 = filePath
                pathelevasiImg3 = filePath
            }
        }

        if (requestCode==111){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_elevasiImg5.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_elevasiImg5)
                survey.elevasiImg5 = filePath
                pathelevasiImg5 = filePath
            }
        }

        if (requestCode==112){
            val uri = data?.data
            val filePath = ImagePicker.getFilePath(data).toString()
            if(filePath != null && !ImagePicker.getFilePath(data).toString().equals("null")){

                img_elevasiImg6.visibility = View.VISIBLE
                Glide.with(activity).load(filePath).into(img_elevasiImg6)
                survey.elevasiImg6 = filePath
                pathelevasiImg6 = filePath
            }
        }

    }

    fun returnData(id:Int){
        if (survey.getData(id)) {

        } else {

        }
    }

    fun selectNavigasi(button: Button, code:Int){
        button.setOnClickListener {
            when(code){
                1 -> navigasiIntent(EntryKordinatActivity::class.java)
                //2 -> navigasiIntent(EntryElevasiActivity::class.java)
                3 -> navigasiIntent(EntryGenanganActivity::class.java)
                4 -> navigasiIntent(EntryTutupanLahanActivity::class.java)
                5 -> navigasiIntent(EntryFloraFaunaActivity::class.java)
                6 ->  navigasiIntent(EntryDrainaseActivity::class.java)
                7 -> navigasiIntent(EntryKualitasAirTanahActivity::class.java)
                8 -> navigasiIntent(EntryKarakteristikSubstratumTanahActivity::class.java)
                9 -> navigasiIntent(EntryTipeLuapanActivity::class.java)
                10 -> navigasiIntent(EntryKetebalanGambutActivity::class.java)
                11 -> navigasiIntent(EntryKarakteristikSubstratumDibawahLapisanGambutActivity::class.java)
                12 -> navigasiIntent(EntryTingkatKerusakanLahanActivity::class.java)
                13 -> navigasiIntent(EntryKebakaranLahanActivity::class.java)
                14 -> navigasiIntent(EntryPorositasKelengasanActivity::class.java)
                15 -> navigasiIntent(EntryDataGambarActivity::class.java)

            }
        }
    }

    private fun navigasiIntent(classtujuan: Class<*>) {
        survey.elevasi = txt_elevasi.text.toString()
        survey.elevasiImg1 = pathelevasiImg1
        survey.elevasiImg2 = pathelevasiImg2
        survey.elevasiImg3 = pathelevasiImg3
        survey.elevasiImg4 = pathelevasiImg4
        survey.elevasiImg5 = pathelevasiImg5
        survey.elevasiImg6 = pathelevasiImg6

        survey.update()
        val intent = Intent(activity,classtujuan)
        intent.putExtra("id", survey.id?.toInt())
        startActivity(intent)

    }

    fun deleteImage(button: ImageButton, imageView: ImageView, code: Int){
        button.setOnClickListener {
            when(code){
                1-> pathelevasiImg1 = ""
                2-> pathelevasiImg2 = ""
                3-> pathelevasiImg3 = ""
                4-> pathelevasiImg4 = ""
                5-> pathelevasiImg5 = ""
                6-> pathelevasiImg6 = ""
            }

            Glide.with(activity).load("").into(imageView)
        }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
    override fun onResume() {
        if(id != null){
            survey.getData(id)
        }
        super.onResume()
    }

}